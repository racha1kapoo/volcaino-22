#include "ArrayChannelProfileViewer.h"
#include <QIcon>
#include <QGroupBox>
ArrayChannelProfileViewer::ArrayChannelProfileViewer(){

}
ArrayChannelProfileViewer::ArrayChannelProfileViewer(TableViewOperation *table,int row,int column):
    mArrayMatrix{},mTable(table),mRow(row),mColumn(column),mRowCount(0),mXValue(0),mYValue(0),
    mBase(0),mRange(0)
{

    setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    this->setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));

    this->setWindowTitle("Array Channel Profile Viewer");
    DEBUG_TRACE(QString::number(mRow));
    DEBUG_TRACE(QString::number(mColumn));
    mAxisX = new QValueAxis;
    mAxisY = new QValueAxis;
    this->setArrayMatrix();
    mXMax=mArrayMatrix.size();

    mRowCount=mTable->model()->rowCount();


    hLayout = new QHBoxLayout(this);

    auto widget1 = new QWidget(this);
    widget1->setMinimumSize(this->size()/6);
    {
        hLayout->addWidget(widget1);
        QVBoxLayout *widget1Layout = new QVBoxLayout(widget1);

        QGroupBox *channel = new QGroupBox(tr("Channel"),widget1);
        widget1Layout->addWidget(channel);
        mChannelText=new QLineEdit(channel);
        QVBoxLayout *layout = new QVBoxLayout(channel);
        layout->addWidget(mChannelText);

        QGroupBox *line = new QGroupBox(tr("Line"),widget1);
        widget1Layout->addWidget(line);
        mLineText=new QLineEdit(line);
        QVBoxLayout *lineLayout = new QVBoxLayout(line);
        lineLayout->addWidget(mLineText);

        int lineIndex;
        auto list=mTable->getChannelList();
        DEBUG_TRACE(list);

        for(int i=0;i<list.size();++i){
            DEBUG_TRACE(list[i]);
            if(list[i].contains("Line")){
                lineIndex=i;
                DEBUG_TRACE(lineIndex);
                continue;
            }
            if(i==mColumn)this->mChannelText->setText(list[i]);
        }
        QModelIndex index=mTable->model()->index(mRow,lineIndex);
        auto lineNo=mTable->model()->data(index).toString();

        this->setLineNo(lineNo);

        QGroupBox *scale = new QGroupBox(tr("Scaling"),widget1);
        widget1Layout->addWidget(scale);
        QVBoxLayout *scaleLayout = new QVBoxLayout(scale);
        mFix=new QCheckBox("Fix",scale);
        scaleLayout->addWidget(mFix);
        mAutoButton=new QPushButton("Auto",scale);
        scaleLayout->addWidget(mAutoButton);
        QFormLayout *BRForm=new QFormLayout(scale);
        scaleLayout->addLayout(BRForm);
        QLabel *base=new QLabel("Base",scale);
        mBaseText=new QLineEdit(scale);
        QLabel *range=new QLabel("Range",scale);
        mRangeText=new QLineEdit(scale);

        BRForm->addRow(base, mBaseText);
        BRForm->addRow(range, mRangeText);

        QGroupBox *displayWin = new QGroupBox(tr("Displayed Windows"),widget1);
        widget1Layout->addWidget(displayWin);
        QVBoxLayout *displayWinLayout = new QVBoxLayout(displayWin);
        QHBoxLayout *fromtoWinLayout = new QHBoxLayout(displayWin);
        displayWinLayout->addLayout(fromtoWinLayout);
        mFromWin=new QLineEdit(displayWin);
        QLabel *to=new QLabel("To",displayWin);
        mToWin=new QLineEdit(displayWin);
        fromtoWinLayout->addWidget(mFromWin);
        fromtoWinLayout->addWidget(to);
        fromtoWinLayout->addWidget(mToWin);
        QHBoxLayout *totalWinLayout = new QHBoxLayout(displayWin);
        displayWinLayout->addLayout(totalWinLayout);
        QLabel *totalWin=new QLabel("Total Windows",displayWin);
        mTotalWinText=new QLineEdit(totalWin);
        mTotalWinText->setEnabled(false);
        totalWinLayout->addWidget(totalWin);
        totalWinLayout->addWidget(mTotalWinText);
        mFromWin->setText(QString::number(1));
        mToWin->setText(QString::number(mArrayMatrix.size()));
        mTotalWinText->setText(QString::number(mArrayMatrix.size()));

        QGroupBox *selectDataVal = new QGroupBox(tr("Selected Data Value"),widget1);
        widget1Layout->addWidget(selectDataVal);
        QVBoxLayout *selectDataValLayout = new QVBoxLayout(selectDataVal);
        QFormLayout *dataValForm=new QFormLayout(selectDataVal);
        selectDataValLayout->addLayout(dataValForm);
        QLabel *yVal=new QLabel("Y",scale);
        mYValText=new QLineEdit(scale);
        QLabel *xVal=new QLabel("X",scale);
        mXValText=new QLineEdit(scale);
        dataValForm->addRow(yVal, mYValText);
        dataValForm->addRow(xVal, mXValText);

        mOk=new QPushButton("Ok",widget1);
        mCancel=new QPushButton("Cancel",widget1);
        QHBoxLayout *okCancelLayout = new QHBoxLayout(displayWin);
        widget1Layout->addLayout(okCancelLayout);
        okCancelLayout->addWidget(mOk);
        okCancelLayout->addWidget(mCancel);

    }


    auto widget2 = new QWidget(this);
    widget2->setMinimumSize(this->size()/6);
    hLayout->addWidget(widget2);
    {
        auto widget2Layout = new QVBoxLayout(widget2);

        QGroupBox *profile = new QGroupBox(tr("Profile"),widget2);
        widget2Layout->addWidget(profile);
        QRadioButton *linear=new QRadioButton("Linear",profile);
        QRadioButton *log=new QRadioButton("Log",profile);
        QRadioButton *logLnr=new QRadioButton("Log/Lnr",profile);
        QLabel *logMin=new QLabel("Log Min",profile);
        QLineEdit *logMinText=new QLineEdit(logMin);

        QVBoxLayout *profileLayout = new QVBoxLayout(profile);
        profileLayout->addWidget(linear);
        profileLayout->addWidget(log);
        profileLayout->addWidget(logLnr);
        profileLayout->addWidget(logMin);
        profileLayout->addWidget(logMinText);

        QGroupBox *xSpacing = new QGroupBox(tr("X Spacing"),widget2);
        widget2Layout->addWidget(xSpacing);
        QRadioButton *trueButton=new QRadioButton("True",xSpacing);
        QRadioButton *falseButton=new QRadioButton("False",xSpacing);

        QVBoxLayout *xSpacingLayout = new QVBoxLayout(xSpacing);
        xSpacingLayout->addWidget(trueButton);
        xSpacingLayout->addWidget(falseButton);

        QGroupBox *theme = new QGroupBox(tr("Theme"),widget2);
        widget2Layout->addWidget(theme);
        mLight=new QRadioButton("Light",theme);
        mDark=new QRadioButton("Dark",theme);

        QVBoxLayout *themeLayout = new QVBoxLayout(theme);
        themeLayout->addWidget(mLight);
        themeLayout->addWidget(mDark);

        mLight->setChecked(true);

        widget2Layout->addStretch(1);

        mHelp=new QPushButton("Help",widget2);
        widget2Layout->addWidget(mHelp);
    }

    auto widget3 = new QWidget(this);
    widget3->setMinimumSize(this->size()*(2/3));
    hLayout->addWidget(widget3);
    {
        widget3Layout = new QVBoxLayout(widget3);

        QGroupBox *row = new QGroupBox(tr("Row"));
        widget3Layout->addWidget(row);
        QVBoxLayout *rowLayout = new QVBoxLayout(row);

        mSlider=new QSlider(Qt::Horizontal);
        mSlider->setMinimum(1);
        mSlider->setMaximum(mArrayMatrix.size());
        rowLayout->addWidget(mSlider);

        QHBoxLayout *sliderLayout = new QHBoxLayout(row);
        rowLayout->addLayout(sliderLayout);
        mLeft=new QPushButton("<",row);
        mRight=new QPushButton(">",row);
        mLeftText=new QLineEdit(row);
        mLeftText->setText(QString::number(1));
        QLabel *ofVal=new QLabel("of",row);
        mRightText=new QLineEdit(row);
        mRightText->setText(QString::number(mArrayMatrix.size()));
        QLabel *fudicial=new QLabel("Fiducial",row);
        mFudicialText=new QLineEdit(row);
        mFudicialText->setText(QString::number(mRow));
        mPlot=new QPushButton("Plot",row);

        sliderLayout->addWidget(mLeft);
        sliderLayout->addWidget(mRight);
        sliderLayout->addWidget(mLeftText);
        sliderLayout->addWidget(ofVal);
        sliderLayout->addWidget(mRightText);
        sliderLayout->addWidget(fudicial);
        sliderLayout->addWidget(mFudicialText);
        sliderLayout->addWidget(mPlot);

        mpChart = new QtCharts::QChart;
        mpChart->grabGesture(Qt::PanGesture);
        mpChart->grabGesture(Qt::PinchGesture);
        mpChartView = new ShowChartView(mpChart);
        mpChartView->setMinimumSize(widget3->size()*2);
        mpChartView->setMouseTracking(true);

        auto series = new QLineSeries;
        mpMousePt = new MouseEventTracking(mpChart);

        mAxisY->setLabelFormat("%.2f");
        mAxisY->setTitleText("Spectrometer Value");
        mAxisX->setLabelFormat("%d");
        mAxisX->setTitleText("Array Index");

        mpChart->setAcceptHoverEvents(true);
        mpChart->legend()->setVisible(true);

        mpChartView->setRenderHint(QPainter::Antialiasing);
        mpChartView->scene()->addItem(mpChart);
        mpChartView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);

        widget3Layout->addWidget(mpChartView);
        updateChart(mArrayMatrix);

    }

    connect(mOk,&QPushButton::clicked,this,&ArrayChannelProfileViewer::onOkClicked);
    connect(mCancel,&QPushButton::clicked,this,&ArrayChannelProfileViewer::onCancelClicked);
    connect(mHelp,&QPushButton::clicked,this,&ArrayChannelProfileViewer::onHelpClicked);
    connect(mSlider,&QSlider::sliderMoved,this,&ArrayChannelProfileViewer::onSliderMoved);
    connect(mLeft,&QPushButton::clicked,this,&ArrayChannelProfileViewer::onLeftArrowClicked);
    connect(mRight,&QPushButton::clicked,this,&ArrayChannelProfileViewer::onRightArrowClicked);
    connect(mPlot,&QPushButton::clicked,this,&ArrayChannelProfileViewer::onPlotClicked);
    connect(mSlider,&QSlider::valueChanged,this,&ArrayChannelProfileViewer::onSliderMoved);
    connect(mLeftText,&QLineEdit::textChanged,this,&ArrayChannelProfileViewer::onLeftTextChanged);
    connect(mFudicialText,&QLineEdit::textChanged,this,&ArrayChannelProfileViewer::onFiducialTextChanged);

    connect(mFix,&QCheckBox::toggled,[this](bool toggled){

        if(toggled){
            this->removeChartGraph();

            mAxisX->setRange(mFromWin->text().toFloat(),mToWin->text().toFloat());
            mAxisY->setRange(mBaseText->text().toFloat(),mBaseText->text().toFloat()+mRangeText->text().toFloat());
            updateChart(mArrayMatrix);

            DEBUG_TRACE(mAxisY->min());
            DEBUG_TRACE(mAxisY->max());
            this->mBaseText->setText(QString::number(mAxisY->min()));
            this->mRangeText->setText(QString::number(abs(mAxisY->min())+abs(mAxisY->max())));
            this->mFromWin->setText(QString::number(mAxisX->min()));
            this->mToWin->setText(QString::number(mAxisX->max()));

            DEBUG_TRACE(this->mBaseText->text());
            DEBUG_TRACE(this->mRangeText->text());
        }

    });


    connect(mAutoButton,&QPushButton::clicked,[this](){

        this->mFix->setCheckState(Qt::CheckState::Unchecked);

        this->getScaleValues();

        updateChart(mArrayMatrix);

        mAxisX->setRange(mFromWin->text().toFloat(),mToWin->text().toFloat());
        mAxisY->setRange(mBaseText->text().toFloat(),mBaseText->text().toFloat()+mRangeText->text().toFloat());
        mTotalWinText->setText(mToWin->text());
        DEBUG_TRACE(this->mFromWin->text());
        DEBUG_TRACE(this->mToWin->text());


    });
    connect(mLight,&QRadioButton::clicked,[this](bool toggled){
        this->mLight->setChecked(toggled);

        if(this->mLight->isChecked()){
            this->mpChart->setTheme(QChart::ChartThemeBlueIcy);
        }
        else{
            this->mpChart->setTheme(QChart::ChartThemeBlueCerulean);

        }

    });
    connect(mDark,&QRadioButton::clicked,[this](bool toggled){
        this->mDark->setChecked(toggled);

        if(this->mDark->isChecked()){
            this->mpChart->setTheme(QChart::ChartThemeBlueCerulean);
        }
        else{
            this->mpChart->setTheme(QChart::ChartThemeBlueIcy);
        }
    });

    setPointXYOnGraph(0);

}
ArrayChannelProfileViewer::~ArrayChannelProfileViewer()
{
    this->close();
}
ArrayChannelProfileViewer* ArrayChannelProfileViewer::getInstance(){

    static ArrayChannelProfileViewer theInstance;
    return &theInstance;

}

void ArrayChannelProfileViewer::setLineNo(QString line){

    DEBUG_TRACE(line);
    this->mLineText->setText(line);
}

void ArrayChannelProfileViewer::setArrayMatrix(){
    QModelIndex index=mTable->model()->index(mRow,mColumn);
    mArrayMatrix.clear();
    auto arrayStr=mTable->model()->data(index).toString().remove("]").remove("[");
    auto arrayList=arrayStr.split(',');
    DEBUG_TRACE(arrayList);
    DEBUG_TRACE(arrayList.size());
    for(int i=1;i<arrayList.size();++i)
        mArrayMatrix.insert(std::pair<float,float>(i,arrayList[i].toFloat()));

}

void ArrayChannelProfileViewer::onOkClicked(){}

void ArrayChannelProfileViewer::onCancelClicked(){
    this->close();
}

void ArrayChannelProfileViewer::onHelpClicked(){}

void ArrayChannelProfileViewer::onSliderMoved(int position){
    DEBUG_TRACE(position);
    mLeftText->setText(QString::number(position));

    setPointXYOnGraph(mSlider->sliderPosition());
}

void ArrayChannelProfileViewer::onLeftArrowClicked(){
    if(mRow>0){
        mRow=mRow-1;
        this->setArrayMatrix();
        this->mFudicialText->setText(QString::number(mRow));
    }
}

void ArrayChannelProfileViewer::onRightArrowClicked(){
    if(mRow<mRowCount){
        mRow=mRow+1;
        this->setArrayMatrix();
        this->mFudicialText->setText(QString::number(mRow));
    }
}

void ArrayChannelProfileViewer::onPlotClicked(){}

void ArrayChannelProfileViewer::setPointXYOnGraph(int position){
    DEBUG_TRACE(position);
    for (auto it = this->mArrayMatrix.begin(); it != this->mArrayMatrix.end(); ++it){
        if (it->first == position){
            this->mXValText->setText(QString::number(it->first));
            this->mYValText->setText(QString::number(it->second));
            emit mSeries->clicked(QPointF(it->first,it->second));
        }
    }

}

void ArrayChannelProfileViewer::onLeftTextChanged(QString text){
    mSlider->setSliderPosition(text.toInt());
    setPointXYOnGraph(mSlider->sliderPosition());
}

void ArrayChannelProfileViewer::onFiducialTextChanged(QString text){
    mRow=text.toInt();
    this->setArrayMatrix();
    mpChartView->setUpdatesEnabled(false);

    updateChart(this->mArrayMatrix);

    mpChartView->setUpdatesEnabled(true);
}

void ArrayChannelProfileViewer::updateChart(std::map<float,float>arrayMatrix){
    auto series = new QLineSeries;

    this->removeChartGraph();

    this->getScaleValues();

    this->mBaseText->setText(QString::number(mBase));
    this->mRangeText->setText(QString::number(mRange));
    this->mFromWin->setText(QString::number(mXMin));
    this->mToWin->setText(QString::number(mXMax));
    this->mTotalWinText->setText(QString::number(arrayMatrix.size()));

    if(mpMousePt)this->mpMousePt->setEnableCrossHair(false);

    series->setName(mChannelText->text()+" "+mFudicialText->text());
    series->setUseOpenGL(false);
    series->setPointsVisible(true);

    if(this->mLight->isChecked())mpChart->setTheme(QChart::ChartThemeBlueIcy);
    if(this->mDark->isChecked())mpChart->setTheme(QChart::ChartThemeBlueCerulean);


    for(auto item:arrayMatrix){
        series->append(item.first,item.second);
    }
    QVXYModelMapper *mapper = new QVXYModelMapper();
    mapper->setSeries(series);
    mpChart->addSeries(series);
    QString mSeriesColorHex = "#000000";

    // get the color of the mSeries and use it for showing the mapped area
    mSeriesColorHex = "#" + QString::number(series->pen().color().rgb(), 16).right(6).toUpper();
    //mpChart->createDefaultAxes();
    mpChart->addAxis(mAxisY, Qt::AlignLeft);
    series->attachAxis(mAxisY);


    mpChart->addAxis(mAxisX, Qt::AlignBottom);
    series->attachAxis(mAxisX);


    this->setSeries(series);
    if(mXValue!=0){
        this->onSliderMoved(mXValue);
    }

    connect(series, &QLineSeries::clicked,[this,arrayMatrix](const QPointF &point){

        QString text=QString::number(point.x());
        DEBUG_TRACE(text);



        //pMousePt->setUpdateSingleIndicator(point);

        int pointX =static_cast<int>(point.x());
        for (auto it = arrayMatrix.begin(); it != arrayMatrix.end(); ++it){
            if (it->first == pointX){
                DEBUG_TRACE(it->first);
                DEBUG_TRACE(it->second);
                this->setXYValues(it->first,it->second);

            }
        }
        DEBUG_TRACE(point);

//        this->mpMousePt->setZValue(11);
//        this->mpMousePt->updateGeometry();
//        this->mpMousePt->show();

    }

    );

}

void ArrayChannelProfileViewer::setSeries(QLineSeries *series){
    mSeries=series;
}

void ArrayChannelProfileViewer::setXYValues(int x,int y){
    this->mXValue=x;
    this->mYValue=y;
    this->mpMousePt->setEnableCrossHair(true);
    //this->mpMousePt->setText(QString("X: %1 \nY: %2 ").arg(x).arg(y));
    //this->mpMousePt->setAnchor(QPointF(x,y));
    this->mXValText->setText(QString::number(x));
    this->mYValText->setText(QString::number(y));
    this->mSlider->setSliderPosition(x);
}

void ArrayChannelProfileViewer::setXYValues(float x,float y){
    this->mXValue=x;
    this->mYValue=y;
    this->mpMousePt->setEnableCrossHair(true);
    //this->mpMousePt->setText(QString("X: %1 \nY: %2 ").arg(x).arg(y));
    //this->mpMousePt->setAnchor(QPointF(x,y));
    this->mXValText->setText(QString::number(x));
    this->mYValText->setText(QString::number(y));
    this->mSlider->setSliderPosition(x);
}

void ArrayChannelProfileViewer::removeChartGraph(){
    mpChart->removeAllSeries();
    mpChart->removeAxis(mAxisX);
    mpChart->removeAxis(mAxisY);
}

void ArrayChannelProfileViewer::getScaleValues(){
    auto max = std::max_element(mArrayMatrix.begin(), mArrayMatrix.end(),
                                [](const std::pair<int, int>& p1, const std::pair<int, int>& p2) {
        return p1.second < p2.second; });
    auto min = std::min_element(mArrayMatrix.begin(), mArrayMatrix.end(),
                                [](const std::pair<int, int>& p1, const std::pair<int, int>& p2) {
        return p1.second < p2.second; });

    this->mBase=min->second;
    this->mRange=max->second+min->second;

    this->mXMin=1;
    this->mXMax=mArrayMatrix.size();


}
