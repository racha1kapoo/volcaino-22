/**
 *  @file   ArrayChannelProfileViewer.h
 *  @brief  Display the profile of an array channel
 *  @author Rachana Kapoor
 *  @date   ‎July ‎11, ‎2022
 ***********************************************/

#ifndef ARRAYCHANNELPROFILEVIEWER_H
#define ARRAYCHANNELPROFILEVIEWER_H
#include <QObject>
#include <QDialog>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QRadioButton>
#include <QGroupBox>
#include <QPushButton>
#include <QWidget>
#include<QGraphicsSimpleTextItem>
#include <QtCharts/QChartView>
#include <QtCharts/QtCharts>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QGraphicsScene>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCore/qglobal.h>
#include <QtWidgets/QGraphicsTextItem>
#include "MouseEventTracking.h"
#include "TableViewOperation.h"
#include "ShowChartView.h"

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE
class ArrayChannelProfileViewer:public QDialog
{
    Q_OBJECT
public:
    ///default constructor
    ArrayChannelProfileViewer();

    /// @brief This parameterised constructor is responsible for creating a array channel profile viewer window as per
    /// the table, row and column specified.
    /// @param table: An instance of TableViewOperation
    /// @param row: row no
    /// @param column: column no
    /// @return  void
    ArrayChannelProfileViewer(TableViewOperation *table,int row,int column);

    /// Class destructor
    ~ArrayChannelProfileViewer();

    /// @brief static function
    ///
    /// @return   Instance of class Database
    ///
    static ArrayChannelProfileViewer* getInstance();

    /// @brief This function sets the line no.
    /// @param  line: Line no
    /// @return void
    void setLineNo(QString line);


private:
    /// @brief This function creates an array matrix containing index as key mapped to certain value .
    /// @return void
    void setArrayMatrix();

    /// @brief This function is invoked when 'OK' button pressed.
    /// @return void
    void onOkClicked();

    /// @brief This function is invoked when 'CANCEL' button pressed.
    /// @return void
    void onCancelClicked();

    /// @brief This function is invoked when 'HELP' button pressed.
    /// @return void
    void onHelpClicked();

    /// @brief This function is invoked when slider position is changed.
    /// @param position: position value
    /// @return void
    void onSliderMoved(int position);

    /// @brief This function is invoked when '<' button pressed.
    /// @return void
    void onLeftArrowClicked();

    /// @brief This function is invoked when '>' button pressed.
    /// @return void
    void onRightArrowClicked();

    /// @brief This function is invoked when 'PLOT' button pressed.
    /// @return void
    void onPlotClicked();

    /// @brief This function sets the slider position if text changed and sets the X & Y data value.
    /// @param text: index
    /// @return void
    void onLeftTextChanged(QString text);

    /// @brief This function updates/changes Chart as per the fiducial/record changed.
    /// @param text: index
    /// @return void
    void onFiducialTextChanged(QString text);

    /// @brief This function sets the X & Y data value whenever change detected.
    /// @param position: index
    /// @return void
    void setPointXYOnGraph(int position);

    /// @brief This function updates the Charts as per record selected.
    /// @param arrayMatrix: map of values
    /// @return void
    void updateChart(std::map<float,float>arrayMatrix);

    /// @brief This function sets the line series chart.
    /// @return void
    void setSeries(QLineSeries *series);

    /// @brief This function sets the X & Y data value and sets the slider position if text changed.
    /// @param x: x integer value
    /// @param y: y integer value
    /// @return void
    void setXYValues(int x,int y);

    /// @brief This function sets the X & Y data value and sets the slider position if text changed.
    /// @param x: x float value
    /// @param y: y float value
    /// @return void
    void setXYValues(float x,float y);

    /// @brief This function removes the existing chart and axes.
    /// @return void
    void removeChartGraph();

    /// @brief This function sets the min,max,base and range value for the axes.
    /// @return void
    void getScaleValues();

    MouseEventTracking *mpMousePt;

    std::map<float,float>mArrayMatrix;

    QWidget *widget1;

    QWidget *widget2;

    QWidget *widget3;

    QHBoxLayout *hLayout;

    QVBoxLayout *vLayout;

    QVBoxLayout *widget3Layout;

    TableViewOperation *mTable;

    int mRow;

    int mColumn;

    int mRowCount;

    QLineEdit *mChannelText;

    QLineEdit *mLineText;

    QLineEdit *mBaseText;

    QLineEdit *mRangeText;

    QLineEdit *mFromWin;

    QLineEdit *mToWin;

    QLineEdit *mTotalWinText;

    QLineEdit *mYValText;

    QLineEdit *mXValText;

    QCheckBox *mFix;

    QPushButton *mAutoButton;

    QPushButton *mOk;

    QPushButton *mCancel;

    QPushButton *mHelp;

    QRadioButton *mLight;

    QRadioButton *mDark;

    QSlider *mSlider;

    QPushButton *mLeft;

    QPushButton *mRight;

    QLineEdit *mLeftText;

    QLineEdit *mRightText;

    QLineEdit *mFudicialText;

    QPushButton *mPlot;

    QtCharts::QChart *mpChart;

    ShowChartView *mpChartView;

    QLineSeries *mSeries;

    QValueAxis *mAxisY;

    QValueAxis *mAxisX;

    int mXValue;

    int mYValue;

    float mBase;

    float mRange;

    int mXMin;

    int mXMax;


};

#endif // ARRAYCHANNELPROFILEVIEWER_H
