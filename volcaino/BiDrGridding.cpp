#include "BiDrGridding.h"
#define BLANKDATA -1
#define REALDATA 1
#define VINTPDATA 2
#define HINTPDATA 3
BiDrGridding::BiDrGridding(std::vector<double> inputPars, QString filePath, bool isBDRGRD)
{
    mIsBDRGRD = isBDRGRD;
    mCellSize = inputPars[0];
    mMaxInterpDist = inputPars[1];
    mFile.setFileName(filePath);

    mMaxSteps = mMaxInterpDist/mCellSize;
    //mMaxLineSpan = mMaxSteps/4;   //TODO
}


BiDrGridding::~BiDrGridding()
{
    //    valRowCol.clear();
    //    lineRowCol.clear();
    //    valFlagRowCol.clear();
    //    mRowCols.clear();
    //    mTheRowCol.clear();

    emit enableBoxOperations(true);
}

void BiDrGridding::setDataBase(QString tableName, QString gridValue, QString gridX, QString gridY)
{

    mTableName = tableName;
    mGridValue = gridValue;
    mGridX = gridX;
    mGridY = gridY;

    auto db =DataBase::getInstance();
    if (db->openDatabase())
        mSql=db->getSqlQuery();

    mReadFromDB = true;
}

std::vector<std::vector<double> > &BiDrGridding::getGridData()
{
    return valRowCol;
}

std::vector<std::vector<int> > &BiDrGridding::getGridDataFlag()
{
    return valFlagRowCol;
}

void BiDrGridding::loadData()
{
    if(!mReadFromDB)
    {
        if(mFile.open(QIODevice::ReadOnly))
        {
            mFile.readLine();
            QList<QByteArray> line;
            bool isFirstTime = true;

            while(!mFile.atEnd())
            {
                line = mFile.readLine().split(',');

                double theX = line[xTrack].toDouble();
                double theY = line[yTrack].toDouble();
                double theVal = line[valTrack].toDouble();

                if(isFirstTime)
                {
                    mYmin = theY;
                    mYmax = theY;

                    mXmin = theX;
                    mXmax = theX;

                    mValueMax = theVal;
                    mValueMin = theVal;

                    isFirstTime = false;
                }

                if(theY > mYmax)
                {
                    mYmax = theY;
                }

                if(theY < mYmin)
                {
                    mYmin = theY;
                }

                if(theX > mXmax)
                {
                    mXmax = theX;
                }

                if(theX < mXmin)
                {
                    mXmin = theX;
                }

                if(theVal > mValueMax)
                {
                    mValueMax = theVal;
                }

                if(theVal < mValueMin)
                {
                    mValueMin = theVal;
                }
            }

            mImgWidth = (mXmax-mXmin)/(mCellSize)+1;
            mImgHeight = (mYmax-mYmin)/(mCellSize)+1;
            mRmg1Range = qAbs(mValueMax-mValueMin);

            mXmin = (int)mXmin/(int)mCellSize * (int)mCellSize;
            mYmin = (int)mYmin/(int)mCellSize * (int)mCellSize;

            valRowCol.resize(mImgHeight, std::vector<double>(mImgWidth));
            lineRowCol.resize(mImgHeight, std::vector<int>(mImgWidth));
            valFlagRowCol.resize(mImgHeight, std::vector<int>(mImgWidth));
            mTheRowCol.resize(2);

            mFile.seek(0);
            mFile.readLine();
            while(!mFile.atEnd())
            {
                line = mFile.readLine().split(',');

                int lineNo = line[lineTrack].toInt();
                double theX = line[xTrack].toDouble();
                double theY = line[yTrack].toDouble();
                double theVal = line[valTrack].toDouble();

                int row = (mYmax-theY)/mCellSize;
                int col = (theX - mXmin)/mCellSize;

                valRowCol[row][col] += theVal;
                lineRowCol[row][col] = lineNo;
                valFlagRowCol[row][col] += 1;
            }

            mFile.close();
        }
    }
    else
    {
        bool isFirstTime = true;
        QString valueQuery = "SELECT "+mGridValue+", "+mGridX+", "+mGridY+" FROM "+mTableName;
        if (mSql.exec(valueQuery)) {
            while (mSql.next()) {
                if(!mSql.value(0).isNull() && !mSql.value(1).isNull()
                        && !mSql.value(2).isNull())
                {
                    double gridValue = mSql.value(0).toDouble();
                    double gridX = mSql.value(1).toDouble();
                    double gridY = mSql.value(2).toDouble();

                    if(isFirstTime)
                    {
                        mYmin = gridY;
                        mYmax = gridY;

                        mXmin = gridX;
                        mXmax = gridX;

                        mValueMax = gridValue;
                        mValueMin = gridValue;

                        isFirstTime = false;
                    }
                    if(gridY > mYmax)
                    {
                        mYmax = gridY;
                    }

                    if(gridY < mYmin)
                    {
                        mYmin = gridY;
                    }

                    if(gridX > mXmax)
                    {
                        mXmax = gridX;
                    }

                    if(gridX < mXmin)
                    {
                        mXmin = gridX;
                    }

                    if(gridValue > mValueMax)
                    {
                        mValueMax = gridValue;
                    }

                    if(gridValue < mValueMin)
                    {
                        mValueMin = gridValue;
                    }
                }
            }
        }

        mImgWidth = (mXmax-mXmin)/(mCellSize)+1;
        mImgHeight = (mYmax-mYmin)/(mCellSize)+1;
        mRmg1Range = qAbs(mValueMax-mValueMin);

        mXmin = (int)mXmin/(int)mCellSize * (int)mCellSize;
        mYmin = (int)mYmin/(int)mCellSize * (int)mCellSize;

        valRowCol.resize(mImgHeight, std::vector<double>(mImgWidth));
        lineRowCol.resize(mImgHeight, std::vector<int>(mImgWidth));
        valFlagRowCol.resize(mImgHeight, std::vector<int>(mImgWidth));
        mTheRowCol.resize(2);

        if (mSql.exec(valueQuery)) {
            while (mSql.next()) {
                if(!mSql.value(0).isNull() && !mSql.value(1).isNull()
                        && !mSql.value(2).isNull())
                {
                    double gridValue = mSql.value(0).toDouble();
                    double gridX = mSql.value(1).toDouble();
                    double gridY = mSql.value(2).toDouble();

                    int row = (mYmax-gridY)/mCellSize;
                    int col = (gridX - mXmin)/mCellSize;

                    valRowCol[row][col] += gridValue;
                    valFlagRowCol[row][col] += 1;
                }
            }
        }

    }
}


void BiDrGridding::prepareData()
{

    for(int row = 0; row < mImgHeight; row++)
    {
        for(int col = 0; col < mImgWidth; col++)
        {
            if(valFlagRowCol[row][col] >= 1)
            {
                valRowCol[row][col] = valRowCol[row][col]/valFlagRowCol[row][col];
                valFlagRowCol[row][col] = REALDATA;

                mTheRowCol[0] = row;
                mTheRowCol[1] = col;
                mRowCols.push_back(mTheRowCol);
            }
            else
            {
                valFlagRowCol[row][col] = BLANKDATA;
            }
        }
    }

    //    std::sort(mDataSet.begin(), mDataSet.end(), [](const std::vector<double>& v1, const std::vector<double>& v2)
    //    {
    //        if(v1[0] < v2[0])  return true;
    //        else if(v1[0] == v2[0] && v1[2] < v2[2])  return true;

    //        else return false;
    //    });

}

void BiDrGridding::allocateBuff()
{
    //valRowCol.resize(mImgHeight, std::vector<double>(mImgWidth));
}


void BiDrGridding::verticalGridding()
{

    int RowColSize = mRowCols.size();
    for(int i = 0; i < RowColSize; i++)
    {
        int row = mRowCols[i][0];
        int col = mRowCols[i][1];
        int lineNumberOri = lineRowCol[row][col];
        double value = valRowCol[row][col];

        // Search for the next possible real grid
        for(int theRow = row+1; theRow <= row+mMaxSteps; theRow++)
        {
            if(theRow >= mImgHeight)  {break;}
            for(int theCol = col- mMaxSteps; theCol <= col+mMaxSteps; theCol++)
            {
                if(theCol >= mImgWidth)
                {
                    break;
                }
                if(theCol < 0)
                {
                    continue;
                }
                // For V intp, only look for real data && data within the same line
                if(valFlagRowCol[theRow][theCol] == REALDATA && lineRowCol[theRow][theCol] == lineNumberOri)
                {
                    //Found
                    double rowSteps = theRow-row;
                    double colSteps = theCol-col;


                    if(rowSteps > 1)   //Need to interpolate
                    {
                        double interpValRange = valRowCol[theRow][theCol]-valRowCol[row][col];
                        for(double step = 1; step <= rowSteps-1; step++)
                        {   //Do interpolation
                            int newCol = 0;
                            int newRow = 0;
                            double newVal = 0;
                            newCol = col + step/(rowSteps-1)*colSteps;
                            newRow = row + step;
                            newVal = value + step/(rowSteps-1)*interpValRange;

                            //                                if(valFlagRowCol[newRow][newCol] != BLANKDATA)
                            //                                {
                            //                                    qDebug() << "Already data V!";
                            //                                }
                            valRowCol[newRow][newCol] = newVal;
                            valFlagRowCol[newRow][newCol] = VINTPDATA;
                            mTheRowCol[0] = newRow;
                            mTheRowCol[1] = newCol;
                            mRowCols.push_back(mTheRowCol);
                        }

                        theRow = (row+mMaxSteps)*2;  //Finished for this grid, move to next
                        theCol = (col+mMaxSteps/2)*2;
                        break;
                    }
                    else    //No need to interpolate for this grid, move to next
                    {
                        theRow = (row+mMaxSteps)*2;
                        theCol = (col+mMaxSteps/2)*2;
                        break;
                    }
                }
            }
        }

    }

}

void BiDrGridding::horizontalGridding()
{

    // Then do esat west interpolation, always interpolate to the esat
    int RowColSizeNew = mRowCols.size();
    //        qDebug() << valFlagRowCol[5][213];
    //        qDebug() << mRowCols[119053];
    for(int i = 0; i < RowColSizeNew; i++)
    {
        int row = mRowCols[i][0];
        int col = mRowCols[i][1];

        double value = valRowCol[row][col];

        for(int theCol = col+1; theCol <= col+mMaxSteps; theCol++)
        {
            if(theCol >= mImgWidth)  {break;}
            //For H Intp, look for both REAL&VINTP DATA
            if((valFlagRowCol[row][theCol] == REALDATA || valFlagRowCol[row][theCol] == VINTPDATA)
                    && valFlagRowCol[row][col+1] != HINTPDATA)
            {
                //Found
                double colSteps = theCol-col;

                if(colSteps > 1)   //Need to interpolate
                {
                    double interpValRange = valRowCol[row][theCol]-valRowCol[row][col];
                    for(double step = 1; step <= colSteps-1; step++)
                    {
                        int newCol = 0;
                        double newVal = 0;

                        newCol = col + step;
                        newVal = value + step/(colSteps-1)*interpValRange;


                        valRowCol[row][newCol] = newVal;
                        valFlagRowCol[row][newCol] = HINTPDATA;
                    }

                    theCol = 2*(col+mMaxSteps);
                    break;
                }
                else
                {
                    theCol = 2*(col+mMaxSteps);
                    break;
                }
            }
        }

    }

}


void BiDrGridding::paintLines()
{
    //    int row;
    //    int col;
    //    for(int i = 0; i < mDataSet.size(); i++)
    //    {
    //        row = mDataSet[i][yTrack]+widthOffset;//(mYmax - mDataSet[i][yTrack])/mCellSize+widthOffset;
    //        col = mDataSet[i][xTrack]+widthOffset+widthIncrease;//(mDataSet[i][xTrack] - mXmin)/mCellSize+widthOffset+widthIncrease;

    //        //        int hueAddIndex = qRound((qAbs(mDataSet[i][lineTrack]-mValueMin))/(mRmg1Range)*20.0);  //hsv index 0~9
    //        //        int hueValue = 300.0 - 15.0*hueAddIndex;

    //        for(int j = -widthIncrease; j < widthIncrease; j++)
    //        {
    //            mImg.setPixelColor(col+j, row, Qt::black);
    //        }
    //    }

}

void BiDrGridding::paintBitMap()
{
    for(int row = 0; row < mImgHeight; row++)
    {
        for(int col = 0; col < mImgWidth; col++)
        {
            if(valFlagRowCol[row][col] == REALDATA || valFlagRowCol[row][col] == VINTPDATA
                    || valFlagRowCol[row][col] == HINTPDATA)
                //if(valFlagRowCol[row][col] == REALDATA || valFlagRowCol[row][col] == VINTPDATA)
            {
                mNumberOfValid++;
                int hueAddIndex = qRound((qAbs(valRowCol[row][col]-mValueMin))/(mRmg1Range)*30.0);
                int hueValue = 300.0 - 10.0*hueAddIndex;

                mImg.setPixelColor(col, row, QColor::fromHsv(hueValue,255,255));
            }
        }
    }

    mPixMap = QPixmap::fromImage(mImg);

}

void BiDrGridding::paintHPixMap(const std::vector<std::vector<double> > &input)
{

    int row;
    int col;


    for(int i = 0; i < input.size(); i++)
    {
        row = input[i][yTrack]+widthOffset;//(mYmax - mLine1DataSet[i][yTrack])/mCellSize+widthOffset;
        col = input[i][xTrack]+widthOffset+widthIncrease;//(mLine1DataSet[i][2] - mXmin)/mCellSize+widthOffset+widthIncrease;


        int hueAddIndex = qRound((qAbs(input[i][valTrack]-mValueMin))/(mRmg1Range)*20.0);  //hsv index 0~9
        int hueValue = 300.0 - 15.0*hueAddIndex;


        mImg.setPixelColor(col, row, QColor::fromHsv(hueValue,255,255));

    }

    //mNumberOfValid += input.size();
}

void BiDrGridding::configPixMap()
{
    mImg = QImage(mImgWidth+widthMargin, mImgHeight+heightMargin, QImage::Format_RGB32);
    mImg.fill(Qt::white);
}

void BiDrGridding::startGridding()
{
    mStatusMsg = "Loading and preparing data...";
    emit statusChanged(mStatusMsg);
    loadData();

    time1 = QDateTime::currentMSecsSinceEpoch();
    prepareData();

    configPixMap();

    if(!mReadFromDB)
    {
        mStatusMsg = "Loading data done, doing NorthSouth interpolation...";
        emit statusChanged(mStatusMsg);

        verticalGridding();

        if(mIsBDRGRD)
        {
            mStatusMsg = "NorthSouth interpolation done, doing WestEast interpolation...";
            emit statusChanged(mStatusMsg);
            horizontalGridding();
            time2 = QDateTime::currentMSecsSinceEpoch();

            mStatusMsg = "WestEast interpolation done, painting bit map...";
            emit statusChanged(mStatusMsg);
        }
        else
        {
            time2 = QDateTime::currentMSecsSinceEpoch();

            mStatusMsg = "Painting bit map...";
            emit statusChanged(mStatusMsg);
        }
    }
    paintBitMap();
    if(mImg.save("../Rmg1.jpg", "JPEG"))
    {
        mStatusMsg = "Bit map saved!";
        emit statusChanged(mStatusMsg);
    }

    mStatusMsg = "Bi-Directional gridding took " + QString::number(time2-time1) + " ms";
    emit statusChanged(mStatusMsg);

    emit gridProcessDone(this);

}
