/**
 *  @file   BiDrGridding.h
 *  @brief  Does bi-directional gridding algorithm
 *  @author Shuo Li
 *  @date   Nov 8, ‎2022
 ***********************************************/
#ifndef BIDRGRIDDING_H
#define BIDRGRIDDING_H
#include <QObject>
#include <QWidget>
#include <QFile>
#include <QDebug>
#include <float.h>
#include <algorithm>
#include <QImage>
#include <QMap>
#include <QRgb>
#include <math.h>
#include <QtAlgorithms>
#include <QDateTime>
#include "GriddingBase.h"
#include "qsqlquery.h"
#include <QHash>
#include <QSet>
#include <unordered_map>
#include <QSetIterator>
#include <unordered_set>
#include <QThread>
#include "DataBase.h"
class BiDrGridding: public GriddingBase
{
    Q_OBJECT
public:
    ///default constructor
    BiDrGridding(std::vector<double> inputPars = {},QString filePath = "", bool isBDRGRD = true);

    /// Class destructor, it frees allocated vectors
    ~BiDrGridding();

    /// @brief This function sets the data base, if user selects input from data base
    /// @param tableName: Table name
    /// @param gridValue: Value channel name
    /// @param gridX: X coordinate channel name
    /// @param gridY: Y coordinate channel name
    void setDataBase(QString tableName, QString gridValue, QString gridX, QString gridY);

    //QPixmap mPixMap;
    /// @brief This function returns the gridded value as 2d vector
    /// @return Gridded value
    std::vector<std::vector<double>>& getGridData() override;

    /// @brief This function returns the gridded value flags as 2d vector
    /// @return Gridded flags, BLANKDATA -1, REALDATA 1, VINTPDATA 2, HINTPDATA 3
    std::vector<std::vector<int>>& getGridDataFlag() override;

signals:

private:
    QFile mFile;


    //QSetIterator<std::vector<double>> mDataSetIterator;
//    std::vector<double> mDataSetY;

//    struct VectorHash {
//        size_t operator()(const std::vector<double>& v) const {
//            std::hash<double> hasher;
//            size_t seed = 0;
//            for (double i : v) {
//                seed ^= hasher(i) + 0x9e3779b9 + (seed<<6) + (seed>>2);
//            }
//            return seed;
//        }
//    };

//    std::unordered_set<std::vector<double>, VectorHash> mDataSet;   //X,Y,VAL

//    std::unordered_map<int, std::vector< double > > map;

//    std::vector<std::vector<double>> mValuesToPlot;

    //    std::vector<std::vector<double>> mLine1DataSet;
    //    std::vector<double> mLine1DataSetY;
    //    std::vector<std::vector<double>> mLine2DataSet;
    //    std::vector<double> mLine2DataSetY;


    std::vector<std::vector<double>> valRowCol;
    std::vector<std::vector<int>> lineRowCol;
    std::vector<std::vector<int>> valFlagRowCol;
    std::vector<std::vector<int>> mRowCols;   //grid with values
    std::vector<int> mTheRowCol;
    //std::unordered_map<int,int> rowCol;


    double mRmg1Range;
    /// @brief This function loads data from data base or a .csv file
    void loadData();

    /// @brief This function pre-processes data, sets grid flags.
    void prepareData();

    /// @brief This function allocates vectors for gridding.
    void allocateBuff();

    /// @brief This function interpolates along lines
    void verticalGridding();

    /// @brief This function interpolates between 2 adjacent lines
    void horizontalGridding();

    void getGridFilePars();

    void paintLines();

    /// @brief This function paints the bit map to show in the GUI
    void paintBitMap();

    void paintHPixMap(const std::vector<std::vector<double>> &input);

    /// @brief This function sets the pix map size and fills it with white
    void configPixMap();

    //bool sortByLY(const QVector<float>& v1, const QVector<float>& v2);
    //bool sortByY(const QVector<float>& v1, const QVector<float>& v2);

    int widthIncrease = 1;
    int widthOffset = 3;
    int widthMargin = 2*widthIncrease + (widthOffset+1)*2;
    int heightMargin = (widthOffset+1)*2;


    int mLineCount = 0;

    double mMaxInterpDist;
    int mMaxSteps;
    int mMaxLineSpan;

    QColor mTestColor;

    int lineTrack = 0;
    int xTrack = 1;
    int yTrack = 2;
    int valTrack = 3;
    bool mIsBDRGRD = false;

    int time1;
    int time2;

    QString mTableName;
    QString mGridValue;
    QString mGridX;
    QString mGridY;

     QSqlQuery mSql;

     bool mReadFromDB = false;

public slots:
    /// @brief This function is the entry to start gridding
    void startGridding();
};

#endif // BIDRGRIDDING_H
