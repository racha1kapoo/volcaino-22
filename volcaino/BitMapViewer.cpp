#include "BitMapViewer.h"
#include <QColorSpace>
#include <QPixmap>
#include <QImageReader>
#include <QIcon>

BitMapViewer::BitMapViewer():mFileName(""),mImageLabel(new QLabel(this))
{
   setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

   this->setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));

   this->setWindowTitle(mFileName);
}

BitMapViewer::~BitMapViewer()
{
    this->close();
}

BitMapViewer::BitMapViewer(const QString &fileName)
    :mFileName(fileName),mImageLabel(new QLabel(this)){

    setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    this->setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));

    this->setWindowTitle(mFileName);

    mImageLabel->setBackgroundRole(QPalette::Base);
    mImageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    mImageLabel->setScaledContents(true);

    loadImage();
}

bool BitMapViewer::loadImage()
{
    QPixmap pic(mFileName);

    mImageLabel->setPixmap(pic);
    mImageLabel->showMaximized();

    this->show();

    return true;
}

void BitMapViewer::setImage(const QString &fileName)
{
    mFileName=fileName;
}
