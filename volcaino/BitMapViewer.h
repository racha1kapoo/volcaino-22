/**
 *  @file   BitMapViewer.h
 *  @brief  Display the Image/BitMap
 *  @author Rachana Kapoor
 *  @date   ‎June 06, ‎2022
 ***********************************************/

#ifndef BITMAPVIEWER_H
#define BITMAPVIEWER_H

#include <QObject>
#include <QDialog>
#include <QImage>
#include <QLabel>
#include "TypeDefConst.h"
#include <QPixmap>

class BitMapViewer:public QDialog
{
    Q_OBJECT
public:
    ///default constructor
    BitMapViewer();

    /// Class destructor
    ~BitMapViewer();

    /// @brief This parameterised constructor is responsible for creating an instance of a
    /// class as well setting the image. This constructor when called display the image/bitmap as
    /// per the given parameter.
    /// @param filename: path of the image/bitmap
    /// @return  void
    ///
    BitMapViewer(const QString &fileName);

private:

    /// @brief This function is responsible for setting as well as displaying the image/bitmap
    /// @return  bool
    ///
    bool loadImage();

    /// @brief This function sets the image/bitmap file path.
    /// @return  void
    ///
    void setImage(const QString &fileName);

    QString mFileName;

    QLabel *mImageLabel;
};

#endif // BITMAPVIEWER_H
