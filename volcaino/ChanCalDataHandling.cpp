#include "ChanCalDataHandling.h"
#define _USE_MATH_DEFINES
#define ERROR_FAILED_QUERY(qsql) qDebug() << __FILE__ << __LINE__ << "FAILED QUERY [" \
    << (qsql).lastQuery() << "]" << (qsql).lastError()

ChanCalDataHandling::ChanCalDataHandling(QString expression, QString tableName, QMap<QString, QString> chanMap)
    : mExpression(expression), mTableName(tableName), mChannelMap(chanMap)
{
    mExpList = mExpression.split(";");
    for (int i = 0; i < mExpList.length(); i++) {
        if (mExpList.at(i).isEmpty())
            mExpList.removeAt(i);
    }
}

ChanCalDataHandling::~ChanCalDataHandling()
{
    QString query = "DROP TABLE TMP";
    if (!DataBase::getInstance()->getSqlQuery().exec(query))
        ERROR_FAILED_QUERY(mSql);

    for (index = mChan2Data.begin(); index != mChan2Data.end(); index++) {
        if (index.value()) {
            delete []index.value();
            index.value() = nullptr;
        }
    }
}

bool ChanCalDataHandling::configureDB()
{
    auto db = DataBase::getInstance();
    QString query = "DROP TABLE TMP";

    if (db->openDatabase()) {
        mSql = db->getSqlQuery();
        if (!mSql.exec(query))
            ERROR_FAILED_QUERY(mSql);

        mChanList = db->getHeaderDetails(mTableName);
        if (mChanList.isEmpty())
            return false;

        mTableRows = db->getRowCount(mTableName);
        if (mTableRows == 0)
            return false;

        return true;
    }
    return false;
}

bool ChanCalDataHandling::checkStringChan()
{
    std::map<QString,SqliteColumninfo> allChansInfo;
    QStringList textChans;
    QStringList inputChans;
    allChansInfo = DataBase::getInstance()->getDbTableInfo(mTableName);

    std::map<QString,SqliteColumninfo>::iterator itDB;
    for (itDB = allChansInfo.begin(); itDB != allChansInfo.end(); itDB++) {
        if (itDB->second.type == "TEXT")
            textChans.append(itDB->first);
    }

    QMap<QString,QString>::iterator itMap;
    for (itMap = mChannelMap.begin(); itMap != mChannelMap.end(); itMap++)
        inputChans.append(itMap.value().toUpper());

    for (QString str : textChans) {
        if (inputChans.contains(str))
            return false;
    }

    return true;
}

bool ChanCalDataHandling::expValidateCalulate(QString expression)
{
    if (expression.isEmpty())
        return false;
    if (expression.contains(";") && expression.indexOf(";") != (expression.length() - 1))
        return false;
    if (expression.back() == ";")
        expression.chop(1);

    std::vector<int> bracketStack;
    std::vector<int> bracketPos;
    QString exp = expression.simplified().replace(" ","");
    if (exp.length() != expression.length())
        return false;
    if (!exp.contains("="))
        return false;

    QString newChanName;
    newChanName = exp.mid(0, exp.indexOf("="));

    exp = exp.mid(exp.indexOf("=")+1);

    for (int i = 0; i < exp.length(); i++) {
        if (exp[i] == "(")
            bracketStack.push_back(i);

        if (exp[i] == ")") {
            bracketPos.push_back(bracketStack.back());
            bracketPos.push_back(i);
            bracketStack.pop_back();
        }
    }

    if (!bracketStack.empty())
        return false; //Error

    for (int i = 0; i < bracketPos.size(); i += 2) { //first calculate all vals in brackets
        QString subExpression = exp.mid(bracketPos[i]+1, bracketPos[i+1]-1-bracketPos[i]);

        //qDebug() << exp;
        if (!calculate(exp, bracketPos[i], bracketPos[i+1], true))
            return false;
    }

    exp = exp.simplified().replace(" ","");
    if (!calculate(exp, -1, exp.length(), false))
        return false;

    mChan2Data.insert(newChanName, mTmpChanData.back());
    //mTmpChanData.pop_back();
    mTmpChanNames.clear();
    mTmpIndex = 0;

    for (int i = 0; i < mTmpChanData.size(); i++) {
        if (!mChan2Data.values().contains(mTmpChanData[i]))
            delete []mTmpChanData[i];
    }
    mTmpChanData.clear();

    return true;
}

void ChanCalDataHandling::appendColumn()
{
    newChanName = mChan2Data.firstKey();
    createTempTable();

    if (mChanList.contains(newChanName))
        DataBase::getInstance()->deleteColumn(mTableName, newChanName);

    DataBase::getInstance()->addColumn(mTableName, newChanName, "FLOAT", false, "0");

    QString query = "UPDATE "+mTableName+ " SET "+newChanName + " =(SELECT "+newChanName+ " FROM TMP WHERE TMP.rowid = "+mTableName+".rowid)";

    DataBase::getInstance()->startTransaction();

    if (!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);

    DataBase::getInstance()->closeTransaction();

    emit statusChanged("Channel calculation done, appended to DB");
}

bool ChanCalDataHandling::calculate(QString &expression, int start, int end, bool hasBracket)
{
    QString subExpression = expression.mid(start+1, end-1-start);

    std::vector<double *> tmpChanVals;
    int tmpIndex = 0;
    QStringList tmpChanNames;

    while (subExpression.contains("sin") || subExpression.contains("cos")
          || subExpression.contains("tan") || subExpression.contains("sqrt")
          || subExpression.contains("log") || subExpression.contains("ln")
          || subExpression.contains("abs") || subExpression.contains("^")) {
        if (subExpression.contains("sin")) {
            int indexStart = subExpression.indexOf("sin")+3;
            int indexEnd = indexStart;
            while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                  && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                indexEnd++;

            if (indexEnd == indexStart) {  //possibly: sin-2, sin-chan
                if (subExpression[indexEnd] != "-")
                    return false;

                indexEnd++;
                while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                       && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                    indexEnd++;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if (!chanOrNumValidate(operand))
                return false;
            double *tmp = nullptr;

            QString tmpChan;
            if (isOperandNumber(operand)) {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            } else {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            arithmetics("", "sin", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart-3;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);
            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while (tmpChan.length() < subString.length())
                tmpChan.push_back(" ");
            subExpression.replace(subString, tmpChan);
        } else if (subExpression.contains("cos")) {
            int indexStart = subExpression.indexOf("cos")+3;
            int indexEnd = indexStart;
            while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                   && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                indexEnd++;

            if (indexEnd == indexStart) {  //possibly: sin-2, sin-chan
                if (subExpression[indexEnd] != "-")
                    return false;

                //int indexEndOri = indexEnd;
                indexEnd++;
                while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                       && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                    indexEnd++;
                //QString possible = subExpression.mid(indexEndOri, indexEnd-indexEndOri);
                //if(!chanOrNumValidate(possible, tmpChanNames))  return false;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if (!chanOrNumValidate(operand))
                return false;

            double *tmp = nullptr;
            QString tmpChan;
            if (isOperandNumber(operand)) {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            } else {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            arithmetics("", "cos", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart-3;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);
            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while (tmpChan.length() < subString.length())
                tmpChan.push_back(" ");
            subExpression.replace(subString, tmpChan);
        } else if (subExpression.contains("tan")) {
            int indexStart = subExpression.indexOf("tan")+3;
            int indexEnd = indexStart;
            while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                   && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                indexEnd++;

            if (indexEnd == indexStart) {  //possibly: sin-2, sin-chan
                if (subExpression[indexEnd] != "-")
                    return false;

                //int indexEndOri = indexEnd;
                indexEnd++;
                while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                       && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                    indexEnd++;
                //QString possible = subExpression.mid(indexEndOri, indexEnd-indexEndOri);
                //if(!chanOrNumValidate(possible, tmpChanNames))  return false;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if (!chanOrNumValidate(operand))
                return false;
            double *tmp = nullptr;
            QString tmpChan;
            if (isOperandNumber(operand)) {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            } else {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            arithmetics("", "tan", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart-3;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);

            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while (tmpChan.length() < subString.length())
                tmpChan.push_back(" ");
            subExpression.replace(subString, tmpChan);
        } else if (subExpression.contains("sqrt")) {
            int indexStart = subExpression.indexOf("sqrt")+4;
            int indexEnd = indexStart;
            while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                  && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                indexEnd++;

            if (indexEnd == indexStart) {  //possibly: sin-2, sin-chan
                if (subExpression[indexEnd] != "-")
                    return false;

                //int indexEndOri = indexEnd;
                indexEnd++;
                while (indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-"
                       && subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                    indexEnd++;
                //QString possible = subExpression.mid(indexEndOri, indexEnd-indexEndOri);
                //if(!chanOrNumValidate(possible, tmpChanNames))  return false;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if (!chanOrNumValidate(operand))
                return false;
            double *tmp = nullptr;
            QString tmpChan;
            if (isOperandNumber(operand)) {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            } else {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            }
            arithmetics("", "sqrt", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart - 4;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);
            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while (tmpChan.length() < subString.length())
                tmpChan.push_back(" ");
            subExpression.replace(subString, tmpChan);
        } else if (subExpression.contains("log")) {
            int indexStart = subExpression.indexOf("log")+3;
            int indexEnd = indexStart;
            while(indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-" &&
                  subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
            {
                indexEnd++;
            }


            if(indexEnd == indexStart)   //possibly: sin-2, sin-chan
            {
                if(subExpression[indexEnd] != "-")
                {return false;}

                //int indexEndOri = indexEnd;
                indexEnd++;
                while(indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-" &&
                      subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                {
                    indexEnd++;
                }
                //QString possible = subExpression.mid(indexEndOri, indexEnd-indexEndOri);
                //if(!chanOrNumValidate(possible, tmpChanNames))  return false;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if(!chanOrNumValidate(operand))  return false;
            double *tmp = nullptr;
            QString tmpChan;
            if(isOperandNumber(operand))
            {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            }
            else
            {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            arithmetics("", "log", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart-3;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);
            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while(tmpChan.length() < subString.length())
            {
                tmpChan.push_back(" ");
            }
            subExpression.replace(subString, tmpChan);
        }

        else if(subExpression.contains("ln"))
        {
            int indexStart = subExpression.indexOf("ln")+2;
            int indexEnd = indexStart;
            while(indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-" &&
                  subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
            {
                indexEnd++;
            }


            if(indexEnd == indexStart)   //possibly: sin-2, sin-chan
            {
                if(subExpression[indexEnd] != "-")
                {return false;}

                //int indexEndOri = indexEnd;
                indexEnd++;
                while(indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-" &&
                      subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                {
                    indexEnd++;
                }
                //QString possible = subExpression.mid(indexEndOri, indexEnd-indexEndOri);
                //if(!chanOrNumValidate(possible, tmpChanNames))  return false;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if(!chanOrNumValidate(operand))  return false;
            double *tmp = nullptr;
            QString tmpChan;
            if(isOperandNumber(operand))
            {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            }
            else
            {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            arithmetics("", "ln", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart-2;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);
            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while(tmpChan.length() < subString.length())
            {
                tmpChan.push_back(" ");
            }
            subExpression.replace(subString, tmpChan);
        }

        else if(subExpression.contains("abs"))
        {
            int indexStart = subExpression.indexOf("abs")+3;
            int indexEnd = indexStart;
            while(indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-" &&
                  subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
            {
                indexEnd++;
            }


            if(indexEnd == indexStart)   //possibly: sin-2, sin-chan
            {
                if(subExpression[indexEnd] != "-")
                {return false;}

                //int indexEndOri = indexEnd;
                indexEnd++;
                while(indexEnd < subExpression.length() && subExpression[indexEnd] != "+" && subExpression[indexEnd] != "-" &&
                      subExpression[indexEnd] != "*" && subExpression[indexEnd] != "/" && subExpression[indexEnd] != "%")
                {
                    indexEnd++;
                }
                //QString possible = subExpression.mid(indexEndOri, indexEnd-indexEndOri);
                //if(!chanOrNumValidate(possible, tmpChanNames))  return false;
            }
            QString operand = subExpression.mid(indexStart, indexEnd-indexStart).simplified();
            if(!chanOrNumValidate(operand))  return false;
            double *tmp = nullptr;
            QString tmpChan;
            if(isOperandNumber(operand))
            {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            }
            else
            {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            arithmetics("", "abs", operand, tmpChanNames, tmp, tmpChanVals);

            indexStart = indexStart-3;
            QString subString = subExpression.mid(indexStart, indexEnd-indexStart);
            tmpChanNames.push_back(tmpChan);
            tmpIndex++;
            while(tmpChan.length() < subString.length())
            {
                tmpChan.push_back(" ");
            }
            subExpression.replace(subString, tmpChan);
        }

        else if(subExpression.contains("^"))
        {
            int indexStart = subExpression.indexOf("^");
            int indexBack = indexStart+1;
            int indexFront = indexStart-1;
            indexStart++;

            while(indexFront >= 0 && subExpression[indexFront] != "+" && subExpression[indexFront] != "-" &&
                  subExpression[indexFront] != "*" && subExpression[indexFront] != "/" && subExpression[indexFront] != "%")
            {
                indexFront--;
            }

            if(indexFront >= 0 && subExpression[indexFront] == "-")   //Possibly -2^2
            {
                if(indexFront == 0 || (indexFront > 0 &&(subExpression[indexFront-1] == "+" || subExpression[indexFront-1] == "-" ||
                                                         subExpression[indexFront-1] == "*" || subExpression[indexFront-1] == "/" || subExpression[indexFront-1] == "%")))
                {
                    //indexFront = indexFront;   //3+-2^2, -2^2
                }
                else
                {
                    indexFront++;   //3-2^2
                }
            }
            else
            {
                indexFront++;
            }

            while(indexBack < subExpression.length() && subExpression[indexBack] != "+" && subExpression[indexBack] != "-" &&
                  subExpression[indexBack] != "*" && subExpression[indexBack] != "/" && subExpression[indexBack] != "%")
            {
                indexBack++;
            }

            if(indexBack == indexStart)
            {
                if(subExpression[indexBack] != "-")
                {return false;}

                //int indexEndOri = indexBack;
                indexBack++;
                while(indexBack < subExpression.length() && subExpression[indexBack] != "+" && subExpression[indexBack] != "-" &&
                      subExpression[indexBack] != "*" && subExpression[indexBack] != "/" && subExpression[indexBack] != "%")
                {
                    indexBack++;
                }
                //indexBack--;
            }
            //            else
            //            {
            //                indexBack--;
            //            }

            QString operand1 = subExpression.mid(indexFront, indexStart-1-indexFront).simplified();
            if(!chanOrNumValidate(operand1))  return false;
            QString operand2 = subExpression.mid(indexStart, indexBack-indexStart).simplified();
            if(!chanOrNumValidate(operand2))  return false;

            double *tmp = nullptr;
            QString tmpChan;
            if(!isOperandNumber(operand1) || !isOperandNumber(operand2))
            {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            else
            {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            }

            arithmetics(operand1, "^", operand2, tmpChanNames, tmp, tmpChanVals);

            tmpChanNames.push_back(tmpChan);
            tmpIndex++;

            QString subString = subExpression.mid(indexFront, indexBack-indexFront);
            while(tmpChan.length() < subString.length())
            {
                tmpChan.push_back(" ");
            }
            subExpression.replace(subString, tmpChan);
        }
    }

    while(subExpression.contains("*") || subExpression.contains("/") || subExpression.contains("%"))
    {
        int indexStart = 0;
        int index1 = 9999;
        int index2 = 9999;
        int index3 = 9999;
        if(subExpression.contains("*"))
        {
            index1 = subExpression.indexOf("*");
        }
        if(subExpression.contains("/"))
        {
            index2 = subExpression.indexOf("/");
        }
        if(subExpression.contains("%"))
        {
            index3 = subExpression.indexOf("%");
        }

        indexStart = std::min(std::min(index1, index2), index3);

        QString theOperator;
        if(indexStart == index1)  theOperator = "*";
        else if(indexStart == index2)  theOperator = "/";
        else if(indexStart == index2)  theOperator = "%";

        int indexFront = indexStart-1;
        int indexBack = indexStart+1;
        indexStart++;
        while(indexFront >= 0 && subExpression[indexFront] != "+" && subExpression[indexFront] != "-")
        {
            indexFront--;
        }

        if(indexFront >= 0 && subExpression[indexFront] == "-")   //Possibly -2^2
        {
            if(indexFront == 0 || (indexFront > 0 &&(subExpression[indexFront-1] == "+" || subExpression[indexFront-1] == "-" ||
                                                     subExpression[indexFront-1] == "*" || subExpression[indexFront-1] == "/" || subExpression[indexFront-1] == "%")))
            {
                //indexFront = indexFront;
            }
            else
            {
                indexFront++;
            }
        }
        else
        {
            indexFront++;
        }

        while(indexBack < subExpression.length() && subExpression[indexBack] != "+" && subExpression[indexBack] != "-" &&
              subExpression[indexBack] != "*" && subExpression[indexBack] != "/" && subExpression[indexBack] != "%")
        {
            indexBack++;
        }

        if(indexBack == indexStart)   //
        {
            if(subExpression[indexBack] != "-")
            {return false;}

            indexBack++;
            while(indexBack < subExpression.length() && subExpression[indexBack] != "+" && subExpression[indexBack] != "-" &&
                  subExpression[indexBack] != "*" && subExpression[indexBack] != "/" && subExpression[indexBack] != "%")
            {
                indexBack++;
            }
        }


        QString operand1 = subExpression.mid(indexFront, indexStart-1-indexFront).simplified();
        if(!chanOrNumValidate(operand1))  return false;
        QString operand2 = subExpression.mid(indexStart, indexBack-indexStart).simplified();
        if(!chanOrNumValidate(operand2))  return false;


        double *tmp = nullptr;
        QString tmpChan;
        if(!isOperandNumber(operand1) || !isOperandNumber(operand2))
        {
            tmp = new double[mTableRows];
            tmpChanVals.push_back(tmp);
            tmpChan = "t" + QString::number(tmpIndex)+"a";
        }
        else
        {
            tmp = new double[1];
            tmpChanVals.push_back(tmp);
            tmpChan = "t" + QString::number(tmpIndex)+"n";
        }

        arithmetics(operand1, theOperator, operand2, tmpChanNames, tmp, tmpChanVals);

        QString subString = subExpression.mid(indexFront, indexBack-indexFront);
        tmpChanNames.push_back(tmpChan);
        while(tmpChan.length() < subString.length())
        {
            tmpChan.push_back(" ");
        }
        tmpIndex++;
        subExpression.replace(subString, tmpChan);

    }

    while(subExpression.contains("+") || subExpression.contains("-"))
    {
        int indexStart = 0;
        int index1 = 9999;
        int index2 = 9999;


        if(subExpression.contains("+"))
        {
            index1 = subExpression.indexOf("+");
        }
        for(int i = 1; i < subExpression.length()-1; i++)
        {
            if(subExpression[i] == "-" && subExpression[i-1] != "+")
            {
                index2 = i;
                break;
            }
        }

        indexStart = std::min(index1, index2);

        if(indexStart == 9999)
        {
            break;
        }
        else
        {
            QString theOperator;
            if(indexStart == index1)  theOperator = "+";
            else if(indexStart == index2)  theOperator = "-";

            int indexFront = indexStart-1;
            int indexBack = indexStart+1;
            indexStart++;

            while(indexFront >= 0 && subExpression[indexFront] != "+" && subExpression[indexFront] != "-")
            {
                indexFront--;
            }

            if(indexFront >= 0 && subExpression[indexFront] == "-")   //Possibly -2^2
            {
                if(indexFront == 0 || (indexFront > 0 &&(subExpression[indexFront-1] == "+" || subExpression[indexFront-1] == "-" ||
                                                         subExpression[indexFront-1] == "*" || subExpression[indexFront-1] == "/" || subExpression[indexFront-1] == "%")))
                {
                    //indexFront = indexFront;
                }
                else
                {
                    indexFront++;
                }
            }
            else
            {
                indexFront++;
            }

            while(indexBack < subExpression.length() && subExpression[indexBack] != "+" && subExpression[indexBack] != "-")
            {
                indexBack++;
            }

            if(indexBack == indexStart)
            {
                if(subExpression[indexBack] != "-")
                {return false;}

                indexBack++;
                while(indexBack < subExpression.length() && subExpression[indexBack] != "+" && subExpression[indexBack] != "-")
                {
                    indexBack++;
                }
            }

            QString operand1 = subExpression.mid(indexFront, indexStart-1-indexFront).simplified();
            if(!chanOrNumValidate(operand1))  return false;
            QString operand2 = subExpression.mid(indexStart, indexBack-indexStart).simplified();
            if(!chanOrNumValidate(operand2))  return false;

            //        if((!chanNames.contains(operand1.simplified()) && !tmpChanNames.contains(operand1.simplified()))
            //           ||(!chanNames.contains(operand2.simplified()) && !tmpChanNames.contains(operand2.simplified())))
            //        {return false;}

            double *tmp = nullptr;
            QString tmpChan;
            if(!isOperandNumber(operand1) || !isOperandNumber(operand2))
            {
                tmp = new double[mTableRows];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"a";
            }
            else
            {
                tmp = new double[1];
                tmpChanVals.push_back(tmp);
                tmpChan = "t" + QString::number(tmpIndex)+"n";
            }

            arithmetics(operand1, theOperator, operand2, tmpChanNames, tmp, tmpChanVals);

            QString subString = subExpression.mid(indexFront, indexBack-indexFront);
            tmpChanNames.push_back(tmpChan);
            while(tmpChan.length() < subString.length())
            {
                tmpChan.push_back(" ");
            }

            tmpIndex++;

            subExpression.replace(subString, tmpChan);
        }
    }

    if(tmpChanNames.isEmpty())
    {
        QString subExp = subExpression.simplified();
        if(subExp[0] == "-")  subExp.remove(0,1);

        if(!chanOrNumValidate(subExpression.simplified()))
        {return false;}
        tmpChanNames.push_back(subExp);

        if(isOperandNumber(subExp))
        {
            double *val = new double;
            *val = subExpression.toDouble();
            tmpChanVals.push_back(val);
        }
        else
        {
            if(mChanList.contains(subExp))
            {
                double *valArray = new double[mTableRows];
                getChanAllVals(subExpression.simplified(), valArray);


                tmpChanVals.push_back(valArray);
            }
            else if(mTmpChanNames.contains(subExp))
            {
                int dataIndex = subExp[1].digitValue();
                //double *valArray = new double[mTableRows];
                //valArray = mTmpChanData[dataIndex];

                if(subExpression[0] == "-")
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                    }
                }
                tmpChanVals.push_back(mTmpChanData[dataIndex]);
            }
            else if(mChan2Data.contains(subExp))
            {
                if(subExpression[0] == "-")
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mChan2Data[subExp][i] = -mChan2Data[subExp][i];
                    }
                }
                tmpChanVals.push_back(mChan2Data[subExp]);
            }
            else
            {
                return false;
            }
        }
    }

    if(hasBracket)
    {
        int replaceStart = start;
        int replaceLength = end-start+1;

        QString tmpChannel;

        if(isOperandNumber(tmpChanNames.back()))
        {
            tmpChannel = "T"+QString::number(mTmpIndex)+"N";
        }
        else
        {
            tmpChannel = "T"+QString::number(mTmpIndex)+"A";
        }
        mTmpChanNames.push_back(tmpChannel);

        while(tmpChannel.length() < replaceLength)
        {
            tmpChannel.push_back(" ");
        }

        mTmpChanData.push_back(tmpChanVals.back());
        mTmpIndex++;

        expression.replace(replaceStart, replaceLength, tmpChannel);
    }
    else
    {
        if(!tmpChanNames.isEmpty())
        {
            QString tmpChannel;
            if(isOperandNumber(tmpChanNames.back()))
            {
                tmpChannel = "T"+QString::number(mTmpIndex)+"N";
            }
            else
            {
                tmpChannel = "T"+QString::number(mTmpIndex)+"A";
            }

            mTmpChanNames.push_back(tmpChannel);
            mTmpChanData.push_back(tmpChanVals.back());
            mTmpIndex++;
        }
    }

    for(int i = 0; i < tmpChanVals.size()-1; i++)
    {
        delete []tmpChanVals[i];
    }
    return true;
}

bool ChanCalDataHandling::isFourDiff(QString exp)
{
    QString newChanName;
    newChanName = exp.mid(0, exp.indexOf("="));
    if (exp.contains("diff")) {
        int openBrack = exp.indexOf("(");
        int closeBrack = exp.indexOf(")");
        QString existentChan = exp.mid(openBrack+1, closeBrack-openBrack-1);
        std::vector<double> orgValues;

        QString query = "SELECT " + existentChan + " FROM " + mTableName;
        int index = 0;

        if (mSql.exec(query)) {
            while (mSql.next()) {
                if (mSql.value(0).isNull()) {
                    orgValues.push_back(NAN);
                } else {
                    orgValues.push_back(mSql.value(0).toDouble());
                }
                index++;
            }
        }

        if (exp.contains("1")) {
            calcDiffValues(orgValues, newChanName, 1);
        } else if (exp.contains("2")) {
            calcDiffValues(orgValues, newChanName, 2);
        } else if (exp.contains("3")) {
            calcDiffValues(orgValues, newChanName, 3);
        } else if (exp.contains("4")) {
            calcDiffValues(orgValues, newChanName, 4);
        } else {
            return true;
        }
        return true;
    }
    return false;
}

void ChanCalDataHandling::calcDiffValues(std::vector<double> vals, QString newChan, int deg)
{
    QStringList typeList;
    typeList << newChan+" FLOAT,";
    std::vector<double> res;

    if (deg == 1) {
        for (int i = 0; i < mTableRows; i++) {
            if (i > mTableRows - 2) {
                res.push_back(NAN);
            } else {
                double val = vals[i + 1] - vals[i];
                res.push_back(val);
            }
        }
    } else if (deg == 2) {
        for (int i = 0; i < mTableRows; i++) {
            if (i < 1 || i > mTableRows - 2) {
                res.push_back(NAN);
            } else {
                double val = vals[i + 1] - (2 * vals[i]) + vals[i - 1];
                res.push_back(val);
            }
        }
    } else if (deg == 3) {
        for (int i = 0; i < mTableRows; i++) {
            if (i < 1 || i > mTableRows - 3) {
                res.push_back(NAN);
            } else {
                double val = vals[i + 2] - (3 * vals[i + 1]) + (3 * vals[i]) - vals[i - 1];
                res.push_back(val);
            }
        }
    } else {
        for (int i = 0; i < mTableRows; i++) {
            if (i < 2 || i > mTableRows - 3) {
                res.push_back(NAN);
            } else {
                double val = vals[i + 2] - (4 * vals[i + 1]) + (6 * vals[i]) - (4 * vals[i - 1]) + vals[i - 2];
                res.push_back(val);
            }
        }
    }

    DataBase::getInstance()->createTable("TMP", typeList);
    DataBase::getInstance()->startTransaction();

    for (int i = 0; i < mTableRows; i++) {
        if (!std::isnan(res[i])) {
            QString rec = QString::number(res[i], 'f', 4);
            DataBase::getInstance()->insertRecord(rec,"TMP");
        } else {
            QString blnk = "NAN";
            DataBase::getInstance()->insertRecord(blnk,"TMP");
        }
    }
    DataBase::getInstance()->closeTransaction();

    if (mChanList.contains(newChan))
        DataBase::getInstance()->deleteColumn(mTableName, newChan);

    DataBase::getInstance()->addColumn(mTableName, newChan, "FLOAT", false, "0");

    QString query = "UPDATE "+mTableName+ " SET "+newChan + " =(SELECT "+newChan+ " FROM TMP WHERE TMP.rowid = "+mTableName+".rowid)";

    DataBase::getInstance()->startTransaction();

    if (!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);

    DataBase::getInstance()->closeTransaction();

    emit statusChanged("Channel calculation done, appended to DB");

    emit calculateDone();
}

bool ChanCalDataHandling::chanOrNumValidate(QString input)
{
    if (input.isEmpty())
        return false;

    bool ok = false;
    input.toDouble(&ok);
    if (ok)
        return true;

    if (input.indexOf("-") == 0)
        input.remove(0,1);

    if (mChanList.contains(input))
        return true;
    if (mChan2Data.contains(input))
        return true;
    if (input == "pi" || input == "e")
        return true;

    if (input[0].toUpper() == "T" && input.length() == 3
            && (input[2].toUpper() == "A" || input[2].toUpper() == "N"))
        return true;

    //    if(input[0].toUpper() == "T" && input.length() == 3)
    //    {
    //        if(input[0] == 't')
    //        {
    //            int i = input[1].digitValue();
    //            if()
    //        }
    //        if(tmpChans.contains(input))  return true;
    //        if(mTmpChanNames.contains(input))  return true;
    //    }

    return false;
}

bool ChanCalDataHandling::isOperandNumber(QString input)
{
    bool ok = false;
    input.toDouble(&ok);

    if (ok)
        return true;
    if (input == "pi" || input == "e")
        return true;
    if ((input[0] == "T" && input[2] == "N")
            || (input[0] == "t" && input[2] == "n"))
        return true;

    return false;
}

bool ChanCalDataHandling::isDoublePtrArray(double *input)
{
    if (input + 1 == nullptr)
        return true;
    return false;
}

bool ChanCalDataHandling::arithmetics(QString operand1, QString theOperator, QString operand2, QStringList tmpChanNames, double *outputData, std::vector<double *> tmpChanData)
{
    //double *operand2Ptr = nullptr;
    //double *operand1Ptr = nullptr;
    double *operand1ArrayPtr = nullptr;
    double *operand2ArrayPtr = nullptr;

    bool isOp1NotNumNeg = false;
    bool isOp2NotNumNeg = false;
    bool op1Array = false;
    bool op2Array = false;
    isOp1NotNumNeg = opNotNumNeg(operand1);
    if (isOp1NotNumNeg)
        operand1.remove(0, 1);
    isOp2NotNumNeg = opNotNumNeg(operand2);
    if (isOp2NotNumNeg)
        operand2.remove(0, 1);

    if (theOperator.isEmpty() || operand2.isEmpty())
        return false;

    if(operand1.isEmpty())
    {
        if(mChanList.contains(operand2))
        {
            operand2ArrayPtr = new double[mTableRows];
            getChanAllVals(operand2, operand2ArrayPtr);


            if(isOp2NotNumNeg)
            {
                for(int i = 0; i < mTableRows; i++)
                {
                    operand2ArrayPtr[i] = -operand2ArrayPtr[i];
                }
            }

            chanArithmetic(false, nullptr, theOperator, true, operand2ArrayPtr, outputData);

        }
        else if(tmpChanNames.contains(operand2))
        {
            int dataIndex = operand2[1].digitValue();

            if(!isOperandNumber(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        tmpChanData[dataIndex][i] = -tmpChanData[dataIndex][i];
                    }
                }
                chanArithmetic(false, nullptr, theOperator, true, tmpChanData[dataIndex], outputData);
            }
            else
            {
                //operand2Ptr = &tmpChanData[dataIndex][0];
                if(isOp2NotNumNeg)
                {
                    *tmpChanData[dataIndex] = -*tmpChanData[dataIndex];
                }
                chanArithmetic(false, nullptr, theOperator, false, tmpChanData[dataIndex], outputData);
            }
        }
        else if(mTmpChanNames.contains(operand2))
        {
            int dataIndex = operand2[1].digitValue();

            if(!isOperandNumber(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                    }
                }
                chanArithmetic(false, nullptr, theOperator, true, mTmpChanData[dataIndex], outputData);
            }
            else
            {
                if(isOp2NotNumNeg)
                {
                    *mTmpChanData[dataIndex] = -*mTmpChanData[dataIndex];
                }
                chanArithmetic(false, nullptr, theOperator, false, mTmpChanData[dataIndex], outputData);
            }
        }

        else if(mChan2Data.contains(operand2))
        {
            if(isOp2NotNumNeg)
            {
                for(int i = 0; i < mTableRows; i++)
                {
                    mChan2Data[operand2][i] = -mChan2Data[operand2][i];
                }
            }
            chanArithmetic(false, nullptr, theOperator, true, mChan2Data[operand2], outputData);
        }

        else
        {
            //Single value
            //double operand2Num = strOp2DoubleOp(operand2);
            unitArithmetic(0,theOperator,strOp2DoubleOp(operand2),outputData);
        }

    }
    else
    {
        if(mChanList.contains(operand1))
        {
            operand1ArrayPtr = new double[mTableRows];
            getChanAllVals(operand1, operand1ArrayPtr);


            if(isOp1NotNumNeg)
            {
                for(int i = 0; i < mTableRows; i++)
                {
                    operand1ArrayPtr[i] = -operand1ArrayPtr[i];
                }
            }

            if(mChanList.contains(operand2))
            {
                operand2ArrayPtr = new double[mTableRows];
                getChanAllVals(operand2, operand2ArrayPtr);


                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        operand2ArrayPtr[i] = -operand2ArrayPtr[i];
                    }
                }

                chanArithmetic(true, operand1ArrayPtr, theOperator, true, operand2ArrayPtr, outputData);

            }
            else if(tmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();
                if(!isOperandNumber(operand2))
                {
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            tmpChanData[dataIndex][i] = -tmpChanData[dataIndex][i];
                        }
                    }

                    chanArithmetic(true, operand1ArrayPtr, theOperator, true, tmpChanData[dataIndex], outputData);
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *tmpChanData[dataIndex] = -*tmpChanData[dataIndex];
                    }

                    chanArithmetic(true, operand1ArrayPtr, theOperator, false, tmpChanData[dataIndex], outputData);
                }

            }
            else if(mTmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();
                if(!isOperandNumber(operand2))
                {
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                        }
                    }

                    chanArithmetic(true, operand1ArrayPtr, theOperator, true, mTmpChanData[dataIndex], outputData);
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *mTmpChanData[dataIndex] = -*mTmpChanData[dataIndex];
                    }

                    chanArithmetic(true, operand1ArrayPtr, theOperator, false, mTmpChanData[dataIndex], outputData);
                }

            }
            else if(mChan2Data.contains(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mChan2Data[operand2][i] = -mChan2Data[operand2][i];
                    }
                }
                chanArithmetic(true, operand1ArrayPtr, theOperator, true, mChan2Data[operand2], outputData);
            }
            else
            {
                double op2 = strOp2DoubleOp(operand2);
                chanArithmetic(true, operand1ArrayPtr,theOperator,false, &op2,outputData);
            }

        }
        else if(tmpChanNames.contains(operand1))
        {
            int dataIndexOp1 = operand1[1].digitValue();
            if(!isOperandNumber(operand1))
            {
                op1Array = true;
                if(isOp1NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        tmpChanData[dataIndexOp1][i] = -tmpChanData[dataIndexOp1][i];
                    }
                }
            }
            else
            {
                op1Array = false;
                if(isOp1NotNumNeg)
                {
                    *tmpChanData[dataIndexOp1] = -*tmpChanData[dataIndexOp1];
                }
            }

            if(mChanList.contains(operand2))
            {
                operand2ArrayPtr = new double[mTableRows];
                getChanAllVals(operand2, operand2ArrayPtr);


                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        operand2ArrayPtr[i] = -operand2ArrayPtr[i];
                    }
                }

                chanArithmetic(op1Array, tmpChanData[dataIndexOp1], theOperator, true, operand2ArrayPtr, outputData);

            }

            else if(tmpChanNames.contains(operand2))
            {

                int dataIndex = operand2[1].digitValue();
                if(!isOperandNumber(operand2))
                {
                    op2Array = true;
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            tmpChanData[dataIndex][i] = -tmpChanData[dataIndex][i];
                        }
                    }
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *tmpChanData[dataIndex] = -*tmpChanData[dataIndex];
                    }
                }

                chanArithmetic(op1Array, tmpChanData[dataIndexOp1], theOperator, op2Array, tmpChanData[dataIndex], outputData);
            }

            else if(mTmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    op2Array = true;
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                        }
                    }
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *mTmpChanData[dataIndex] = -*mTmpChanData[dataIndex];
                    }
                }

                chanArithmetic(op1Array, tmpChanData[dataIndexOp1], theOperator, op2Array, mTmpChanData[dataIndex], outputData);
            }
            else if(mChan2Data.contains(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mChan2Data[operand2][i] = -mChan2Data[operand2][i];
                    }
                }
                chanArithmetic(op1Array, tmpChanData[dataIndexOp1], theOperator, true, mChan2Data[operand2], outputData);
            }
            else
            {
                double op2 = strOp2DoubleOp(operand2);

                chanArithmetic(op1Array, tmpChanData[dataIndexOp1],theOperator,false, &op2,outputData);
            }
        }

        else if(mTmpChanNames.contains(operand1))
        {
            int dataIndexOp1 = operand1[1].digitValue();

            if(!isOperandNumber(operand1))
            {
                op1Array = true;
                if(isOp1NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mTmpChanData[dataIndexOp1][i] = -mTmpChanData[dataIndexOp1][i];
                    }
                }
            }
            else
            {
                if(isOp1NotNumNeg)
                {
                    *mTmpChanData[dataIndexOp1] = -*mTmpChanData[dataIndexOp1];
                }
            }

            if(mChanList.contains(operand2))
            {
                operand2ArrayPtr = new double[mTableRows];
                getChanAllVals(operand2, operand2ArrayPtr);


                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        operand2ArrayPtr[i] = -operand2ArrayPtr[i];
                    }
                }

                chanArithmetic(op1Array, mTmpChanData[dataIndexOp1], theOperator, true, operand2ArrayPtr, outputData);

            }
            else if(tmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    op2Array = true;
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            tmpChanData[dataIndex][i] = -tmpChanData[dataIndex][i];
                        }
                    }
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *tmpChanData[dataIndex] = -*tmpChanData[dataIndex];
                    }
                }
                chanArithmetic(op1Array, mTmpChanData[dataIndexOp1], theOperator, op2Array, tmpChanData[dataIndex], outputData);
            }
            else if(mTmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    op2Array = true;
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                        }
                    }
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *mTmpChanData[dataIndex] = -*mTmpChanData[dataIndex];
                    }
                }

                chanArithmetic(op1Array, mTmpChanData[dataIndexOp1], theOperator, op2Array, mTmpChanData[dataIndex], outputData);

            }
            else if(mChan2Data.contains(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mChan2Data[operand2][i] = -mChan2Data[operand2][i];
                    }
                }
                chanArithmetic(op1Array, mTmpChanData[dataIndexOp1], theOperator, true, mChan2Data[operand2], outputData);
            }
            else
            {
                double op2 = strOp2DoubleOp(operand2);
                chanArithmetic(op1Array, mTmpChanData[dataIndexOp1],theOperator,false, &op2,outputData);

            }
        }

        else if(mChan2Data.contains(operand1))
        {
            if(isOp1NotNumNeg)
            {
                for(int i = 0; i < mTableRows; i++)
                {
                    mChan2Data[operand1][i] = -mChan2Data[operand1][i];
                }
            }

            if(mChanList.contains(operand2))
            {
                operand2ArrayPtr = new double[mTableRows];
                getChanAllVals(operand2, operand2ArrayPtr);

                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        operand2ArrayPtr[i] = -operand2ArrayPtr[i];
                    }
                }

                chanArithmetic(true, mChan2Data[operand1], theOperator, true, operand2ArrayPtr, outputData);

            }
            else if(tmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    op2Array = true;
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            tmpChanData[dataIndex][i] = -tmpChanData[dataIndex][i];
                        }
                    }
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *tmpChanData[dataIndex] = -*tmpChanData[dataIndex];
                    }
                }
                chanArithmetic(true, mChan2Data[operand1], theOperator, op2Array, tmpChanData[dataIndex], outputData);
            }

            else if(mTmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    op2Array = true;
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                        }
                    }
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *mTmpChanData[dataIndex] = -*mTmpChanData[dataIndex];
                    }
                }

                chanArithmetic(true, mChan2Data[operand1], theOperator, op2Array, mTmpChanData[dataIndex], outputData);

            }
            else if(mChan2Data.contains(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mChan2Data[operand2][i] = -mChan2Data[operand2][i];
                    }
                }
                chanArithmetic(true, mChan2Data[operand1], theOperator, true, mChan2Data[operand2], outputData);
            }
            else
            {
                double op2 = strOp2DoubleOp(operand2);
                chanArithmetic(true, mChan2Data[operand1],theOperator,false, &op2,outputData);

            }
        }

        else
        {
            double op1 = strOp2DoubleOp(operand1);
            if(mChanList.contains(operand2))
            {
                operand2ArrayPtr = new double[mTableRows];
                getChanAllVals(operand2, operand2ArrayPtr);


                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        operand2ArrayPtr[i] = -operand2ArrayPtr[i];
                    }
                }

                chanArithmetic(false, &op1, theOperator, true, operand2ArrayPtr, outputData);


            }
            else if(tmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            tmpChanData[dataIndex][i] = -tmpChanData[dataIndex][i];
                        }
                    }
                    chanArithmetic(false, &op1, theOperator, true, tmpChanData[dataIndex], outputData);
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *tmpChanData[dataIndex] = -*tmpChanData[dataIndex];
                    }

                    chanArithmetic(false, &op1, theOperator, false, tmpChanData[dataIndex], outputData);
                }
            }
            else if(mTmpChanNames.contains(operand2))
            {
                int dataIndex = operand2[1].digitValue();

                if(!isOperandNumber(operand2))
                {
                    if(isOp2NotNumNeg)
                    {
                        for(int i = 0; i < mTableRows; i++)
                        {
                            mTmpChanData[dataIndex][i] = -mTmpChanData[dataIndex][i];
                        }
                    }
                    chanArithmetic(false, &op1, theOperator, true, mTmpChanData[dataIndex], outputData);
                }
                else
                {
                    if(isOp2NotNumNeg)
                    {
                        *mTmpChanData[dataIndex] = -*mTmpChanData[dataIndex];
                    }
                    chanArithmetic(false, &op1, theOperator, false, mTmpChanData[dataIndex], outputData);
                }
            }
            else if(mChan2Data.contains(operand2))
            {
                if(isOp2NotNumNeg)
                {
                    for(int i = 0; i < mTableRows; i++)
                    {
                        mChan2Data[operand2][i] = -mChan2Data[operand2][i];
                    }
                }
                chanArithmetic(false, &op1, theOperator, true, mChan2Data[operand2], outputData);
            }
            else
            {
                double op2 = strOp2DoubleOp(operand2);
                unitArithmetic(op1, theOperator, op2, outputData);
            }
        }
    }

    if (operand1ArrayPtr)
        delete []operand1ArrayPtr;
    if (operand2ArrayPtr)
        delete []operand2ArrayPtr;

    return true;
}

void ChanCalDataHandling::unitArithmetic(double operand1, QString theOperator, double operand2, double *outPut)
{
    /*
    while(subExpression.contains("sin") || subExpression.contains("cos") ||
          subExpression.contains("tan") || subExpression.contains("sqrt") ||
          subExpression.contains("lg") || subExpression.contains("ln") ||
          subExpression.contains("abs") || subExpression.contains("^"))
    */

    //    double operand1Num;
    //    double operand2Num;

    //    if(operand1 == "pi")  {operand1Num = M_PI;}
    //    if(operand1 == "e")   {operand1Num = M_E;}
    //    if(operand2 == "pi")  {operand2Num = M_PI;}
    //    if(operand2 == "e")   {operand2Num = M_E;}

    //    operand1Num = operand1.toDouble();
    //    operand2Num = operand2.toDouble();

    if (isnan(operand1) || isnan(operand2)) {
        *outPut = NAN;
    } else if (theOperator == "sin") {
        *outPut = std::sin(operand2);
    } else if (theOperator == "cos") {
        *outPut = std::cos(operand2);
    } else if (theOperator == "tan") {
        if (operand2 == M_PI_2) {
            *outPut = nullValue;
        } else {
            *outPut = std::tan(operand2);
        }
    } else if (theOperator == "sqrt") {
        if (operand2 < 0) {
            *outPut = nullValue;
        } else {
            *outPut = std::sqrt(operand2);
        }
    } else if (theOperator == "log") {
        if (operand2 < 0) {
            *outPut = nullValue;
        } else {
            *outPut = std::log10(operand2);
        }
    } else if (theOperator == "ln") {
        if (operand2 < 0) {
            *outPut = nullValue;
        } else {
            *outPut = std::log(operand2);
        }
    } else if (theOperator == "abs") {
        *outPut = std::abs(operand2);
    } else if (theOperator == "^") {
        *outPut = std::pow(operand1, operand2);
    } else if (theOperator == "+") {
        *outPut = operand1 + operand2;
    } else if (theOperator == "-") {
        *outPut = operand1 - operand2;
    } else if (theOperator == "*") {
        *outPut = operand1 * operand2;
    } else if (theOperator == "/") {
        if (operand2 == 0) {
            *outPut = nullValue;
        } else {
            *outPut = operand1 / operand2;
        }
    } else if (theOperator == "%") {
        if (operand2 == 0) {
            *outPut = nullValue;
        } else {
            *outPut = (int)operand1 % (int)operand2;
        }
    }
}

void ChanCalDataHandling::chanArithmetic(bool op1Chan, double *op1Input, QString theOperator, bool op2Chan, double *op2Input, double *outPut)
{
    int index = 0;

    if (op1Input == nullptr) {
        if (op2Chan) {
            while (index < mTableRows) {
                unitArithmetic(0, theOperator, op2Input[index], &outPut[index]);
                index++;
            }
        } else {
            unitArithmetic(0, theOperator, *op2Input, &outPut[index]);
        }
    } else {
        if (op2Chan && op1Chan) {
            while (index < mTableRows) {
                unitArithmetic(op1Input[index], theOperator, op2Input[index], &outPut[index]);
                index++;
            }
        } else if (!op2Chan && op1Chan) {
            while (index < mTableRows) {
                unitArithmetic(op1Input[index], theOperator, *op2Input, &outPut[index]);
                index++;
            }
        } else if (op2Chan && !op1Chan) {
            while (index < mTableRows) {
                unitArithmetic(*op1Input, theOperator, op2Input[index], &outPut[index]);
                index++;
            }
        } else if (!op2Chan && !op1Chan) {
            unitArithmetic(*op1Input, theOperator, *op2Input, outPut);
        }
    }
}

void ChanCalDataHandling::getChanAllVals(QString chanName, double *outPuts)
{
    if(chanName.contains("$"))
        chanName.remove("$");

    QString query="SELECT " +chanName +" FROM "+mTableName;
    int index = 0;

    if (mSql.exec(query)) {
        while (mSql.next()) {
            if (mSql.value(0).isNull()) {
                outPuts[index] = NAN;
            } else {
                outPuts[index] = mSql.value(0).toDouble();
            }
            index++;
        }
    }
}

double ChanCalDataHandling::strOp2DoubleOp(QString operand)
{
    if (operand == "pi")
        return M_PI;
    if (operand == "e") {
        return M_E;
    }
    return operand.toDouble();
}

bool ChanCalDataHandling::opNotNumNeg(QString operand)
{
    if (operand.isEmpty())
        return false;

    bool ok = false;
    operand.toDouble(&ok);

    if (ok) {
        return false;
    } else {
        if (operand[0] == "-")
            return true;
    }
    return false;
}

void ChanCalDataHandling::createTempTable()
{
    QStringList typeList;
    typeList << newChanName+" FLOAT,";

    DataBase::getInstance()->createTable("TMP", typeList);
    DataBase::getInstance()->startTransaction();

    for (int i = 0; i < mTableRows; i++) {
        if (!isnan(mChan2Data[mChan2Data.firstKey()][i])) {
            QString rec = QString::number(mChan2Data[mChan2Data.firstKey()][i], 'f', 4);
            DataBase::getInstance()->insertRecord(rec,"TMP");
        } else {
            QString blnk = "NAN";
            DataBase::getInstance()->insertRecord(blnk,"TMP");
        }
    }
    DataBase::getInstance()->closeTransaction();
}

bool ChanCalDataHandling::reWriteFormula(QString &exp)
{
    QString newChanName;
    newChanName = exp.mid(0, exp.indexOf("="));
    if (!newChanName.startsWith("$") && !newChanName.at(0).isUpper()) {
        return false;
    } else if (newChanName.startsWith("$")) {
        newChanName.remove(0,1);
        variableList << newChanName;
        exp.remove(0,1);
    }

    QString rest = exp.mid(exp.indexOf("=") + 1);
    for (int i = 0; i < variableList.count(); i++) {
        if (rest.contains(variableList[i])) {
            if (!rest.contains("$")) {
                return false;
            } else {
                QString subStr("$");
                QString newStr("");
                exp.replace(exp.indexOf(subStr), subStr.size(), newStr);
            }
        }
    }

    QMap<QString, QString>::iterator it2;

    for (it2 = mChannelMap.begin(); it2 != mChannelMap.end(); ++it2) {
        if (exp.contains(it2.key()))
            exp.replace(it2.key(), it2.value());
    }
    return true;
}

void ChanCalDataHandling::startProcessing()
{
//    DEBUG_TRACE("Start Processing time: " << QDateTime::currentDateTime());
    bool expError = false;
    if (configureDB()) {
        if (checkStringChan()) {
            for (QString exp: mExpList) {
                if (!reWriteFormula(exp)) {
                    expError = true;
                    emit statusChanged("Expression Error!");
                    break;
                }
                if (isFourDiff(exp)) {
                    //run this func

                    return;
                }
                if (!expValidateCalulate(exp)) {
                    expError = true;
                    emit statusChanged("Expression Error!");
                    break;
                }
            }
            if (!expError)
                appendColumn();
        } else {
            emit statusChanged("Formula has string channels!");
        }
    } else {
        emit statusChanged("DataBase Error!");
    }
    emit calculateDone();
//    DEBUG_TRACE("End Processing time: " << QDateTime::currentDateTime());
}
