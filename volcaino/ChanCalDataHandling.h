/**
 *  @file   ChanCalDataHandling.h
 *  @brief  Handles channel calculator back-end calculations
 *  @author Shuo Li, Rami Yassine
 *  @date   Nov 8, ‎2022
 *
 *  Example: If we want to calculate C0=(C1+(C2*(C3+C4+C5)))+3*4, where C0~C5 are all DB channel names
 *  Decode and calculate process:
 *  C0=(C1+(C2*(t0a+C5)))  //Calculate C3+C4, mark it as t0a (small t means temp operands when calculating within the bracket,
 *                         // 0 is the index of the temp operand, a means array, n means single value)
 *  C0=(C1+(C2*(t1a)))  //Calculate t0a+C5, mark as t1a
 *  C0=(C1+(C2*T0A))  //The result within the bracket is marked as T0A (Captial T means the reuslt from a pair of brackets,
 *                    //or if the calculation is not within brackets, 0 is the index of the temp operand, A means array, N means single value)
 *  C0=(C1+(t2a))
 *  C0=(C1+T1A)
 *  C0=(t3a)
 *  C0=T2A+T3N        // (T3N means a single value)
 ***********************************************/
#ifndef CHANCALDATAHANDLING_H
#define CHANCALDATAHANDLING_H
#include <QObject>
#include <QDebug>
#include <QString>
#include <QSet>
#include <QStringList>
#include <QSqlQuery>
#include "DataBase.h"
#include <math.h>
#include <cmath>
#include "TypeDefConst.h"
#include <QStringList>
#include <QSqlError>
#include <QHash>
#include <QMap>
#include "TableViewOperation.h"

class ChanCalDataHandling: public QObject
{
    Q_OBJECT
public:
    ///default constructor
    ChanCalDataHandling(QString expression, QString tableName, QMap<QString, QString> chanMap);

    ///Class destructor, deletes the temp table, deletes intermediate values
    ~ChanCalDataHandling();
private:
    QString mExpression;
    QStringList mExpList;

    /// @brief This function configures data base
    /// @return Whether data base is successfully configured
    bool configureDB();
    /// @brief This function checks whether there's string data type channel
    /// @return Whether there's string data type channel
    bool checkStringChan();
    /// @brief This function validates whether a line of formula is valid, if yes, continues to calculate
    /// @param expression: One line of formula
    /// @return Whether there's error in the formula
    bool expValidateCalulate(QString expression);
    /// @brief This function appends the result to data base
    void appendColumn();
    /// @brief This function is the entry to calculate the minimal part within a pair of brackets
    /// @param expression: One line of formula
    /// @param start: Starting index of the minimal part
    /// @param end: Ending index of the minimal part
    /// @param hasBracket: Whether there are remaining brackets in the line of formula
    /// @return Whether there's error in the formula
    bool calculate(QString &expression, int start, int end, bool hasBracket);
    bool isFourDiff(QString exp);
    void calcDiffValues(std::vector<double> vals, QString newChan, int deg);

    QSet<QString> chanNames;

    std::vector<double *> mTmpChanData;   //result from a bracket

    QMap<QString, double*> mChan2Data;   //result of a line
    QMap<QString, double*>::iterator index;

    int mTmpIndex = 0;
    int mTableRows;
    QStringList mTmpChanNames;
    QStringList mChanList;
    QStringList variableList;

    QSqlQuery mSql;

    QString mTableName;

    QString newChanName;

    QMap<QString, QString> mChannelMap;

    /// @brief This function checks whether an operand (channel name or number) is valid
    /// @param input: Input operand
    /// @return Whether the operand is valid
    bool chanOrNumValidate(QString input);
    /// @brief This function checks whether an operand (Number) is a number
    /// @param input: Input operand
    /// @return Whether the number is a number
    bool isOperandNumber(QString input);

    bool isDoublePtrArray(double *input);

    /// @brief This function is the entry to calculate after we decode and get operand1&2, operator & intermediate values
    /// @param operand1: Operand 1
    /// @param theOperator: Operator
    /// @param operand2: Operand 2
    /// @param tmpChanNames: A list of t0a, t0n, T1A...
    /// @param tmpChanData: A vector of temp data corresponding to t0a, t0n, T1A...
    /// @return Whether there's error
    bool arithmetics(QString operand1, QString theOperator, QString operand2,
                    QStringList tmpChanNames, double *outputData, std::vector<double*>(tmpChanData));

    /// @brief This function calculates a single result, for example, when calculating C1*C2, this function calculates one value of C1 * one value of C2
    /// @param operand1: Operand 1
    /// @param theOperator: Operator
    /// @param operand2: Operand 2
    /// @param outPut: Result
    void unitArithmetic(double operand1, QString theOperator, double operand2, double *outPut);

    /// @brief This function calculates after getting op1 & 2 data, and does unit calculations
    /// @param op1Chan: Whether op1 is a channel
    /// @param op1Input: op1 data input
    /// @param op1Chan: Whether op2 is a channel
    /// @param op1Input: op2 data input
    /// @param outPut: Output
    void chanArithmetic(bool op1Chan, double *op1Input, QString theOperator, bool op2Chan, double *op2Input, double *outPut);
    /// @brief This function gets all data from a chnnal
    /// @param chanName: Channel name
    /// @param outPuts: Channel data
    void getChanAllVals(QString chanName, double *outPuts);
    /// @brief This function returns op2 value from the string
    /// @param operand: op2
    /// @return op2 value
    double strOp2DoubleOp(QString operand);
    /// @brief This function checks whether an operand is a negative channel
    /// @param operand: Operand
    /// @return If the operand is negative channel
    bool opNotNumNeg(QString operand);
    /// @brief This function creates a temp table to store the calculated result
    void createTempTable();
    /// @brief This function checks and rewrites a line of input formulas.
    /// @param exp: a line of input formulas
    /// @return Whether this line is valid
    bool reWriteFormula(QString &exp);
public slots:
    /// @brief This is the entry to start processing
    void startProcessing();
signals:
    void calculateDone();
    void statusChanged(QString status);
//    void columnAppended();
//    void getColumnVals(QString colName);
};

#endif // CHANCALDATAHANDLING_H
