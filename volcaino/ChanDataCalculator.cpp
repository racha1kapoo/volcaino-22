#include "ChanDataCalculator.h"

ChanDataCalculator::ChanDataCalculator(QWidget *thisParent, QString table)
    : QDialog(thisParent), mTable(table)
{
    setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);

    this->setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));

    this->resize(QGuiApplication::primaryScreen()->geometry().width()/3,
                 QGuiApplication::primaryScreen()->geometry().height()/2);

    this->setWindowTitle("Channel Calculator - ["+table+"]");

    //Status Bar initilization
    QVBoxLayout *vLayoutStat = new QVBoxLayout;
    mStatBar = new QStatusBar;
    vLayoutStat->addWidget(mStatBar);
    mStatBar->showMessage("");
    vLayoutStat->setMargin(0);
    vLayoutStat->setSpacing(0);

    //Opening Database connection
    if (!DataBase::getInstance()->openDatabase())
        DEBUG_TRACE(ERROR_DATABASE_FAILURE);

    //Layout for whole window
    QVBoxLayout* vLayoutWhole = new QVBoxLayout;

    ///////////////////////Top Layout Elements/////////////////////////

    //Input Validation for Expressions
    QRegExp re("(\\$*[^\\s\\W]+[^\\s\\w]+)+");
    mVald = new QRegExpValidator(re, this);

    QPushButton *btnCalc = new QPushButton(tr("Calculate"));
    connect(btnCalc, &QPushButton::clicked, this, &ChanDataCalculator::startCalculate);

    mEdtExpr = new QLineEdit;
    mEdtExpr->setClearButtonEnabled(true);
    mEdtExpr->setValidator(mVald);

    //Expression section layout
    QHBoxLayout* hLayoutExpr = new QHBoxLayout;
    hLayoutExpr->addWidget(new QLabel(tr("Expression:")));
    hLayoutExpr->addSpacing(40);
    hLayoutExpr->addWidget(mEdtExpr);
    hLayoutExpr->addWidget(btnCalc);

    ///////////////////////Mid Layout Elements/////////////////////////

    mBtnExpand = new QPushButton(tr("Operators \u23F7"));
    QPushButton *btnChanVar = new QPushButton(tr("Insert Channel Variable"));

    //Mid Buttons Layout
    QHBoxLayout* hLayoutButtons = new QHBoxLayout;
    hLayoutButtons->addWidget(mBtnExpand, 0, Qt::AlignRight);
    hLayoutButtons->addWidget(btnChanVar, 0, Qt::AlignLeft);

    //GroupBox Static Elements
    QPushButton *btnPlus = new QPushButton("\u002B");
    btnPlus->setFixedWidth(50);
    QPushButton *btnMinus = new QPushButton("\u002D");
    btnMinus->setFixedWidth(50);
    QPushButton *btnMult = new QPushButton("\u002A");
    btnMult->setFixedWidth(50);
    QPushButton *btnDiv = new QPushButton("\u00F7");
    btnDiv->setFixedWidth(50);
    QPushButton *btnBrackOpen = new QPushButton("\u0028");
    btnBrackOpen->setFixedWidth(50);
    QPushButton *btnBrackClose = new QPushButton("\u0029");
    btnBrackClose->setFixedWidth(50);
    QPushButton *btnSqrt = new QPushButton("\u221A");
    btnSqrt->setFixedWidth(50);
    QPushButton *btnE = new QPushButton("\u212E");
    btnE->setFixedWidth(50);
    QPushButton *btnPi = new QPushButton("\u03C0");
    btnPi->setFixedWidth(50);
    QPushButton *btnMod = new QPushButton("\u0025");
    btnMod->setFixedWidth(50);
    QPushButton *btnPow = new QPushButton("\u0078\u02B8");
    btnPow->setFixedWidth(50);
    QPushButton *btnEq = new QPushButton("\u003D");
    btnEq->setFixedWidth(50);

    //Grid Layout for static elements
    QGridLayout* gLayoutKeys = new QGridLayout;
    gLayoutKeys->addWidget(btnPlus, 0, 0);
    gLayoutKeys->addWidget(btnBrackOpen, 0, 1);
    gLayoutKeys->addWidget(btnPow, 0, 2);
    gLayoutKeys->addWidget(btnMinus, 1, 0);
    gLayoutKeys->addWidget(btnBrackClose, 1, 1);
    gLayoutKeys->addWidget(btnE, 1, 2);
    gLayoutKeys->addWidget(btnMult, 2, 0);
    gLayoutKeys->addWidget(btnSqrt, 2, 1);
    gLayoutKeys->addWidget(btnPi, 2, 2);
    gLayoutKeys->addWidget(btnDiv, 3, 0);
    gLayoutKeys->addWidget(btnMod, 3, 1);
    gLayoutKeys->addWidget(btnEq, 3, 2);
    gLayoutKeys->setAlignment(Qt::AlignCenter);

    //init buttons for Maths tab
    QPushButton *btnLogE = new QPushButton("log\u2091");
    QPushButton *btnLog10 = new QPushButton("log\u2081\u2080");
    QPushButton *btnExpE = new QPushButton("\u212E\u02E3");
    QPushButton *btnExp10 = new QPushButton("10\u02E3");

    //layout for Maths Operations tab
    QGridLayout* gLayoutMath = new QGridLayout;
    gLayoutMath->addWidget(btnLog10, 0, 0);
    gLayoutMath->addWidget(btnLogE, 0, 1);
    gLayoutMath->addWidget(btnExpE, 1, 0);
    gLayoutMath->addWidget(btnExp10, 1, 1);
    gLayoutMath->setAlignment(Qt::AlignTop);

    //init buttons for Trigonometric Ops Tab
    QPushButton *btnCos = new QPushButton("cos");
    QPushButton *btnSin = new QPushButton("sin");
    QPushButton *btnTan = new QPushButton("tan");
    QPushButton *btnASin = new QPushButton("asin");
    QPushButton *btnACos = new QPushButton("acos");
    QPushButton *btnATan = new QPushButton("atan");
    QPushButton *btnATan2 = new QPushButton("atan2");
    QPushButton *btnSinH = new QPushButton("sinh");
    QPushButton *btnCosH = new QPushButton("cosh");
    QPushButton *btnTanH = new QPushButton("tanh");

    //Layout for Trigonometric Operations tab
    QGridLayout* gLayoutTrig = new QGridLayout;
    gLayoutTrig->addWidget(btnSin, 0, 0);
    gLayoutTrig->addWidget(btnCos, 0, 1);
    gLayoutTrig->addWidget(btnTan, 0, 2);
    gLayoutTrig->addWidget(btnASin, 1, 0);
    gLayoutTrig->addWidget(btnACos, 1, 1);
    gLayoutTrig->addWidget(btnATan, 1, 2);
    gLayoutTrig->addWidget(btnATan2, 1, 3);
    gLayoutTrig->addWidget(btnSinH, 2, 0);
    gLayoutTrig->addWidget(btnCosH, 2, 1);
    gLayoutTrig->addWidget(btnTanH, 2, 2);

    //Buttons for Logical Ops init
    QPushButton *btnBigger = new QPushButton(">");
    QPushButton *btnSmaller = new QPushButton("<");
    QPushButton *btnBiggEq = new QPushButton(">=");
    QPushButton *btnSmallEq = new QPushButton("<=");
    QPushButton *btnNot = new QPushButton("!");
    QPushButton *btnEqEq = new QPushButton("==");
    QPushButton *btnNotEq = new QPushButton("!=");
    QPushButton *btnAnd = new QPushButton("&&");
    QPushButton *btnOr = new QPushButton("||");
    QPushButton *btnDummy = new QPushButton("DUMMY");

    //Layout for Logical Ops tab
    QGridLayout* gLayoutLogic = new QGridLayout;
    gLayoutLogic->addWidget(btnBigger, 0, 0);
    gLayoutLogic->addWidget(btnSmaller, 0, 1);
    gLayoutLogic->addWidget(btnBiggEq, 0, 2);
    gLayoutLogic->addWidget(btnSmallEq, 0, 3);
    gLayoutLogic->addWidget(btnNot, 1, 0);
    gLayoutLogic->addWidget(btnEqEq, 1, 1);
    gLayoutLogic->addWidget(btnNotEq, 1, 2);
    gLayoutLogic->addWidget(btnAnd, 2, 0, 1, 2);
    gLayoutLogic->addWidget(btnOr, 2, 2, 1, 2);
    gLayoutLogic->addWidget(btnDummy, 3, 0, 1, 4);

    //Buttons for Special tab init
    QPushButton *btnSMin = new QPushButton("min");
    QPushButton *btnSMax = new QPushButton("max");
    QPushButton *btnSDiv = new QPushButton("div");
    QPushButton *btnSign = new QPushButton("sign");
    QPushButton *btnCeil = new QPushButton("ceil");
    QPushButton *btnFloor = new QPushButton("floor");
    QPushButton *btnAbs = new QPushButton("abs");
    QPushButton *btnRound = new QPushButton("round");
    QPushButton *btnWindow = new QPushButton("window");
    QPushButton *btnClip = new QPushButton("clip");
    QPushButton *btnRandBtwn = new QPushButton("roundbetween");

    //Layout for Special Ops tab
    QGridLayout* gLayoutSpecial = new QGridLayout;
    gLayoutSpecial->addWidget(btnSMin, 0, 0);
    gLayoutSpecial->addWidget(btnSMax, 0, 1);
    gLayoutSpecial->addWidget(btnSDiv, 0, 2);
    gLayoutSpecial->addWidget(btnSign, 0, 3);
    gLayoutSpecial->addWidget(btnCeil, 1, 0);
    gLayoutSpecial->addWidget(btnFloor, 1, 1);
    gLayoutSpecial->addWidget(btnAbs, 1, 2);
    gLayoutSpecial->addWidget(btnRound, 1, 3);
    gLayoutSpecial->addWidget(btnWindow, 2, 0);
    gLayoutSpecial->addWidget(btnClip, 2, 1);
    gLayoutSpecial->addWidget(btnRandBtwn, 2, 2);

    //Buttons for Statistics tab init
    QPushButton *btnMin = new QPushButton("min");
    QPushButton *btnMax = new QPushButton("max");
    QPushButton *btnRange = new QPushButton("range");
    QPushButton *btnSum = new QPushButton("sum");
    QPushButton *btnSum2 = new QPushButton("sum2");
    QPushButton *btnSum3 = new QPushButton("sum3");
    QPushButton *btnSum4 = new QPushButton("sum4");
    QPushButton *btnMean = new QPushButton("mean");
    QPushButton *btnMedian = new QPushButton("median");
    QPushButton *btnMode = new QPushButton("mode");
    QPushButton *btnStdDev = new QPushButton("stddev");
    QPushButton *btnVar = new QPushButton("var");
    QPushButton *btnCount = new QPushButton("count");
    QPushButton *btnStdErr = new QPushButton("stderr");
    QPushButton *btnCountZero = new QPushButton("countzero");
    QPushButton *btnCountDum = new QPushButton("countdum");
    QPushButton *btnGeoMean = new QPushButton("geomean");
    QPushButton *btnMinPos = new QPushButton("minpos");
    QPushButton *btnCountPos = new QPushButton("countpos");

    //Layout for Statistics tab
    QGridLayout* gLayoutStat = new QGridLayout;
    gLayoutStat->addWidget(btnMin, 0, 0);
    gLayoutStat->addWidget(btnMax, 0, 1);
    gLayoutStat->addWidget(btnRange, 0, 2);
    gLayoutStat->addWidget(btnMean, 0, 3);
    gLayoutStat->addWidget(btnSum, 1, 0);
    gLayoutStat->addWidget(btnSum2, 1, 1);
    gLayoutStat->addWidget(btnSum3, 1, 2);
    gLayoutStat->addWidget(btnSum4, 1, 3);
    gLayoutStat->addWidget(btnCount, 2, 0);
    gLayoutStat->addWidget(btnStdDev, 2, 1);
    gLayoutStat->addWidget(btnStdErr, 2, 2);
    gLayoutStat->addWidget(btnVar, 2, 3);
    gLayoutStat->addWidget(btnCountZero, 3, 0);
    gLayoutStat->addWidget(btnCountDum, 3, 1);
    gLayoutStat->addWidget(btnGeoMean, 3, 2);
    gLayoutStat->addWidget(btnMinPos, 3, 3);
    gLayoutStat->addWidget(btnCountPos, 4, 0);
    gLayoutStat->addWidget(btnMedian, 4, 1);
    gLayoutStat->addWidget(btnMode, 4, 2);

    //Button for Filters tab init
    QPushButton *btnFourDiff = new QPushButton("\u0034\u1D57\u02B0 Diff");
    QPushButton *btnThirdDiff = new QPushButton("3\u02B3\u1D48 Diff");
    QPushButton *btnSecDiff = new QPushButton("2\u207F\u1D48 Diff");
    QPushButton *btnFirstDiff = new QPushButton("1\u02E2\u1D57 Diff");

    //Layout for Filters tab
    QGridLayout* gLayoutFilter = new QGridLayout;
    gLayoutFilter->addWidget(btnFourDiff, 0, 0, Qt::AlignTop);
    gLayoutFilter->addWidget(btnThirdDiff, 0, 1, Qt::AlignTop);
    gLayoutFilter->addWidget(btnSecDiff, 0, 2, Qt::AlignTop);
    gLayoutFilter->addWidget(btnFirstDiff, 0, 3, Qt::AlignTop);

//    gLayoutFilter->setAlignment(Qt::AlignLeft);

    //Tab initialization
    QTabWidget* tabHidden = new QTabWidget;
    tabHidden->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    QWidget* tabMath = new QWidget;
    QWidget* tabLogic = new QWidget;
    QWidget* tabSpec = new QWidget;
    QWidget* tabTrig = new QWidget;
    QWidget* tabStat = new QWidget;
    QWidget* tabFilters = new QWidget;
    tabHidden->addTab(tabMath, tr("Math"));
    tabMath->setLayout(gLayoutMath);
    tabHidden->addTab(tabLogic, tr("Logical"));
    tabLogic->setLayout(gLayoutLogic);
    tabHidden->addTab(tabSpec, tr("Special"));
    tabSpec->setLayout(gLayoutSpecial);
    tabHidden->addTab(tabTrig, tr("Trig"));
    tabTrig->setLayout(gLayoutTrig);
    tabHidden->addTab(tabStat, tr("Statistics"));
    tabStat->setLayout(gLayoutStat);
    tabHidden->addTab(tabFilters, tr("Filters"));
    tabFilters->setLayout(gLayoutFilter);

    //Hidden Group Layout
    QHBoxLayout* hLayoutGroup = new QHBoxLayout;
    hLayoutGroup->addLayout(gLayoutKeys,0);
    hLayoutGroup->addWidget(tabHidden,1);

    //QGroupBox Init
    mHidden = new QGroupBox(tr("Operators and Functions"));
    mHidden->setLayout(hLayoutGroup);
    mHidden->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    mHidden->hide();

    //layout for Mid Section
    QVBoxLayout* vLayoutMid = new QVBoxLayout;
    vLayoutMid->addLayout(hLayoutButtons);
    vLayoutMid->addWidget(mHidden, 1, Qt::AlignHCenter);

    ///////////////////////Seperator Element/////////////////////////

    //Line Seperator init
    QFrame* line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);
    QFrame* line2 = new QFrame(this);
    line2->setFrameShape(QFrame::HLine);
    line2->setFrameShadow(QFrame::Sunken);

    //table widget init
    QStringList headers;
    headers << "" << "";
    mTableWdg = new QTableWidget(1, 2, this);
    mTableWdg->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    mTableWdg->setAlternatingRowColors(true);
    mTableWdg->setHorizontalHeaderLabels(headers);
    mTableWdg->resizeColumnToContents(0);

    hLayoutTable = new QHBoxLayout;
    hLayoutTable->addWidget(new QLabel(tr("Assign channels: ")), 0, Qt::AlignTop);
    hLayoutTable->addWidget(mTableWdg, 2, Qt::AlignLeft);

    ///////////////////////Bottom Layout Elements/////////////////////////

    //Bottom Buttons
    mLblFile = new QLabel;
    mLblPath = new QLabel;
    QPushButton *btnSave = new QPushButton(tr("Save"));
    QPushButton *btnLoad = new QPushButton(tr("Load"));

    //Bottom Buttons Layout
    QHBoxLayout* hLayoutBottom = new QHBoxLayout;
    hLayoutBottom->addWidget(mLblFile, 0);
    hLayoutBottom->addWidget(mLblPath, 1);
    hLayoutBottom->addWidget(btnSave, 0);
    hLayoutBottom->addWidget(btnLoad, 0);

    ///////////////////////Whole Layout/////////////////////////

    mTableBox = new QLabel(mTable, this);
    mTableBox->setStyleSheet("border: 1px groove gray; padding: 3px;");
    QLabel* lblTableBox = new QLabel(tr("Table name:"));

    QHBoxLayout* hLayoutTables = new QHBoxLayout;
    hLayoutTables->addWidget(lblTableBox,0);
    hLayoutTables->addSpacing(25);
    hLayoutTables->addWidget(mTableBox, 1, Qt::AlignLeft);

    //Whole Layout setting
    vLayoutWhole->addLayout(hLayoutExpr);
    vLayoutWhole->addLayout(vLayoutMid);
    vLayoutWhole->addWidget(line);
    vLayoutWhole->addLayout(hLayoutTables);
    vLayoutWhole->addLayout(hLayoutTable);
    vLayoutWhole->addWidget(line2);
    vLayoutWhole->addLayout(hLayoutBottom);
    vLayoutWhole->addLayout(vLayoutStat);
    setLayout(vLayoutWhole);

    ///////////////////////Connections/////////////////////////

    connect(this, &ChanDataCalculator::statusChanged, this, &ChanDataCalculator::updateStatus);
    connect(this, &ChanDataCalculator::channelAdded, this, &ChanDataCalculator::updateMap);
    connect(btnSave, &QPushButton::clicked, this, &ChanDataCalculator::saveExpression);
    connect(btnLoad, &QPushButton::clicked, this, &ChanDataCalculator::loadExpression);
    connect(mEdtExpr, &QLineEdit::textChanged, this, &ChanDataCalculator::addRow);
    connect(btnChanVar, &QPushButton::clicked, this, &ChanDataCalculator::addVar);
    connect(mBtnExpand, &QPushButton::clicked, this, &ChanDataCalculator::showHidden);
    connect(btnPlus, &QPushButton::clicked, this, &ChanDataCalculator::addPlus);
    connect(btnMinus, &QPushButton::clicked, this, &ChanDataCalculator::addMinus);
    connect(btnMult, &QPushButton::clicked, this, &ChanDataCalculator::addMult);
    connect(btnDiv, &QPushButton::clicked, this, &ChanDataCalculator::addDiv);
    connect(btnBrackOpen, &QPushButton::clicked, this, &ChanDataCalculator::addOpenBrack);
    connect(btnBrackClose, &QPushButton::clicked, this, &ChanDataCalculator::addCloseBrack);
    connect(btnMod, &QPushButton::clicked, this, &ChanDataCalculator::addPercent);
    connect(btnEq, &QPushButton::clicked, this, &ChanDataCalculator::addEq);
    connect(btnSqrt, &QPushButton::clicked, this, &ChanDataCalculator::addSqrt);
    connect(btnPow, &QPushButton::clicked, this, &ChanDataCalculator::addPower);
    connect(btnPi, &QPushButton::clicked, this, &ChanDataCalculator::addPi);
    connect(btnE, &QPushButton::clicked, this, &ChanDataCalculator::addE);
    connect(btnCos, &QPushButton::clicked, this, &ChanDataCalculator::addCos);
    connect(btnSin, &QPushButton::clicked, this, &ChanDataCalculator::addSin);
    connect(btnTan, &QPushButton::clicked, this, &ChanDataCalculator::addTan);
    connect(btnLog10, &QPushButton::clicked, this, &ChanDataCalculator::addLog);
    connect(btnExp10, &QPushButton::clicked, this, &ChanDataCalculator::addExp10);
    connect(btnExpE, &QPushButton::clicked, this, &ChanDataCalculator::addExpE);
    connect(btnFourDiff, &QPushButton::clicked, this, &ChanDataCalculator::fourthDiff);
    connect(btnThirdDiff, &QPushButton::clicked, this, &ChanDataCalculator::thirdDiff);
    connect(btnSecDiff, &QPushButton::clicked, this, &ChanDataCalculator::secondDiff);
    connect(btnFirstDiff, &QPushButton::clicked, this, &ChanDataCalculator::firstDiff);
}

void ChanDataCalculator::startCalculate()
{
    QString status = "Calculating...";
    emit statusChanged(status);
    //    if(chanCalData)
    //    {
    //        delete chanCalData;
    //        chanCalData = nullptr;
    //    }
    QString expression = mEdtExpr->text();
    QString tableName = mTable;

    chanCalData = new ChanCalDataHandling(expression, tableName, mChannelMap);
    QThread* thread = new QThread();
    connect(chanCalData, &ChanCalDataHandling::destroyed, this, &ChanDataCalculator::sendRef);
    chanCalData->moveToThread(thread);

    connect(thread, &QThread::started, chanCalData, &ChanCalDataHandling::startProcessing);
    connect(chanCalData, &ChanCalDataHandling::calculateDone, thread, &QThread::quit);
    connect(chanCalData, &ChanCalDataHandling::statusChanged, this, &ChanDataCalculator::updateStatus);
//    connect(chanCalData, &ChanCalDataHandling::columnAppended, this, &ChanDataCalculator::sendRef);
    connect(thread, &QThread::finished, chanCalData, &ChanCalDataHandling::deleteLater);
    connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    thread->start();
}

void ChanDataCalculator::updateStatus(QString status)
{
    mStatBar->showMessage(status);
}

void ChanDataCalculator::setFilePath()
{
    QString theCalFilePath = QFileDialog::getSaveFileName(this,tr("Save calculation file"), QDir().dirName().append("../untitled.txt")
                                                          ,tr("*.txt"));
    QString calFilePathTrimmed;

    if (theCalFilePath.isEmpty() && calFilePath.isEmpty()) {
        QString status = "Please specify output file";
        emit statusChanged(status);
    } else if (!theCalFilePath.isEmpty()) {
        mCalFile.setFileName(theCalFilePath);
        calFilePath = theCalFilePath;
        calFilePathTrimmed = theCalFilePath;

        if (theCalFilePath.length() > 100) {
            calFilePathTrimmed = calFilePathTrimmed.right(60);
            calFilePathTrimmed = calFilePathTrimmed.right(60-calFilePathTrimmed.indexOf("/"));
            calFilePathTrimmed = "..." + calFilePathTrimmed;
        }
        QString status = "Output calculation file: " + calFilePathTrimmed;
        emit statusChanged(status);
    } else if (!calFilePath.isEmpty() && theCalFilePath.isEmpty()) {
        mCalFile.setFileName(calFilePath);
        calFilePathTrimmed = calFilePath;
        if (calFilePathTrimmed.length() > 100) {
            calFilePathTrimmed = calFilePathTrimmed.right(60);
            calFilePathTrimmed = calFilePathTrimmed.right(60-calFilePathTrimmed.indexOf("/"));
            calFilePathTrimmed = "..." + calFilePathTrimmed;
        }
        QString status = "Output calculation file: " + calFilePathTrimmed;
        emit statusChanged(status);
    }
}

void ChanDataCalculator::saveExpression()
{
    QString exp = mEdtExpr->text();

    QString theCalFilePath = QFileDialog::getSaveFileName(this,tr("Save calculation file"), QDir().dirName().append("../untitled.txt"), tr("*.txt"));
    QString calFilePathTrimmed;
    QStringList lstPath = theCalFilePath.split("/");

    if (!theCalFilePath.isEmpty()) {
        mCalFile.setFileName(theCalFilePath);
        calFilePath = theCalFilePath;
        calFilePathTrimmed = theCalFilePath;

        if (theCalFilePath.length() > 100) {
            calFilePathTrimmed = calFilePathTrimmed.right(100);
            calFilePathTrimmed = calFilePathTrimmed.right(100-calFilePathTrimmed.indexOf("/"));
            calFilePathTrimmed = "..." + calFilePathTrimmed;
        }
        QString status = "Output calculation file: " + calFilePathTrimmed;
        emit statusChanged(status);

        QTextStream stream(&mCalFile);
        if (!exp.isEmpty()) {
            if (mCalFile.isOpen())
                mCalFile.close();

            if (mCalFile.open(QIODevice::WriteOnly)) {
                mCalFile.seek(0);
                stream << "///" << mTable << "\n";
                QMap<QString, QString>::iterator it2;
                for (it2 = mChannelMap.begin(); it2 != mChannelMap.end(); ++it2)
                    stream << "//" << it2.key() << ":" << it2.value() << "\n";
                stream << exp;

                QString outputPathTrimmed = calFilePath;
                if (outputPathTrimmed.length() > 50) {
                    outputPathTrimmed = outputPathTrimmed.right(50);
                    outputPathTrimmed = outputPathTrimmed.right(50-outputPathTrimmed.indexOf("/"));
                    outputPathTrimmed = "..." + outputPathTrimmed;
                }
                mLblPath->setText(lstPath.last());
                mLblFile->setText(tr("Saved file:"));
                QString status = "Calculation file generated, saved at: " + outputPathTrimmed;
                emit statusChanged(status);
                mCalFile.close();
            } else {
                QString status = "Invalid file path!";
                emit statusChanged(status);
            }
        } else {
            QString status = "Empty expression!";
            emit statusChanged(status);
        }
    }
}

void ChanDataCalculator::loadExpression()
{
    QString theCalFileLoadPath =
            QFileDialog::getOpenFileName(this,
                                         tr("Open a calculation file"), QDir().dirName().append("../")
                                         ,(tr("*.txt")));
    QStringList lstPath = theCalFileLoadPath.split("/");
    if (!theCalFileLoadPath.isEmpty()) {
        mCalFile.setFileName(theCalFileLoadPath);
        if (mCalFile.isOpen())
            mCalFile.close();
        if (mCalFile.open(QIODevice::ReadOnly)) {
            mChannelMapTmp.clear();
            while (!mCalFile.atEnd()) {
                QString line = mCalFile.readLine();
                if (line.at(0) == "/" && line.at(1) == "/" && line.at(2) == "/") {
                    line = line.remove("///").simplified();
                    if (DataBase::getInstance()->getTableList().contains(line))
                        mTableBox->setText(line);
                } else if (line.at(0) == "/" && line.at(1) == "/") {
                    line = line.remove("//").simplified();
                    QStringList keyVal = line.split(":");

                    mChannelMapTmp.insert(keyVal[0], keyVal[1]);
                } else {
                    mEdtExpr->setText(line);
                }
            }

            QMap<QString, QString>::iterator it2;
            for (it2 = mChannelMapTmp.begin(); it2 != mChannelMapTmp.end(); ++it2) {
                if (!mTableBox->text().isEmpty()) {
                    //if(DataBase::getInstance()->getHeaderDetails(mTableBox->currentText()).contains(it2.value()))
                    {
                         int rowIndex = mTmpChanNames.indexOf(it2.key());
                         if (rowIndex >= 0)
                             mLstBoxes.at(rowIndex)->setCurrentText(it2.value());
                    }
                }
            }
            mLblPath->setText(lstPath.last());
            mLblFile->setText(tr("Loaded file:"));
            QString status = "Expression loaded from calculation file";
            emit statusChanged(status);
            mCalFile.close();
        } else {
            QString status = "Cannot open calculation file";
            emit statusChanged(status);
        }
    }
}

void ChanDataCalculator::updateMap()
{
    mChannelMap.clear();
    if (!mTmpChanNames.isEmpty() && !mLstBoxes.isEmpty()) {
        for (int i = 0; i < mTmpChanNames.count(); i++)
            mChannelMap.insert(mTmpChanNames[i], mLstBoxes.at(i)->currentText());
        DEBUG_TRACE("Map Keys: " << mChannelMap.keys() << " Map Values: " << mChannelMap.values());
    }
    if (!mTmpVars.isEmpty()) {
        for (int i = 0; i < mTmpVars.count(); i++)
            mChannelMap.insert(mTmpVars[i], mTmpVars[i]);
        DEBUG_TRACE("Tmp Keys: " << mChannelMap.keys() << " Tmp Values: " << mChannelMap.values());
    }
    DEBUG_TRACE("Number of pointers: " << mLstBoxes.count());
}

void ChanDataCalculator::addRow(const QString &str)
{
    QRegularExpression re("(?<!\\!)[A-Z]+[a-z]*\\d*"); //restriction is that it has to start with capital letters
    QRegularExpressionMatchIterator i = re.globalMatch(str);
    QRegularExpression re2("(?<=\\!)[a-z]+\\d*"); //restriction is that it has to start with capital letters
    QRegularExpressionMatchIterator i2 = re2.globalMatch(str);
    QStringList oldChanList = mTmpChanNames;
    mTmpChanNames.clear();
    mTmpVars.clear();
    while (i.hasNext()) {
        QRegularExpressionMatch match = i.next();
        QString chan = match.captured();
        mTmpChanNames << chan;
    }
    while (i2.hasNext()) {
        QRegularExpressionMatch match = i2.next();
        QString chan = match.captured();
        mTmpVars << chan;
    }

    mTmpVars.removeDuplicates();
    mTmpChanNames.removeDuplicates();
    DEBUG_TRACE("Old List Chan: " << oldChanList);
    DEBUG_TRACE("New List Chan: " << mTmpChanNames);
    DEBUG_TRACE("Tmp List: " << mTmpVars);

    if (oldChanList == mTmpChanNames) {
        DEBUG_TRACE("No change to channels.");
        return;
    } else if (oldChanList.count() == mTmpChanNames.count()) {
        for (int i = 0; i < oldChanList.count(); i++) {
            if (oldChanList[i] != mTmpChanNames[i]) {
                mTableWdg->setCellWidget(i,0,new QLabel(mTmpChanNames[i]));
                emit channelAdded();
                DEBUG_TRACE("Changed qlabel text");
                return;
            }
        }
    } else if (oldChanList.count() > mTmpChanNames.count()) {
        mTableWdg->setRowCount(mTmpChanNames.count());
        if (mTmpChanNames.isEmpty()) {
            for (int i = 0; i < mLstBoxes.count(); i++)
                delete mLstBoxes[i];
            mLstBoxes.clear();
        } else {
            for (int i = 0; i < mTmpChanNames.count(); i++) {
                if (oldChanList[i] != mTmpChanNames[i])
                    mTableWdg->setCellWidget(i, 0, new QLabel(mTmpChanNames[i]));
            }
            for (int i = 0; i < oldChanList.count() - mTmpChanNames.count(); i++) {
                delete mLstBoxes.last();
                mLstBoxes.pop_back();
            }
        }
        emit channelAdded();
        DEBUG_TRACE("Removed labels and boxes");
        return;
    } else {
        for (int i = 0; i < oldChanList.count(); i++) {
            if (oldChanList[i] != mTmpChanNames[i])
                mTableWdg->setCellWidget(i,0,new QLabel(mTmpChanNames[i]));
        }
        mTableWdg->setRowCount(mTmpChanNames.count());
        for (int i = oldChanList.count(); i < mTmpChanNames.count(); i++) {
            QComboBox* tmp = new QComboBox(mTableWdg);
            tmp->setLineEdit(new QLineEdit(tmp));

            tmp->addItems(DataBase::getInstance()->getHeaderDetails(mTable));

            mTableWdg->setItem(i,0,new QTableWidgetItem);
            mTableWdg->setItem(i,1,new QTableWidgetItem);
            mTableWdg->setCellWidget(i,0,new QLabel(mTmpChanNames.at(i)));
            mTableWdg->setCellWidget(i,1,tmp);
            mLstBoxes << tmp;
            connect(tmp, &QComboBox::currentTextChanged, this, &ChanDataCalculator::updateMap);
        }
        emit channelAdded();
        DEBUG_TRACE("Added new Channels");
    }
}

void ChanDataCalculator::addVar()
{
    if (mTmpChanNames.isEmpty()) {
        QString var = "C0";
        mEdtExpr->insert(var);
        return;
    } else {
        int tmp = 0;
        QRegularExpression re("(C)(?<num>\\d+)");
        for (int i = 0; i < mTmpChanNames.count(); i++) {
            QRegularExpressionMatch match = re.match(mTmpChanNames.at(i));
            if (match.hasMatch()) {
                QString num = match.captured("num");
                int varNum = num.toInt();
                if (tmp <= varNum)
                    tmp = varNum;
            }
        }
        tmp++;
        QString var = "C" + QString::number(tmp);
        mEdtExpr->insert(var);
    }
}

void ChanDataCalculator::showHidden()
{
    if (mHidden->isHidden()) {
        mHidden->show();
        mBtnExpand->setText(tr("Operators \u23F6"));
    } else {
        mHidden->hide();
        mBtnExpand->setText(tr("Operators \u23F7"));
    }
}

void ChanDataCalculator::addExp10()
{
    mEdtExpr->insert("10^");
}

void ChanDataCalculator::addExpE()
{
    mEdtExpr->insert("e^");
}

void ChanDataCalculator::sendRef()
{
    emit calcDone();
}

void ChanDataCalculator::fourthDiff()
{
    mEdtExpr->insert("4diff()");
}

void ChanDataCalculator::thirdDiff()
{
    mEdtExpr->insert("3diff()");
}

void ChanDataCalculator::secondDiff()
{
    mEdtExpr->insert("2diff()");
}

void ChanDataCalculator::firstDiff()
{
    mEdtExpr->insert("1diff()");
}

void ChanDataCalculator::addPlus()
{
    mEdtExpr->insert("+");
}

void ChanDataCalculator::addMinus()
{
    mEdtExpr->insert("-");
}

void ChanDataCalculator::addMult()
{
    mEdtExpr->insert("*");
}

void ChanDataCalculator::addDiv()
{
    mEdtExpr->insert("/");
}

void ChanDataCalculator::addOpenBrack()
{
    mEdtExpr->insert("(");
}

void ChanDataCalculator::addCloseBrack()
{
    mEdtExpr->insert(")");
}

void ChanDataCalculator::addPercent()
{
    mEdtExpr->insert("%");
}

void ChanDataCalculator::addEq()
{
    mEdtExpr->insert("=");
}

void ChanDataCalculator::addPi()
{
    mEdtExpr->insert("pi");
}

void ChanDataCalculator::addE()
{
    mEdtExpr->insert("e");
}

void ChanDataCalculator::addSqrt()
{
    mEdtExpr->insert("sqrt()");
}

void ChanDataCalculator::addPower()
{
    mEdtExpr->insert("^");
}

void ChanDataCalculator::addSin()
{
    mEdtExpr->insert("sin()");
}

void ChanDataCalculator::addCos()
{
    mEdtExpr->insert("cos()");
}

void ChanDataCalculator::addTan()
{
    mEdtExpr->insert("tan()");
}

void ChanDataCalculator::addLog()
{
    mEdtExpr->insert("log");
}

//void ChanDataCalculator::calculateThreadDone(int status)
//{
//    if(status == 0)
//    {
//        expStatus->setText("Channel calculation done");
//    }
//    else if(status == 1)
//    {
//        expStatus->setText("Expression error");
//    }
//    else if(status == 2)
//    {
//        expStatus->setText("Database error");
//    }
//}
