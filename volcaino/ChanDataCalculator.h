#ifndef CHANDATACALCULATOR_H
#define CHANDATACALCULATOR_H

#include <QDialog>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QFrame>
#include <QPushButton>
#include <QLineEdit>
#include <QGroupBox>
#include <QTabWidget>
#include <QTableWidget>
#include <QComboBox>
#include <QApplication>
#include <QScreen>
#include <QRect>
#include <QSizePolicy>
#include <QDebug>
#include <QRegExp>
#include <QThread>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMap>
#include "ChanCalDataHandling.h"
#include "DataBase.h"
#include "qstatusbar.h"
#include "TableViewOperation.h"

class ChanDataCalculator: public QDialog
{
        Q_OBJECT
public:
    explicit ChanDataCalculator(QWidget *thisParent, QString table);
signals:
    ///
    /// @brief channelAdded is signaled when a change to the list of channels happens (addition or removal)
    ///
    void channelAdded();
    ///
    /// @brief statusChanged is signaled when an event that the user has to be aware of happens
    /// @param status is the message we want to display
    ///
    void statusChanged(QString status);
    ///
    /// @brief calcDone signals that the calculation is done, and calculator object is destroyed
    ///
    void calcDone();
private slots:
    ///
    /// @brief startCalculate starts the calculation
    ///
    void startCalculate();
    ///
    /// @brief updateStatus sets the status of the application to the message emitted
    /// @param status is the message we want to display
    ///
    void updateStatus(QString status);
    ///
    /// @brief setFilePath sets the file path
    ///
    void setFilePath();
    ///
    /// @brief saveExpression saves the expression file
    ///
    void saveExpression();
    ///
    /// @brief loadExpression loads the expression from the saved file
    ///
    void loadExpression();
    ///
    /// @brief updateMap updates the map of channels and their variable names
    ///
    void updateMap();
    ///
    /// @brief addRow is triggered everytime an input is entered in the Expression Line Edit, to see if a new channel variable is entered
    /// @param str is the string inside the Expression line edit
    ///
    void addRow(const QString &str);
    ///
    /// @brief addVar adds a Channel variable in the form of (C0,C1,C2,...)
    ///
    void addVar();
    ///
    /// @brief showHidden shows the Operators tab
    ///
    void showHidden();
    ///
    /// @brief addPlus inserts a '+' in the Expression Line Edit
    ///
    void addPlus();
    ///
    /// @brief addMinus inserts a '-' in the Expression Line Edit
    ///
    void addMinus();
    ///
    /// @brief addMult inserts a '*' in the Expression Line Edit
    ///
    void addMult();
    ///
    /// @brief addDiv inserts a '/' in the Expression Line Edit
    ///
    void addDiv();
    ///
    /// @brief addOpenBrack inserts a '(' in the Expression Line Edit
    ///
    void addOpenBrack();
    ///
    /// @brief addCloseBrack inserts a ')' in the Expression Line Edit
    ///
    void addCloseBrack();
    ///
    /// @brief addPercent inserts a '%' in the Expression Line Edit
    ///
    void addPercent();
    ///
    /// @brief addEq inserts a '=' in the Expression Line Edit
    ///
    void addEq();
    ///
    /// @brief addPi inserts a 'pi' in the Expression Line Edit
    ///
    void addPi();
    ///
    /// @brief addE inserts an 'e' in the Expression Line Edit
    ///
    void addE();
    ///
    /// @brief addSqrt inserts a 'sqrt' in the Expression Line Edit
    ///
    void addSqrt();
    ///
    /// @brief addPower inserts a '^' in the Expression Line Edit
    ///
    void addPower();
    ///
    /// @brief addSin inserts a 'sin' in the Expression Line Edit
    ///
    void addSin();
    ///
    /// @brief addCos inserts a 'cos' in the Expression Line Edit
    ///
    void addCos();
    ///
    /// @brief addTan inserts a 'tan' in the Expression Line Edit
    ///
    void addTan();
    ///
    /// @brief addLog inserts a 'log' in the Expression Line Edit
    ///
    void addLog();
    ///
    /// @brief addExp10 inserts a '^10' in the expression Line Edit
    ///
    void addExp10();
    ///
    /// @brief addExpE inserts a '^e' in the expression Line Edit
    ///
    void addExpE();
    ///
    /// @brief sendRef emits a signal to notify the DB table to refresh its contents
    ///
    void sendRef();
    ///
    /// @brief fourthDiff inserts a '4diff' in the expression Line Edit
    ///
    void fourthDiff();
    ///
    /// @brief thirdDiff inserts a '3diff' in the expression Line Edit
    ///
    void thirdDiff();
    ///
    /// @brief secondDiff inserts a '2diff' in the expression Line Edit
    ///
    void secondDiff();
    ///
    /// brief firstDiff inserts a '1diff' in the expression Line Edit
    ///
    void firstDiff();

private:
    QStatusBar *mStatBar;
    QValidator *mVald;
    QLineEdit *mEdtExpr;
    QPushButton *mBtnExpand;
    QLabel *mLblPath;
    QLabel *mLblFile;
    QLabel *mTableBox;
    QGroupBox *mHidden;
    QTableWidget *mTableWdg;
    QHBoxLayout *hLayoutTable;
    bool expValidator(QString expression);
    bool expValidateCalulate(QString expression);
    bool validateExp(QString expression);
    bool calculate(QString &expression, int start, int end, bool hasBracket);
    QString calculate(QString expression);
    QSet<QString> chanNames;
    std::vector<double *> mTmpChanData;
    QStringList mTmpChanNames;
    QStringList mTmpVars;
    QString mTableName;
    QString calFilePath;
    QString calFileLoadPath;
    QString mTable;
    QFile mCalFile;
    QList<QComboBox*> mLstBoxes;
    QMap<QString, QString> mChannelMap;
    QMap<QString, QString> mChannelMapTmp;
    int mTmpIndex = 0;
    ChanCalDataHandling *chanCalData;
};

#endif // CHANDATACALCULATOR_H
