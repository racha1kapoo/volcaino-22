#include "ChanMenuOpData.h"

ChanMenuOpData::ChanMenuOpData()
{
    auto db =DataBase::getInstance();
    if (db->openDatabase())
        mSql=db->getSqlQuery();
}

ChanMenuOpData::~ChanMenuOpData()
{
}

void ChanMenuOpData::setFetchDataObjs(QTableView *table, std::map<int,QString> arrayList)
{
    mTable = table;
    mArrayList = arrayList;
}

void ChanMenuOpData::setFetchDataModel(QString tableName, std::map<int, QString> arrayList)
{
    //QMutexLocker locker(&mutex);

    mArrayList = arrayList;
    mTableName = tableName;
}

void ChanMenuOpData::setChartObjs(std::vector<QLineSeries *> *lineSeries, int selectChanNo)
{
    //mTable = table;
    //mArraySize = arraySize;
    mSelectChanNo = selectChanNo;
    mLineSeries = lineSeries;
}

std::vector<std::vector<std::vector<double>>> &ChanMenuOpData::getArrayData()
{
    return mArrayData;
}
std::vector<std::vector<std::vector<bool>>> &ChanMenuOpData::getValidArrayData()
{
    return mValidArrayRows;
}

int ChanMenuOpData::getArraySize(int chanIndex)
{
    return mArrSizes[chanIndex];
}

int ChanMenuOpData::getRowCount(int chanIndex)
{
    return mRowCounts[chanIndex];
}

void ChanMenuOpData::setAbort(bool abort)
{
    mAbort = abort;
}

int ChanMenuOpData::getNullRowRatio(int col, QSqlQueryModel &dataModel,int firstNotNullRow)
{
    int row = firstNotNullRow;
    while (dataModel.data(dataModel.index(row, col)).isNull())
        row++;
    return row;
}

void ChanMenuOpData::appendData()
{
    int arraySize = mChan2ArrCount[mSelectChanNo];
    int rowCount = mChan2Row[mSelectChanNo];
    int arrayChanIndex = mArrayChanNoList.indexOf(mSelectChanNo);

    for (int arrayNo = 0; arrayNo < arraySize; arrayNo++) {
        mLineSeries->at(arrayNo)->setName(QString::number(arrayNo+1));
        mLineSeries->at(arrayNo)->setUseOpenGL(true);

        for (int row = 0; row < rowCount; row += mDownSampleRate)
            mLineSeries->at(arrayNo)->append(row, mArrayData[arrayChanIndex][row][arrayNo]);
    }

    //    int time3 = QDateTime::currentMSecsSinceEpoch();
    //    qDebug() << "Fetching data took " << time2-time1;
    //    qDebug() << "Appending data took " << time3-time2;
    emit appendDataDone();
}

void ChanMenuOpData::fetchData()
{
    QSqlQueryModel dataModel;
    QString arrayChanNames;
    std::map<int, QString>::iterator it = mArrayList.begin();
    for (it = mArrayList.begin(); it != mArrayList.end(); it++)
        arrayChanNames += it->second + ",";

    arrayChanNames.chop(1);

    QString query = "SELECT "+arrayChanNames+" FROM "+mTableName+" ORDER BY ROWID;";

    //mutex.lock();
    if (!mSql.exec(query))
        DEBUG_TRACE(query);

    dataModel.setQuery(mSql);

    while (dataModel.canFetchMore()) {
        if (mAbort)
            break;
        dataModel.fetchMore();
    }
    emit fetchDBDone();

    int tableRowCount = dataModel.rowCount();
    int arrayChanCount = mArrayList.size();
    mArrayData.resize(arrayChanCount);
    mValidArrayRows.resize(arrayChanCount);

    for (int i = 0; i < arrayChanCount; i++) {
        if (mAbort)
            break;
        //mArrayChanNoList.push_back(it->first);
        int firstNotNullRow = getNullRowRatio(i, dataModel);
        QModelIndex index = dataModel.index(firstNotNullRow,i);
        auto arrayStr = dataModel.data(index).toString().remove("]").remove("[");
        int thisArrSize = arrayStr.split(',').first().toInt();
        mArrSizes.push_back(thisArrSize);


       // int secondNotNullRow = getNullRowRatio(i, dataModel,firstNotNullRow+1);
        mNullRows.push_back(firstNotNullRow);

        //identify the sampling
//        int iSampling =secondNotNullRow-firstNotNullRow;
          int notNullRowCount = tableRowCount;
//        if(iSampling >1)
//            notNullRowCount = notNullRowCount/iSampling;
//        mRowCounts.push_back(notNullRowCount);
          mRowCounts.push_back(notNullRowCount);

        mArrayData[i].resize(thisArrSize, std::vector<double>(notNullRowCount));
        mValidArrayRows[i].resize(thisArrSize, std::vector<bool>(notNullRowCount));
    }

    for (int i = 0; i < arrayChanCount; i++) {
        if (mAbort)
            break;
        int thisRowCount = mRowCounts[i];
        int thisArrSize = mArrSizes[i];
        //int thisNullRow = mNullRows[i];

        for (int row = 0; row < thisRowCount; row++) {
            if (mAbort)
                break;
            //QModelIndex index=dataModel.index(row*thisNullRow,i);
            if(dataModel.data(dataModel.index(row, i)).isNull())
            {
                for (int arrNo = 0; arrNo < thisArrSize; arrNo++) {
                    if (mAbort)
                        break;
                    mArrayData[i][arrNo][row] =nullValue;
                    mValidArrayRows[i][arrNo][row] = false;
                }
            }
            else
            {
                QModelIndex index=dataModel.index(row,i);

                QString arrayStr=dataModel.data(index).toString().remove("]").remove("[");
                QStringList arrayList=arrayStr.split(',');
                arrayList.removeFirst();

                for (int arrNo = 0; arrNo < thisArrSize; arrNo++) {
                    if (mAbort)
                        break;
                    mArrayData[i][arrNo][row] = arrayList[arrNo].toDouble();
                    mValidArrayRows[i][arrNo][row] = true;

                }
            }
        }
    }
    emit fetchDataDone();
}
