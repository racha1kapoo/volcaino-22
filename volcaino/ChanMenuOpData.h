/**
 *  @file   ChanMenuOpData.h
 *  @brief  Fetches data for chart viewers
 *  @author Shuo Li, Rahul Poddaturi
 *  @date   Nov 8, ‎2022
 ***********************************************/
#ifndef CHANMENUOPDATA_H
#define CHANMENUOPDATA_H
#include <QObject>
#include <QTableView>
#include "TypeDefConst.h"
#include <QLineSeries>
#include <QChart>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QChartView>
#include <QtCharts/QtCharts>
#include <vector>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include "DataBase.h"
class ChanMenuOpData: public QObject
{
    Q_OBJECT
public:
    /// Default contructor
    ChanMenuOpData();

    /// Destructor
    ~ChanMenuOpData();
    /// @brief This function sets the fetching data parameters
    /// @param table: Table to fetch data
    /// @param arrayList: Array channels names
    void setFetchDataObjs(QTableView *table, std::map<int,QString> arrayList);
    /// @brief This function sets the fetching data parameters if fetching form data base
    /// @param tableName: Table name
    /// @param arrayList: Array channels names
    void setFetchDataModel(QString tableName, std::map<int,QString> arrayList);

    /// @brief This function sets the chart viewers objects
    /// @param lineSeries: Chart lines series
    /// @param selectChanNo: Selected channel number
    void setChartObjs(std::vector<QLineSeries*> *lineSeries, int selectChanNo);
    /// @brief This function returns the array data
    /// @return Array data in 3d vector
    std::vector<std::vector<std::vector<double>>> &getArrayData();
    /// @brief This function returns valid array data flags
    /// @return Array data valid flags in 3d vector
    std::vector<std::vector<std::vector<bool>>> &getValidArrayData();
    /// @brief This function gets array size
    /// @param chanIndex: Channel index
    /// @return Array index
    int getArraySize(int chanIndex);
    /// @brief This function gets row count
    /// @param chanIndex: Channel index
    /// @return Row count
    int getRowCount(int chanIndex);
    /// @brief This function enables thread termination flag to abort the thread safely
    /// @param abort: Termination flag
    /// @return
    void setAbort(bool abort);




private:
    QTableView *mTable;
    QSqlQueryModel *dataModel;
    //TableViewOperation *mTableOp;
    int mSelectChanNo;
    int mArraySize = 0;
    std::vector<QLineSeries*> *mLineSeries;

    std::vector<std::vector<std::vector<double>>> mArrayData;
    std::vector<std::vector<std::vector<bool>>> mValidArrayRows;
    //std::vector<*ArrayData> mArrayData1;
    //Arraydata -> size, double a[col][row],firstNotNullRow,SamplingRate,no_of_rows,
    //validRows;

    std::map<int,QString> mArrayList;
    QList<int> mArrayChanNoList;
    std::map<int,int> mChan2Row;
    std::map<int,int> mChan2ArrCount;
    std::vector<int> mArrSizes;
    std::vector<int> mRowCounts;
    std::vector<int> mNullRows;

    /// @brief This function calculates how many null rows
    /// @param col: Column number
    /// @param dataModel: Data model
    /// @param firstNotNullRow: First not null row
    /// @return Count of not null rows
    int getNullRowRatio(int col, QSqlQueryModel &dataModel,int firstNotNullRow = 0);

    int mTableRowCount;

    int mDownSampleRate = 1;

    QMutex mutex;

    QSqlQuery mSql;

    QString mTableName;

    bool mAbort = false;


signals:
    void appendDataDone();
    void fetchDataDone();

    void fetchDBDone();

public slots:
    void appendData();

    void fetchData();
};

#endif // CHANMENUOPDATA_H
