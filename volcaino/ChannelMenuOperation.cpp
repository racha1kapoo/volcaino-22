#include "ChannelMenuOperation.h"
#include "DataBase.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QRadioButton>
#include <QGroupBox>
#include <QPushButton>
#include <QTableWidgetItem>
#include <QMessageBox>
#include "TypeDefConst.h"
#include <QTranslator>
#include <QMdiArea>
#include <QMdiSubWindow>
#include "LanguageSelection.h"
#include <QtGui/QMouseEvent>
#include <QVXYModelMapper>
#include <QSortFilterProxyModel>
#include <QHeaderView>
#include "TableViewOperation.h"
#include "MouseEventTracking.h"
#include <QAbstractItemModel>
#include <QStandardItemModel>
#include <QAbstractItemDelegate>
#include <QMouseEvent>
#include "ArrayChannelProfileViewer.h"
#include "ShowChartView.h"
#include "ChartData.h"

ChannelMenuOperation::ChannelMenuOperation(QObject *parent)
    : QObject(parent), mProfileCount(0), mEditChannelList{}
{
    QTranslator translator;
    auto selectLanguage=LanguageSelection::getInstance();
    selectLanguage->setLanguage(selectLanguage->getLanguage(),translator);
    //mpHlayout = new QSplitter(Qt::Vertical);


}

ChannelMenuOperation::~ChannelMenuOperation()
{
}

ChannelMenuOperation* ChannelMenuOperation::getInstance()
{
    static ChannelMenuOperation theInstance;
    return &theInstance;
}

void ChannelMenuOperation::setChanMenuOpData(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    if (table->getArrayList().size() != 0) {
        mChartLoadData = new ChanMenuOpData;

        mThread = new QThread;
        mChartLoadData->moveToThread(mThread);
        mChartLoadData->setFetchDataModel(table->getTableName(), table->getArrayList());
        connect(mThread, &QThread::started, mChartLoadData, &ChanMenuOpData::fetchData);
        connect(mChartLoadData, &ChanMenuOpData::fetchDataDone, mThread, &QThread::quit);
        connect(mThread, &QThread::finished, this, [table, this](){
            delete mThread;
            mThread = nullptr;
            table->viewProfile->setEnabled(true);
        });
        //connect(mThread, &QThread::finished, this, [table](){table->viewProfile->setEnabled(true);});

        mThread->start();
    } else {
        table->viewProfile->setEnabled(true);
    }
    //table->viewProfile->setEnabled(true);
}

ChanMenuOpData *ChannelMenuOperation::getChanMenuOpData()
{
    return mChartLoadData;
}

QThread *ChannelMenuOperation::getThread()
{
    return mThread;
}

void ChannelMenuOperation::updateTrackers(int pos, int index)
{
    for (int i = 1; i < mParentSplit->count(); i++) {
        MouseEventTracking *thisTracker = qobject_cast<ShowChartView *>(mParentSplit->widget(i))->getMouseTracker();
        QPointF from = thisTracker->getDoubleLIndPos();
        QPointF to = thisTracker->getDoubleRIndPos();
        thisTracker->setUpdateDoubleIndicators(from, to);

        QPointF singlePos = thisTracker->getSingleIndPos();
        thisTracker->setUpdateSingleIndicator(singlePos);
    }
}

void ChannelMenuOperation::listChan(QWidget  *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    DEBUG_TRACE(table->getChannelList());
    mAxisX = new QValueAxis;

    QDialog *dialog = new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("DISPLAY CHANNELS"));
    dialog->setMinimumSize(220,100);

    QVBoxLayout *hLayout = new QVBoxLayout(dialog);

    auto listWidget = new QListWidget(dialog);
    listWidget->setSelectionMode(QAbstractItemView::MultiSelection);

    auto list = table->getHiddenChannelList();
    if (list.size() > 0) {
        for (int item : list) {
            auto listItem = new QListWidgetItem(listWidget);
            listItem->setText(table->model()->headerData(item,Qt::Horizontal).toString());
        }
        hLayout->addWidget(listWidget);
        QPushButton *ok = new QPushButton(tr("OK"), dialog);
        hLayout->addWidget(ok);
        dialog->showNormal();

        connect(ok, &QPushButton::clicked, dialog,[dialog,table,list,listWidget](){
            for (int i = 0; i < list.size(); ++i) {
                if (listWidget->item(i)->isSelected())
                    table->setChannelToDisplayState(list[i]);
            }
            dialog->close();
        });
    } else {
        QMessageBox msgBox;
        msgBox.setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint| Qt::FramelessWindowHint);
        msgBox.setText(tr("None of the Channels is hidden."));
        msgBox.exec();
    }
    connect(dialog,&QDialog::finished,[dialog](){dialog->deleteLater();});
}

void ChannelMenuOperation::newChan(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);

    DEBUG_TRACE(table->getSelectedChannel());
    DEBUG_TRACE(table->getTableName());
    DEBUG_TRACE(table->getChannelList());
    // DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());
    QStringList channels = table->getChannelList();

    QDialog *dialog = new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("NEW CHANNEL"));

    QGroupBox *window = new QGroupBox(dialog);
    window->setTitle(tr("Channel Details"));
    QLabel *name = new QLabel(tr("Name"));
    QLineEdit *nameLineEdit = new QLineEdit();
    QLabel *type = new QLabel(tr("Type"));
    QComboBox *typeComboBox = new QComboBox;
    typeComboBox->addItem(tr("INTEGER"));
    typeComboBox->addItem(tr("TEXT"));
    typeComboBox->addItem(tr("DOUBLE"));
    typeComboBox->addItem(tr("FLOAT"));
    QLabel *defaultVal = new QLabel(tr("Default"));
    QLineEdit *defLineEdit = new QLineEdit();
    defLineEdit->setText("0");
    QLabel *notNull = new QLabel(tr("Not Null"));
    QRadioButton *notNullCheckBox= new QRadioButton;
    notNullCheckBox->setChecked(true);

    QFormLayout *layout = new QFormLayout(window);
    layout->addRow(name, nameLineEdit);
    layout->addRow(type, typeComboBox);
    layout->addRow(defaultVal, defLineEdit);
    layout->addRow(notNull,notNullCheckBox);

    QPushButton *ok=new QPushButton(tr("OK"));
    QPushButton *cancel=new QPushButton(tr("CANCEL"));
    layout->addRow(ok,cancel);
    window->show();
    dialog->showNormal();

    connect(typeComboBox,&QComboBox::currentTextChanged,dialog,[typeComboBox,defLineEdit](){
        switch (typeComboBox->currentIndex()) {
        case 0:
            defLineEdit->setText("0");
            break;
        case 1:
            defLineEdit->setText("N/A");
            break;
        case 2:
            defLineEdit->setText("0.00");
            break;
        case 3:
            defLineEdit->setText("0.0");
            break;
        default:
            break;
        }
    });

    connect(nameLineEdit, &QLineEdit::textChanged, dialog, [ok,channels,nameLineEdit](){
        if (channels.contains(nameLineEdit->text(), Qt::CaseInsensitive)) {
            nameLineEdit->setStyleSheet("QLineEdit { background-color: red }");
            ok->setDisabled(true);
        } else {
            nameLineEdit->setStyleSheet("QLineEdit { background-color: white }");
            ok->setDisabled(false);
        }
    });
    auto Db = DataBase::getInstance();

    connect(ok, &QPushButton::clicked, dialog, [Db, table, dialog, nameLineEdit, typeComboBox, defLineEdit, notNullCheckBox]() {
        Db->addColumn(table->getTableName(), nameLineEdit->text().toLower(), typeComboBox->currentText(), notNullCheckBox->isChecked(), defLineEdit->text());
        dialog->close();
        table->refreshTable();
    });

    connect(cancel, &QPushButton::clicked, dialog, [dialog]() {
        dialog->close();
    });

    connect(dialog, &QDialog::finished, [dialog]() {
        dialog->deleteLater();
    });
}

void ChannelMenuOperation::deleteChan(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    //QString column=table->horizontalHeaderItem(table->getSelectedChannel())->text();
    QString column = table->model()->headerData(table->getSelectedChannel(),Qt::Horizontal).toString();
    QString text = tr("Do you want to delete channel ");
    QString str = text+column+" ?";
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("DELETE CHANNEL"));
    msgBox.setText(str);
    msgBox.setStandardButtons(QMessageBox::Ok| QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    auto Db = DataBase::getInstance();

    switch (ret) {
    case QMessageBox::Ok:
        DEBUG_TRACE(table->getSelectedChannel());
        table->setChannelToDisplayState(table->getSelectedChannel());
        msgBox.close();
        Db->deleteColumn(table->getTableName(),column.toLower());
        table->refreshTable();
        break;
    case QMessageBox::Cancel:
        msgBox.close();
        break;
    default:
        // should never be reached
        break;
    }
}

void ChannelMenuOperation::copyCreate(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    auto Db = DataBase::getInstance();

    //DEBUG_TRACE(table->getSelectedChannel());
    //DEBUG_TRACE(table->getTableName());
    //DEBUG_TRACE(table->getChannelList());
    // DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());

    QStringList channels = table->getChannelList();

    QDialog *dialog = new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("COPY CHANNEL"));

    QGroupBox *window = new QGroupBox(dialog);
    window->setTitle(tr("Channel Details"));
    QLabel *name = new QLabel(tr("Name"),window);
    QLineEdit *nameLineEdit = new QLineEdit(window);
    QLabel *type = new QLabel(tr("Type"),window);
    QComboBox *typeComboBox = new QComboBox(window);
    typeComboBox->addItem(tr("INTEGER"));
    typeComboBox->addItem(tr("TEXT"));
    typeComboBox->addItem(tr("DOUBLE"));
    typeComboBox->addItem(tr("FLOAT"));
    QLabel *defaultVal = new QLabel(tr("Default"),window);
    QLineEdit *defLineEdit = new QLineEdit(window);

    QLabel *notNull = new QLabel(tr("Not Null"),window);
    QRadioButton *notNullCheckBox= new QRadioButton(window);
    notNullCheckBox->setChecked(true);
    QLabel *chanList = new QLabel(tr("Channel List"),window);
    QComboBox *chanComboBox = new QComboBox(window);
    auto list=table->getChannelList();
    for (QString item : list)
        chanComboBox->addItem(item);
    chanComboBox->setCurrentText(table->model()->headerData(table->getSelectedChannel(), Qt::Horizontal).toString());
    //DEBUG_TRACE(table->model()->headerData(table->getSelectedChannel(),Qt::Horizontal).toString());
    //DEBUG_TRACE(chanComboBox->currentText());
    //DEBUG_TRACE(table->getArrayList());
    auto array = table->getArrayList();
    auto itr = array.find(table->getSelectedChannel()-1);      //decrease channel index of rowid
    QListWidget *listWidget = new QListWidget();;
    QLabel *filter;
    bool arrayFlag = false;
    std::vector<std::vector<QString>> arrayMatrix;
    if (itr != array.end()) {
        listWidget->setSelectionMode(QAbstractItemView::MultiSelection);
        filter = new QLabel(tr("Filter"),window);
        arrayFlag = true;
        auto arrayData = Db->getColumnData(table->getTableName(), itr->second);
        auto firstRow = arrayData[0];
        firstRow.remove('[');
        auto arrayList = firstRow.split(',');
        int arrayCount = arrayList.first().toInt();
        for (int i = 1; i <= arrayCount; i++)
            listWidget->addItem(QString::number(i));
        for (int i = 0; i < arrayData.size(); ++i) {
            auto arrayRow = arrayData[i];
            arrayRow.remove('[').remove(']');
            auto rowItems = arrayRow.split(',');
            rowItems.removeFirst();
            std::vector<QString> vec;
            for (int j = 0; j < rowItems.size(); ++j)
                vec.push_back(rowItems[j]);
            arrayMatrix.push_back(vec);
        }
    }

    QFormLayout *layout = new QFormLayout(window);
    layout->addRow(name, nameLineEdit);
    layout->addRow(type, typeComboBox);
    layout->addRow(defaultVal, defLineEdit);
    layout->addRow(notNull,notNullCheckBox);
    layout->addRow(chanList,chanComboBox);
    if (arrayFlag)
        layout->addRow(filter,listWidget);

    QPushButton *ok = new QPushButton(tr("OK"));
    QPushButton *cancel = new QPushButton(tr("CANCEL"));
    layout->addRow(ok, cancel);

    connect(typeComboBox, &QComboBox::currentTextChanged, dialog, [typeComboBox, defLineEdit](){
        switch (typeComboBox->currentIndex()) {
        case 0:
            defLineEdit->setText("0");
            break;
        case 1:
            defLineEdit->setText("N/A");
            break;
        case 2:
            defLineEdit->setText("0.00");
            break;
        case 3:
            defLineEdit->setText("0.0");
            break;
        default:
            break;
        }
    });

    defLineEdit->setEnabled(false);
    notNullCheckBox->setEnabled(false);
    //typeComboBox->setEnabled(false);
    auto map = Db->getDbTableInfo(table->getTableName());
    auto it = map.find(chanComboBox->currentText().toUpper());
    if (it != map.end()) {
        typeComboBox->setCurrentText(it->second.type);
        //defLineEdit->setText(it->second.defaultVal);
        switch (typeComboBox->currentIndex()) {
        case 0:
            defLineEdit->setText("0");
            break;
        case 1:
            defLineEdit->setText("N/A");
            break;
        case 2:
            defLineEdit->setText("0.00");
            break;
        case 3:
            defLineEdit->setText("0.0");
            break;
        default:
            break;
        }
        notNullCheckBox->setChecked(it->second.NotNull);
    }

    window->show();
    dialog->showNormal();

    connect(chanComboBox, &QComboBox::currentTextChanged, dialog, [map, chanComboBox, defLineEdit, typeComboBox, notNullCheckBox](){
        defLineEdit->setEnabled(false);
        notNullCheckBox->setEnabled(false);
        typeComboBox->setEnabled(false);
        auto it = map.find(chanComboBox->currentText().toUpper());
        if (it != map.end()) {
            typeComboBox->setCurrentText(it->second.type);
            switch (typeComboBox->currentIndex()) {
            case 0:
                defLineEdit->setText("0");
                break;
            case 1:
                defLineEdit->setText("N/A");
                break;
            case 2:
                defLineEdit->setText("0.00");
                break;
            case 3:
                defLineEdit->setText("0.0");
                break;
            default:
                break;
            }
            notNullCheckBox->setChecked(it->second.NotNull);
        }
    });

    connect(nameLineEdit, &QLineEdit::textChanged, dialog, [ok, channels, nameLineEdit](){
        if (channels.contains(nameLineEdit->text(),Qt::CaseInsensitive)) {
            nameLineEdit->setStyleSheet("QLineEdit { background-color: red }");
            ok->setDisabled(true);
        } else {
            nameLineEdit->setStyleSheet("QLineEdit { background-color: white }");
            ok->setDisabled(false);
        }
    });

    connect(ok, &QPushButton::clicked, dialog, [this, Db, table, dialog, nameLineEdit, typeComboBox, defLineEdit, notNullCheckBox, chanComboBox, arrayMatrix, listWidget, arrayFlag](){
        if (arrayFlag) {
            auto list = listWidget->selectedItems();
            QStringList newArray;
            Db->startTransaction();
            Db->executeQuery("CREATE TABLE tempTable (temp FLOAT[]);");

            // DEBUG_TRACE(arrayMatrix);
            for (auto vec : arrayMatrix) {
                QString data;
                data.append("'[").append(QString::number(list.count())).append(',');
                for (auto item : list) {
                    auto rowOne = vec;
                    data.append(rowOne.at(item->text().toInt()-1)).append(',');
                }
                data.append("]");
                data.replace(",]", "]'");
                //DEBUG_TRACE(data);
                Db->insertRecord(data, "tempTable");
            }
            Db->addColumn(table->getTableName(), nameLineEdit->text().toLower(), typeComboBox->currentText()+"[]", notNullCheckBox->isChecked(), defLineEdit->text());
            //DEBUG_TRACE(newArray);
            QString query = "UPDATE "+table->getTableName()+ " SET "+nameLineEdit->text().toLower() + " =(SELECT temp FROM tempTable WHERE tempTable.rowid = "+table->getTableName()+".rowid);";
            Db->executeQuery(query);
            Db->executeQuery("drop table temptable;");

            Db->closeTransaction();

            dialog->close();
            table->refreshTable();
            DEBUG_TRACE(table->model()->columnCount());
            table->setChannel(table->model()->columnCount()-1);
            this->displayProfile(table);
        } else {
            Db->copyColumn(table->getTableName(),nameLineEdit->text().toLower(),typeComboBox->currentText(),notNullCheckBox->isChecked(),defLineEdit->text(),chanComboBox->currentText());
            dialog->close();
            table->refreshTable();
        }
    });
    connect(cancel, &QPushButton::clicked, dialog, [dialog](){
        dialog->close();
    });
}

void ChannelMenuOperation::hide(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);

    DEBUG_TRACE(table->getSelectedChannel());
    DEBUG_TRACE(table->getTableName());
    DEBUG_TRACE(table->getChannelList());
    // DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());
    table->setChannelToHiddenState(table->getSelectedChannel());
}

void ChannelMenuOperation::hideAll(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);

    DEBUG_TRACE(table->getSelectedChannel());
    DEBUG_TRACE(table->getTableName());
    DEBUG_TRACE(table->getChannelList());
    //   DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());
    auto list = table->getChannelList();
    for (int i = 0; i < list.size(); ++i)
        table->setChannelToHiddenState(i);
}

void ChannelMenuOperation::displayAll(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    DEBUG_TRACE(table->getChannelList());
    auto list = table->getChannelList();
    for (int i = 0; i < list.size(); ++i)
        table->setChannelToDisplayState(i);
}

void ChannelMenuOperation::displayProfile(QWidget *widget){
    DEBUG_TRACE(mProfileCount);
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);


    if (mProfileCount < 4) {
        //mProfileCount=mProfileCount+1;
        mChannelList.append(table->getSelectedChannel());
        this->setProfile(table, table->getSelectedChannel(), table->getArrayNoList().contains(table->getSelectedChannel()-1));
    }
}

void ChannelMenuOperation::editChannel(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    DEBUG_TRACE(table->getSelectedChannel());
    if (mEditChannelList.contains(table->getSelectedChannel()))
        mEditChannelList.removeOne(table->getSelectedChannel());
    mEditChannelList.append(table->getSelectedChannel());
    DEBUG_TRACE(mEditChannelList);
    int r = table->model()->rowCount(QModelIndex())-1;
    int col = table->getSelectedChannel();
    QModelIndex id = table->model()->index(0, col, QModelIndex());
    DEBUG_TRACE(table->model()->flags(id));

    DEBUG_TRACE(id);
    DEBUG_TRACE(r);
    DEBUG_TRACE(table->model()->flags(id));
    DEBUG_TRACE(table->currentIndex());
    table->setItemDelegateForColumn(table->getSelectedChannel(),new QItemDelegate());
    table->setEditTriggers(QAbstractItemView::AllEditTriggers);
    DEBUG_TRACE(table->model()->flags(id));
}

void ChannelMenuOperation::arrayViewChannel(QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);
    DEBUG_TRACE(table->getSelectedChannel());
    ArrayChannelProfileViewer *array = new ArrayChannelProfileViewer(table, 0, table->getSelectedChannel());
    array->show();
}

void ChannelMenuOperation::setProfile(TableViewOperation* table, int selectColNo, bool isArrayChan)
{
    QSplitter *parentSplit = qobject_cast<QSplitter *>(table->parentWidget());
    mParentSplit = parentSplit;

    ChartData *mpChart = new ChartData();
    auto mpChartView = new ShowChartView(mpChart, parentSplit);
    auto pMousePt = new MouseEventTracking(mpChart);

    mpChartView->setSelectedCol(selectColNo);


    if (!isArrayChan)
    {
        mpChartView->setChartType(ChartType::ProfileNormal);
        mpChart->populateSingleChannelDataFromTable(table, selectColNo);

    }
    else
    {
        mpChartView->setChartType(ChartType::ProfileArray);

        auto array = table->getArrayList();
        int chanIndex = distance(array.begin(),array.find(selectColNo-1));
        int arraySize = mChartLoadData->getArraySize(chanIndex);
        int rowCount = mChartLoadData->getRowCount(chanIndex);

        mpChart->populateArrayData(mChartLoadData, arraySize, chanIndex, 0, rowCount);
        mpChartView->setArrayCount(arraySize);
        mpChart->legend()->hide();
    }

    mpChartView->setMouseTracker(pMousePt);
    mpChartView->setTitle(table->model()->headerData(selectColNo, Qt::Horizontal).toString());
    mpChartView->setChanContextMenu();


    mpChartView->setRubberMode(rubberMode::zoomAllMode);

    //QAbstractItemModel *tableDataModel = table->model();
    int rowCount = table->model()->rowCount();
    mpChart->axes(Qt::Horizontal).back()->setMax(rowCount);
    mpChart->saveAxisIniRanges();

    if(parentSplit->count() == 2)
    {
        updateGlbalXAxisRanges(parentSplit, updateType::update);
    }

    if(parentSplit->count() > 2)
        mpChart->setXAxisRanges(xGLobalMins.back(), xGLobalMaxs.back());

    parentSplit->addWidget(mpChartView);

    if(parentSplit->count() == 2)
    {
        mBestChartLeft = mpChart->plotArea().left();
    }

    if(parentSplit->count() > 2)
    {
        if(mpChart->plotArea().left() > mBestChartLeft)
        {mBestChartLeft = mpChart->plotArea().left();}
        adjustChartsLeftMargins(parentSplit);
    }



    {
        connect(table, &TableViewOperation::cellSelected, mpChartView, &ShowChartView::selectOnChart);
        connect(table, &TableViewOperation::multiCellsSelected, mpChartView, &ShowChartView::selectMultiCellsOnChart);
        connect(table, &TableViewOperation::clearTrackers, mpChartView, &ShowChartView::clearChartViewTrackers);

        connect(mpChart, &ChartData::plotAreaChanged, this,
                [pMousePt]()
        {
            QPointF from = pMousePt->getDoubleLIndPos();
            QPointF to = pMousePt->getDoubleRIndPos();
            pMousePt->setUpdateDoubleIndicators(from, to);

            QPointF singlePos = pMousePt->getSingleIndPos();
            pMousePt->setUpdateSingleIndicator(singlePos);
        });

        connect(mpChartView, &ShowChartView::seriesValSelect, this, [selectColNo, table, pMousePt](int index) {
            if(pMousePt->getSingleIndVisible())
            {
                table->setFocus();
                table->scrollToTop();

                QModelIndex theIndex = table->model()->index(index,selectColNo);
                table->selectionModel()->select(theIndex, QItemSelectionModel::ClearAndSelect);
                table->scrollTo(theIndex);
            }
        }
        );

        connect(mpChartView, &ShowChartView::seriesValuesSelects, this,[selectColNo,table, pMousePt](int start, int end) {
            table->setFocus();
            table->scrollToTop();
            table->selectionModel()->clearSelection();

            QPointF from(start, 0);
            QPointF to(end, 0);
            pMousePt->showIniDoubleIndicators();
            pMousePt->setUpdateDoubleIndicators(from, to);

            for (int i = start; i < end; i++) {
                QModelIndex index = table->model()->index(i, selectColNo);
                table->selectionModel()->select(index, QItemSelectionModel::Select);
                table->scrollTo(index);

                if(i % 10 == 0)
                    QCoreApplication::processEvents();
            }
        });

        connect(mpChartView, &ShowChartView::thisRubberBandChanged, this,
                [this](ShowChartView *view, QPointF fp, QPointF tp, int startIndex, int endIndex, bool isYMode) {
            mNeedUpdate = true;
            emit this->allRubberBandChanged(view, fp, tp, startIndex, endIndex, isYMode);
        });

        connect(this, &ChannelMenuOperation::allRubberBandChanged, mpChartView,
                [mpChartView, parentSplit, this](QChartView *view, QPointF fp, QPointF tp, int startIndex, int endIndex, bool isYMode) {
            mpChartView->zoomInFunc(view, fp, tp, startIndex, endIndex, isYMode);

            if (mNeedUpdate) {
                mNeedUpdate = false;
                updateGlbalXAxisRanges(parentSplit, updateType::update);
            }
            mpChartView->getChart()->getYAxisRanges(updateType::update);
            mpChartView->update();

            mChartViewCount++;
            if(mChartViewCount == parentSplit->count()-1)
            {
                mChartViewCount = 0;
                reAlignXAxis();
                //emit reAlignXAxisSig();
            }
        });

        //connect(this, &ChannelMenuOperation::allRubberBandChanged,this, &ChannelMenuOperation::reAlignXAxis, Qt::UniqueConnection);


        connect(mpChartView, &ShowChartView::setXYRanges, this,
                [this,mpChart](qreal xmin, qreal xmax, qreal ymin, qreal ymax) {
            mNeedUpdate = true;

            if(ymin != -1 && ymax != -1)
            {
                mpChart->axes(Qt::Vertical).back()->setMin(ymin);
                mpChart->axes(Qt::Vertical).back()->setMax(ymax);
            }

            emit this->allSetXYRanges(xmin,xmax);
        });

        connect(this, &ChannelMenuOperation::allSetXYRanges, mpChartView,
                [this,mpChartView,parentSplit,pMousePt,mpChart](qreal xmin, qreal xmax) {

            if(xmin != -1 && xmax != -1)
            {
                mpChart->axes(Qt::Horizontal).back()->setMin(xmin);
                mpChart->axes(Qt::Horizontal).back()->setMax(xmax);
            }

            if (mNeedUpdate) {
                mNeedUpdate = false;
                updateGlbalXAxisRanges(parentSplit, updateType::update);
            }
            mpChartView->getChart()->getYAxisRanges(updateType::update);
            QPointF singlePos = pMousePt->getSingleIndPos();
            pMousePt->setUpdateSingleIndicator(singlePos);

            QPointF from = pMousePt->getDoubleLIndPos();
            QPointF to = pMousePt->getDoubleRIndPos();
            pMousePt->setUpdateDoubleIndicators(from, to);
            mpChartView->update();

            mChartViewCount++;
            if(mChartViewCount == parentSplit->count()-1)
            {
                mChartViewCount = 0;
                reAlignXAxis();
                //emit reAlignXAxisSig();
            }
        });

        connect(mpChartView, &ShowChartView::thisZoomReset, this, [parentSplit, this]() {
            mNeedUpdate = true;
            updateGlbalXAxisRanges(parentSplit, updateType::reset);

            emit this->allZoomReset();
        });

        connect(this, &ChannelMenuOperation::allZoomReset, mpChart, [pMousePt, mpChart, isArrayChan, this]() {
            mpChart->setAxisIniRanges();
            mpChart->getYAxisRanges(updateType::reset);
            mpChart->update();
            QPointF singlePos = pMousePt->getSingleIndPos();
            pMousePt->setUpdateSingleIndicator(singlePos);

            QPointF from = pMousePt->getDoubleLIndPos();
            QPointF to = pMousePt->getDoubleRIndPos();
            pMousePt->setUpdateDoubleIndicators(from, to);

            mChartViewCount++;
            if(mChartViewCount == mParentSplit->count()-1)
            {
                mChartViewCount = 0;
                reAlignXAxis();
                //emit reAlignXAxisSig();
            }
        });

        connect(mpChartView, &ShowChartView::backToLast, this, [parentSplit, this]() {
            mNeedUpdate = true;
            updateGlbalXAxisRanges(parentSplit, updateType::revert);

            emit this->allGoBack();
        });

        connect(this, &ChannelMenuOperation::allGoBack, mpChartView, [mpChart, pMousePt, this]() {
            mpChart->setXAxisRanges(xGLobalMins.back(), xGLobalMaxs.back());
            mpChart->getYAxisRanges(updateType::revert);
            mpChart->setYAxisRanges();
            mpChart->update();

            QPointF singlePos = pMousePt->getSingleIndPos();
            pMousePt->setUpdateSingleIndicator(singlePos);
            QPointF from = pMousePt->getDoubleLIndPos();
            QPointF to = pMousePt->getDoubleRIndPos();
            pMousePt->setUpdateDoubleIndicators(from, to);

            mChartViewCount++;
            if(mChartViewCount == mParentSplit->count()-1)
            {
                mChartViewCount = 0;
                reAlignXAxis();
                //emit reAlignXAxisSig();
            }

        });

        connect(mpChartView, &ShowChartView::scrollSignal, this, [this](double dx, double dy) {
            emit this->allScroll(dx, dy);
        });

        connect(this, &ChannelMenuOperation::allScroll, mpChartView, [mpChartView, pMousePt, parentSplit, this](double dx, double dy) {
            //double ratio = mpChartView->getChart()->plotArea().width()/parentSplit->width();
            mpChartView->move(dx, dy);
            mpChartView->update();

            QPointF from = pMousePt->getDoubleLIndPos();
            QPointF to = pMousePt->getDoubleRIndPos();
            pMousePt->setUpdateDoubleIndicators(from, to);

            if(dy != 0)
            {
                mChartViewCount++;
                if(mChartViewCount == parentSplit->count()-1)
                {
                    mChartViewCount = 0;
                    reAlignXAxis();
                    //emit reAlignXAxisSig();
                }
            }
        });

        //connect(this, &ChannelMenuOperation::allScroll, this, [mpChartView, pMousePt, parentSplit](double dx, double dy) {


        connect(mpChartView, &ShowChartView::scrollDone, this, [mpChartView, parentSplit, this]() {

            updateGlbalXAxisRanges(parentSplit, updateType::update);
            //mpChartView->getChart()->getYAxisRanges(updateType::update);
        });

        connect(mpChartView, &ShowChartView::zoomInX, this, [this](qreal factor, qreal xcenter)
        {mNeedUpdate=true;emit this->allZoomInX(factor,xcenter);});

        connect(this, &ChannelMenuOperation::allZoomInX, this,[this,parentSplit,mpChartView](qreal factor, qreal xcenter)
        {
            mpChartView->zoomAlongX(factor, xcenter);

            QPointF singlePos = mpChartView->getMouseTracker()->getSingleIndPos();
            mpChartView->getMouseTracker()->setUpdateSingleIndicator(singlePos);

            QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
            QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
            mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);

            if (mNeedUpdate) {
                mNeedUpdate = false;
                updateGlbalXAxisRanges(parentSplit, updateType::update);
            }
            mpChartView->update();

        });

        //connect(this, &ChannelMenuOperation::reAlignXAxisSig, this, &ChannelMenuOperation::reAlignXAxis, Qt::UniqueConnection);

    }
}

QList<int> ChannelMenuOperation::getChannelList()
{
    return mChannelList;
}

QList <int> ChannelMenuOperation::getEditChannelList()
{
    return mEditChannelList;
}

void ChannelMenuOperation::find(QWidget *widget)
{
    if (!thereIsFindVal) {
        thereIsFindVal = true;
        QDialog *findChan = new QDialog(widget->parentWidget()->parentWidget());
        findChan->setAttribute(Qt::WA_DeleteOnClose);
        findChan->setWindowFlags(Qt::FramelessWindowHint);
        findChan->setStyleSheet("background-color:white;");

        QVBoxLayout *vLayout = new QVBoxLayout(findChan);
        QLabel *lable = new QLabel("Find a Value",findChan);

        QHBoxLayout *hLayout = new QHBoxLayout;
        QLineEdit *lineEdit = new QLineEdit(findChan);
        lineEdit->setFixedWidth(250);
        QPushButton *search = new QPushButton(findChan);
        search->setFixedHeight(lineEdit->height());
        search->setFixedWidth(search->height()*3);
        search->setText("Find");
        connect(search, &QPushButton::clicked, this, [this, lineEdit, widget]()
        {
            this->searchChanValue(lineEdit->displayText(), widget);
        }
        );

        QPushButton *close = new QPushButton(findChan);
        close->setFixedHeight(lineEdit->height());
        close->setFixedWidth(close->height()*3);
        close->setText("Close");
        connect(close, &QPushButton::clicked, this, [this, findChan](){
            findChan->close();
            thereIsFindVal = false;
        });

        hLayout->addWidget(lineEdit);
        hLayout->addWidget(search);
        hLayout->addWidget(close);

        vLayout->addWidget(lable);
        vLayout->addLayout(hLayout);

        findChan->setMinimumWidth(lineEdit->width()+search->width()*2);
        findChan->setMinimumHeight(lineEdit->height());

        //findChan->move(mTableViewOp->horizontalHeader()->x(), mTableViewOp->horizontalHeader()->y());

        findChan->move(widget->rect().left()+findChan->width()*1.1, widget->rect().bottom()-findChan->height()*2.5);
        //findChan->move(widget->rect().left(), widget->rect().top()+findChan->height()*3);
        findChan->show();
    }

}

void ChannelMenuOperation::displayHideSeries()
{
    QLegendMarker *marker = qobject_cast<QLegendMarker*> (sender());
    Q_ASSERT(marker);

    switch (marker->type()) {
    case QLegendMarker::LegendMarkerTypeXY:
    {
        // Toggle visibility of series
        marker->series()->setVisible(!marker->series()->isVisible());

        // Turn legend marker back to visible, since hiding series also hides the marker
        // and we don't want it to happen now.
        marker->setVisible(true);

        // Dim the marker, if series is not visible
        qreal alpha = 1.0;

        if (!marker->series()->isVisible())
            alpha = 0.5;

        QColor color;
        QBrush brush = marker->labelBrush();
        color = brush.color();
        color.setAlphaF(alpha);
        brush.setColor(color);
        marker->setLabelBrush(brush);

        brush = marker->brush();
        color = brush.color();
        color.setAlphaF(alpha);
        brush.setColor(color);
        marker->setBrush(brush);

        QPen pen = marker->pen();
        color = pen.color();
        color.setAlphaF(alpha);
        pen.setColor(color);
        marker->setPen(pen);

        break;
    }
    default:
    {
        qDebug() << "Unknown";
        break;
    }
    }
}

void ChannelMenuOperation::updateGlbalXAxisRanges(QSplitter *splitter, updateType type)
{
    double xMax;
    double xMin;

    qobject_cast<ShowChartView *>(splitter->widget(1))->getChart()->getXAxisRanges(xMin, xMax);

    if (type == updateType::update) {
        xGLobalMaxs.push_back(xMax);
        xGLobalMins.push_back(xMin);
    } else if (type == updateType::revert) {
        if (xGLobalMaxs.size() > 1) {
            xGLobalMaxs.pop_back();
            xGLobalMins.pop_back();
        }
    } else if (type == updateType::reset) {
        while (xGLobalMaxs.size() > 1) {
            xGLobalMaxs.pop_back();
            xGLobalMins.pop_back();
        }
    }
}

void ChannelMenuOperation::adjustChartsLeftMargins(QSplitter *splitter)
{
    for(int i = 1; i < splitter->count(); i++)
    {
        ChartData *chart = qobject_cast<ShowChartView *>(splitter->widget(i))->getChart();
        int marginDiff = chart->plotArea().left() - mBestChartLeft;

        if(marginDiff != 0)
        {
            QMargins margin = chart->margins();
            margin.setLeft(margin.left()-marginDiff);
            chart->setMargins(margin);
            qobject_cast<ShowChartView *>(splitter->widget(i))->update();
        }
    }
}

void ChannelMenuOperation::reAlignXAxis()
{

    //qDebug() << "I am called";
    bool needAlignment = false;
    int bestMargin = 0;
    for(int i = 1; i < mParentSplit->count()-1; i++)
    {
        if(qobject_cast<ShowChartView *>(mParentSplit->widget(i))->getChart()->plotArea().left()
                != qobject_cast<ShowChartView *>(mParentSplit->widget(i+1))->getChart()->plotArea().left())
        {
            needAlignment = true;
            break;
        }
    }

    if(needAlignment)
    {
        //QApplication::sendPostedEvents();
        for(int i = 1; i < mParentSplit->count(); i++)
        {
            ChartData *chart = qobject_cast<ShowChartView *>(mParentSplit->widget(i))->getChart();
            QMargins margin = chart->margins();
            margin.setLeft(20);
            chart->setMargins(margin);

            QApplication::sendPostedEvents();
            if(i == 1)
            {
                bestMargin = chart->plotArea().left();
                //qDebug() << bestMargin;
            }
            else
            {
                //qDebug() << chart->plotArea().left();
                if(chart->plotArea().left() > bestMargin)
                {
                    bestMargin = chart->plotArea().left();
                }
            }
        }

        //QApplication::sendPostedEvents();

        for(int i = 1; i < mParentSplit->count(); i++)
        {
            ChartData *chart = qobject_cast<ShowChartView *>(mParentSplit->widget(i))->getChart();
            int marginDiff = chart->plotArea().left() - bestMargin;

            if(marginDiff != 0)
            {
                QMargins margin = chart->margins();
                margin.setLeft(margin.left()-marginDiff);
                chart->setMargins(margin);
                chart->update();
            }
        }
    }

}


void ChannelMenuOperation::findAdjustChartsLMargin(QSplitter *splitter)
{
    mBestChartLeft = qobject_cast<ShowChartView *>(splitter->widget(1))->getChart()->plotArea().left();

    for(int i = 2; i < splitter->count(); i++)
    {
        ChartData *chart = qobject_cast<ShowChartView *>(splitter->widget(i))->getChart();
        if(chart->plotArea().left() > mBestChartLeft)
        {
            mBestChartLeft = chart->plotArea().left();
        }
    }

    adjustChartsLeftMargins(splitter);
}




void ChannelMenuOperation::adjustTrackLine(MouseEventTracking *track)
{
    //track->setUpdateSingleIndicator(QPointF(track->lastX, track->lastY), true);
    //track->updateGeometry();
}

void ChannelMenuOperation::searchChanValue(QString chanVal, QWidget *widget)
{
    TableViewOperation *table = qobject_cast<TableViewOperation *>(widget);

    if (chanVal.isEmpty()) {
        QApplication::beep();
    } else {
        if (chanVal != mSerachValue) {  //Search for a new val
            mSerachValue = chanVal;
            mNoWhereFound = true;
            for (int i = 0; i < table->model()->rowCount(); i++) {
                if (table->model()->data(table->model()->index(i, table->getSelectedChannel())).toString() == chanVal) {
                    mNoWhereFound = false;
                    break;
                }
            }

            if (!mNoWhereFound) {
                mSerachValue = chanVal;
                matchIndex = 0;
                while (table->model()->data(table->model()->index(matchIndex, table->getSelectedChannel())).toString() != chanVal)
                    matchIndex++;

                table->setFocus();
                QModelIndex modelIndex = table->model()->index(matchIndex, table->getSelectedChannel());
                table->scrollTo(modelIndex);
                table->selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);
            } else {
                QApplication::beep();
            }
        } else {
            if (!mNoWhereFound) {
                matchIndex++;
                if (matchIndex == table->model()->rowCount(QModelIndex())-1)
                    matchIndex = 0;
                while (table->model()->data(table->model()->index(matchIndex, table->getSelectedChannel())).toString() != chanVal) {
                    matchIndex++;
                    if (matchIndex == table->model()->rowCount(QModelIndex())-1)
                        matchIndex = 0;
                }

                table->setFocus();
                QModelIndex modelIndex = table->model()->index(matchIndex,table->getSelectedChannel());
                table->scrollTo(modelIndex);
                table->selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);
            } else {
                QApplication::beep();
            }
        }
    }
}

void ChannelMenuOperation::allZoomInAlongX(qreal factor, qreal xcenter)
{


}
