/**
 *  @file   DataBase.h
 *  @brief  This class displays the context menu when right clicked on the header of the table.
 *          All the functionalities of context menu are implemented in this class.
 *          The options are:
 *              1. List
 *              2. New (Ctrl+N)
 *              3. Delete (Del)
 *              4. Copy (Ctrl+C)
 *              5. Hide (Space)
 *              6. Hide All
 *              7. Display All
 *              8. View Profile
 *              8. Edit Channel (not yet implemented)
 *              9. Array Viewer
 *
 *           Details:
 *           1. List: It displays the list of hidden channels. User can select multiple channels to make them visible in the table.
 *           2. New: This option allows you to create new channel. user has option to select data type and default value.
 *           3. Delete: This option allows user to delete any channel. A message box will pop up for the confirmation too.
 *           4. Copy: This option allows user to create a new channel from the existing channel. It will copy data as well as column schema details.
 *           5. Hide: It hides the channel from the table.
 *           6. Hide All: This option hides all the channels (Use option 'List' to unhide channels.
 *           7. View Profile: On selecting 'View Profile', a line series chart of the seleected channel shall appear with options to zoom in, zoom out and display track line.
 *              User can perform following functions by using keyboard shortcuts:
 *              a. Zoom In: +
 *              b. Zoom out: -
 *              c. Move up: Up arrow
 *              d. Move down: Down arrow
 *              e. Move right: Right arrow
 *              f. Move left: Left arrow
 *              g. Reset/Back to initial state: Esc
 *           User can perform zoom in function to a particular area by selecting rectangular area using mouse left click and then release.
 *
 *           For Array/EM data:
 *           If User want to view profile of an Array data or EM data, then an additional option, 'Display All' shall appear. User can select hide/display a
 *           particular chart by toggling the legend marker.
 *
 *
 *  @author Rachana Kapoor
 *  @date   ‎February ‎23, ‎2022
 ***********************************************/

#ifndef CHANNELMENUOPERATION_H
#define CHANNELMENUOPERATION_H

#include <QObject>
#include <QMenu>
#include <QTableWidget>
#include <QDialog>
#include <QSplitter>
#include<QGraphicsSimpleTextItem>
#include <QtCharts/QChartView>
#include <QtCharts/QtCharts>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QGraphicsScene>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCore/qglobal.h>
#include <QtWidgets/QGraphicsTextItem>
#include "MouseEventTracking.h"
#include <QGridLayout>
#include <QThread>
#include "ChanMenuOpData.h"
#include "TypeDefConst.h"

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE
class TableViewOperation;

class ChannelMenuOperation:public QObject
{
    Q_OBJECT
public:

    ///default constructor
    ChannelMenuOperation(QObject *parent = nullptr);

    /// Class destructor
    ~ChannelMenuOperation();

    /// @brief static function
    ///
    /// @return   Instance of class TableViewOperation
    ///
    static ChannelMenuOperation* getInstance();

    void setChanMenuOpData(QWidget  *widget);





public slots:
    /// @brief This function display all the available channels that are not currently
    /// being displayed in the table window. This function also gives an option to unhide
    /// the channel.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void listChan(QWidget  *widget);

    /// @brief This function creates a new channel in the database.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void newChan(QWidget *table);

    /// @brief This function deletes a selected channel from database.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void deleteChan(QWidget  *widget);

    /// @brief This function copies the data of selected channel and creates a
    /// new channel with the copied data.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void copyCreate(QWidget  *widget);

    /// @brief This function hides the selected channel.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void hide(QWidget  *widget);

    /// @brief This function hides all the channels.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void hideAll(QWidget  *widget);

    /// @brief This function displays all the hidden channels.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void displayAll(QWidget  *widget);

    /// @brief This function displays all the hidden channels.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void displayProfile(QWidget  *widget);

    /// @brief This function enables/diable the editing of channels.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void editChannel(QWidget  *widget);

    /// @brief This function enables/diable the editing of channels.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void arrayViewChannel(QWidget  *widget);

    /// @brief This function displays all the hidden channels.
    /// @return  list-List of channels for displaying profile
    ///
    QList <int> getChannelList();
    QList <int> getEditChannelList();

    void find(QWidget  *widget);
    ChanMenuOpData *getChanMenuOpData();

    QThread *getThread();

    void updateTrackers(int pos, int index);

signals:
    void allZoomIn();
    void allZoomOut();
    void allZoomReset();
    void allEnableTrackLine();
    void allRubberBandChanged(QChartView *view, QPointF fp, QPointF tp, int startIndex, int endIndex, bool isYMode);
    void allAdjustTrackLine();
    void allScroll(double dx, double dy);
    void allGoBack();
    void allZoomInX(qreal factor, qreal xcenter);
    void allSetXYRanges(qreal xmin, qreal xmax);
    void reAlignXAxisSig();

private:

    void setProfile(TableViewOperation* table, int selectColNo, bool isArrayChan);

    //void setChanProfile(TableView* table, int selectColNo);


    //void setArrayProfile(TableViewOperation* table, int selectColNo);

    void displayHideSeries();


    void updateGlbalXAxisRanges(QSplitter *splitter, updateType type);
    //QSplitter *mpHlayout = nullptr;

    void findAdjustChartsLMargin(QSplitter *splitter);

    void adjustChartsLeftMargins(QSplitter *splitter);

    void reAlignXAxis();

    int mProfileCount;

    QList <int> mChannelList;

    QList <int> mEditChannelList;

    QValueAxis *mAxisX;

    QMap<QPushButton *, QWidget *> mButton2Widget;

    int count = 0;

    QString mSerachValue;

    bool mNoWhereFound = true;

    int matchIndex = 0;

    bool thereIsFindVal = false;

    //std::vector<QLineSeries *> mLineSeries;

    std::vector<QLineSeries *> mLineSeries;

//    std::vector<double> yMaxs;
//    std::vector<double> yMins;
    std::vector<double> xGLobalMaxs;
    std::vector<double> xGLobalMins;

    bool mNeedUpdate = true;

    ChanMenuOpData *mChartLoadData = nullptr;
    QThread* mThread = nullptr;

    double mBestChartLeft;

    QSplitter *mParentSplit;

    int mChartViewCount = 0;

private slots:
    void adjustTrackLine(MouseEventTracking *);

    void searchChanValue(QString chanVal, QWidget *);

    void allZoomInAlongX(qreal factor, qreal xcenter);



};

#endif // CHANNELMENUOPERATION_H
