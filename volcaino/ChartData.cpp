#include "ChartData.h"

ChartData::ChartData()
{
    mShowLegend = true;
    layout()->setContentsMargins(0, 0, 0, 0);
    setBackgroundRoundness(0);
}

ChartData::~ChartData()
{
}

void ChartData::populateSingleChannelDataFromTable(TableViewOperation *table, int selectColNo)
{
    this->grabGesture(Qt::PanGesture);
    this->grabGesture(Qt::PinchGesture);
    this->legend()->hide();
    mSeries = new QLineSeries;
    mSeries->setUseOpenGL(true);

    QAbstractItemModel *tableDataModel = table->model();
    int columnNo = selectColNo;
    int rowCount = tableDataModel->rowCount();
    mNullCounts = 0;
    mNotNullCount = 0;

    for (int row = 0; row < rowCount; row++) {
        if (!tableDataModel->data(tableDataModel->index(row, columnNo)).isNull()) {
            if (mNotNullCount <= 2)
                mNotNullCount++;
            mSeries->append(row, tableDataModel->data(tableDataModel->index(row, columnNo)).toReal());
        } else {
            if (mNotNullCount == 1)
                mNullCounts++;
        }
    }

    this->addSeries(mSeries);
    setIniAxis();
}

void ChartData::populateSingleChannelDataFromQuery(QString qsQuery)
{
    this->grabGesture(Qt::PanGesture);
    this->grabGesture(Qt::PinchGesture);
    this->legend()->hide();
    mSeries = new QLineSeries;
    mSeries->setUseOpenGL(true);

    QSqlQuery mSql = DataBase::getInstance()->getSqlQuery();
    if (!mSql.exec(qsQuery))
        return;

    mNormalChanData.clear();
    int count = 0;
    while (mSql.next()) {
        bool bVal = mSql.value(0).isNull();
        if (!bVal) {
            mSeries->append(count, mSql.value(0).toReal());
            mNormalChanData.push_back(mSql.value(0).toReal());
        } else {
            mNormalChanData.push_back(nullValue);
        }
        count++;
    }
    this->addSeries(mSeries);
    setIniAxis();
}

void ChartData::populateArrayData(ChanMenuOpData *opData, int arraySize, int chanIndex,int FromX,int ToX)
{
    mArrayChanData = opData->getArrayData()[chanIndex];
    mValidArrayChanData = opData->getValidArrayData()[chanIndex];

    this->grabGesture(Qt::PanGesture);
    this->grabGesture(Qt::PinchGesture);
    if (arraySize > 50)
        mShowLegend = false;

    if (mShowLegend) {
        this->legend()->show();
    } else {
        this->legend()->hide();
    }

    mLineSeries.clear();
    mLineSeries.resize(arraySize);

    for (int arrayNo = 0; arrayNo < arraySize; arrayNo++) {
        mLineSeries[arrayNo] = new QLineSeries;
        bool isFirstValidRow = false;
        for (int row = FromX; row < ToX; row++) {
            if(opData->getValidArrayData()[chanIndex][arrayNo][row])
            {
                isFirstValidRow = true;
                mLineSeries[arrayNo]->append(row, opData->getArrayData()[chanIndex][arrayNo][row]);
            }
            if(!isFirstValidRow)
                mLineSeries[arrayNo]->append(row,0);
        }

        if (mShowLegend)
            mLineSeries[arrayNo]->setName(QString::number(arrayNo+1));
        mLineSeries[arrayNo]->setUseOpenGL(true);

        this->addSeries(mLineSeries[arrayNo]);
    }
    setIniAxis();
}

void ChartData::populateSeriesUsingAnotherChart(ChartData* chart)
{
    auto iterB = chart->getArrayLineSeries().begin();
    auto iterE = chart->getArrayLineSeries().end();
    for (;iterB != iterE; iterB++) {
        this->addSeries(*iterB);
    }
    setIniAxis();
}

void ChartData::setIniAxis()
{
    this->createDefaultAxes();
    this->setAcceptHoverEvents(true);

    dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->setTickCount(3);
}

void ChartData::saveAxisIniRanges()
{
    iniXMin = dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->min();
    iniXMax = dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->max();

    iniYMin = dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->min();
    iniYMax = dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->max();

    mYmin.push_back(iniYMin);
    mYmax.push_back(iniYMax);
}

void ChartData::saveAxisIniRangesPLM()
{

    iniXMin = dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->min();
    iniXMax = dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->max();

    iniYMin = dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->min();
    iniYMax = dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->max();

    mYmin.push_back(iniYMin);
    mYmax.push_back(iniYMax);

    mXmin.push_back(iniXMin);
    mXmax.push_back(iniXMax);
}

void ChartData::setAxisIniRanges()
{
    this->axes(Qt::Horizontal).back()->setRange(iniXMin, iniXMax);
    this->axes(Qt::Vertical).back()->setRange(iniYMin, iniYMax);
}

void ChartData::setXAxisRanges(double xMin, double xMax)
{
    this->axes(Qt::Horizontal).back()->setRange(xMin, xMax);
}

void ChartData::getXAxisRanges(double &xMin, double &xMax)
{
    xMin = dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->min();
    xMax = dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->max();
}

void ChartData::setYAxisRanges()
{
    this->axes(Qt::Vertical).back()->setRange(mYmin.back(), mYmax.back());
}

void ChartData::setXAxisRanges()
{
    this->axes(Qt::Horizontal).back()->setRange(mXmin.back(), mXmax.back());

}

//void ChartData::setIniAxisRanges(double xMin, double xMax)
//{
//    this->axes(Qt::Horizontal).back()->setRange(xMin, xMax);

//    xMaxs.pop_back();
//    xMins.pop_back();

//    xMaxs.push_back(dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->max());
//    xMins.push_back(dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->min());
//}

void ChartData::setLineseries(QLineSeries *series)
{
    mSeries = series;
}

int ChartData::xHistoryCount()
{
    return mXmin.size();
}

int ChartData::yHistoryCount()
{
    return mYmin.size();
}

void ChartData::getYAxisRanges(updateType type)
{
    if (type == updateType::reset) {
        while (mYmin.size() > 1) {
            mYmin.pop_back();
            mYmax.pop_back();
        }
    } else if (type == updateType::update) {
        mYmax.push_back(dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->max());
        mYmin.push_back(dynamic_cast<QValueAxis *>(this->axes(Qt::Vertical).back())->min());
    } else if (type == updateType::revert) {
        if (mYmin.size() > 1) {
            mYmin.pop_back();
            mYmax.pop_back();
        }
    }
}

void ChartData::getXAxisRanges(updateType type)
{
    if (type == updateType::reset) {
        while (mXmin.size() > 1) {
            mXmin.pop_back();
            mXmax.pop_back();
        }
    } else if (type == updateType::update) {
        mXmax.push_back(dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->max());
        mXmin.push_back(dynamic_cast<QValueAxis *>(this->axes(Qt::Horizontal).back())->min());
    } else if (type == updateType::revert) {
        if (mXmin.size() > 1) {
            mXmin.pop_back();
            mXmax.pop_back();
        }
    }
}

QLineSeries *ChartData::getLineSeries()
{
    return mSeries;
}

QSplineSeries *ChartData::getSplineSeries()
{
    return mSpline;
}

std::vector<double> &ChartData::getNormalChanData()
{
    return mNormalChanData;
}

std::vector<std::vector<bool>> &ChartData::getValidArrayChanData()
{
    return mValidArrayChanData;
}

std::vector<std::vector<double>> &ChartData::getArrayChanData()
{
    return mArrayChanData;
}

void ChartData::setSplineSeries(QSplineSeries *series)
{
    mSpline = series;
}
//std::vector<double> ChartData::getYMaxs()
//{
//    return yMaxs;
//}

//std::vector<double> ChartData::getYMins()
//{
//    return yMins;
//}

//std::vector<double> ChartData::getXMaxs()
//{
//    return xMaxs;
//}

//std::vector<double> ChartData::getXMins()
//{
//    return xMins;
//}

