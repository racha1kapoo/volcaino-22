/**
 *  @file   ChartData.h
 *  @brief  Handles chart data for chart viewers
 *  @author Shuo Li, Rahul Poddaturi
 *  @date   Nov 8, ‎2022
 ***********************************************/
#ifndef CHARTDATA_H
#define CHARTDATA_H
#include <QtCharts/QChart>
#include "TableViewOperation.h"
#include <QLineSeries>
#include "ChanMenuOpData.h"
#include "TypeDefConst.h"
class ChartData: public QtCharts::QChart
{
    Q_OBJECT
public:
    /// Class constructor
    ChartData();
    /// Destructor
    ~ChartData();
    /// @brief This function populates single channel data from table
    /// @param table: Pointer to TableViewOperation
    /// @param selectColNo: Selected col number
    void populateSingleChannelDataFromTable(TableViewOperation* table = nullptr,
                                            int selectColNo = -1);

    /// @brief This function populates single channel data from data base query
    /// @param qsQuery: Data base query
    void populateSingleChannelDataFromQuery(QString qsQuery);
    /// @brief This function populates array channel data from ChanMenuOpData
    /// @param opData: Pointer to ChanMenuOpData
    /// @param arraySize: Array size
    /// @param chanIndex: Channel index
    /// @param FromX: Starting row
    /// @param ToX: Ending row
    void populateArrayData(ChanMenuOpData *opData, int arraySize, int chanIndex,int FromX,int ToX);
    /// @brief This function populates series using another chart
    /// @param chart: Pointer to another chart
    void populateSeriesUsingAnotherChart(ChartData* chart);
    /// @brief This function sets if a chart displays legends
    /// @param bVal: Whether to display legends
    void setShowLegend(bool bVal) {mShowLegend = bVal;}

    //void saveAxisRanges();
    /// @brief This function initializes chart axis
    void setIniAxis();
    /// @brief This function saves axis initial ranges
    void saveAxisIniRanges();
    /// @brief This function saves axis initial ranges for PLM
    void saveAxisIniRangesPLM();
    /// @brief This function sets axis initial ranges
    void setAxisIniRanges();

    /// @brief This function sets x-axis ranges
    void setXAxisRanges(double xMin, double xMax);

    /// @brief This function gets x-axis ranges
    void getXAxisRanges(double &xMin, double &xMax);

    /// @brief This function sets y-axis ranges
    void setYAxisRanges();

    /// @brief This function sets x-axis ranges
    void setXAxisRanges();
    //    enum updateType
    //    {
    //        update,
    //        revert,
    //        reset,
    //    };
    /// @brief This function gets new y-axis ranges when update required
    /// @param type: Update type
    void getYAxisRanges(updateType type);

    /// @brief This function gets new x-axis ranges when update required
    /// @param type: Update type
    void getXAxisRanges(updateType type);

    //void setIniAxisRanges(double xMin, double xMax);
    QLineSeries *getLineSeries();
    QSplineSeries* getSplineSeries();

    /// @brief This function gets array channel viewer line series
    /// @return Line series
    std::vector<QLineSeries *> getArrayLineSeries(){return mLineSeries;}

    std::vector<double> getYMaxs();
    std::vector<double> getYMins();

    /// @brief This function gets normal chan data
    /// @return Normal chan data
    std::vector<double> &getNormalChanData();

    /// @brief This function gets array channel data flags
    /// @return Array channel data flags
    std::vector<std::vector<bool>> &getValidArrayChanData();

    /// @brief This function gets array channel data
    /// @return Array channel data
    std::vector<std::vector<double>> &getArrayChanData();

    //    std::vector<double> getXMaxs();
    //    std::vector<double> getXMins();

    /// @brief This function sets spline series
    /// @param series: Pointer to spline series
    void setSplineSeries(QSplineSeries *series);

    /// @brief This function sets line series
    /// @param series: Pointer to line series
    void setLineseries(QLineSeries *series);
    int mNullCounts = 0;
    /// @brief This function returns count of x-axis ranges history
    /// @return Count of x-axis ranges history
    int xHistoryCount();
    /// @brief This function returns count of y-axis ranges history
    /// @return Count of y-axis ranges history
    int yHistoryCount();

private:
    int mNotNullCount = 0;
    QLineSeries *mSeries = nullptr;
    QSplineSeries *mSpline = nullptr;

    std::vector<QLineSeries *> mLineSeries;
    std::vector<double> mNormalChanData;
    std::vector<std::vector<double>> mArrayChanData;
    std::vector<std::vector<bool>> mValidArrayChanData;

    bool mShowLegend;

    double iniXMin;
    double iniXMax;
    double iniYMin;
    double iniYMax;

    std::vector<double> mYmin;
    std::vector<double> mYmax;
    std::vector<double> mXmin;
    std::vector<double> mXmax;
};

#endif // CHARTDATA_H
