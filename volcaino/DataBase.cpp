#include "DataBase.h"
#include "mainwindow.h"
#include <QSqlDriver>
#include <QDebug>
#include <QSqlError>
#include <QDir>
#include <QThread>

#define cDataBase "VOLCAINO.db"

#define ERROR_FAILED_QUERY(qsql) qDebug() << __FILE__ << __LINE__ << "FAILED QUERY [" \
    << (qsql).lastQuery() << "]" << (qsql).lastError()

DataBase::DataBase()
    : mpDb(nullptr), mSql(nullptr)
{
    this->createDatabase("MainThreadConnection");
}

DataBase::DataBase(QString str)
    : mpDb(nullptr), mSql(nullptr)
{
    this->createDatabase(str);
}

DataBase::~DataBase()
{
    this->closeDatabase();
}

DataBase* DataBase::getInstance()
{
    static DataBase theInstance;
    return &theInstance;
}

void DataBase::createDatabase(const QString str)
{
    mpDb = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE", str));
    mpDb->setHostName("localhost");
    mpDb->setDatabaseName(this->setDatabase());

    DEBUG_TRACE(mpDb->open());

    mSql = QSqlQuery("", QSqlDatabase::database(str));
}

QString DataBase::setDatabase()
{
    QDir dataBaseDir;
    dataBaseDir.setPath(QDir::currentPath());
    //dataBaseDir.cdUp();
    //QString dbFile=dataBaseDir.path()+cDataBase;
    QString dbFile = cDataBase;

    DEBUG_TRACE(dbFile);

    return dbFile;
}

bool DataBase::openDatabase()
{
    if (!mpDb->isOpen())
        mpDb->open();

    return mpDb->isOpen();
}

void DataBase::closeDatabase()
{
    if (mpDb->isOpen())
        mpDb->close();
}

void DataBase::startTransaction()
{
    QString transaction = "BEGIN TRANSACTION;";
    DEBUG_TRACE(transaction);
    if (!mSql.exec(transaction))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::closeTransaction()
{
    QString transaction = "COMMIT;";
    DEBUG_TRACE(transaction);
    if (!mSql.exec(transaction))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::insertRecord(const QString& record, const QString& table){

    QString query="INSERT INTO "+table+" VALUES("+record+");";
    QString queryn = "INSERT INTO "+table+" VALUES(NULL);";
    //DEBUG_TRACE(query);

    if(record == "NAN"){
        if(!mSql.exec(queryn))
            ERROR_FAILED_QUERY(mSql);
    }else{
        if(!mSql.exec(query))
            ERROR_FAILED_QUERY(mSql);

    }
}

void DataBase::createTable(const QString &tableName, const QStringList &columnList){

    QString stmt = "CREATE TABLE "+tableName+"("; //CREATE TABLE TMP(NEWCHAN);
    for(int i=0;i<columnList.size();++i)
        stmt.append(columnList[i]);

    stmt.chop(1);
    stmt.append(");");

    DEBUG_TRACE(stmt);

    if(!mSql.exec(stmt))
        ERROR_FAILED_QUERY(mSql);
}

QStringList DataBase::getTableList(){

    QStringList list;
    QString query="SELECT name FROM sqlite_master WHERE type='table' order by name;";

    DEBUG_TRACE(query);

    if(!mSql.exec(query)){

        ERROR_FAILED_QUERY(mSql);
    }
    else{
        while (mSql.next()) {
            list.append(mSql.value(0).toString());
        }
    }
    return list;
}

void DataBase::addColumn(const QString &table,const QString &column,const QString &type,const bool &nullFlag,const QString defValue){

    QString nullNotNull="NOT NULL";
    if(!nullFlag)nullNotNull="NULL";

    QString query="ALTER TABLE "+table+" ADD COLUMN "+column+" "+type;//+ " DEFAULT "+defValue+" "+nullNotNull; this was removed to allow blanks
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::deleteColumn(const QString &table,const QString &column){

    QString query="ALTER TABLE "+table+" DROP COLUMN "+column+";";
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::copyColumn(const QString &table,const QString &column,const QString &type,const bool &nullFlag,const QString defValue, const QString columnToCopy){

    this->addColumn(table,column,type,nullFlag,defValue);

    QString query="UPDATE "+table+ " SET "+column+" = "+columnToCopy;
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::copyColumn(const QString &table,const QString &column, const QString columnToCopy){


    QString query="UPDATE "+table+ " SET "+column+" = "+columnToCopy;
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

std::map<QString,SqliteColumninfo> DataBase::getDbTableInfo(const QString &table){
    std::map<QString,SqliteColumninfo> map;
    QString query="SELECT * FROM PRAGMA_TABLE_INFO('"+table+"');";
    if(mSql.exec(query)){
        while (mSql.next()) {
            SqliteColumninfo colInfo;
            colInfo.columnId=mSql.value(0).toInt();
            colInfo.columnName=mSql.value(1).toString().toUpper();
            colInfo.type=mSql.value(2).toString().toUpper();
            colInfo.NotNull=mSql.value(3).toInt();
            colInfo.defaultVal=mSql.value(4).toString().toUpper();
            colInfo.pKey=mSql.value(5).toInt();

            map.insert(std::pair<QString,SqliteColumninfo>(colInfo.columnName,colInfo));
        }
    }
    return map;
}

QStringList DataBase::getHeaderDetails(QString table){
    QString query="SELECT * FROM PRAGMA_TABLE_INFO('"+table+"');";
    QStringList columnNames;
    if(mSql.exec(query)){
        while (mSql.next()) {
            columnNames.append(mSql.value(1).toString());

        }
    }
    return columnNames;
}

QSqlQuery DataBase::getSqlQuery(){
    return mSql;
}

QStringList DataBase::getFlightNo(QString table){
    QString query="SELECT DISTINCT flightNo FROM '"+table+"';";
    QStringList flightNo;
    if(mSql.exec(query)){
        mSql.next();
        flightNo.append(mSql.value(0).toString());
        DEBUG_TRACE(flightNo);
    }
    return flightNo;
}

QStringList DataBase::getFlightDate(QString table){
    QString query="SELECT DISTINCT flightDate FROM '"+table+"';";
    QStringList flightDate;
    if(mSql.exec(query)){
        mSql.next();
        flightDate.append(mSql.value(0).toString());
    }
    return flightDate;
}

std::map<QString,QString> DataBase::getLineTypeLineNo(const QString &table){
    std::map<QString,QString> map;
    QStringList lineType;
    QString query="SELECT DISTINCT LineType FROM '"+table+"';";
    if(mSql.exec(query)){
        while (mSql.next()) {
            lineType.append(mSql.value(0).toString());
        }
    }
    for(auto type:lineType){
        query="SELECT DISTINCT LineNo FROM '"+table+"' where LineType='"+type+"';";
        if(mSql.exec(query)){
            while (mSql.next()) {
                map.insert(std::pair<QString,QString>(mSql.value(0).toString(),type));
            }
        }
    }
    return map;
}
 int DataBase::getRowCount(QString table){
    QString query="SELECT COUNT(*) FROM '"+table+"';";
    int count=0;
    if(mSql.exec(query)){
        mSql.next();
        count=mSql.value(0).toInt();
    }
    return count;
}


 QStringList DataBase::getColumnData(const QString &table,const QString &column){
     QString query="SELECT "+column+" FROM '"+table+"';";
     mSql.exec(query);
     QStringList data;
     while( mSql.next()){
         data.append(mSql.value(0).toString());
     }
     return data;
 }
 void DataBase::executeQuery(const QString& query){

     if(!mSql.exec(query))
         ERROR_FAILED_QUERY(mSql);
 }
