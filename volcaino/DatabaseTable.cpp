#include "DatabaseTable.h"
#include "ui_DatabaseTable.h"
#include "DataBase.h"
#include <QTableWidget>
#include <QPushButton>
#include <QFileDialog>
#include <QThread>
#include "ImportFile.h"
#include "mainwindow.h"
#include <QSqlQuery>
#include <QSqlDatabase>

DatabaseTable::DatabaseTable(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatabaseTable),
    mTable(""),
    mFile("")
{
    ui->setupUi(this);

    connect(this,&DatabaseTable::setTable,ImportFile::getInstance(),&ImportFile::setTableName);
    connect(this,&DatabaseTable::setFile,ImportFile::getInstance(),&ImportFile::setFile);

}
DatabaseTable* DatabaseTable::getInstance(){

    static DatabaseTable theInstance;
    return &theInstance;

}
DatabaseTable::~DatabaseTable()
{
    delete ui;
}

void DatabaseTable::populateTableList(){
    this->show();
    QStringList list;
    auto db =DataBase::getInstance();
    if(db->openDatabase()){
        list=db->getTableList();
    }
    ui->tableWidget->setRowCount(list.size());
    ui->tableWidget->setColumnCount(1);
    for (int i=0;i<list.size();++i ) {
        ui->tableWidget->setItem(i,0,new QTableWidgetItem(list.at(i)));
    }

}

void DatabaseTable::selectCSVFile(){

    QString filename = QFileDialog::getOpenFileName(
                NULL,
                "Open Document",
                QDir().dirName().append("../"),
                "Document files (*.csv)");

    if(!filename.isNull()){
        mFile=filename;
        this->populateTableList();
    }

}

void DatabaseTable::selectXYZFile(){

    QString filename =  QFileDialog::getOpenFileName(
                NULL,
                "Open Document",
                QDir().dirName().append("../"),
                "Document files (*.XYZ)");

    if(!filename.isNull()){
        mFile=filename;
        this->populateTableList();
    }

}

void DatabaseTable::selectNUVFile(){

    QString filename =  QFileDialog::getOpenFileName(
                NULL,
                "Open Document",
                QDir().dirName().append("../"),
                "Document files (*.NUV;*.PEI;*.NDI)");

    if(!filename.isNull()){
        mFile=filename;
        this->populateTableList();
    }

}

void DatabaseTable::selectGBNFile(){

    QString filename =  QFileDialog::getOpenFileName(
                NULL,
                "Open Document",
                QDir().dirName().append("../"),
                "Document files (*.GBN)");

    if(!filename.isNull()){
        mFile=filename;
        this->populateTableList();
    }

}

void DatabaseTable::setTableName(QString table)
{
    DEBUG_TRACE(table);

    if(table!="")
       // if (table!=mTable)
        {
            mTable=table;
            emit setTable(mTable);
            emit setFile(mFile);
        }
}


void DatabaseTable::on_pushButton_Cancel_clicked()
{
    QString table="";
    table = mFile.split('/').last();
    table.chop(4);
    table.remove(" ");
    this->close();
    this->setTableName(table);

}


void DatabaseTable::on_tableWidget_cellClicked(int row, int column)
{
    QString table=ui->tableWidget->currentIndex().data().toString();
    this->close();
    this->setTableName(table);
}


