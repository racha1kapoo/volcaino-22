/**
 *  @file   DatabaseTable.h
 *  @brief  Data view class for Database tables
 *  @author Rachana Kapoor
 *  @date   Dec 14, 2021
 ***********************************************/

#ifndef DATABASETABLE_H
#define DATABASETABLE_H

#include <QDialog>
#include "TypeDefConst.h"
namespace Ui {
class DatabaseTable;
}

class DatabaseTable : public QDialog
{
    Q_OBJECT

public:
    ///Class constructor
    explicit DatabaseTable(QWidget *parent = nullptr);

    /// Class destructor
    ~DatabaseTable();

    /// @brief static function
    ///
    /// @return   Instance of class DatabaseTable
    ///
    static DatabaseTable* getInstance();

private slots:
    /// @brief This function closes the GUI when "CANCEL" button is pressed.
    /// @return  void
    ///
    void on_pushButton_Cancel_clicked();

    /// @brief This function sets the current row as a table name when selected.
    /// @param row: Numeric value of the selected row
    /// @param column: Numeric value of the selected column
    /// @return  void
    ///
    void on_tableWidget_cellClicked(int row, int column);

    /// @brief This function opens a file brower to select a ".csv" formatted file.
    /// @return  void
    ///
    void selectCSVFile();

    /// @brief This function opens a file brower to select a ".XYZ" formatted file.
    /// @return  void
    ///
    void selectXYZFile();


    /// @brief This function opens a file brower to select a ".NUV" formatted file.
    /// @return  void
    ///
    void selectNUVFile();

    /// @brief This function opens a file brower to select a ".GBN" formatted file.
    /// @return  void
    ///
    void selectGBNFile();

    /// @brief This function gets the list of tables from the database
    /// and populate the data in the table widget (GUI)
    /// @return  void
    ///
    void populateTableList();

signals:
    /// @brief This signal is emmitted when the table name is set.
    /// @param table: Name of the table
    /// @return  void
    ///
    void setTable(QString);

    /// @brief This signal is emmitted when the file name is set.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setFile(QString);

private:


    /// @brief This function formats the table name as per the database valid format.
    /// @param table: Name of the table
    /// @return  void
    ///
    void setTableName(QString table);

    Ui::DatabaseTable *ui;

    QString mTable;

    QString mFile;

};

#endif // DATABASETABLE_H
