#include "DesignManager.h"

DesignManager::DesignManager()
{
    pData = new ChanMenuOpData;
}
DesignManager::~DesignManager()
{
    if(pData)
        pData->deleteLater();
}

DesignManager* DesignManager::getInstance(){

    static DesignManager theInstance;
    return &theInstance;

}
void DesignManager::stopProcessing()
{
    if(mpThread!=nullptr)
        pData->setAbort(true);
}
void DesignManager::PopulateChanelData(QString qsTableName,std::map<int,QString> arrayList)
{
//    if(mpThread == nullptr)
//    {
//        if(pData)
        {
            delete pData;
            pData = NULL;
        }
        pData = new ChanMenuOpData;
        mpThread = new QThread;
        pData->moveToThread(mpThread);
        pData->setFetchDataModel(qsTableName,arrayList);
        connect(mpThread, &QThread::started, pData, &ChanMenuOpData::fetchData);
        connect(pData, &ChanMenuOpData::fetchDataDone, mpThread, &QThread::quit);
        connect(mpThread, &QThread::finished, this, [this](){delete mpThread;mpThread=nullptr;});
        mpThread->start();
//    }
}
ChanMenuOpData* DesignManager::GetArrayChannelData()
{
    return pData;
}
QThread* DesignManager::GetThread()
{
    return mpThread;
}
