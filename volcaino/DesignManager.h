
/**
 *  @file   DesignManager.h
 *  @brief  Utility Class to hold required Objects
 *  @author Rahul Poddaturi
 *  @date   2022-10-04
 ***********************************************/
#ifndef DESIGNMANAGER_H
#define DESIGNMANAGER_H

#include <QObject>
#include <QThread>
#include "ChanMenuOpData.h"

class DesignManager:public QObject
{
    Q_OBJECT
public:


    ///default constructor
    DesignManager();

    //DesignManager(QString str);

    /// Class destructor
    ~DesignManager();

    /// @brief static function
    ///
    /// @return   Instance of class Database
    ///
    static DesignManager* getInstance();


    void PopulateChanelData(QString qsTableName,std::map<int,QString> arrayList);
    void stopProcessing();

    ChanMenuOpData* GetArrayChannelData();
    QThread*    GetThread();
  private:
    ChanMenuOpData* pData;
    QThread* mpThread;
};

#endif // DESIGNMANAGER_H
