#include "ExportGBN.h"
#include <cmath>
#include <QDateTime>
ExportGBN::ExportGBN(QObject *parent, QString tableName, QString expFilePath):
    QObject(parent), mFilePath(expFilePath), mTableName(tableName)
{
}

ExportGBN::~ExportGBN()
{
    //DataBase::getInstance()->closeDatabase();
}

bool ExportGBN::thereIsDuplicatedLines()
{
    return mThereIsDupLines;
}

void ExportGBN::startExportGBN()
{
    emit exportGBNProgress("Exporting GBN file...", 0);
    emit exportGBNProgress("Exporting GBN file...", 10);
    if (configureDB()) {
        getFidParms();
        getDiffLineRows();
    }

    if (!mFilePath.isEmpty()) {
        //        mFilePath += "/"+mTableName+".gbn";
        mGBNFile.setFileName(mFilePath);
        if (mGBNFile.open(QIODevice::WriteOnly)) {
            writeFirst17Chars();
            mGBNFile.write(&mSubChar, 1);

            //int time1 = QDateTime::currentMSecsSinceEpoch();
            exportDBChanForGBN();
            //int time2 = QDateTime::currentMSecsSinceEpoch();
            //qDebug() << "exportDBChanForGBN took " << time2-time1;

            emit exportGBNProgress("Exporting data records...", 20);
            //int time3 = QDateTime::currentMSecsSinceEpoch();
            readDataFromLines();
            //int time4 = QDateTime::currentMSecsSinceEpoch();

            char end = 0x00;
            mGBNFile.write(&end, 1);
            mGBNFile.close();
            emit exportGBNProgress("Export GBN file done", 100);

            //DataBase::getInstance()->closeDatabase();
            //qDebug() << "Write GBN done";
        } else {
            mErrMsg += mGBNFile.errorString() + "\n";
            mGBNFile.close();
            //DataBase::getInstance()->closeDatabase();
        }
    } else {
        mErrMsg += "Invalid file path\n";
        //DataBase::getInstance()->closeDatabase();
    }
}

void ExportGBN::writeFirst17Chars()
{
    QTextStream stream(&mGBNFile);
    stream << "OASIS BINARY DATABASE";
    stream << "\n";
}

void ExportGBN::exportChanRec(bool isArrChan, QString chanName, long chanParms[5])
{
    QString channelName = chanName;

    long channelType = chanParms[0];
    long arrayDepth = chanParms[1];
    long displayFormat = chanParms[2];
    long displayWidth = chanParms[3];
    long displayDecimals = chanParms[4];

    if (isArrChan) {
        char binaryExport[85] = {0};
        binaryExport[0] = 0x04;
        char binaryChanBuffName[64] = {0};
        memcpy(binaryChanBuffName, channelName.toUtf8().data(), channelName.length());
        memcpy(&binaryExport[1], binaryChanBuffName, 64);

        unsigned char binaryChanBuffData[4];
        memcpy(binaryChanBuffData, &channelType, 4);
        memcpy(&binaryExport[65], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &arrayDepth, 4);
        memcpy(&binaryExport[69], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &displayFormat, 4);
        memcpy(&binaryExport[73], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &displayWidth, 4);
        memcpy(&binaryExport[77], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &displayDecimals, 4);
        memcpy(&binaryExport[81], reinterpret_cast<char*>(binaryChanBuffData), 4);

        mGBNFile.write(binaryExport, 85);
    } else {
        char binaryExport[81] = {0};
        binaryExport[0] = 0x01;
        char binaryChanBuffName[64] = {0};
        memcpy(binaryChanBuffName, channelName.toUtf8().data(), channelName.length());
        memcpy(&binaryExport[1], binaryChanBuffName, 64);

        unsigned char binaryChanBuffData[4];

        if (channelType == GS_UNSIGNED_BYTE) {
            long inputType = -mBuffSizeRatio;
            memcpy(binaryChanBuffData, &inputType, 4);
        } else {
            memcpy(binaryChanBuffData, &channelType, 4);
        }
        memcpy(&binaryExport[65], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &displayFormat, 4);
        memcpy(&binaryExport[69], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &displayWidth, 4);
        memcpy(&binaryExport[73], reinterpret_cast<char*>(binaryChanBuffData), 4);

        memcpy(binaryChanBuffData, &displayDecimals, 4);
        memcpy(&binaryExport[77], reinterpret_cast<char*>(binaryChanBuffData), 4);

        mGBNFile.write(binaryExport, 81);
    }
}

template<class T>
void ExportGBN::exportDataRecNum(QList<long> dataParms, QList<float> fidParms, const T *data)
{
    long channelNumber = dataParms[0];
    int32_t binaryType = dataParms[1];
    double fidStart = fidParms[0];
    double fidIncrement = fidParms[1];
    long length = dataParms[2];

    unsigned char chanBuffLong[4];
    unsigned char chanBuffDouble[8];
    unsigned char *chanData;

    //    if(binaryType == GS_UNSIGNED_BYTE)
    //    {
    //        chanData = new unsigned char[length*mBuffSizeRatio*chanTypeSize(binaryType)]();
    //    }
    //    else
    {
        chanData = new unsigned char[length*chanTypeSize(binaryType)];
    }

    char binaryExport[29] = {0};
    binaryExport[0] = 0x03;
    memcpy(chanBuffLong, &channelNumber, 4);
    memcpy(&binaryExport[1], reinterpret_cast<char*>(chanBuffLong), 4);


    //    if(binaryType == GS_UNSIGNED_BYTE)
    //    {
    //        long inputType = -mBuffSizeRatio;
    //        memcpy(chanBuffLong, &inputType, 4);
    //    }
    //    else
    {
        memcpy(chanBuffLong, &binaryType, 4);
    }
    memcpy(&binaryExport[5], reinterpret_cast<char*>(chanBuffLong), 4);


    memcpy(chanBuffDouble, &fidStart, 8);
    memcpy(&binaryExport[9], reinterpret_cast<char*>(chanBuffDouble), 8);

    memcpy(chanBuffDouble, &fidIncrement, 8);
    memcpy(&binaryExport[17], reinterpret_cast<char*>(chanBuffDouble), 8);

    memcpy(chanBuffLong, &length, 4);
    memcpy(&binaryExport[25], reinterpret_cast<char*>(chanBuffLong), 4);
    mGBNFile.write(binaryExport, 29);

    //    if(binaryType == GS_UNSIGNED_BYTE)
    //    {
    //        int chanDataIndex = 0;
    //        for(int i = 0; i < length; i++)
    //        {

    //            //unsigned char val = 49;
    //            unsigned char val[2] = {'a','b'};
    //            memcpy(&chanData[chanDataIndex], val, 2);
    //            chanData[chanDataIndex+2] = '\0';
    //            chanDataIndex += mBuffSizeRatio;
    //        }
    //        mGBNFile.write(reinterpret_cast<char*>(chanData), length*mBuffSizeRatio*chanTypeSize(binaryType));
    //    }
    //    else
    {
        memcpy(chanData, data, length*chanTypeSize(binaryType));
        mGBNFile.write(reinterpret_cast<char*>(chanData), length*chanTypeSize(binaryType));

    }

    delete []chanData;
}

void ExportGBN::exportDataRecAscii(QList<long> dataParms, QList<float> fidParms, QStringList data)
{
    long channelNumber = dataParms[0];
    int32_t binaryType = GS_UNSIGNED_BYTE;
    double fidStart = 0;
    double fidIncrement = 1;
    long length = dataParms[2];

    unsigned char chanBuffLong[4];
    unsigned char chanBuffDouble[8];
    unsigned char *chanData;

    chanData = new unsigned char[length*mBuffSizeRatio]();

    char binaryExport[29] = {0};
    binaryExport[0] = 0x03;
    memcpy(chanBuffLong, &channelNumber, 4);
    memcpy(&binaryExport[1], reinterpret_cast<char*>(chanBuffLong), 4);

    long inputType = -mBuffSizeRatio;
    memcpy(chanBuffLong, &inputType, 4);

    memcpy(&binaryExport[5], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffDouble, &fidStart, 8);
    memcpy(&binaryExport[9], reinterpret_cast<char*>(chanBuffDouble), 8);

    memcpy(chanBuffDouble, &fidIncrement, 8);
    memcpy(&binaryExport[17], reinterpret_cast<char*>(chanBuffDouble), 8);

    memcpy(chanBuffLong, &length, 4);
    memcpy(&binaryExport[25], reinterpret_cast<char*>(chanBuffLong), 4);
    mGBNFile.write(binaryExport, 29);

    int chanDataIndex = 0;
    for (int i = 0; i < length; i++) {
        QString val = data[i];
        memcpy(&chanData[chanDataIndex], val.toStdString().c_str(), val.size());

        chanData[chanDataIndex+val.size()] = '\0';
        chanDataIndex += mBuffSizeRatio;
    }
    mGBNFile.write(reinterpret_cast<char*>(chanData), length*mBuffSizeRatio*chanTypeSize(binaryType));

    delete []chanData;
}

void ExportGBN::exportLineRec(long lineParms[7])
{
    long lineNumber = lineParms[0];
    long lineVersion = lineParms[1];
    long lineType = lineParms[2];
    long flight = lineParms[3];
    long year = lineParms[4];
    long month = lineParms[5];
    long day = lineParms[6];

    char binaryExport[29] = {0};
    binaryExport[0] = 0x02;

    unsigned char chanBuffLong[4];
    memcpy(chanBuffLong, &lineNumber, 4);
    memcpy(&binaryExport[1], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffLong, &lineVersion, 4);
    memcpy(&binaryExport[5], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffLong, &lineType, 4);
    memcpy(&binaryExport[9], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffLong, &flight, 4);
    memcpy(&binaryExport[13], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffLong, &year, 4);
    memcpy(&binaryExport[17], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffLong, &month, 4);
    memcpy(&binaryExport[21], reinterpret_cast<char*>(chanBuffLong), 4);

    memcpy(chanBuffLong, &day, 4);
    memcpy(&binaryExport[25], reinterpret_cast<char*>(chanBuffLong), 4);

    mGBNFile.write(binaryExport, 29);
}

uint8_t ExportGBN::chanTypeSize(int chanType)
{
    switch (chanType) {
    case(GS_BYTE):
        return 1;
    case(GS_USHORT):
        return 2;
    case(GS_SHORT):
        return 2;
    case(GS_LONG):
        return 4;
    case(GS_FLOAT):
        return 4;
    case(GS_DOUBLE):
        return 8;
    case(GS_UNSIGNED_BYTE):
        return 1;
    default:
        return 0;
    }
}

bool ExportGBN::configureDB()
{
    auto db = DataBase::getInstance();

    if (db->openDatabase()) {
        mSql = db->getSqlQuery();
        mChanList = db->getHeaderDetails(mTableName);
        //mChanInfos = db->getDbTableInfo(mTableName);
        mRowCount = db->getRowCount(mTableName);
        mChanListLength = mChanList.length();
        mChanListStr = mChanList.join(",");

        getLineChannels();

        return true;
    }

    return false;
}

void ExportGBN::exportDBChanForGBN()
{
    QString query="SELECT * FROM "+mTableName
            +" LIMIT 1 OFFSET 0";

    if (mSql.exec(query)) {
        while (mSql.next()) {
            for (int i = 0; i < mChanListLength; i++) {
                if (!editedChansHash.contains(i)) {
                    if (fiducialIncs[i] == -1) {
                        mChanTypes.push_back(-1);
                        long chanParms[5] = {GS_DOUBLE, -1, GSF_NORMAL, 8, 6};
                        exportChanRec(false, mChanList[i], chanParms);
                    } else {
                        if (mSql.value(i).toString().at(0) == '[') {
                            QString wholeStr = mSql.value(i).toString();

                            mArrayChanIndex.insert(i);
                            //mArrayChanNames.append(mChanList[i]);
                            int index = wholeStr.indexOf(",");
                            int arrayLength = wholeStr.mid(1,index-1).toInt();
                            mArrayChanToLength.insert(i, arrayLength);

                            int start = wholeStr.indexOf(",")+1;
                            QString firstNum;
                            int end = start+1;
                            while (wholeStr[end] != ",")
                                end++;

                            firstNum = wholeStr.mid(start, end-start);
                            if (dataType(firstNum) == GS_DOUBLE) {
                                long chanParms[5] = {GS_DOUBLE, arrayLength, GSF_NORMAL, 8, 4};
                                mChanTypes.push_back(GS_DOUBLE);
                                mArrDoubleChans.append(i);
                                exportChanRec(true, mChanList[i], chanParms);
                            } else if (dataType(firstNum) == GS_LONG) {
                                if (fiducialIncs[i] != (float)mMinFid) {
                                    long chanParms[5] = {GS_DOUBLE, arrayLength, GSF_NORMAL, 8, 2};
                                    mChanTypes.push_back(GS_DOUBLE);
                                    mArrDoubleChans.append(i);
                                    exportChanRec(true, mChanList[i], chanParms);
                                } else {
                                    long chanParms[5] = {GS_LONG, arrayLength, GSF_NORMAL, 8, 4};
                                    mChanTypes.push_back(GS_LONG);
                                    mArrIntChans.append(i);
                                    exportChanRec(true, mChanList[i], chanParms);
                                }
                            }
                        } else {
                            if (dataType(mSql.value(i).toString()) == GS_DOUBLE) {
                                long chanParms[5] = {GS_DOUBLE, -1, GSF_NORMAL, 8, 6};
                                mChanTypes.push_back(GS_DOUBLE);
                                mNormalDoubleChans.append(i);
                                exportChanRec(false, mChanList[i], chanParms);
                            } else if (dataType(mSql.value(i).toString()) == GS_LONG) {
                                if (fiducialIncs[i] != (float)mMinFid) {
                                    long chanParms[5] = {GS_DOUBLE, -1, GSF_NORMAL, 8, 2};
                                    mChanTypes.push_back(GS_DOUBLE);
                                    mNormalDoubleChans.append(i);
                                    exportChanRec(false, mChanList[i], chanParms);
                                } else {
                                    long chanParms[5] = {GS_LONG, -1, GSF_NORMAL, 8, 0};
                                    mChanTypes.push_back(GS_LONG);
                                    mNormalIntChans.append(i);
                                    exportChanRec(false, mChanList[i], chanParms);
                                }
                            } else if (dataType(mSql.value(i).toString()) == GS_UNSIGNED_BYTE) {
                                long chanParms[5] = {GS_UNSIGNED_BYTE, -1, GSF_NORMAL, 8, 0};
                                mChanTypes.push_back(GS_UNSIGNED_BYTE);
                                mAsciiChans.append(i);
                                exportChanRec(false, mChanList[i], chanParms);
                            }
                        }
                    }
                } else {
                    int modelColIndex = editedChans.indexOf(i);
                    QString firstValidRec = mEdChanModel.data(mEdChanModel.index(edChanFirstValidR[i], modelColIndex)).toString();

                    if (firstValidRec.at(0) == '[') {
                        mArrayChanIndex.insert(i);
                        int index = firstValidRec.indexOf(",");
                        int arrayLength = firstValidRec.mid(1,index-1).toInt();
                        mArrayChanToLength.insert(i, arrayLength);

                        int start = firstValidRec.indexOf(",")+1;
                        QString firstNum;
                        int end = start+1;
                        while (firstValidRec[end] != ",")
                            end++;

                        firstNum = firstValidRec.mid(start, end-start);
                        if (dataType(firstNum) == GS_DOUBLE) {
                            long chanParms[5] = {GS_DOUBLE, arrayLength, GSF_NORMAL, 8, 4};
                            mChanTypes.push_back(GS_DOUBLE);
                            mArrDoubleChans.append(i);
                            exportChanRec(true, mChanList[i], chanParms);
                        } else if (dataType(firstNum) == GS_LONG) {
                            if (fiducialIncs[i] != (float)mMinFid) {
                                long chanParms[5] = {GS_DOUBLE, arrayLength, GSF_NORMAL, 8, 2};
                                mChanTypes.push_back(GS_DOUBLE);
                                mArrDoubleChans.append(i);
                                exportChanRec(true, mChanList[i], chanParms);
                            } else {
                                long chanParms[5] = {GS_LONG, arrayLength, GSF_NORMAL, 8, 4};
                                mChanTypes.push_back(GS_LONG);
                                mArrIntChans.append(i);
                                exportChanRec(true, mChanList[i], chanParms);
                            }
                        }
                    } else {
                        if (dataType(firstValidRec) == GS_DOUBLE) {
                            long chanParms[5] = {GS_DOUBLE, -1, GSF_NORMAL, 8, 6};
                            mChanTypes.push_back(GS_DOUBLE);
                            mNormalDoubleChans.append(i);
                            exportChanRec(false, mChanList[i], chanParms);
                        } else if (dataType(firstValidRec) == GS_LONG) {
                            if (fiducialIncs[i] != (float)mMinFid) {
                                long chanParms[5] = {GS_DOUBLE, -1, GSF_NORMAL, 8, 2};
                                mChanTypes.push_back(GS_DOUBLE);
                                mNormalDoubleChans.append(i);
                                exportChanRec(false, mChanList[i], chanParms);
                            } else {
                                long chanParms[5] = {GS_LONG, -1, GSF_NORMAL, 8, 0};
                                mChanTypes.push_back(GS_LONG);
                                mNormalIntChans.append(i);
                                exportChanRec(false, mChanList[i], chanParms);
                            }
                        } else if (dataType(firstValidRec) == GS_UNSIGNED_BYTE) {
                            long chanParms[5] = {GS_UNSIGNED_BYTE, -1, GSF_NORMAL, 8, 0};
                            mChanTypes.push_back(GS_UNSIGNED_BYTE);
                            mAsciiChans.append(i);
                            exportChanRec(false, mChanList[i], chanParms);
                        }
                    }
                }
            }
        }
    }
}

void ExportGBN::readDataFromLines()
{
    int start = 1;
    int end = 0;
    int rowCount = 1;

    if (diffLineRows.length() == 1) {
        exportDBLinesDataRec(1, DataBase::getInstance()->getRowCount(mTableName));
    } else {
        while (1) {
            if (diffLineRows.contains(rowCount)) {
                if (diffLineRows.last() != rowCount) {
                    end = rowCount - 1;
                    exportDBLinesDataRec(start, end);
                    start = rowCount;
                } else {
                    exportDBLinesDataRec(rowCount, DataBase::getInstance()->getRowCount(mTableName));
                    break;
                }
            }
            rowCount++;
        }
    }
}

void ExportGBN::exportDBLinesDataRec(int start, int end)
{

    //std::vector<int> chanPushCounts;
    //chanPushCounts.resize(mChanListLength);
    //export line records
    int range = end - start + 1;
    {
        double lineNo = 0;
        long lineVersion = 0;
        long lineType = LINE_RANDOM;
        long flightNo = 0;
        long year = 1970;
        long month = 1;
        long day = 1;

        bool needToAddVersion = false;   //If need to add version due to duplication

        if (!mLineNoChan.isEmpty()) {
            int lineNoIndex = (start-1)/fiducialIncs[mChanList.indexOf(mLineNoChan)];
            QString lineNoQuery = "SELECT LINENO FROM " + mTableName
                    + " WHERE LINENO IS NOT NULL LIMIT 1 OFFSET " + QString::number(lineNoIndex);

            if (mSql.exec(lineNoQuery)){
                if (mSql.next())
                    lineNo = mSql.value(0).toDouble();
            }

            double lineNoHash = lineNo;
            while (mUsedLineNosHash.contains(lineNoHash)) {
                if (lineNoHash < 0) {
                    lineNoHash = lineNoHash - 1.0/mPossDuplLines;
                } else {
                    lineNoHash = lineNoHash + 1.0/mPossDuplLines;
                }
                needToAddVersion = true;
                mThereIsDupLines = true;
            }

            if (needToAddVersion) {
                double tmp;
                double lineFract = std::modf(lineNoHash, &tmp);
                lineVersion = qAbs(lineFract*mPossDuplLines);
            }
            mUsedLineNosHash.insert(lineNoHash);
        }

        if (!mLineTypeChan.isEmpty()) {
            int lineTypeIndex = (start-1)/fiducialIncs[mChanList.indexOf(mLineTypeChan)];
            QString lineTypeQuery = "SELECT "+mLineTypeChan+" FROM " + mTableName
                    + " WHERE "+mLineTypeChan+" IS NOT NULL LIMIT 1 OFFSET " + QString::number(lineTypeIndex);

            if (mSql.exec(lineTypeQuery)) {
                if (mSql.next()) {
                    if (mSql.value(0).userType() != QMetaType::QString) {
                        lineType = mSql.value(0).toInt();
                        if (lineType < LINE_NORMAL || lineType > LINE_RANDOM)
                            lineType = LINE_RANDOM;
                    } else {
                        lineType = lineTypeStr2Int(mSql.value(0).toString());
                    }
                }
            }
        }

        if (mChanList.contains("flightDate")) {
            int fDateIndex = (start-1)/fiducialIncs[mChanList.indexOf("flightDate")];
            QString fDateQuery = "SELECT flightDate FROM " + mTableName
                    + " WHERE flightDate IS NOT NULL LIMIT 1 OFFSET " + QString::number(fDateIndex);

            if (mSql.exec(fDateQuery)) {
                if (mSql.next()) {
                    QString flightDate = mSql.value(0).toString();
                    QStringList fdList = flightDate.split("-");

                    year = fdList[0].toInt();
                    month = fdList[1].toInt();
                    day = fdList[2].toInt();
                }
            }
        }

        if (mChanList.contains("flightNo")) {
            int fNoIndex = (start-1)/fiducialIncs[mChanList.indexOf("flightNo")];
            QString fNoQuery = "SELECT flightNo FROM " + mTableName
                    + " WHERE flightNo IS NOT NULL LIMIT 1 OFFSET " + QString::number(fNoIndex);

            if (mSql.exec(fNoQuery)) {
                if (mSql.next())
                    flightNo = mSql.value(0).toInt();
            }
        }

        long lineParms[7];

        if (!needToAddVersion && (int)lineNo != lineNo) { //No duplicated lines, found line version in line #, like 10010.2
            double fractpart, intpart;
            fractpart = modf (lineNo , &intpart);
            lineNo = intpart;

            while ((int)fractpart != qRound(fractpart) || (int)fractpart == 0)
                fractpart *= 10.0;

            lineVersion = fractpart;
        }

        lineParms[0] = lineNo;
        lineParms[1] = lineVersion;
        lineParms[2] = lineType;
        lineParms[3] = flightNo;
        lineParms[4] = year;
        lineParms[5] = month;
        lineParms[6] = day;

        exportLineRec(lineParms);
    }


    //export data records
    {
        // Allocate data sctructure
        QList<QList<long>> dataParms;
        QList<QList<float>> fidParms;
        std::vector<int> chanPushIndex;

        double **exportNormalDouble = new double*[mNormalDoubleChans.length()];
        double **exportArrayDouble = new double*[mArrDoubleChans.length()];
        int **exportNormalInt = new int*[mNormalIntChans.length()];
        int **exportArrayInt = new int*[mArrIntChans.length()];
        QStringList *exportDataAscii = new QStringList[mAsciiChans.length()];

        int normalDoubleIndex = 0;
        int normalIntIndex = 0;
        int arrayDoubleIndex = 0;
        int arrayIntIndex = 0;

        for (int i = 0; i < mChanListLength; i++) {
            //if(mChanTypes[i] != -1)
            {

                if (!mArrayChanIndex.contains(i)) {
                    QList<long> theDataParms;
                    QList<float> theFidParms;

                    theDataParms.append(i);
                    theDataParms.append(mChanTypes[i]);
                    theDataParms.append(range/fiducialIncs[i]);

                    theFidParms.append(0);
                    theFidParms.append(fiducialIncs[i]);

                    dataParms.append(theDataParms);
                    fidParms.append(theFidParms);
                    chanPushIndex.push_back(0);

                    if (mChanTypes[i] == GS_LONG) {
                        exportNormalInt[normalIntIndex] = new int [dataParms[i][2]];
                        normalIntIndex++;
                    } else if (mChanTypes[i] == GS_DOUBLE) {
                        exportNormalDouble[normalDoubleIndex] = new double [dataParms[i][2]];
                        normalDoubleIndex++;
                    }
                } else {
                    QList<long> theDataParms;
                    QList<float> theFidParms;

                    theDataParms.append(i);
                    theDataParms.append(mChanTypes[i]);
                    theDataParms.append(range/fiducialIncs[i]*mArrayChanToLength[i]);

                    theFidParms.append(0);
                    theFidParms.append(fiducialIncs[i]);

                    dataParms.append(theDataParms);
                    fidParms.append(theFidParms);
                    chanPushIndex.push_back(0);

                    if (mChanTypes[i] == GS_LONG) {
                        exportArrayInt[arrayIntIndex] = new int [dataParms[i][2]];
                        arrayIntIndex++;
                    } else if (mChanTypes[i] == GS_DOUBLE) {
                        exportArrayDouble[arrayDoubleIndex] = new double [dataParms[i][2]];
                        arrayDoubleIndex++;
                    }

                }
            }
        }

        QString query="SELECT * FROM "+mTableName
                +" LIMIT "+ QString::number(range)
                +" OFFSET " + QString::number(start-1);

        // Read from DB
        int row = start-1;
        if (mSql.exec(query)) {
            while (mSql.next()) {
                if(row % 1000 == 0)
                {
                    double currentProgress = (double)row/(double)mRowCount*70.0+20.0;
                    emit exportGBNProgress("Exporting data records...", currentProgress);
                }
                for (int i = 0; i < mChanListLength; i++) {
                    if (mChanTypes[i] != -1) {  //No "whole blank" channels
                        if (!editedChansHash.contains(i)) {
                            if (!mArrayChanIndex.contains(i)) {
                                if (mChanTypes[i] == GS_LONG) {
                                    if (!mSql.value(i).isNull()) {
                                        exportNormalInt[mNormalIntChans.indexOf(i)][chanPushIndex[i]] =  mSql.value(i).toInt();
                                        chanPushIndex[i] += 1;
                                        //chanPushCounts[i] += 1;
                                    }
                                } else if (mChanTypes[i] == GS_DOUBLE) {
                                    if (!mSql.value(i).isNull()) {
                                        exportNormalDouble[mNormalDoubleChans.indexOf(i)][chanPushIndex[i]] =  mSql.value(i).toDouble();
                                        chanPushIndex[i] += 1;
                                        //chanPushCounts[i] += 1;
                                    }
                                } else if (mChanTypes[i] == GS_UNSIGNED_BYTE) {
                                    if (!mSql.value(i).isNull()) {
                                        if (mAsciiChans.indexOf(i) >= 0)
                                            exportDataAscii[mAsciiChans.indexOf(i)].append(mSql.value(i).toString());
                                        //chanPushCounts[i] += 1;
                                    }
                                }
                            } else {
                                if (!mSql.value(i).isNull()) {
                                    QString arrayData = mSql.value(i).toString();
                                    arrayData.remove('[');
                                    arrayData.remove(']');
                                    QStringList arrayList = arrayData.split(',');
                                    arrayList.removeFirst();

                                    if (mChanTypes[i] == GS_DOUBLE) {
                                        for (QString data: arrayList) {
                                            if (data[0] == "-") {
                                                data = data.mid(1);
                                                exportArrayDouble[mArrDoubleChans.indexOf(i)][chanPushIndex[i]]
                                                        = -data.toDouble();
                                            } else {
                                                exportArrayDouble[mArrDoubleChans.indexOf(i)][chanPushIndex[i]]
                                                        = data.toDouble();
                                            }
                                            chanPushIndex[i] += 1;
                                            //chanPushCounts[i] += 1;
                                        }
                                    } else if (mChanTypes[i] == GS_LONG) {
                                        for (QString data: arrayList) {
                                            if (data[0] == "-") {
                                                data = data.mid(1);
                                                exportArrayInt[mArrIntChans.indexOf(i)][chanPushIndex[i]]
                                                        = -data.toInt();
                                            } else {
                                                exportArrayInt[mArrIntChans.indexOf(i)][chanPushIndex[i]]
                                                        = data.toInt();
                                            }

                                            chanPushIndex[i] += 1;
                                            //chanPushCounts[i] += 1;
                                        }
                                    }
                                }
                            }
                        } else {
                            if (row % (int)fiducialIncs[i] == 0) {
                                if (!mArrayChanIndex.contains(i)) {
                                    if (mChanTypes[i] == GS_LONG) {
                                        int modelColIndex = editedChans.indexOf(i);
                                        QVariant value = mEdChanModel.data(mEdChanModel.index(row, modelColIndex));
                                        if (value.isNull()) {
                                            exportNormalInt[mNormalIntChans.indexOf(i)][chanPushIndex[i]] = GS_S4DM;
                                        } else {
                                            exportNormalInt[mNormalIntChans.indexOf(i)][chanPushIndex[i]] =  value.toInt();
                                        }
                                        chanPushIndex[i] += 1;
                                    } else if (mChanTypes[i] == GS_DOUBLE) {
                                        int modelColIndex = editedChans.indexOf(i);

                                        QVariant value = mEdChanModel.data(mEdChanModel.index(row, modelColIndex));
                                        if (value.isNull()) {
                                            exportNormalDouble[mNormalDoubleChans.indexOf(i)][chanPushIndex[i]] = GS_R8DM;
                                        } else {
                                            exportNormalDouble[mNormalDoubleChans.indexOf(i)][chanPushIndex[i]] =  value.toDouble();
                                        }
                                        chanPushIndex[i] += 1;
                                    } else if (mChanTypes[i] == GS_UNSIGNED_BYTE) {
                                        int modelColIndex = editedChans.indexOf(i);
                                        QVariant value = mEdChanModel.data(mEdChanModel.index(row, modelColIndex));
                                        if (value.isNull()) {
                                            exportDataAscii[mAsciiChans.indexOf(i)].append("");
                                        } else {
                                            exportDataAscii[mAsciiChans.indexOf(i)].append(value.toString());
                                        }
                                    }
                                } else {
                                    int modelColIndex = editedChans.indexOf(i);
                                    QVariant value = mEdChanModel.data(mEdChanModel.index(row, modelColIndex));
                                    int arrSize = mArrayChanToLength[i];

                                    if (mChanTypes[i] == GS_DOUBLE) {
                                        if (value.isNull()) {
                                            for (int arr = 0; arr < arrSize; arr++) {
                                                exportArrayDouble[mArrDoubleChans.indexOf(i)][chanPushIndex[i]] = GS_R8DM;
                                                chanPushIndex[i] += 1;
                                            }
                                        } else {
                                            QString arrayData=value.toString();
                                            arrayData.remove('[');
                                            arrayData.remove(']');
                                            QStringList arrayList=arrayData.split(',');
                                            arrayList.removeFirst();

                                            for (QString data: arrayList) {
                                                if (data[0] == "-") {
                                                    data = data.mid(1);
                                                    exportArrayDouble[mArrDoubleChans.indexOf(i)][chanPushIndex[i]]
                                                            = -data.toDouble();
                                                } else {
                                                    exportArrayDouble[mArrDoubleChans.indexOf(i)][chanPushIndex[i]]
                                                            = data.toDouble();
                                                }
                                                chanPushIndex[i] += 1;
                                                //chanPushCounts[i] += 1;
                                            }
                                        }
                                    } else if (mChanTypes[i] == GS_LONG) {
                                        if (value.isNull()) {
                                            for (int arr = 0; arr < arrSize; arr++) {
                                                exportArrayInt[mArrIntChans.indexOf(i)][chanPushIndex[i]] = GS_S4DM;
                                                chanPushIndex[i] += 1;
                                            }
                                        } else {
                                            QString arrayData = value.toString();
                                            arrayData.remove('[');
                                            arrayData.remove(']');
                                            QStringList arrayList = arrayData.split(',');
                                            arrayList.removeFirst();

                                            for (QString data: arrayList) {
                                                if (data[0] == "-") {
                                                    data = data.mid(1);
                                                    exportArrayInt[mArrIntChans.indexOf(i)][chanPushIndex[i]]
                                                            = -data.toInt();
                                                } else {
                                                    exportArrayInt[mArrIntChans.indexOf(i)][chanPushIndex[i]]
                                                            = data.toInt();
                                                }

                                                chanPushIndex[i] += 1;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                row++;
            }

            // Write to file
            for (int i = 0; i < mChanListLength; i++) {
                if (mChanTypes[i] != -1) {
                    if (!mArrayChanIndex.contains(i)) {
                        if (mChanTypes[i] == GS_DOUBLE) {
                            exportDataRecNum<double>(dataParms[i], fidParms[i], exportNormalDouble[mNormalDoubleChans.indexOf(i)]);
                            delete []exportNormalDouble[mNormalDoubleChans.indexOf(i)];
                        } else if (mChanTypes[i] == GS_LONG) {
                            exportDataRecNum<int>(dataParms[i], fidParms[i], exportNormalInt[mNormalIntChans.indexOf(i)]);
                            delete []exportNormalInt[mNormalIntChans.indexOf(i)];
                        } else if (mChanTypes[i] == GS_UNSIGNED_BYTE) {
                            if (!exportDataAscii[mAsciiChans.indexOf(i)].isEmpty())
                                exportDataRecAscii(dataParms[i], fidParms[i], exportDataAscii[mAsciiChans.indexOf(i)]);
                        }
                    } else {
                        if (mChanTypes[i] == GS_DOUBLE) {
                            if (exportArrayDouble[mArrDoubleChans.indexOf(i)])
                                exportDataRecNum<double>(dataParms[i], fidParms[i], exportArrayDouble[mArrDoubleChans.indexOf(i)]);
                            delete []exportArrayDouble[mArrDoubleChans.indexOf(i)];
                        } else if (mChanTypes[i] == GS_LONG) {
                            if (exportArrayInt[mArrIntChans.indexOf(i)])
                                exportDataRecNum<int>(dataParms[i], fidParms[i], exportArrayInt[mArrIntChans.indexOf(i)]);
                            delete []exportArrayInt[mArrIntChans.indexOf(i)];
                        }
                    }
                }
            }
        }

        delete []exportNormalDouble;
        delete []exportArrayDouble;
        delete []exportNormalInt;
        delete []exportArrayInt;
        delete []exportDataAscii;
    }

}

void ExportGBN::getDiffLineRows()
{
    for (QString chan: mLineChanNames) {
        if (!mLineNoChan.isEmpty() && !mLineTypeChan.isEmpty())
            break;

        if (chan.toUpper() == "LINENO") {
            mLineNoChan = chan;   //This channel name is sure
        } else if (chan.toUpper() == "LINETYPE") {
            mLineTypeChan = chan;
        } else {
            mLineTypeChan = chan;
        }
    }

    if (!mLineNoChan.isEmpty() && !mLineTypeChan.isEmpty()) {
        QString channels = mLineTypeChan + ", " + mLineNoChan;
        QString query="SELECT " + channels+" FROM "+mTableName;
        int rowCount = 1;
        QString lineStr;

        if (mSql.exec(query)) {
            while (mSql.next()) {
                if (rowCount == 1)
                    lineStr = mSql.value(0).toString()+mSql.value(1).toString();

                if (!mSql.value(0).isNull() && !mSql.value(1).isNull()) {
                    if (mSql.value(0).toString()+mSql.value(1).toString() != lineStr) {
                        lineStr = mSql.value(0).toString()+mSql.value(1).toString();
                        //There must be a line fiducialIncs 1??
                        //DiffLineRows.append((rowCount-1) * fiducialIncs[1] + 1);
                        diffLineRows.append(rowCount);
                    }
                }
                rowCount++;
            }
        }

        if(diffLineRows.size() == 0)
            diffLineRows.push_back(DataBase::getInstance()->getRowCount(mTableName));
    } else if (!mLineNoChan.isEmpty() && mLineTypeChan.isEmpty()) {
        QString channels = mLineNoChan;
        QString query="SELECT " + channels+" FROM "+mTableName;
        int rowCount = 1;
        QString lineStr;

        if (mSql.exec(query)) {
            while (mSql.next()) {
                if (rowCount == 1)
                    lineStr = mSql.value(0).toString();

                if (!mSql.value(0).isNull()) {
                    if (mSql.value(0).toString() != lineStr) {
                        lineStr = mSql.value(0).toString();
                        //There must be a line fiducialIncs 1??
                        //DiffLineRows.append((rowCount-1) * fiducialIncs[1] + 1);
                        diffLineRows.append(rowCount);
                    }
                }
                rowCount++;
            }
        }
        if (diffLineRows.size() == 0)
            diffLineRows.push_back(DataBase::getInstance()->getRowCount(mTableName));
    } else if (mLineNoChan.isEmpty() && !mLineTypeChan.isEmpty()) {
        diffLineRows.append(DataBase::getInstance()->getRowCount(mTableName));
    } else {
        emit exportGBNProgress("Line channel not found", -1);

    }
}

void ExportGBN::getFidParms()
{
    double totalRows = DataBase::getInstance()->getRowCount(mTableName);
    //int chanListLength = mChanList.length();
    QString countPartStr;

    for (int i = 0; i < mChanListLength; i++) {
        countPartStr += "COUNT(";
        countPartStr += mChanList[i];
        countPartStr += "),";
    }
    countPartStr.chop(1);

    QString query = "SELECT "+countPartStr+" FROM " + mTableName;
    if (mSql.exec(query)) {
        while (mSql.next()) {
            for (int i = 0; i < mChanListLength; i++) {
                if (mSql.value(i).toInt() == 0) {
                    fiducialIncs.push_back(-1);
                } else {
                    double thisFid = totalRows/(double)(mSql.value(i).toInt());
                    fiducialIncs.push_back(thisFid);

                    if (qRound(thisFid)-thisFid != 0) {
                        //qDebug() << mSql.value(i).toInt();
                        editedChans.push_back(i);
                        editedChansHash.insert(i);
                    }
                }
            }
        }
        mMinFid = *std::min_element(fiducialIncs.begin(), fiducialIncs.end());
    }

    if (!editedChans.empty()) {
        QString chanList;
        for (int i = 0; i < editedChans.size(); i++) {
            chanList += mChanList[editedChans[i]];
            chanList += ", ";
            mEditedChanList.push_back(mChanList[editedChans[i]]);
        }
        chanList.chop(2);

        QString queryEdChan;
        queryEdChan += "SELECT "+chanList+" FROM "+mTableName;

        if (!mSql.exec(queryEdChan))
            DEBUG_TRACE(query);

        mEdChanModel.setQuery(mSql);
        while (mEdChanModel.canFetchMore())
            mEdChanModel.fetchMore();

        getFidsFromEdChans(mEdChanModel);
    }
}

void ExportGBN::setPEIFlightParms()
{
    QString flightNo = DataBase::getInstance()->getFlightNo(mTableName)[0];
    QString flightDate = DataBase::getInstance()->getFlightDate(mTableName)[0];

    mPEIFYear = flightDate.split("-")[0].toLong();
    mPEIFMonth = flightDate.split("-")[1].toLong();
    mPEIFDay = flightDate.split("-")[2].toLong();
    mPEIFNo = flightNo.toLong();
}

bool ExportGBN::isDecimal(QString value)
{
    //    std::regex e ("^-?[0-9]+\.(0?[0-9])*$");      //^\d+\.(0?[0-9]|1[0-3])$

    //    if (std::regex_match(value.toStdString(),e))
    //        return true;

    //    return false;
    if (value.contains("."))
        return true;
    return false;
}

bool ExportGBN::isString(QString value)
{
    if (value.contains("-") || value.contains(":") || value.contains("/"))
        return true;
    std::regex e ("([A-Za-z])+");

    if (std::regex_search(value.toStdString(), e))
        return true;

    return false;
}

bool ExportGBN::isInteger(QString value)
{
    //    std::regex e ("^0$|^-?[1-9][0-9]*$");

    //    if (std::regex_match (value.toStdString(),e))
    //        return true;

    //    return false;
    if (!value.contains("."))
        return true;
    return false;
}

void ExportGBN::getLineChannels()
{

    //QString queryArr="SELECT * FROM PRAGMA_TABLE_INFO('"+mTableName+"') where type LIKE '%[]%';";
    QString queryLine = "SELECT * FROM PRAGMA_TABLE_INFO('"+mTableName+"') WHERE name LIKE '%line%';";

    //    if(mSql.exec(queryArr)){
    //        while(mSql.next())
    //        {
    //            mArrayChanIndex.push_back(mSql.value(0).toInt());
    //            mArrayChanNames.append(mSql.value(1).toString());
    //        }
    //    }

    //    for(QString str: mArrayChanNames)
    //    {
    //        QString query="SELECT " + str + " FROM "+mTableName
    //                +" LIMIT 1 OFFSET 0";

    //        if(mSql.exec(query)){
    //            while(mSql.next())
    //            {
    //                QString firstRecord = mSql.value(0).toString();
    //                int index = firstRecord.indexOf(",");
    //                mArrayChanLength.append(firstRecord.mid(1,index-1).toInt());
    //            }
    //        }
    //    }

    //    for(int i = 0; i < mArrayChanIndex.length(); i++)
    //    {
    //        mArrayChanToLength.insert(mArrayChanIndex[i],  mArrayChanLength[i]);
    //    }

    if (mSql.exec(queryLine)) {
        while (mSql.next()) {
            mLineChanIndex.push_back(mSql.value(0).toInt());
            mLineChanNames.append(mSql.value(1).toString());
        }
    }
}

int ExportGBN::dataType(QString value)
{
    if (value[0] == "-")
        value = value.mid(1);
    bool okInt = false;
    bool okFloat = false;

    value.toInt(&okInt);
    if (okInt)
        return GS_LONG;

    value.toFloat(&okFloat);
    if (okFloat)
        return GS_DOUBLE;

    return GS_UNSIGNED_BYTE;
}

void ExportGBN::getFidsFromEdChans(QSqlQueryModel &model)
{
    double totalRow = DataBase::getInstance()->getRowCount(mTableName);

    for (int index = 0; index < editedChans.size(); index++) {
nextLoop:
        double thisNullIntervalMin = totalRow;
        double firstNotNullRow = 0;
        double secondNotNullRow = 1;

        while (1) {
            if (firstNotNullRow >= totalRow)
                goto nextLoop;
            if (!model.data(model.index(firstNotNullRow, index)).isNull()) {
                //Find the first row not null, start to find the next not null row
                edChanFirstValidR[editedChans[index]] = firstNotNullRow;
                secondNotNullRow = firstNotNullRow+1;
                while (model.data(model.index(secondNotNullRow, index)).isNull()) {
                    secondNotNullRow++;
                    if (secondNotNullRow >= totalRow)
                        goto nextLoop;
                }
                //Find the not null interval, if less than minInterval, set minInterval
                int findNullInterval = secondNotNullRow - firstNotNullRow;
                if (findNullInterval == 1) {
                    thisNullIntervalMin = 1;
                    goto saveMinInterval;
                }

                else if (findNullInterval == 10) {
                    thisNullIntervalMin = 10;
                    goto saveMinInterval;
                }

                else if (findNullInterval == 2) {
                    thisNullIntervalMin = 2;
                    goto saveMinInterval;
                }
//                if (findNullInterval < thisNullIntervalMin)
//                    thisNullIntervalMin = findNullInterval;  This is only a quick fix

                firstNotNullRow = secondNotNullRow;
                secondNotNullRow = firstNotNullRow+1;
            } else {
                firstNotNullRow++;
            }
        }
saveMinInterval:
        fiducialIncs[editedChans[index]] = thisNullIntervalMin;
    }
}

int ExportGBN::lineTypeStr2Int(QString lineType)
{
    if (lineType == "L" || lineType.toUpper() == "LINE") {
        return 0;
    } else if (lineType == "B" || lineType.toUpper() == "BASE") {
        return 1;
    } else if (lineType == "T" || lineType.toUpper() == "TIE") {
        return 2;
    } else if (lineType == "S" || lineType.toUpper() == "TEST") {
        return 3;
    } else if (lineType == "R" || lineType.toUpper() == "TREND") {
        return 4;
    } else if (lineType == "P" || lineType.toUpper() == "SPECIAL") {
        return 5;
    } else if (lineType == "D" || lineType.toUpper() == "RANDOM") {
        return 6;
    } else {
        return 6;
    }
}
