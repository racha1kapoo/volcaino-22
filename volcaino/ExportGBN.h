/**
 *  @file   ExportGBN.h
 *  @brief  Export GBN file from Data Base
 *  @author Shuo Li
 *  @date   Nov 8, ‎2022
 ***********************************************/
#ifndef EXPORTGBN_H
#define EXPORTGBN_H
#include <QObject>
#include <QFile>
#include <QString>
#include <QFileDialog>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QSet>
#include "DataBase.h"
#include "TypeDefConst.h"
#include <regex>
#include <QDir>
class ExportGBN: public QObject
{
    Q_OBJECT
public:
    ///default constructor
    ExportGBN(QObject *parent = nullptr, QString tableName = "", QString expFilePath = "");

    /// Class destructor, it closes data base
    ~ExportGBN();

    bool thereIsDuplicatedLines();

    void startExportGBN();
private:
    /// @brief This function writes "OASIS BINARY DATABASE" at the beginning of a GBN file,
    /// based on GBN format specification
    /// @param
    /// @return
    void writeFirst17Chars();

    /// @brief This function takes channel informations to write to a GBN file
    /// @param isArrChan: If this channel is an array channel
    /// @param chanName: Channel name
    /// @param chanParms[5]: An array of channel parameters
    /// @return
    void exportChanRec(bool isArrChan, QString chanName, long chanParms[5]);   //0x01 & 0x04

    /// @brief This function takes numeric data record informations to write to a GBN file
    /// @param dataParms: Data record parameters
    /// @param fidParms: Fiducial parameters
    /// @param data: Input data array
    /// @return
    template<class T>
    void exportDataRecNum(QList<long>dataParms, QList<float>fidParms, const T *data);  //0x03

    /// @brief This function takes ascii data record informations to write to a GBN file
    /// @param dataParms: Data record parameters
    /// @param fidParms: Fiducial parameters
    /// @param data: Input data string list
    /// @return
    void exportDataRecAscii(QList<long>dataParms, QList<float>fidParms, QStringList data);  //0x03

    /// @brief This function takes line record informations to write to a GBN file
    /// @param lineParms: Line parameters
    /// @return
    void exportLineRec(long lineParms[7]);  //0x02

    /// @brief This function takes channel type and returns the size in byte
    /// @param chanType: Channel type
    /// @return Size in byte
    uint8_t chanTypeSize(int chanType);

    /// @brief This function sets up data base and gets channel list, if an error happened
    /// it returned false
    /// @param
    /// @return Whether successful
    bool configureDB();

    /// @brief This function gets channel informations from the database, processes them to write to GBN file
    /// @param
    /// @return
    void exportDBChanForGBN();

    /// @brief This function exports data to GBN based on different survey lines
    /// @param
    /// @return
    void readDataFromLines();

    /// @brief This function gets data and line informations form the data base, based on different lines rows
    /// , and processes them to write to GBN file
    /// @param start: Start line's row number
    /// @param end: End line's row number
    /// @return
    void exportDBLinesDataRec(int start, int end);

    /// @brief This function first gets all data from line channel of DB, then iterates through them to get different
    /// lines starting and ending row numbers
    /// @param
    /// @return
    void getDiffLineRows();

    /// @brief This function gets informations from data base to calculate fiducial parameters
    /// @param
    /// @return
    void getFidParms();

    /// @brief This function sets PEI file's flight parameters
    /// @param
    /// @return
    void setPEIFlightParms();

    bool isDecimal(QString value);

    bool isString(QString value);

    bool isInteger(QString value);

    void getLineChannels();

    int dataType(QString value);

    void getFidsFromEdChans(QSqlQueryModel &model);

    QSet<int> mArrayChanIndex;
    QList<QString> mArrayChanNames;
    QList<int> mArrayChanLength;
    QList<int> mLineChanIndex;
    QList<QString> mLineChanNames;
    QMap<int, int>mArrayChanToLength;

    QString mFilePath;

    QFile mGBNFile;

    QString mErrMsg;

    const char mSubChar = char(26);

    const uint8_t mBuffSizeRatio = 16;   //TODO

    int mCount = 0;

    QSqlDatabase mQSLDB;

    QSqlQuery mSql;

    QString mTableName;

    QString mLineAllInfoChan = "";
    QString mLineNoChan = "";
    QString mLineTypeChan = "";

    QStringList mChanList;
    QStringList mEditedChanList;

    int mChanListLength;

    std::vector<int> mChanTypes;

    QList<int> mNormalStringChans;
    QList<int> mNormalDoubleChans;
    QList<int> mNormalIntChans;
    QList<int> mArrIntChans;
    QList<int> mArrDoubleChans;

    QList<int> mAsciiChans;

    QString mChanListStr;

    QStringList mLineColList;

    QList<int> diffLineRows;

    std::vector<float> fiducialIncs;

    QList<float> fiducialIncsEdChans;

    QList<int> editedChans;
    QSet<int> editedChansHash;
    std::vector<int> firstValidRows;
    QSqlQueryModel mEdChanModel;

    std::map<int, int> edChanFirstValidR;

    double mMinFid;

    QList<int> mDecimals;

    bool mIsPEI = false;

    int mPEIReadEndLine = 0;

    QSet<float> mUsedLineNosHash;

    const float mPossDuplLines = 1000;   //TODO

    long mPEIFYear;
    long mPEIFMonth;
    long mPEIFDay;
    long mPEIFNo;

    int mAsciiChanNo;

    const int mPEIChanOffSet = 3;

    int lineTypeStr2Int(QString lineType);

    bool mThereIsDupLines;

    int mRowCount = 0;


    //std::map<QString,SqliteColumninfo> mDBInfos;
signals:
    void exportGBNProgress(QString state, int progress);
};

#endif // EXPORTGBN_H
