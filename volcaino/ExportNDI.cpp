#include "ExportNDI.h"


ExportNDI::ExportNDI(QObject *parent, QString tabeleName, QString expFileName):
    QObject(parent), mFilePath(expFileName), mTableName(tabeleName)
{


}

ExportNDI::~ExportNDI()
{

    if(mStringChans.length() > 0)
    {
        for(int i = 0; i < mStringChans.length(); ++i)
        {
            delete []mStringDataBuff[i];
        }
        delete[] mStringDataBuff;
    }

    if(mIntChans.length() > 0)
    {
        for(int i = 0; i < mIntChans.length(); i++)
        {
            delete []mIntDataBuff[i];
        }
        delete []mIntDataBuff;
    }

    if(mDoubleChans.length() > 0)
    {
        for(int i = 0; i < mDoubleChans.length(); i++)
        {
            delete []mDoubleDataBuff[i];
        }
        delete []mDoubleDataBuff;
    }

}

void ExportNDI::startExportNDI()
{
    emit exportNUVProgress("Exporting NUV file...", 0);
    emit exportNUVProgress("Exporting NUV file...", 10);
    if(configureDB())
    {
        getArrayChannels();
        getFlightDateNo();
        getSamplingRatesRows();
        getDataTypes();

        if(!mFilePath.isEmpty())
        {
            //            mFilePath += "/"+mTableName+".ndi";
            mNDIFile.setFileName(mFilePath);
            if(mNDIFile.open(QIODevice::WriteOnly))
            {
                writeHeaderChDetails();
                const char subChar = char(26);
                mNDIFile.write(&subChar,1);
                allocateBuff();

                emit exportNUVProgress("Exporting data records...", 20);
                //int time3 = QDateTime::currentMSecsSinceEpoch();
                readDataDB();
                //int time4 = QDateTime::currentMSecsSinceEpoch();
                //qDebug() << "Phase 2 took " << time4 - time3;

                //int time5 = QDateTime::currentMSecsSinceEpoch();
                writeBinaryData();
                int time2 = QDateTime::currentMSecsSinceEpoch();
                //qDebug() << "Phase 3 took " << time2 - time1;

                mNDIFile.close();
                emit exportNUVProgress("Export NUV file done", 100);

                //qDebug() << "Write NDI file done";
            }
        }
        else
        {
            mNDIFile.close();
            //DataBase::getInstance()->closeDatabase();
        }
    }
    else
    {
        //DataBase::getInstance()->closeDatabase();
    }
}

bool ExportNDI::configureDB()
{
    auto db =DataBase::getInstance();

    if(db->openDatabase())
    {
        mSql=db->getSqlQuery();

        mChanList = db->getHeaderDetails(mTableName);
        if(mChanList.indexOf("flightDate") >= 0)
            mUnwantedChIdx.insert(mChanList.indexOf("flightDate"));
        if(mChanList.indexOf("flightNo") >= 0)
            mUnwantedChIdx.insert(mChanList.indexOf("flightNo"));

        mChanListLength = mChanList.length();
        mChanListStr = mChanList.join(",");

        return true;
    }

    return false;
}

void ExportNDI::getArrayChannels()
{
    QString queryArr="SELECT * FROM PRAGMA_TABLE_INFO('"+mTableName+"') where type LIKE '%[]%';";


    if(mSql.exec(queryArr)){
        while(mSql.next())
        {
            mArrayChanIndex.push_back(mSql.value(0).toInt());
            mArrayChanNames.append(mSql.value(1).toString());
            mThereIsArray = true;
        }
    }

    for(QString str: mArrayChanNames)
    {
        QString query="SELECT " + str + " FROM "+mTableName
                +" LIMIT 1 OFFSET 0";

        if(mSql.exec(query)){
            while(mSql.next())
            {
                QString firstRecord = mSql.value(0).toString();
                int index = firstRecord.indexOf(",");
                mArrayChanLength.append(firstRecord.mid(1,index-1).toInt());
            }
        }
    }

    for(int i = 0; i < mArrayChanIndex.length(); i++)
    {
        mArrayChanToLength.insert(mArrayChanIndex[i],  mArrayChanLength[i]);
    }

}

void ExportNDI::getSamplingRatesRows()
{
    int totalRows = DataBase::getInstance()->getRowCount(mTableName);
    QString countPartStr;


    for(int i = 0; i < mChanListLength; i++)
    {
        countPartStr += "COUNT(";
        countPartStr += mChanList[i];
        countPartStr += "),";
    }
    countPartStr.chop(1);
    int minRows = totalRows;

    QString query="SELECT "+countPartStr+" FROM " + mTableName;
    if(mSql.exec(query))
    {
        if(mSql.next())
        {
            for(int i = 0; i < mChanListLength; i++)
            {
                if(mSql.value(i).toInt() != 0)
                {
                    mChannelRows.push_back(mSql.value(i).toInt());

                    if(mSql.value(i).toInt() < minRows)
                    {
                        minRows = mSql.value(i).toInt();
                    }
                }
                else
                {
                    mChannelRows.push_back(-1);
                }
            }
        }
    }

    if(mSql.exec(query))
    {
        if(mSql.next())
        {
            for(int i = 0; i < mChanListLength; i++)
            {
                if(mSql.value(i).toInt() != 0)
                {
                    samplingRates.append(mSql.value(i).toInt()/minRows);
                }
                else
                {
                    samplingRates.append(-1);
                }
            }
        }
    }

    //    for(int i = 0; i < mArrayChanIndex.length(); i++)
    //    {
    //        samplingRates[mArrayChanIndex[i]] = samplingRates[mArrayChanIndex[i]] * mArrayChanLength[i];
    //    }
}

void ExportNDI::getDataTypes()
{

    QString query="SELECT * FROM "+mTableName
            +" LIMIT 1 OFFSET 0";

    if(mSql.exec(query))
    {
        while(mSql.next())
        {
            for(int i = 0; i < mChanListLength; i++)
            {
                //long chanParms[5] = {0};
                if(mSql.value(i).isNull())
                {
                    mChanTypes.append(-1);
                }
                else
                {
                    if(mSql.value(i).toString().at(0) == '[')
                    {
                        QString wholeStr = mSql.value(i).toString();
                        int start = wholeStr.indexOf(",")+1;
                        QString firstNum;
                        int end = start+1;
                        while(wholeStr[end] != ",")
                        {
                            end++;
                        }

                        firstNum = wholeStr.mid(start, end-start);

                        if(dataType(firstNum) == NDI_DB)
                        {
                            mChanTypes.append(NDI_DB);
                        }
                        else if(dataType(firstNum) == NDI_INT)
                        {
                            mChanTypes.append(NDI_INT);
                        }
                    }
                    else
                    {
                        if(dataType(mSql.value(i).toString()) == NDI_ASCII)
                        {
                            mChanTypes.append(NDI_ASCII);
                            mChanStrLengths.insert(i,1);
                        }
                        else if(dataType(mSql.value(i).toString()) == NDI_DB)
                        {
                            mChanTypes.append(NDI_DB);
                        }
                        else if(dataType(mSql.value(i).toString()) == NDI_INT)
                        {
                            mChanTypes.append(NDI_INT);
                        }
                    }
                }

            }
        }
    }
}
void ExportNDI::getFlightDateNo()
{
    bool fDateFound = false;
    bool fNoFound = false;

    for(QString channel: mChanList)
    {
        if(fDateFound && fNoFound) break;

        if(channel == "flightDate")
        {
            QString query="SELECT " + channel + " FROM "+mTableName
                    +" LIMIT 1 OFFSET 0";

            if(mSql.exec(query)){
                if(mSql.next())
                {
                    mFlightDate = mSql.value(0).toString();
                    fDateFound = true;
                    //mUnwantedChIdx.insert(mChanList.indexOf("flightDate"));
                }
            }
        }

        if(channel == "flightNo")
        {
            QString query="SELECT " + channel + " FROM "+mTableName
                    +" LIMIT 1 OFFSET 0";

            if(mSql.exec(query)){
                if(mSql.next())
                {
                    mFlightNo = mSql.value(0).toString();
                    fNoFound = true;
                    //mUnwantedChIdx.insert(mChanList.indexOf("flightNo"));
                }
            }
        }
    }
}

void ExportNDI::writeHeaderChDetails()
{
    QTextStream stream(&mNDIFile);
    stream << "Nuvia Dynamics Inc. BINARY Data Format";
    stream << "\n";
    stream << "Mission: " << mFlightNo << "\n";
    stream << "*********************************" << "\n";

    QString year = mFlightDate.split("-")[0];
    QString month = mFlightDate.split("-")[1];
    QString day = mFlightDate.split("-")[2];

    stream << "ZDA=<$GPZDA,123.45,";
    stream << day << "," << month << "," << year;
    stream << ",456,789*123>";
    stream << "\n";
    stream << "*********************************" << "\n";

    stream << "Data is stored in one second RECORDPEIs\n";
    stream << "Each data channel or data array is described by a string:\n";
    stream << "Name,Array,Data_type,Const,Offset,Unit,Comment\n";
    stream << "For as (Const,Offset,Unit) not applicable\n";
    stream << "For db and fl Const = dec.digits\n";
    stream << "For other Const = multiplication constant\n";
    stream << "Any data type ending with * such as wd*\n";
    stream << "indicates that data is a spectrum, while\n";
    stream << "without * the data is a time sequence\n";
    stream << "Data Types:\n";
    stream << "as=ascii    1 byte   ASCII character\n";
    stream << "db=real     8 byte   5.0e-324..1.7e308 15-16d.\n";
    stream << "li=longint  4 byte   -2147483648..2147483647\n";

    stream << "RECORDPEI definition:\n";
    stream << "BEGIN\n";

    for(int i = 0; i < mChanListLength; i++)
    {
        if(!mUnwantedChIdx.contains(i) && mChanTypes[i] != -1)
        {
            QString channelDetails;
            channelDetails += mChanList[i] + ",";
            if(mArrayChanIndex.contains(i))
            {
                if(samplingRates[i] != 1)
                {
                    channelDetails += QString::number(mArrayChanToLength[i]);
                    channelDetails += "*";
                    channelDetails += QString::number(samplingRates[i]);
                    channelDetails += ",";
                }
                else
                {
                    channelDetails += QString::number(mArrayChanToLength[i]);
                    channelDetails += ",";
                }
            }
            else
            {
                channelDetails += QString::number(samplingRates[i]) + ",";
            }

            QString channelType;
            if(mChanTypes[i] == NDI_ASCII)  channelType = "as";
            else if(mChanTypes[i] == NDI_INT)  channelType = "li";
            else if(mChanTypes[i] == NDI_DB)  channelType = "db";
            //else if(mChanTypes[i] == -1)  continue;   //Should skip this null channel
            if(mArrayChanIndex.contains(i))
            {
                channelType += "*";
            }
            channelDetails += channelType +",";


            channelDetails += "1.000000,0.000000,,Comment";
            stream << channelDetails << "\n";
        }
    }

    stream << "END\n";
}

void ExportNDI::readDataDB()
{
    //QString limit = QString::number(mTotalRows);
    QString dataQuery = "SELECT * FROM " + mTableName;
    int row = 0;
    if(mSql.exec(dataQuery))
    {
        while(mSql.next())
        {
            if(row % 1000 == 0)
            {
                double currentProgress = (double)row/(double)mTotalRows*70.0+20.0;
                emit exportNUVProgress("Exporting data records...", currentProgress);
            }
            for(int channel = 0; channel < mChanListLength; channel++)
            {
                if(!mUnwantedChIdx.contains(channel) && mChanTypes[channel] != -1
                        && !mSql.value(channel).isNull())
                    //if(!mUnwantedChIdx.contains(channel) && mChanTypes[channel] != -1)
                {
                    if(!mArrayChanIndex.contains(channel))
                    {
                        if(mChanTypes[channel] == NDI_ASCII)
                        {
                            //char *str = mSql.value(channel).toString().toLocal8Bit().data()[0];   //TODO
                            mStringDataBuff[mStringChans.indexOf(channel)][mChanPushIndex[channel]] = mSql.value(channel).toString().toLocal8Bit().data()[0];
                            mChanPushIndex[channel] += 1;

                        }

                        else if(mChanTypes[channel] == NDI_INT)
                        {
                            mIntDataBuff[mIntChans.indexOf(channel)][mChanPushIndex[channel]] = mSql.value(channel).toInt();
                            mChanPushIndex[channel] += 1;

                        }

                        else if(mChanTypes[channel] == NDI_DB)
                        {
                            mDoubleDataBuff[mDoubleChans.indexOf(channel)][mChanPushIndex[channel]] = mSql.value(channel).toDouble();
                            mChanPushIndex[channel] += 1;
                        }
                    }
                    else
                    {
                        QString arrayData=mSql.value(channel).toString();
                        arrayData.remove('[');
                        arrayData.remove(']');
                        QStringList arrayList=arrayData.split(',');
                        arrayList.removeFirst();

                        if(mChanTypes[channel] == NDI_DB)
                        {
                            for(QString data: arrayList)
                            {
                                if(data[0] == "-")
                                {
                                    data = data.mid(1);
                                    mDoubleDataBuff[mDoubleChans.indexOf(channel)][mChanPushIndex[channel]] = -data.toDouble();
                                }
                                else
                                {
                                    mDoubleDataBuff[mDoubleChans.indexOf(channel)][mChanPushIndex[channel]] = data.toDouble();
                                }
                                mChanPushIndex[channel] += 1;
                            }
                        }
                        else if(mChanTypes[channel] == NDI_INT)
                        {
                            for(QString data: arrayList)
                            {
                                if(data[0] == "-")
                                {
                                    data = data.mid(1);
                                    mIntDataBuff[mIntChans.indexOf(channel)][mChanPushIndex[channel]] = -data.toInt();
                                }
                                else
                                {
                                    mIntDataBuff[mIntChans.indexOf(channel)][mChanPushIndex[channel]] = data.toInt();
                                }
                                mChanPushIndex[channel] += 1;
                            }
                        }
                    }
                }
            }
            row++;
        }
    }



}

void ExportNDI::allocateBuff()
{

    mMaxSamplingRate = *std::max_element(samplingRates.begin(), samplingRates.end());
    mTotalRows = DataBase::getInstance()->getRowCount(mTableName);

    for(int channel = 0; channel < mChanListLength; channel++)
    {
        mChanPushIndex.push_back(0);
        mChanReadIndex.push_back(0);

        //if(!mUnwantedChIdx.contains(channel))
        {
            if(mChanTypes[channel] == NDI_DB)
            {
                mDoubleChans.append(channel);
            }
            else if(mChanTypes[channel] == NDI_INT)
            {
                mIntChans.append(channel);
            }
            else if(mChanTypes[channel] == NDI_ASCII)
            {
                mStringChans.append(channel);
            }
        }
    }
    mStringDataBuff = new char*[mStringChans.length()];
    mIntDataBuff = new int*[mIntChans.length()];
    mDoubleDataBuff = new double*[mDoubleChans.length()];

    for(int channel = 0; channel < mChanListLength; channel++)
    {
        //if(mUnwantedChIdx.contains(channel))
        {
            if(!mArrayChanIndex.contains(channel))
            {
                if(mChanTypes[channel] == NDI_ASCII)
                {
                    mStringDataBuff[mStringChans.indexOf(channel)] = new char[mChannelRows[channel]];
                    byteCountOneSec += samplingRates[channel]*mChanStrLengths[channel];
                }
                else if(mChanTypes[channel] == NDI_INT)
                {

                    mIntDataBuff[mIntChans.indexOf(channel)] = new int[mChannelRows[channel]];
                    byteCountOneSec += samplingRates[channel]*4;

                }
                else if(mChanTypes[channel] == NDI_DB)
                {

                    mDoubleDataBuff[mDoubleChans.indexOf(channel)] = new double[mChannelRows[channel]];
                    byteCountOneSec += samplingRates[channel]*8;

                }
            }
            else
            {

                if(mChanTypes[channel] == NDI_INT)
                {
                    mIntDataBuff[mIntChans.indexOf(channel)] = new int[mChannelRows[channel]*mArrayChanToLength[channel]];
                    byteCountOneSec += samplingRates[channel]*mArrayChanToLength[channel]*4;
                }
                else if(mChanTypes[channel] == NDI_DB)
                {

                    mDoubleDataBuff[mDoubleChans.indexOf(channel)] = new double[mChannelRows[channel]*mArrayChanToLength[channel]];
                    byteCountOneSec += samplingRates[channel]*mArrayChanToLength[channel]*8;
                }
            }
        }
    }

    //mBinaryDataOneSec = new unsigned char[byteCountOneSec];
}

void ExportNDI::writeBinaryData()
{
    int totalRows = mTotalRows/mMaxSamplingRate;

    for(int row = 0; row < totalRows; row++)
    {
        for(int channel = 0; channel < mChanListLength; channel++)
        {
            if(!mUnwantedChIdx.contains(channel) && mChanTypes[channel] != -1)
            {
                if(mChanTypes[channel] == NDI_ASCII)
                {
                    //                    char *tmp = new char[samplingRates[channel]*mChanStrLengths[channel]+1];
                    //                    std::strcpy(tmp, mStringDataBuff[mStringChans.indexOf(channel)][mChanReadIndex[channel]].c_str());
                    //                    memcpy(&tmp, &mStringDataBuff[mStringChans.indexOf(channel)][mChanReadIndex[channel]],
                    //                            samplingRates[channel]*mChanStrLengths[channel]);

                    mNDIFile.write(&mStringDataBuff[mStringChans.indexOf(channel)][mChanReadIndex[channel]], samplingRates[channel]*mChanStrLengths[channel]);
                    mChanReadIndex[channel] += samplingRates[channel]*mChanStrLengths[channel];

                    //delete []tmp;
                }
                else if(mChanTypes[channel] == NDI_INT)
                {
                    if(mArrayChanIndex.contains(channel))
                    {
                        unsigned char *tmp = new unsigned char[samplingRates[channel]*mArrayChanToLength[channel]*4];
                        memcpy(tmp, &mIntDataBuff[mIntChans.indexOf(channel)][mChanReadIndex[channel]], samplingRates[channel]*mArrayChanToLength[channel]*4);

                        mChanReadIndex[channel] += samplingRates[channel]*mArrayChanToLength[channel];
                        mNDIFile.write(reinterpret_cast<char*>(tmp), samplingRates[channel]*mArrayChanToLength[channel]*4);
                        delete []tmp;
                    }
                    else
                    {
                        unsigned char *tmp = new unsigned char[samplingRates[channel]*4];
                        memcpy(tmp, &mIntDataBuff[mIntChans.indexOf(channel)][mChanReadIndex[channel]], samplingRates[channel]*4);

                        mChanReadIndex[channel] += samplingRates[channel];
                        mNDIFile.write(reinterpret_cast<char*>(tmp), samplingRates[channel]*4);
                        delete []tmp;
                    }

                }
                else if(mChanTypes[channel] == NDI_DB)
                {
                    if(mArrayChanIndex.contains(channel))
                    {
                        unsigned char *tmp = new unsigned char[samplingRates[channel]*mArrayChanToLength[channel]*8];
                        memcpy(tmp, &mDoubleDataBuff[mDoubleChans.indexOf(channel)][mChanReadIndex[channel]], samplingRates[channel]*mArrayChanToLength[channel]*8);

                        mChanReadIndex[channel] += samplingRates[channel]*mArrayChanToLength[channel];
                        mNDIFile.write(reinterpret_cast<char*>(tmp), samplingRates[channel]*mArrayChanToLength[channel]*8);
                        delete []tmp;
                    }
                    else
                    {
                        unsigned char *tmp = new unsigned char[samplingRates[channel]*8];
                        memcpy(tmp, &mDoubleDataBuff[mDoubleChans.indexOf(channel)][mChanReadIndex[channel]], samplingRates[channel]*8);

                        mChanReadIndex[channel] += samplingRates[channel];
                        mNDIFile.write(reinterpret_cast<char*>(tmp), samplingRates[channel]*8);
                        delete []tmp;
                    }
                }
            }
        }
    }
}


bool ExportNDI::isString(QString value)
{
    if((value.contains("-") && value.indexOf("-") != 0)
            ||(value.contains(":") && value.indexOf(":") != 0)
            ||(value.contains("/") && value.indexOf("/") != 0))
    {
        return true;
    }
    std::regex e ("([A-Za-z])+");

    if (std::regex_search (value.toStdString(),e))
        return true;

    return false;
}

int ExportNDI::dataType(QString value)
{
    if(value[0] == "-")
    {
        value = value.mid(1);
    }

    bool okInt = false;
    bool okFloat = false;

    value.toInt(&okInt);
    if(okInt)  return NDI_INT;

    value.toFloat(&okFloat);
    if(okFloat)  return NDI_DB;

    return NDI_ASCII;
}

bool ExportNDI::isFloat(QString value)
{
    bool ok = false;
    value.toFloat(&ok);

    return ok;
}
