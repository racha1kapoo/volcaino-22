/**
 *  @file   ExportNDI.h
 *  @brief  Export NUV(NDI) file from Data Base
 *  @author Shuo Li
 *  @date   Nov 8, ‎2022
 ***********************************************/
#ifndef EXPORTNDI_H
#define EXPORTNDI_H
#include <QObject>
#include <QDir>
#include "DataBase.h"
#include <QFileDialog>
#include <QSqlQuery>
#include <QString>
#include <QFile>
#include "TypeDefConst.h"
#include <QTextStream>
#include <regex>
#include "TypeDefConst.h"
#include <QDateTime>
class ExportNDI: public QObject
{
    Q_OBJECT
public:
    ExportNDI(QObject *parent = nullptr, QString tabeleName = "", QString expFileName = "");

    ~ExportNDI();

    void startExportNDI();
private:
    QString mFilePath;
    QString mTableName = "B2021_11_27_13_49";
    QSqlQuery mSql;
    QStringList mChanList;
    int mChanListLength;
    QString mChanListStr;
    QList<int> mArrayChanIndex;
    QList<QString> mArrayChanNames;
    QList<int> mArrayChanLength;
    QMap<int, int>mArrayChanToLength;
    QList<int> samplingRates;
    QFile mNDIFile;

    QString mFlightDate = "1970-01-01";
    QString mFlightNo = "0";
    QList<int> mChanTypes;
    QMap<int, int>mChanStrLengths;

    QList<int> mStringChans;
    QList<int> mDoubleChans;
    QList<int> mIntChans;

    QSet<int> mUnwantedChIdx;
    std::vector<int> mChannelRows;
    //QSet<int> mNullChIdx;
    //QList<int> chanPushIndex;

    char **mStringDataBuff;
    double **mDoubleDataBuff;
    int **mIntDataBuff;

    int mMaxSamplingRate;
    int mTotalRows;
    std::vector<int>mChanPushIndex;
    std::vector<int>mChanReadIndex;
    bool mThereIsArray  =false;
    int byteCountOneSec = 0;
    unsigned char *mBinaryDataOneSec;
    /// @brief This function sets up data base and gets channel list, if an error happened
    /// it returned false
    /// @param
    /// @return Whether successful
    bool configureDB();

    /// @brief This function extracts array/spectrum channels
    /// @param
    /// @return
    void getArrayChannels();

    /// @brief This function gets different channels sampling rates
    /// @param
    /// @return
    void getSamplingRatesRows();

    /// @brief This function gets different channels data types
    /// @param
    /// @return
    void getDataTypes();

    /// @brief This function gets flight date and number
    /// @param
    /// @return
    void getFlightDateNo();

    /// @brief This function writes comments to NDI file
    /// @param
    /// @return
    void writeHeaderChDetails();

    /// @brief This function reads data from DB
    /// @param
    /// @return
    void readDataDB();

    /// @brief This function allocates data buff to load data from DB
    /// @param
    /// @return
    void allocateBuff();

    /// @brief This function writes binary data to file
    /// @param
    /// @return
    void writeBinaryData();


    bool isString(QString value);

    int dataType(QString value);
    bool isFloat(QString value);

signals:
    void exportNUVProgress(QString state, int progress);

};

#endif // EXPORTNDI_H
