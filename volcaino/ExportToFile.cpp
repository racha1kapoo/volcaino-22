#include "ExportToFile.h"
#include <QThread>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QPushButton>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QMessageBox>
#include <QDate>
#include <map>
#include <iomanip>
#include "DataBase.h"
#include "ExportGBN.h"
#include "ExportNDI.h"

ExportToFile::ExportToFile() : mTable{"N/A"}
{

}

ExportToFile* ExportToFile::getInstance()
{
    static ExportToFile theInstance;
    return &theInstance;
}

void ExportToFile::setProgressBar(ProgressBar *theBar)
{
    mBar = theBar;
}

QString& ExportToFile::getTableName()
{
    return mTable;
}

void ExportToFile::setTableName(QString table)
{
    if (table != "") {
        if (table != mTable)
            mTable = table;
    }
}

void ExportToFile::setFile(QString file)
{
    DEBUG_TRACE(file);
}

void ExportToFile::populateTableList(int value)
{
    DEBUG_TRACE(value);
    QStringList list;
    auto db = DataBase::getInstance();
    if (db->openDatabase()) {
        list = db->getTableList();
    }
    DEBUG_TRACE(list);

    QDialog *dialog = new QDialog();
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("TABLE LIST TO EXPORT"));
    dialog->setMinimumSize(220,100);
    QVBoxLayout *hLayout = new QVBoxLayout(dialog);

    auto listWidget = new QListWidget(dialog);
    listWidget->setSelectionMode(QAbstractItemView::SingleSelection);

    if (list.size() > 0) {
        for (auto item : list) {
            auto listItem = new QListWidgetItem(listWidget);
            listItem->setText(item);
        }
        hLayout->addWidget(listWidget);
        QPushButton *ok=new QPushButton(tr("OK"), dialog);
        hLayout->addWidget(ok);
        dialog->showNormal();
        connect(listWidget, &QListWidget::itemSelectionChanged, dialog, [this, list, listWidget]() {
            this->mTable = listWidget->currentItem()->text();
        });

        mFileType = value;
        connect(ok, &QPushButton::clicked, dialog, [dialog, this](){
            dialog->close();

            if (mFileType == FileExtType::CSV) {
                mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                            QDir().dirName().append("../untitled.csv"), tr("*.csv"));
            } else if (mFileType == FileExtType::XYZ) {
                mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                            QDir().dirName().append("../untitled.XYZ"), tr("*.XYZ"));
            } else if (mFileType == FileExtType::gbn) {
                mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                            QDir().dirName().append("../untitled.gbn"), tr("*.gbn"));
            } else if (mFileType == FileExtType::NUV) {
                mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                            QDir().dirName().append("../untitled.ndi"), tr("*.ndi"));
            }

            QThread *thread = new QThread;
            this->moveToThread(thread);

            disconnect(this, &ExportToFile::currentProgress, mBar, &ProgressBar::updateProgress);
            connect(this, &ExportToFile::currentProgress, mBar, &ProgressBar::updateProgress);

            connect(thread,&QThread::finished,thread,&QObject::deleteLater);
            connect(thread,&QThread::finished, this, [this](){this->moveToThread(QApplication::instance()->thread());});
            connect(this, &ExportToFile::exportFileDone, thread, &QThread::quit);
            connect(thread, &QThread::started, this, &ExportToFile::startExport);
            thread->start();
        });
    }
}

void ExportToFile::getExpInfo(int value, QString table)
{
    this->mTable = table;
    mFileType = value;

    if (mFileType == FileExtType::CSV) {
        mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                    QDir().dirName().append("../untitled.csv"), tr("*.csv"));
    } else if (mFileType == FileExtType::XYZ) {
        mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                    QDir().dirName().append("../untitled.XYZ"), tr("*.XYZ"));
    } else if (mFileType == FileExtType::NUV) {
        mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                    QDir().dirName().append("../untitled.ndi"), tr("*.ndi"));
    } else if (mFileType == FileExtType::gbn) {
        mExpFilePath = QFileDialog::getSaveFileName(nullptr, tr("Save Export File"),
                                                    QDir().dirName().append("../untitled.gbn"), tr("*.gbn"));
    }

    QThread *thread = new QThread;
    this->moveToThread(thread);

    disconnect(this, &ExportToFile::currentProgress, mBar, &ProgressBar::updateProgress);
    connect(this, &ExportToFile::currentProgress, mBar, &ProgressBar::updateProgress);

    connect(thread,&QThread::finished,thread,&QObject::deleteLater);
    connect(thread,&QThread::finished, this, [this](){this->moveToThread(QApplication::instance()->thread());});
    connect(this, &ExportToFile::exportFileDone, thread, &QThread::quit);
    connect(thread, &QThread::started, this, &ExportToFile::startExport);
    thread->start();
}

void ExportToFile::startExport()
{
    if (mFileType == FileExtType::CSV) {
        this->exportToCSV(mExpFilePath);
    } else if (mFileType == FileExtType::XYZ) {
        this->exportToXYZ(mExpFilePath);
    } else if (mFileType == FileExtType::gbn) {
        this->exportToGBN(mExpFilePath);
    } else if (mFileType == FileExtType::NUV) {
        this->exportToNUV(mExpFilePath);
    }

    emit exportFileDone();
}

QString ExportToFile::escapedCSV(QString unexc)
{
    if (!unexc.contains(QLatin1Char(',')))
        return unexc;
    return '\"' + unexc.replace(QLatin1Char('\"'), QStringLiteral("\"\"")) + '\"';
}

void ExportToFile::exportToCSV(QString expFilePath)
{
    auto db = DataBase::getInstance();
    if (db->openDatabase()) {
        emit currentProgress("Exporting CSV file...", 0);

        emit currentProgress("Exporting CSV file...", 45);

        QString query = "";

        auto mSql = db->getSqlQuery();

        auto columnList = db->getHeaderDetails(mTable);

        query = "SELECT * FROM '"+mTable+"' ORDER BY ROWID;";

        mSql.exec(query);

        QFile csvFile(expFilePath);
        if (!csvFile.open(QFile::WriteOnly | QFile::Text)) {
            qDebug("failed to open csv file");
            return;
        }
        //emit started();

        QTextStream outStream(&csvFile);
        outStream.setCodec("UTF-8");

        for (int i = 0; i < columnList.size(); ++i) {
            if (i > 0)
                outStream << ',';
            outStream << escapedCSV(columnList[i]);
        }
        outStream << '\n';

        while (mSql.next()) {
            const QSqlRecord record = mSql.record();
            for (int i = 0, recCount = record.count(); i < recCount; ++i) {
                if (i > 0)
                    outStream << ',';
                outStream << escapedCSV(record.value(i).toString());
            }
            outStream << '\n';
        }
//        //emit finished();

//        QMessageBox msgBox;
//        msgBox.setText(tr("Database is successfully exported to ")+lstPath.last());
//        msgBox.exec();
    }
    db->closeDatabase();
    emit currentProgress("Export CSV file done", 100);

}

void ExportToFile::exportToXYZ(QString expFilePath)
{
    auto db = DataBase::getInstance();
    if (db->openDatabase()) {
        emit currentProgress("Exporting XYZ file...", 0);
        emit currentProgress("Exporting XYZ file...", 45);

        int dataType = 0;
        QString query = "";

        QString colList = "";

        auto mSql = db->getSqlQuery();

        auto columnList = db->getHeaderDetails(mTable);

        query = "SELECT * FROM '"+mTable+"' ORDER BY ROWID;";

        mSql.exec(query);

        QFile xyzFile (expFilePath);
        if (!xyzFile.open(QFile::WriteOnly | QFile::Text)) {
            qDebug("failed to open XYZ file");
            return;
        }
        QDate date = QDate::currentDate();
        QString formattedTime = date.toString("dd/MM/yyyy");
        QByteArray formattedTimeMsg = formattedTime.toLocal8Bit();

        qDebug() << "Date:"+formattedTime;

        QTextStream outStream(&xyzFile);
        outStream.setFieldAlignment(QTextStream::AlignRight);
        outStream.setCodec("UTF-8");
        outStream << "/ ------------------------------------------------------------------------------\n";
        outStream << "/ XYZ EXPORT \t["<<formattedTime<<"]\n";;
        outStream << "/ DATABASE   \t["<<mTable<<" SQLite3]\n";
        outStream << "/ ------------------------------------------------------------------------------\n";
        outStream << "/\n";
        auto flights = db->getFlightNo(mTable);
        auto flightDate = db->getFlightDate(mTable);

        for (int j = 0; j < flights.size(); ++j) {
            for (int i = 4; i < columnList.size(); ++i) {
                if (i == 4)
                    outStream << '/';
                outStream <<qSetFieldWidth(15)<<columnList[i]<<qSetFieldWidth(0);
                colList.append(columnList[i]).append(",");
            }
            colList.chop(1);
            outStream << '\n';

            outStream << '/';
            outStream << '=';
            for (int i = 4; i < columnList.size(); ++i) {
                for (int i = 0; i < 14; ++i) {
                    outStream << qSetFieldWidth(0);
                    outStream << "=";
                }
                outStream << " ";
                outStream << qSetFieldWidth(15);
            }
            outStream << qSetFieldWidth(0);

            outStream << '\n';
            outStream << '/';
            outStream << '\n';

            outStream << "//Flight " << flights[j] << "\n";
            outStream << "//Date  " << flightDate[j] << "\n";

            query = "SELECT DISTINCT dataType FROM '"+mTable+"';";
            if (mSql.exec(query)) {
                mSql.next();
                dataType = mSql.value(0).toInt();
            }

            if (!dataType) {
                auto lineTypeNo = db->getLineTypeLineNo(mTable);
                std::map<QString, QString>::iterator it;
                for (it = lineTypeNo.begin(); it != lineTypeNo.end(); ++it) {
                    outStream << it->second << ' ' << it->first << '\n';

                    query = "SELECT "+colList+" FROM '"+mTable+"' WHERE LineType='"+it->second+"' AND LineNo='"+it->first+"' AND flightNo="+flights[j] +" ORDER BY ROWID;";
                    mSql.exec(query);
                    while (mSql.next()) {
                        const QSqlRecord record = mSql.record();
                        for (int i = 0, recCount = record.count(); i < recCount; ++i) {
                            outStream << qSetFieldWidth(15) << record.value(i).toString();
                        }
                        outStream << qSetFieldWidth(0) << '\n';
                    }
                }
            } else {
                QString arrayColName;
                int arrayIndex = 0;
                QStringList lineList;
                QString Line;

                query = "SELECT * FROM PRAGMA_TABLE_INFO('"+mTable+"') where type LIKE '%[]%';";
                if (mSql.exec(query)) {
                    mSql.next();
                    arrayIndex = mSql.value(0).toInt();
                    arrayColName = mSql.value(1).toString();
                }
                DEBUG_TRACE(QString::number(arrayIndex));
                DEBUG_TRACE(arrayColName);

                query = "SELECT name FROM PRAGMA_TABLE_INFO('"+mTable+"') WHERE name LIKE '%line%';";
                DEBUG_TRACE(query);
                if (mSql.exec(query)) {
                    mSql.next();
                    Line = mSql.value(0).toString();

                    query = "SELECT DISTINCT "+ Line +" FROM '"+mTable+"';";
                    DEBUG_TRACE(query);
                    mSql.exec(query);
                    while (mSql.next()) {
                        lineList.append(mSql.value(0).toString());
                    }
                }
                DEBUG_TRACE(lineList);
                DEBUG_TRACE(Line);

                for (auto line : lineList) {
                    outStream << "Line " << line << '\n';
                    query = "SELECT "+colList+" FROM '"+mTable+"' WHERE "+Line+"='"+line+"' AND flightNo="+flights[j] +" ORDER BY ROWID;";
                    DEBUG_TRACE(query);
                    mSql.exec(query);
                    while (mSql.next()) {
                        const QSqlRecord record = mSql.record();
                        QStringList arrayList;

                        for (int i = 0, recCount = record.count(); i < recCount; ++i) {
                            if (i == (arrayIndex - 4)) {// ignored index of first 3 columns
                                QString arrayData = record.value(i).toString();
                                arrayData.remove('[');
                                arrayData.remove(']');
                                arrayList = arrayData.split(',');
                                arrayList.removeFirst();
                                outStream << qSetFieldWidth(15) << arrayList[0];
                                outStream << qSetFieldWidth(0);
                                if ((recCount - 1) == arrayIndex - 4)
                                    outStream << '\n';
                                for (int j = 1; j < arrayList.size(); ++j) {
                                    for (int k = 0, recCount = record.count(); k < recCount; ++k) {
                                        outStream << qSetFieldWidth(0);
                                        if (k == arrayIndex - 4) {
                                            outStream << qSetFieldWidth(15) << arrayList[j];
                                            continue;
                                        }
                                        outStream << qSetFieldWidth(15) << '*';
                                    }
                                    outStream << qSetFieldWidth(0) << '\n';
                                }
                            } else {
                                outStream << qSetFieldWidth(15) << record.value(i).toString();
                            }
                        }
                    }
                }
            }
        }


//        QMessageBox msgBox;
//        msgBox.setText(tr("Database is successfully exported to ")+fileName);
//        msgBox.exec();
    }

    db->closeDatabase();
    emit currentProgress("Export XYZ file done", 100);
}

void ExportToFile::exportToGBN(QString expFilePath)
{
    ExportGBN exportGBN(nullptr,mTable,expFilePath);
    connect(&exportGBN, &ExportGBN::exportGBNProgress, this, &ExportToFile::currentProgress);
    exportGBN.startExportGBN();
}

void ExportToFile::exportToNUV(QString expFilePath)
{
    ExportNDI exportNDI(nullptr, mTable, expFilePath);  //B2021_11_27_13_49, B8111500
    connect(&exportNDI, &ExportNDI::exportNUVProgress, this, &ExportToFile::currentProgress);
    exportNDI.startExportNDI();
}
