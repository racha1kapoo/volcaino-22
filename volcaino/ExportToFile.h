/**
 *  @file   DatabaseTable.h
 *  @brief  Export CSV/XYZ/NUV/GBN file data to database
 *  @author Rachana Kapoor
 *  @date   May 14, 2022
 ***********************************************/
#ifndef EXPORTTOFILE_H
#define EXPORTTOFILE_H

#include <QObject>
#include "DataBase.h"
#include "TypeDefConst.h"
#include <QSqlQuery>
#include <QApplication>
#include "ProgressBar.h"
class ExportToFile:public QObject
{
    Q_OBJECT
public:

    /// @brief This class is a singleton class having only one instance at a time
    ///
    /// @return   Instance of class ExportToFile
    ///
    static ExportToFile* getInstance();

    void setProgressBar(ProgressBar *);

signals:
    /// @brief This signal is emmitted when the application starts reading file.
    /// @return  void
    ///
    void started();

    /// @brief This signal is emmitted when the application finished reading file.
    /// @return  void
    ///
    void finished();

    void exportFileDone();

    void currentProgress(QString status, int value);

public slots:

    /// @brief This function is invoked when setTable signal is emitted.
    /// It sets the table name in this class.
    /// @param file: Name of the table
    /// @return  void
    ///
    void setTableName(QString table);

    /// @brief This function is invoked when setFile signal is emitted.
    /// It sets the file in this class.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setFile(QString file);

    /// @brief This function gets the list of tables from the database
    /// and populate the data in the table widget (GUI)
    /// @return  void
    ///
    void populateTableList(int value);

    void getExpInfo(int value, QString table);


private slots:
    void startExport();

private:

    ///Class constructor
    ExportToFile();

    /// @brief This function is a getter function which returns name of the table
    /// @return  Name of the table
    ///
    QString& getTableName();

    /// @brief This function removess any extra space present in the line
    /// @return QString
    ///
    QString escapedCSV(QString unexc);

    /// @brief This function is called when the file type of the file to be exported is CSV
    /// @return void
    ///
    void exportToCSV(QString expFilePath);

    /// @brief This function is called when the file type of the file to be exported is XYZ
    /// @return void
    ///
    void exportToXYZ(QString expFilePath);

    /// @brief This function is called when the file type of the file to be exported is GBN
    /// @return void
    ///
    void exportToGBN(QString expFilePath);

    void exportToNUV(QString expFilePath);

    QList<QMetaObject::Connection> mConnections;
    QString mTable;
    QString mExpFilePath;

    int mFileType = 0;

    ProgressBar *mBar = nullptr;
};

#endif // EXPORTTOFILE_H
