#include "GridToolBox.h"

GriddingToolBox::GriddingToolBox(QWidget *parent, QString table)
    : QDialog{parent}, mTable(table)
{
    this->setWindowTitle(tr("Gridding ToolBox"));
    this->resize(parent->width(), parent->height());
    setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint); //check

    QLabel *lblRadio = new QLabel(tr("Data from:"));
    mRadioDb = new QRadioButton(tr("Database"));
    mRadioExt = new QRadioButton(tr("External"));
    QHBoxLayout *hLayoutRadio = new QHBoxLayout;
    hLayoutRadio->addWidget(lblRadio);
    hLayoutRadio->addWidget(mRadioDb);
    hLayoutRadio->addWidget(mRadioExt);

    //Top Left Elements initialization
    mBtnOutFile = new QPushButton(tr("Select an output file"));
    mBoxGridMeth = new QComboBox(this);
    mBoxGridMeth->addItems({"Direct", "Bidirectional", "Napstrek"});
    mEdtCellSize = new QLineEdit("10");
    //    mEdtErrorFile = new QLineEdit;

    //Top Left Form initialization
    tLFormLayout = new QFormLayout;
    tLFormLayout->addRow(hLayoutRadio);
    tLFormLayout->addRow(tr("Output grid:"), mBtnOutFile);
    tLFormLayout->addRow(tr("Gridding method:"), mBoxGridMeth);
    tLFormLayout->addRow(tr("Cell size:"), mEdtCellSize);
    //    tLFormLayout->addRow(tr("Error grid file:"), mEdtErrorFile);

    //Grid Configuration Elements - Interpolation initialization
    mLblMaxDist = new QLabel(tr("Max Interp Distance"));
    mEdtMaxDist = new QLineEdit("200");
    mLblMaxIter = new QLabel(tr("Max Iteration Loop"));
    mEdtMaxIter = new QLineEdit("75");
    mLblTrendFact = new QLabel(tr("Trend Factor"));
    mEdtTrendFact = new QLineEdit("100");
    mLblSearchAng = new QLabel(tr("Search Angle"));
    mEdtSearchAng = new QLineEdit("10");
    mLblSearchStp = new QLabel(tr("Search Step Size"));
    mEdtSearchStp = new QLineEdit("0.25");
    mLblAutoStop = new QLabel(tr("Auto Stop?"));
    mBoxAutoStop = new QComboBox();
    mBoxAutoStop->addItems({"Yes", "No"});

    //Grid Configuration Tab Widget initialization
    mGridConfig = new QTabWidget;
    QWidget* tabInterpolation = new QWidget;
    QWidget* tabExtAndData = new QWidget;
    mGridConfig->addTab(tabInterpolation, "Interpolation");
    mGridConfig->addTab(tabExtAndData, "Extends and Data");

    //Grid Configuration Tab Layout initialization
    QGridLayout* gLayoutConf = new QGridLayout;
    gLayoutConf->addWidget(mLblMaxDist, 0, 0);
    gLayoutConf->addWidget(mEdtMaxDist, 0, 1);
    gLayoutConf->addWidget(mLblMaxIter, 1, 0);
    gLayoutConf->addWidget(mEdtMaxIter, 1, 1);
    gLayoutConf->addWidget(mLblTrendFact, 2, 0);
    gLayoutConf->addWidget(mEdtTrendFact, 2, 1);
    gLayoutConf->addWidget(mLblSearchAng, 3, 0);
    gLayoutConf->addWidget(mEdtSearchAng, 3, 1);
    gLayoutConf->addWidget(mLblSearchStp, 4, 0);
    gLayoutConf->addWidget(mEdtSearchStp, 4, 1);
    gLayoutConf->addWidget(mLblAutoStop, 5, 0);
    gLayoutConf->addWidget(mBoxAutoStop, 5, 1);
    tabInterpolation->setLayout(gLayoutConf);

    //func call
    showHidden();

    mLblImage = new QLabel;
    mLblImage->setBackgroundRole(QPalette::Base);
    mLblImage->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    mLblImage->setScaledContents(true);

    QVBoxLayout* vLayoutImage = new QVBoxLayout;
    vLayoutImage->addWidget(mLblImage);


    //Grid Visualization Tab Widget initialization
    mGridView = new QTabWidget;
    QWidget* tabGridPrev = new QWidget;
    QWidget* tabVariogram = new QWidget;
    mGridView->addTab(tabGridPrev, "Grid Preview");
    mGridView->addTab(tabVariogram, "Variogram");
    tabGridPrev->setLayout(vLayoutImage);

    //Bottom Buttons Elements initialization
    mBtnRestore = new QPushButton(tr("Restore Defaults"));
    mBtnStart = new QPushButton(tr("Start Gridding"));
    mBtnExport = new QPushButton(tr("Export GRID"));
    mBtnCancel = new QPushButton(tr("Cancel"));

    //Layout for bottom buttons initialization
    QHBoxLayout* hLayoutButtons = new QHBoxLayout;
    hLayoutButtons->addWidget(mBtnRestore);
    hLayoutButtons->addSpacing(this->width()/12.5);
    hLayoutButtons->addWidget(mBtnStart);
    hLayoutButtons->addWidget(mBtnExport);
    hLayoutButtons->addWidget(mBtnCancel);

    //Layout for Left side of window
    QVBoxLayout* vLayoutleft = new QVBoxLayout;
    vLayoutleft->addLayout(tLFormLayout);
    vLayoutleft->addWidget(mGridConfig);

    //Layout for Top (Right and Left) side of the window
    QHBoxLayout* hLayoutTop = new QHBoxLayout;
    hLayoutTop->addLayout(vLayoutleft);
    hLayoutTop->addWidget(mGridView);
    hLayoutTop->setStretch(0,2);
    hLayoutTop->setStretch(1,3);

    mLblPath = new QLabel(""); //check

    //Layout for the whole window
    QVBoxLayout* vLayoutWhole = new QVBoxLayout(this);
    vLayoutWhole->addLayout(hLayoutTop);
    vLayoutWhole->addWidget(mLblPath);
    vLayoutWhole->addLayout(hLayoutButtons);
    setLayout(vLayoutWhole);

    //connections
    connect(mBoxGridMeth, &QComboBox::currentTextChanged, this, &GriddingToolBox::showHidden);
    //    connect(mBtnInpFile, &QPushButton::clicked, this, &GriddingToolBox::browseFiles);

    connect(mBtnOutFile, &QPushButton::clicked, this, &GriddingToolBox::browseOutputPath);
    connect(mBoxGridMeth, &QComboBox::textActivated, this, &GriddingToolBox::getGridMethod);

    connect(mBtnStart, &QPushButton::clicked, this, &GriddingToolBox::startGridding);
    connect(mBtnExport, &QPushButton::clicked, this, &GriddingToolBox::generateGRDFile);
    //connect(ptrCancel, &QPushButton::clicked, this, &GridToolBox::cancelCloseStop);
    connect(mRadioDb, &QRadioButton::toggled, this, &GriddingToolBox::showDb);
    connect(mRadioExt, &QRadioButton::toggled, this, &GriddingToolBox::showExt);
}

GriddingToolBox::~GriddingToolBox()
{

}

void GriddingToolBox::showHidden()
{
    if (mBoxGridMeth->currentIndex() == 2) {
        mLblMaxIter->show();
        mEdtMaxIter->show();
        mLblTrendFact->show();
        mEdtTrendFact->show();
        mLblSearchAng->show();
        mEdtSearchAng->show();
        mLblSearchStp->show();
        mEdtSearchStp->show();
        mLblAutoStop->show();
        mBoxAutoStop->show();
    } else {
        mLblMaxIter->hide();
        mEdtMaxIter->hide();
        mLblTrendFact->hide();
        mEdtTrendFact->hide();
        mLblSearchAng->hide();
        mEdtSearchAng->hide();
        mLblSearchStp->hide();
        mEdtSearchStp->hide();
        mLblAutoStop->hide();
        mBoxAutoStop->hide();
    }
}

void GriddingToolBox::showExt()
{
    if (mRadioExt->isChecked()) {
        isFromDB = false;
        mLblImp = new QLabel(tr("File to grid:"));
        mBtnInpFile = new QPushButton(tr("Browse Drive"));
        connect(mBtnInpFile, &QPushButton::clicked, this, &GriddingToolBox::browseFiles);
        if (tLFormLayout->rowCount() > 4) { //Pressing on external after pressing on DB first
            tLFormLayout->removeRow(1);
            tLFormLayout->removeRow(1);
            tLFormLayout->removeRow(1);
            tLFormLayout->removeRow(1);
            tLFormLayout->insertRow(1, mLblImp, mBtnInpFile);
        } else { //first time pressing on External
            tLFormLayout->insertRow(1, mLblImp, mBtnInpFile);
        }
    }
}

void GriddingToolBox::showDb()
{
    if (mRadioDb->isChecked()) {
        isFromDB = true;
        QWidget *wDb = new QWidget(this);
        QLabel *lblTable = new QLabel(tr("Table name:"));
        QLabel *lblTableName = new QLabel;

        if (DataBase::getInstance()->openDatabase())
            lblTableName->setText(mTable);

        QLabel *lblXVal = new QLabel(tr("X Value:"));
        boxX = new QComboBox;
        boxX->addItems(DataBase::getInstance()->getHeaderDetails(mTable));
        QLabel *lblYVal = new QLabel(tr("Y Value:"));
        boxY = new QComboBox;
        boxY->addItems(DataBase::getInstance()->getHeaderDetails(mTable));
        QLabel *lblChannelVal = new QLabel(tr("Channel Value:"));
        boxVal = new QComboBox;

        boxVal->addItems(DataBase::getInstance()->getHeaderDetails(mTable));

        hLayoutDb = new QHBoxLayout;
        hLayoutDb->addWidget(lblTable, 0);
        hLayoutDb->addWidget(lblTableName, 1);
        hLayoutDb->setMargin(0);
        wDb->setLayout(hLayoutDb);
        if (tLFormLayout->rowCount() > 4) { //Pressed on Ext first then DB
            tLFormLayout->removeRow(1);
            tLFormLayout->insertRow(1, wDb);
            tLFormLayout->insertRow(2, lblChannelVal, boxVal);
            tLFormLayout->insertRow(3, lblXVal, boxX);
            tLFormLayout->insertRow(4, lblYVal, boxY);
        } else { //Pressed on DB first time
            tLFormLayout->insertRow(1, wDb);
            tLFormLayout->insertRow(2, lblChannelVal, boxVal);
            tLFormLayout->insertRow(3, lblXVal, boxX);
            tLFormLayout->insertRow(4, lblYVal, boxY);
        }
    }
}

void GriddingToolBox::browseFiles()
{
    QString theInputFilePath =
            QFileDialog::getOpenFileName(this, tr("Open a file to grid"), QDir().dirName().append("../")
                                         , (""));

    QString inputPathTrimmed;

    if (!theInputFilePath.isEmpty()) {
        inputFilePath = theInputFilePath;
        inputPathTrimmed = theInputFilePath;
        if (theInputFilePath.length() > 60) {
            inputPathTrimmed = inputPathTrimmed.right(60);
            inputPathTrimmed = inputPathTrimmed.right(60-inputPathTrimmed.indexOf("/"));
            inputPathTrimmed = "..." + inputPathTrimmed;
        }
        gridBoxStatusMsg = "Data source: " + inputPathTrimmed;
        mLblPath->setText(gridBoxStatusMsg);
    } else if (!inputFilePath.isEmpty() && theInputFilePath.isEmpty()) {
        inputPathTrimmed = inputFilePath;
        if (inputFilePath.length() > 60) {
            inputPathTrimmed = inputPathTrimmed.right(60);
            inputPathTrimmed = inputPathTrimmed.right(60-inputPathTrimmed.indexOf("/"));
            inputPathTrimmed = "..." + inputPathTrimmed;
        }
        gridBoxStatusMsg = "Data source: " + inputPathTrimmed;
        mLblPath->setText(gridBoxStatusMsg);
    } else if (inputFilePath.isEmpty() && theInputFilePath.isEmpty()) {
        gridBoxStatusMsg = "Please specify data source";
        mLblPath->setText(gridBoxStatusMsg);
    }
}

void GriddingToolBox::browseOutputPath()
{
    newOutPutFilePath =
            QFileDialog::getSaveFileName(this,tr("Open a file to grid"), QDir().dirName().append("../untitled.GRD")
                                         , tr("*.GRD"));

    QString outputPathTrimmed;

    if (newOutPutFilePath.isEmpty() && oldOutPutFilePath.isEmpty()) {
        mLblPath->setText("Please specify output file");
    } else if (!newOutPutFilePath.isEmpty()) {
        oldOutPutFilePath = newOutPutFilePath;
        outputPathTrimmed = oldOutPutFilePath;

        if (newOutPutFilePath.length() > 60) {
            outputPathTrimmed = outputPathTrimmed.right(60);
            outputPathTrimmed = outputPathTrimmed.right(60-outputPathTrimmed.indexOf("/"));
            outputPathTrimmed = "..." + outputPathTrimmed;
        }
        gridBoxStatusMsg = "Output grd file: " + outputPathTrimmed;
        mLblPath->setText(gridBoxStatusMsg);
    } else if (!oldOutPutFilePath.isEmpty() && newOutPutFilePath.isEmpty()) {
        outputPathTrimmed = oldOutPutFilePath;
        if (outputPathTrimmed.length() > 60) {
            outputPathTrimmed = outputPathTrimmed.right(60);
            outputPathTrimmed = outputPathTrimmed.right(60-outputPathTrimmed.indexOf("/"));
            outputPathTrimmed = "..." + outputPathTrimmed;
        }
        gridBoxStatusMsg = "Output grd file: " + outputPathTrimmed;
        mLblPath->setText(gridBoxStatusMsg);
    }
}

void GriddingToolBox::getGridMethod(QString method)
{
    if (method == "Direct") {
        mGridMethod = DIRECT;
    } else if (method == "Bi-directional") {
        mGridMethod = BDR;
    } else if (method == "Naperstek") {
        mGridMethod = NPK;
    }
}

void GriddingToolBox::startGridding()
{
    if(!isFromDB)
    {
        if (inputFilePath.isEmpty()) {
            mLblPath->setText("Please specify data source");
        } else {
            if (mBoxGridMeth->currentIndex() == DIRECT) {
                if (biDrGrid != nullptr) {
                    delete biDrGrid;
                    biDrGrid = nullptr;
                }
                std::vector<double> inPutPars;
                inPutPars.push_back(mEdtCellSize->text().toDouble());
                inPutPars.push_back(mEdtMaxDist->text().toDouble());


                biDrGrid = new BiDrGridding(inPutPars,inputFilePath,false);
                threadGridding = new QThread();
                biDrGrid->moveToThread(threadGridding);

                connect(threadGridding, &QThread::started, biDrGrid, &BiDrGridding::startGridding);
                connect(biDrGrid, &BiDrGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
                connect(biDrGrid, &BiDrGridding::gridProcessDone, this, &GriddingToolBox::griddingDone);

                threadGridding->start();
                disableOperations();
            }

            if (mBoxGridMeth->currentIndex() == BDR) {
                if (biDrGrid != nullptr) {
                    delete biDrGrid;
                    biDrGrid = nullptr;
                }
                std::vector<double> inPutPars;
                inPutPars.push_back(mEdtCellSize->text().toDouble());
                inPutPars.push_back(mEdtMaxDist->text().toDouble());

                biDrGrid = new BiDrGridding(inPutPars,inputFilePath,true);
                threadGridding = new QThread();
                biDrGrid->moveToThread(threadGridding);

                connect(threadGridding, &QThread::started, biDrGrid, &BiDrGridding::startGridding);
                connect(biDrGrid, &BiDrGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
                connect(biDrGrid, &BiDrGridding::gridProcessDone, this, &GriddingToolBox::griddingDone);

                threadGridding->start();
                disableOperations();

            } else if (mBoxGridMeth->currentIndex() == NPK) {
                if (npkGrid != nullptr) {
                    delete npkGrid;
                    npkGrid = nullptr;
                }
                std::vector<double> inPutPars;
                inPutPars.push_back(mEdtCellSize->text().toDouble());
                inPutPars.push_back(mEdtMaxDist->text().toDouble());
                inPutPars.push_back(mEdtMaxIter->text().toDouble());
                inPutPars.push_back(mEdtTrendFact->text().toDouble());
                inPutPars.push_back(mEdtSearchAng->text().toDouble());
                inPutPars.push_back(mEdtSearchStp->text().toDouble());

                if (mBoxAutoStop->currentText() == "Yes") {
                    inPutPars.push_back(1);
                } else if (mBoxAutoStop->currentText() == "No") {
                    inPutPars.push_back(0);
                }

                threadGridding = new QThread();
                npkGrid = new NPKGridding(inPutPars, inputFilePath);
                npkGrid->moveToThread(threadGridding);

                connect(threadGridding, &QThread::started, npkGrid, &NPKGridding::startGridding);
                connect(npkGrid, &NPKGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
                connect(npkGrid, &NPKGridding::gridProcessDone, this, &GriddingToolBox::griddingDone);

                threadGridding->start();
                disableOperations();
            }
        }
    }
    else
    {
        if(mBoxGridMeth->currentIndex() == NPK) {
            if (npkGrid != nullptr) {
                delete npkGrid;
                npkGrid = nullptr;
            }
            std::vector<double> inPutPars;
            inPutPars.push_back(mEdtCellSize->text().toDouble());
            inPutPars.push_back(mEdtMaxDist->text().toDouble());
            inPutPars.push_back(mEdtMaxIter->text().toDouble());
            inPutPars.push_back(mEdtTrendFact->text().toDouble());
            inPutPars.push_back(mEdtSearchAng->text().toDouble());
            inPutPars.push_back(mEdtSearchStp->text().toDouble());

            if (mBoxAutoStop->currentText() == "Yes") {
                inPutPars.push_back(1);
            } else if (mBoxAutoStop->currentText() == "No") {
                inPutPars.push_back(0);
            }

            threadGridding = new QThread();
            npkGrid = new NPKGridding(inPutPars, inputFilePath);
            if(isFromDB)  npkGrid->setDataBase(mTable,boxVal->currentText(),boxX->currentText(),boxY->currentText());
            npkGrid->moveToThread(threadGridding);

            connect(threadGridding, &QThread::started, npkGrid, &NPKGridding::startGridding);
            connect(npkGrid, &NPKGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
            connect(npkGrid, &NPKGridding::gridProcessDone, this, &GriddingToolBox::griddingDone);

            threadGridding->start();
            disableOperations();
        }
        else if(mBoxGridMeth->currentIndex() == DIRECT)
        {
            if (biDrGrid != nullptr) {
                delete biDrGrid;
                biDrGrid = nullptr;
            }
            std::vector<double> inPutPars;
            inPutPars.push_back(mEdtCellSize->text().toDouble());
            inPutPars.push_back(mEdtMaxDist->text().toDouble());


            biDrGrid = new BiDrGridding(inPutPars,inputFilePath,false);
            if(isFromDB)  biDrGrid->setDataBase(mTable,boxVal->currentText(),boxX->currentText(),boxY->currentText());
            threadGridding = new QThread();
            biDrGrid->moveToThread(threadGridding);

            connect(threadGridding, &QThread::started, biDrGrid, &BiDrGridding::startGridding);
            connect(biDrGrid, &BiDrGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
            connect(biDrGrid, &BiDrGridding::gridProcessDone, this, &GriddingToolBox::griddingDone);

            threadGridding->start();
            disableOperations();
        }
        else
        {
            mLblPath->setText("Not supported");
        }
    }
}

void GriddingToolBox::generateGRDFile()
{
    if (mGridMethod == DIRECT || mGridMethod == BDR) {
        if (biDrGrid == nullptr) {
            gridBoxStatusMsg = "Please grid first";
            mLblPath->setText(gridBoxStatusMsg);
        } else {
            if (newOutPutFilePath.isEmpty() && oldOutPutFilePath.isEmpty()) {
                gridBoxStatusMsg = "Please specify output file";
                mLblPath->setText(gridBoxStatusMsg);
            } else {
                if (!newOutPutFilePath.isEmpty()) {
                    biDrGrid->setOutPutPath(newOutPutFilePath);
                } else if (!oldOutPutFilePath.isEmpty()) {
                    biDrGrid->setOutPutPath(oldOutPutFilePath);
                }

                threadGRDFile = new QThread();
                biDrGrid->moveToThread(QThread::currentThread());
                biDrGrid->moveToThread(threadGRDFile);

                connect(biDrGrid, &BiDrGridding::writeFileProcessDone, threadGRDFile, &QThread::quit);
                connect(biDrGrid, &BiDrGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
                connect(threadGRDFile, &QThread::started, biDrGrid, &GriddingBase::startExportFile);
                connect(threadGRDFile, &QThread::finished, threadGRDFile, &QThread::deleteLater);
                connect(threadGRDFile, &QThread::finished, this, &GriddingToolBox::enableOperations);

                threadGRDFile->start();
                disableOperations();
            }
        }
    } else if (mGridMethod == NPK) {
        if (npkGrid == nullptr) {
            gridBoxStatusMsg = "Please grid first";
            mLblPath->setText(gridBoxStatusMsg);
        } else {
            if (newOutPutFilePath.isEmpty() && oldOutPutFilePath.isEmpty()) {
                gridBoxStatusMsg = "Please specify output file";
                mLblPath->setText(gridBoxStatusMsg);
            } else {
                if (!newOutPutFilePath.isEmpty()) {
                    npkGrid->setOutPutPath(newOutPutFilePath);
                } else if (!oldOutPutFilePath.isEmpty()) {
                    npkGrid->setOutPutPath(oldOutPutFilePath);
                }

                threadGRDFile = new QThread();
                npkGrid->moveToThread(QThread::currentThread());
                npkGrid->moveToThread(threadGRDFile);

                connect(npkGrid, &NPKGridding::writeFileProcessDone, threadGRDFile, &QThread::quit);
                connect(npkGrid, &NPKGridding::statusChanged, this, &GriddingToolBox::updateGridStatus);
                connect(threadGRDFile, &QThread::started, npkGrid, &GriddingBase::startExportFile);
                connect(threadGRDFile, &QThread::finished, threadGRDFile, &QThread::deleteLater);
                connect(threadGRDFile, &QThread::finished, this, &GriddingToolBox::enableOperations);

                threadGRDFile->start();
                disableOperations();
            }
        }
    }

    //    if(mGridMethod == DIRECT && biDrGrid || mGridMethod == BDR && biDrGrid
    //            || mGridMethod == NPK && biDrGrid)
    //    {
    //        threadGRDFile = new QThread();
    //        gridBasePtr->moveToThread(threadGRDFile);

    //        connect(gridBasePtr, &GriddingBase::writeFileProcessDone, threadGRDFile, &QThread::quit);
    //        connect(threadGRDFile, &QThread::started, gridBasePtr, &GriddingBase::startExportFile);
    //        connect(threadGRDFile, &QThread::finished, threadGRDFile, &QThread::deleteLater);
    //        connect(threadGRDFile, &QThread::finished, this, &GridToolBox::enableOperations);

    //        threadGRDFile->start();
    //        disableOperations();
    //    }
    //    else
    //    {

    //    }
}

void GriddingToolBox::updateGridStatus(QString status)
{
    mLblPath->setText(status);
}

void GriddingToolBox::enableOperations()
{
//    mBtnInpFile->setEnabled(true);
//    mBtnOutFile->setEnabled(true);
//    mBoxGridMeth->setEnabled(true);
//    mBtnRestore->setEnabled(true);
//    mBtnStart->setEnabled(true);
//    mBtnExport->setEnabled(true);
}

void GriddingToolBox::disableOperations()
{
//    mBtnInpFile->setEnabled(false);
//    mBtnOutFile->setEnabled(false);
//    mBoxGridMeth->setEnabled(false);
//    mBtnRestore->setEnabled(false);
//    mBtnStart->setEnabled(false);
//    mBtnExport->setEnabled(false);
}

void GriddingToolBox::griddingDone(GriddingBase *base)
{
    if (threadGridding) {
        threadGridding->quit();
        threadGridding->deleteLater();
    }
    loadImage(base);
    enableOperations();
}

void GriddingToolBox::loadImage(GriddingBase *gridBase)
{
    mLblImage->setPixmap(gridBase->mPixMap);
}
