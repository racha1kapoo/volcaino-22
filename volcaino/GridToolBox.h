#ifndef GRIDDINGTOOLBOX_H
#define GRIDDINGTOOLBOX_H

#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QStyle>
#include <QComboBox>
#include <QTabWidget>
#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QString>
#include <QRadioButton>
#include "TypeDefConst.h"
#include "NPKGridding.h"
#include "BiDrGridding.h"
#include "DataBase.h"
#include <QThread>
class GriddingToolBox : public QDialog
{
    Q_OBJECT
public:

    ///Class constructor
    ///
    explicit GriddingToolBox(QWidget *parent = nullptr, QString table="");

    ///Class destructor
    ///
    ~GriddingToolBox();

private slots:

    ///@brief this function shows the hidden form elements in the "Interpolation" tab
    ///
    ///@return void
    ///
    void showHidden();
    ///
    /// \brief showDb - shows the elements of the form related to Database input
    ///
    void showDb();
    ///
    /// \brief showExt - shows the elements of the form related to external file input
    ///
    void showExt();

    void browseFiles();
    void browseOutputPath();
    void getGridMethod(QString method);

    void startGridding();
    void generateGRDFile();
    void updateGridStatus(QString status);
    void enableOperations();
    void disableOperations();
    void griddingDone(GriddingBase *base);

private:
    QPushButton *mBtnInpFile;
    QPushButton* mBtnOutFile;
    QComboBox* mBoxGridMeth;
    QLineEdit* mEdtCellSize;
    QTabWidget* mGridConfig;
    QTabWidget* mGridView;

    QLabel* mLblMaxDist;
    QLineEdit* mEdtMaxDist;

    QLabel* mLblMaxIter;
    QLineEdit* mEdtMaxIter;

    QLabel* mLblTrendFact;
    QLineEdit* mEdtTrendFact;

    QLabel* mLblSearchAng;
    QLineEdit* mEdtSearchAng;

    QLabel* mLblSearchStp;
    QLineEdit* mEdtSearchStp;

    QLabel* mLblAutoStop;
    QComboBox* mBoxAutoStop;

    QPushButton* mBtnRestore;
    QPushButton* mBtnStart;
    QPushButton* mBtnExport;
    QPushButton* mBtnCancel;

    QLabel* mLblImage;
    double mWidth;
    double mAspRatio;
    QLabel* mLblPath;

    QString inputFilePath;
    QString gridBoxStatusMsg;
    QString newOutPutFilePath;
    QString oldOutPutFilePath;
    int mGridMethod = DIRECT;
    BiDrGridding *biDrGrid = nullptr;
    NPKGridding *npkGrid = nullptr;
    QThread* threadGridding;
    QThread *threadGRDFile;

    void loadImage(GriddingBase *);

    QString mTable;

    QRadioButton *mRadioDb;
    QRadioButton *mRadioExt;
    QPushButton *mBtnImp;

    QLabel *mLblImp;
    QHBoxLayout *hLayoutDb;
    QHBoxLayout *hLayoutImp;
    QWidget *wImp;
    QFormLayout *tLFormLayout;

    QList<QMetaObject::Connection> mConnections;

    bool isFromDB = false;

    QComboBox *boxVal;

    QComboBox *boxX;
    QComboBox *boxY;
};

#endif // GRIDDINGTOOLBOX_H
