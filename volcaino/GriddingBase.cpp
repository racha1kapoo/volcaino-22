#include "GriddingBase.h"
GriddingBase::GriddingBase()
{

}

void GriddingBase::getGRIDHeaderPars()
{
    dataStoragePars.resize(5);
    dataStoragePars[0] = 8;
    dataStoragePars[1] = 1;
    dataStoragePars[2] = mImgWidth;
    dataStoragePars[3] = mImgHeight;
    dataStoragePars[4] = 1;   //+- 1??

    geoInformation.resize(5);
    geoInformation[0] = mCellSize;   //??
    geoInformation[1] = mCellSize;   //??
    geoInformation[2] = mXmin;       //??
    geoInformation[3] = mYmin;
    geoInformation[4] = 0;

    dataScaling.resize(2);
    dataScaling[0] = 0;
    dataScaling[0] = 1;

    optionalPars1.resize(64);
    optionalPars2.resize(5);
    optionalPars2[0] = 0;
    optionalPars2[1] = 0;   //??UnitX
    optionalPars2[2] = 0;   //??UnitY
    optionalPars2[3] = 0;   //??UnitZ
    optionalPars2[4] = mNumberOfValid;

    optionalPars3.resize(4);
    optionalPars3[0] = mValueMin;
    optionalPars3[1] = mValueMax;

    optionalPars4 = 0;
    optionalPars5 = 0;

    undefinedPars.resize(324);
    QString str = "You have found an easter egg!";
    for(int i = 0; i < str.size(); i++)
    {
        undefinedPars[i] = str[i].toLatin1();
    }
}

void GriddingBase::startExportFile()
{

    mGRDFile.setFileName(mOutPutPath);
    if(mGRDFile.open(QIODevice::WriteOnly))
    {
        mStatusMsg = "Starting to generate a GRD file...";
        emit statusChanged(mStatusMsg);

        getGRIDHeaderPars();
        mGRDFile.seek(0);
        wGridHDataStoragePars();
        wGridHGeoInformation();
        wGridHDataScaling();
        wGridHOptionalPars1();
        wGridHOptionalPars2();
        wGridHOptionalPars3();
        wGridHOptionalPars4();
        wGridHOptionalPars5();
        wGridHUndfPars();
        wGridData();
        mGRDFile.close();

        QString outputPathTrimmed = mOutPutPath;
        if(outputPathTrimmed.length() > 50)
        {
            outputPathTrimmed = outputPathTrimmed.right(50);
            outputPathTrimmed = outputPathTrimmed.right(50-outputPathTrimmed.indexOf("/"));
            outputPathTrimmed = "..." + outputPathTrimmed;
        }
        mStatusMsg = "GRD file generated, saved at: " + outputPathTrimmed;
        emit statusChanged(mStatusMsg);
    }
    else
    {
        mStatusMsg = "Please specify a vaild output path";
        emit statusChanged(mStatusMsg);
    }
    mGRDFile.close();
    emit writeFileProcessDone();
}

void GriddingBase::setOutPutPath(QString path)
{
    mOutPutPath = path;
}


void GriddingBase::wGridHDataStoragePars()
{
    unsigned char binaryExport[20] = {0};

    memcpy(&binaryExport[0], &dataStoragePars[0], 20);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 20);
}

void GriddingBase::wGridHGeoInformation()
{
    unsigned char binaryExport[40] = {0};

    memcpy(&binaryExport[0], &geoInformation[0], 40);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 40);
}

void GriddingBase::wGridHDataScaling()
{
    unsigned char binaryExport[16] = {0};

    memcpy(&binaryExport[0], &dataScaling[0], 16);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 16);
}

void GriddingBase::wGridHOptionalPars1()
{
    unsigned char binaryExport[64] = {0};

    memcpy(&binaryExport[0], &optionalPars1[0], 64);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 64);
}

void GriddingBase::wGridHOptionalPars2()
{
    unsigned char binaryExport[20] = {0};

    memcpy(&binaryExport[0], &optionalPars2[0], 20);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 20);
}

void GriddingBase::wGridHOptionalPars3()
{
    unsigned char binaryExport[16] = {0};

    memcpy(&binaryExport[0], &optionalPars3[0], 16);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 16);
}

void GriddingBase::wGridHOptionalPars4()
{
    unsigned char binaryExport[8] = {0};

    memcpy(&binaryExport[0], &optionalPars4, 8);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 8);
}

void GriddingBase::wGridHOptionalPars5()
{
    unsigned char binaryExport[4] = {0};

    memcpy(&binaryExport[0], &optionalPars5, 4);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 4);
}

void GriddingBase::wGridHUndfPars()
{
    unsigned char binaryExport[324] = {0};

    memcpy(&binaryExport[0], &undefinedPars[0], 324);
    mGRDFile.write(reinterpret_cast<char*>(binaryExport), 324);
}

void GriddingBase::wGridData()
{
    //int dummyValue;
    //if(toolBoxPtr->mGridMethod == DIRECT || toolBoxPtr->mGridMethod == BDR) dummyValue = 0;
    //else if(toolBoxPtr->mGridMethod == NPK) dummyValue = -1;   //TODO
    int rowCount = mImgHeight;
    int colCount = mImgWidth;

    unsigned char binaryExport[8] = {0};
    std::vector<std::vector<double>> &valRef = getGridData();
    std::vector<std::vector<int>> &flagRef = getGridDataFlag();

    for(int row = rowCount-1; row >= 0; row--)
    {
        for(int col = 0; col < colCount; col++)
        {
            if(flagRef[row][col] != DUMMYVALFLG)
            {memcpy(&binaryExport[0], &valRef[row][col], 8);}

            else
            {
                switch(dataStoragePars[0])
                {
                case 8:
                {memcpy(&binaryExport[0], &dummyValueFloat, 8);}
                    break;

                default:
                    break;
                }
            }
            mGRDFile.write(reinterpret_cast<char*>(binaryExport), 8);
        }
    }
}

