#ifndef GRIDDINGBASE_H
#define GRIDDINGBASE_H
#include <QObject>
#include <QImage>
#include <QPixmap>

#include <QFile>
#include "TypeDefConst.h"
#define DUMMYVALFLG -1
class GriddingBase: public QObject
{
    Q_OBJECT
public:
    GriddingBase();

    void getGRIDHeaderPars();
    std::vector<long> dataStoragePars;
    std::vector<double> geoInformation;
    std::vector<double> dataScaling;
    std::vector<char> optionalPars1;
    std::vector<long> optionalPars2;
    std::vector<float> optionalPars3;
    double optionalPars4;
    long optionalPars5;
    std::vector<char> undefinedPars;

    int mNumberOfValid = 0;

    int mImgWidth;
    int mImgHeight;
    double mCellSize = 1.0;

    double mXmin;
    double mXmax;
    double mYmin;
    double mYmax;

    double mValueMax;
    double mValueMin;

    QString mStatusMsg;

    virtual std::vector<std::vector<double>>& getGridData() = 0;
    virtual std::vector<std::vector<int>>& getGridDataFlag() = 0;

    QImage mImg;
    QPixmap mPixMap;

    //GridToolBox *toolBoxPtr = nullptr;

    void setOutPutPath(QString path);

public slots:
    void startExportFile();

signals:
    void statusChanged(QString);
    void enableBoxOperations(bool);
    void gridProcessDone(GriddingBase *);
    void writeFileProcessDone();

private:
    void wGridHDataStoragePars();
    void wGridHGeoInformation();
    void wGridHDataScaling();
    void wGridHOptionalPars1();
    void wGridHOptionalPars2();
    void wGridHOptionalPars3();
    void wGridHOptionalPars4();
    void wGridHOptionalPars5();
    void wGridHUndfPars();
    void wGridData();
    long long dummyValuesInt[6] = {-127,255,-32767,65535,-2147483647,4294967295};
    double dummyValueFloat = -1.0e32;

    QFile mGRDFile;
    QString mOutPutPath;

};

#endif // GRIDDINGBASE_H
