#include "ImportFile.h"
#include "DatabaseTable.h"
#include "mainwindow.h"
#include <QThread>
#include <QSqlQuery>
#include <QFileDialog>
#include <fstream>
#include <QRegularExpression>
#include <regex>
#include "ImportNUV.h"
#include "ImportGBN.h"

ImportFile::ImportFile():mTable{"N/A"}
{
    QThread *thread=new QThread;
    this->moveToThread(thread);
    //connect(thread,&QThread::finished, this, [](){qDebug() << "I am done!!";});
    //connect(thread,&QThread::finished,this,&QObject::deleteLater);
    thread->start();
}

ImportFile* ImportFile::getInstance()
{
    static ImportFile theInstance;
    return &theInstance;
}

void ImportFile::setTableName(QString table)
{
    if (table != "")
        if (table != mTable)
        {
            while (table.contains("("))
                table.replace("(", "_");

            while (table.contains(")"))
                table.replace(")", "_");

            while (table.contains("-"))
                table.replace("-", "_");

            mTable=table;
        }
}

void ImportFile::setFile(QString file)
{
    DEBUG_TRACE(file);
    QFileInfo fileInfo(file);
    QString ext = fileInfo.suffix();
    if (ext.compare("csv",Qt::CaseInsensitive) == 0) {
        this->setAndLoadCSVFile(file);
    } else if (ext.compare("XYZ",Qt::CaseInsensitive) == 0) {
        this->setAndLoadXYZFile(file);
    } else if (ext.compare("NUV",Qt::CaseInsensitive) == 0
               || ext.compare("PEI",Qt::CaseInsensitive) == 0
               || ext.compare("NDI",Qt::CaseInsensitive) == 0) {
        this->setAndLoadNUVFile(file);
    } else if (ext.compare("gbn",Qt::CaseInsensitive) == 0) {
        this->setAndLoadGBNFile(file);
    }
}

QString& ImportFile::getTableName()
{
    return mTable;
}

void ImportFile::setAndLoadCSVFile(QString filename)
{
    if (!filename.isNull()) {
        emit currentProgress("Starting import CSV...", 0);
        DEBUG_TRACE("selected file : " + filename.toUtf8());
        //emit started();
        auto db = DataBase::getInstance();
        auto status = db->openDatabase();

        if (status) {
            QFile file(filename);
            if (file.open(QIODevice::ReadOnly)) {
                QTextStream ts (&file);
                QStringList header = ts.readLine().split(',');
                QString line = ts.readLine();
                if (line.contains("[")) {
                    QStringList arrayDataList = line.split("\"");
                    arrayDataList.removeAll(",");
                    arrayDataList.removeLast();
                    QStringList allDataList;
                    for (QString data : arrayDataList) {
                        if (!data.startsWith("[")) {
                            allDataList.append(data.split(','));
                            allDataList.removeLast();
                            arrayDataList.removeAt(arrayDataList.indexOf(data));
                        }
                    }
                    allDataList.append(arrayDataList);
                    auto typeList = this->typeDeduction(header,allDataList);
                    db->createTable(this->getTableName(),typeList);
                    ts.seek(0);
                    ts.readLine();
                    db->startTransaction();
                    emit currentProgress("Inserting to database...", 40);

                    while (!ts.atEnd()) {
                        QString line = ts.readLine();
                        QStringList arrayDataList = line.split("\"");
                        arrayDataList.removeAll(",");
                        arrayDataList.removeLast();
                        QStringList allDataList;
                        for (QString data : arrayDataList) {
                            if (!data.startsWith("[")) {
                                if (data == ",,") {
                                    arrayDataList.replace(arrayDataList.indexOf(data), "");
                                    continue;
                                }
                                allDataList.append(data.split(','));
                                allDataList.removeLast();
                                arrayDataList.removeAt(arrayDataList.indexOf(data));
                            }
                        }
                        allDataList.append(arrayDataList);
                        for (int i = 0; i < allDataList.count(); i++) {
                            if (allDataList.at(i).trimmed().isEmpty()) {
                                allDataList.replace(i, "NULL");
                            } else if (typeList.at(i).contains("TEXT") || typeList.at(i).contains("FLOAT[]")) {
                                QString tmp = "'" + allDataList.at(i) + "'";
                                allDataList.replace(i, tmp);
                            }
                        }

                        QString record;
                        for (int i = 0; i < allDataList.length(); ++i) {
                            record.append(allDataList.at(i));
                            record.append(",");
                        }
                        record.chop(1);
                        db->insertRecord(record, this->getTableName());
                    }
                    db->closeTransaction();
                } else {
                    QStringList allDataList = line.split(',');
                    auto typeList = this->typeDeduction(header,allDataList);
                    db->createTable(this->getTableName(),typeList);
                    ts.seek(0);
                    ts.readLine();
                    db->startTransaction();
                    emit currentProgress("Inserting to database...", 40);

                    while (!ts.atEnd()) {
                        QStringList line = ts.readLine().split(',');

                        for (int i = 0; i < line.count(); i++) {
                            if (line.at(i).trimmed().isEmpty()) {
                                line.replace(i, "NULL");
                            } else if (typeList.at(i).contains("TEXT")) {
                                QString tmp = "'" + line.at(i) + "'";
                                line.replace(i, tmp);
                            }
                        }

                        QString record;
                        for (int i = 0; i < line.length(); ++i) {
                            record.append(line.at(i));
                            record.append(",");
                        }
                        record.chop(1);
                        db->insertRecord(record,this->getTableName()); //Extend functionality to cater different data tables
                    }
                    db->closeTransaction();
                }
            }
            file.close();
        }
        emit currentProgress("Import CSV file done", 100);
        emit finished();
    }
}

void ImportFile::setAndLoadXYZFile(QString filename)
{
    bool reading=false;
    if(!filename.isNull()){
        emit currentProgress("Starting import XYZ...", 0);

        DEBUG_TRACE("selected file : " + filename.toUtf8());

        auto db=DataBase::getInstance();

        auto status=db->openDatabase();

        if(status){
            QFile file(filename);
            if(file.open (QIODevice::ReadOnly)){
                QTextStream stream(&file);
                QString line;
                QString lineNo;
                QString lineType;
                QString flightNo;
                QString date;
                QStringList headerList;
                QStringList dataList;
                int counter=0;

                for(int counter=1;counter<13;++counter){
                    line = stream.readLine();
                    if(counter==6){
                        headerList.clear();
                        headerList.append("flightNo");
                        headerList.append("flightDate");
                        headerList.append("lineType");
                        headerList.append("lineNo");
                        headerList.append(line.simplified().split(" "));
                        headerList.removeOne("/");
                        headerList.removeOne("");

                        DEBUG_TRACE(headerList);
                    }
                    for(auto str:headerList){
                        this->isArray(str);
                    }
                    DEBUG_TRACE(mArray);
                    if(mArray.size()!=0){
                        for(auto str:headerList){
                            for(auto& value:mArray){
                                if(str.contains(value.first)){
                                    headerList.removeOne(str);
                                }
                            }
                        }

                        if(line.contains("//Flight")){
                            flightNo.clear();
                            flightNo.append(line.simplified().split(" ").last());
                            DEBUG_TRACE(line+ " " + flightNo);
                            continue;
                        }
                        if(line.contains("//Date")){
                            date.clear();
                            date.append(line.simplified().split(" ").last());
                            DEBUG_TRACE(date);
                            continue;
                        }
                        if(line.contains("Line")||line.contains("Tie")){
                            lineNo.clear();
                            lineNo.append(line.simplified().split(" ").last());
                            lineType.clear();
                            lineType.append(line.simplified().split(" ").first());
                        }
                        if(counter==12){
                            dataList.clear();
                            dataList.append(flightNo);
                            dataList.append(date);
                            dataList.append(lineType);
                            dataList.append(lineNo);
                            auto data=line.simplified().split(" ");
                            while(data.first()=="*" || data.at(1)=="*"){
                                line.clear();
                                data.clear();
                                line = stream.readLine();
                                data=line.simplified().split(" ");

                            }
                            DEBUG_TRACE(data);
                            for(auto& value:mArray){
                                headerList.append(value.first);
                            }
                            DEBUG_TRACE(headerList);
                            DEBUG_TRACE(dataList);
                            for(int i=0;i<headerList.size()-4;++i){
                                dataList.append(data[i]);

                            }

                            DEBUG_TRACE(dataList);
                            auto typeList=this->typeDeduction(headerList,dataList);

                            for(auto &str:typeList){
                                for(auto& value:mArray){
                                    DEBUG_TRACE(str);
                                    if(str.contains(value.first)){
                                        str.remove(",");
                                        str.append("[],");
                                    }
                                }
                            }
                            DEBUG_TRACE(typeList);
                            typeList.push_front("dataType INTEGER DEFAULT 0,");
                            recordCopy.append(1);
                            for(auto str:headerList){
                                recordCopy.append(1);
                                for(auto& value:mArray){
                                    if(str.contains(value.first)){
                                        recordCopy.removeLast();
                                        recordCopy.append(value.second+1);
                                    }
                                }
                            }
                            for(int i=0;i<recordCopy.size();++i){
                                int counter=0;
                                if(i>0){
                                    if(recordCopy[i]>1){
                                        mColumnCounter.push_back(std::make_pair(recordCopy[i],1));
                                        continue;
                                    }
                                    if(recordCopy[i]==recordCopy[i-1])continue;
                                }
                                for(int j=0;j<recordCopy.size();++j){
                                    if(recordCopy[j]==recordCopy[i]&&recordCopy[j]==1){
                                        ++counter;
                                    }
                                }
                                mColumnCounter.push_back(std::make_pair(recordCopy[i],counter));
                            }
                            DEBUG_TRACE(recordCopy);
                            DEBUG_TRACE(mColumnCounter);

                            db->createTable(this->getTableName(),typeList);
                        }

                    } else {
                        if(line.contains("//Flight")){
                            flightNo.clear();
                            flightNo.append(line.simplified().split(" ").last());

                            DEBUG_TRACE(line+ " " + flightNo);

                            continue;
                        }
                        if(line.contains("//Date")){
                            date.clear();
                            date.append(line.simplified().split(" ").last());

                            DEBUG_TRACE(date);

                            continue;
                        }
                        if(line.contains("Line")||line.contains("Tie")){
                            lineNo.clear();
                            lineNo.append(line.simplified().split(" ").last());
                            lineType.clear();
                            lineType.append(line.simplified().split(" ").first());
                        }
                        if(counter==12){
                            dataList.clear();
                            dataList.append(flightNo);
                            dataList.append(date);
                            dataList.append(lineType);
                            dataList.append(lineNo);
                            dataList.append(line.simplified().split(" "));
                            auto typeList=this->typeDeduction(headerList,dataList);

                            DEBUG_TRACE(typeList);

                            db->createTable(this->getTableName(),typeList);
                        }
                    }


                }

                emit currentProgress("Inserting to database...", 40);

                if(mArray.size()!=0){
                    //emit started();
                    db->startTransaction();
                    DEBUG_TRACE(mArray);
                    stream.seek(0);

                    do
                    {
                        counter=counter+1;

                        line = stream.readLine();
                        if(line.isEmpty())continue;

                        if(line.contains("//Flight")){
                            flightNo.clear();
                            flightNo.append(line.simplified().split(" ").last());
                            DEBUG_TRACE(line+" "+flightNo);
                            continue;
                        }
                        if(line.contains("//Date")){
                            date.clear();
                            date.append(line.simplified().split(" ").last());
                            DEBUG_TRACE(date);
                            reading=true;
                            continue;
                        }
                        if(reading){
                            if(line.contains("Line")||line.contains("Tie"))
                            {
                                lineNo.clear();
                                lineNo.append(line.simplified().split(" ").last());
                                lineType.clear();
                                lineType.append(line.simplified().split(" ").first());
                            }
                            else{

                                QStringList lineStr=line.simplified().split(" ");
                                QString record="'1',"+flightNo+",'"+date+"','"+lineType+"',"+lineNo+",";
                                int k=3;
                                for(int i=0; i<lineStr .length ();){
                                    DEBUG_TRACE(lineStr);
                                    for(int k=0;k<mColumnCounter.size();++k){
                                        if(mColumnCounter[k].first==1){
                                            int count=mColumnCounter[k].second;;
                                            if(k==0){count=mColumnCounter[k].second-5;}
                                            for(int j=0; j<count;++j){      //five channels are already added above
                                                record.append("'");
                                                record.append(lineStr.at(i));
                                                record.append("',");
                                                ++i;
                                            }
                                        }
                                        else{
                                            // record.append("'[25,");
                                            record.append("'[");
                                            record.append(QString::number(mColumnCounter[k].first));
                                            record.append(",");
                                            for(int j=0; j<mColumnCounter[k].first;++j){
                                                record.append(lineStr.at(i));
                                                record.append(",");
                                                ++i;
                                            }
                                            record.chop(1);
                                            record.append("]',");
                                        }
                                    }
                                }
                                record.chop(1);
                                DEBUG_TRACE(record);

                                if(counter==12)DEBUG_TRACE(record);


                                db->insertRecord(record,this->getTableName());
                            }
                        }
                    } while (!line.isNull());


                    db->closeTransaction();
                    emit currentProgress("Import XYZ file done", 100);
                    emit finished();

                }
                else{
                    //emit started();
                    db->startTransaction();
                    stream.seek(0);

                    do
                    {
                        counter=counter+1;

                        line = stream.readLine();
                        if(line.isEmpty())continue;

                        if(line.contains("//Flight")){
                            flightNo.clear();
                            flightNo.append(line.simplified().split(" ").last());
                            DEBUG_TRACE(line+" "+flightNo);
                            continue;
                        }
                        if(line.contains("//Date")){
                            date.clear();
                            date.append(line.simplified().split(" ").last());
                            DEBUG_TRACE(date);
                            reading=true;
                            continue;
                        }
                        if(reading){
                            if(line.contains("Line")||line.contains("Tie"))
                            {
                                lineNo.clear();
                                lineNo.append(line.simplified().split(" ").last());
                                lineType.clear();
                                lineType.append(line.simplified().split(" ").first());
                            }
                            else{

                                QStringList lineStr=line.simplified().split(" ");
                                QString record=flightNo+",'"+date+"','"+lineType+"',"+lineNo+",";
                                for(int i=0; i<lineStr .length ();++i){
                                    record.append("'");
                                    record.append(lineStr.at(i));
                                    record.append("',");
                                }
                                record.chop(1);
                                if(counter==12)DEBUG_TRACE(record);
                                db->insertRecord(record,this->getTableName());
                            }
                        }
                    } while (!line.isNull());

                    db->closeTransaction();
                    emit currentProgress("Import XYZ file done", 100);
                    emit finished();
                }
            }
            file.close();
        }
    }
}

void ImportFile::setAndLoadNUVFile(QString filename)
{
    if (!filename.isNull()) {
        emit currentProgress("Starting import NUV...", 0);
        //DEBUG_TRACE("selected file : " + filename.toUtf8());
        auto db=DataBase::getInstance();
        auto status=db->openDatabase();

        if (status) {
            QStringList typeList;
            int flightNo=0;
            QString flightDate="";
            int dataType=0;
            int totalRows=0;
//            unsigned int arraySize=0;  //Change this value as per the header details
//            QString arrayChName;
//            bool isArrayChan=false;
//            int arrayIndex=0;

            emit currentProgress("Reading NUV file...", 10);
            ImportNUV nuvFile(NULL,filename);
            int digits = nuvFile.maxDigits;
            //auto nuvFile=new ImportNUV(NULL,filename);

            //get flight No
            flightNo=nuvFile.getFlightNumber();

            //get flight Date
            flightDate=nuvFile.getFlightDate();

            //get Data type
            dataType=nuvFile.getFileType();

            //get No of records
            totalRows=nuvFile.getTableNumOfRows();

            QList<nuvChStruct> headerStruct=nuvFile.getAllChannels();
            auto data=nuvFile.getPEIDecodedVals();

            emit currentProgress("Inserting to database...", 40);
            if (data) {
                if (dataType == FileType::Spectrum) {
                    typeList.clear();
                    typeList.append("dataType INTEGER,");
                    typeList.append("flightNo INTEGER,");
                    typeList.append("flightDate TEXT,");

                    //                auto totalSpectometerIds=totalRows/arraySize;
                    //                DEBUG_TRACE(totalSpectometerIds);

                    for (int i = 0; i < headerStruct.size(); ++i) {
                        if (headerStruct[i].chStruct.isArrChan) {
                            typeList<<headerStruct[i].chName+" FLOAT[],";
                        } else {
                            typeList<<headerStruct[i].chName+" FLOAT,";
                        }
                    }
                    //DEBUG_TRACE(typeList);
                    emit started();
                    db->createTable(this->getTableName(),typeList);
                    db->startTransaction();

                    for (int i = 0; i < totalRows; ++i) {
                        QString record;
                        record.append(QString::number(dataType));
                        record.append(",");
                        record.append(QString::number(flightNo));
                        record.append(",'");
                        record.append(flightDate);
                        record.append("',");

                        for (unsigned int j = 0; j < headerStruct.size(); ++j) {
                            if (!headerStruct[j].chStruct.isArrChan) {
                                if (data[i][j] == nullValue) {
                                    record.append("NULL,");
                                    //record.append(",");
                                } else {
                                    record.append(QString::number(data[i][j], 'g', digits));
                                    record.append(",");
                                }
                            } else { //Array chan
                                //record.append("NULL,");
                                int rowInOneSec = i % nuvFile.mMaxSps;
                                int theRatio = nuvFile.mMaxSps/headerStruct[j].chStruct.chSps;

                                if (rowInOneSec % theRatio != 0) {
                                    record.append("NULL,");
                                } else {
                                    int nthArrayChan = nuvFile.mArrChans.indexOf(j);
                                    int arrayLength = headerStruct[j].chStruct.arrayLength;
                                    int arrayReadIndex = nuvFile.mArrReadIndex[nthArrayChan];
                                    QString arrRecord = "'[";
                                    arrRecord.append(QString::number(arrayLength));
                                    arrRecord.append(",");

                                    for (int i = 0; i < arrayLength; i++) {
                                        double val = nuvFile.getArrayData()[nthArrayChan][arrayReadIndex];
                                        arrRecord.append(QString::number(val, 'g', digits));
                                        arrRecord.append(",");
                                        arrayReadIndex++;
                                    }

                                    nuvFile.mArrReadIndex[nthArrayChan] = arrayReadIndex;
                                    arrRecord.chop(1);
                                    arrRecord.append("]'");

                                    record.append(arrRecord);
                                    record.append(",");
                                }
                            }
                        }
                        record.chop(1);
                        db->insertRecord(record,this->getTableName());
                    }

                    db->closeTransaction();
                    emit currentProgress("Import NUV file done", 100);
                    emit finished();
                } else {
                    typeList.clear();
                    typeList.append("dataType INTEGER,");
                    typeList.append("flightNo INTEGER,");
                    typeList.append("flightDate TEXT,");

                    for (int i=0;i<headerStruct.size();++i)
                        typeList << headerStruct[i].chName+" FLOAT,";

                    //DEBUG_TRACE(typeList);

                    emit started();
                    db->createTable(this->getTableName(),typeList);
                    db->startTransaction();

                    for (unsigned int i = 0; i < totalRows; ++i) {
                        QString record;
                        record.append(QString::number(dataType));
                        record.append(",");
                        record.append(QString::number(flightNo));
                        record.append(",'");
                        record.append(flightDate);
                        record.append("',");

                        if (i % 5000 == 0) {
                            int value = (double)i/(double)totalRows*60+40;
                            emit currentProgress("Inserting to database...", value);
                        }

                        for (unsigned int j = 0; j < headerStruct.size(); ++j) {
                            if (data[i][j] == nullValue) {
                                record.append("NULL,");
                                //record.append(",");
                                continue;
                            }
                            record.append(QString::number(data[i][j],'g',digits));
                            record.append(",");
                        }
                        record.chop(1);
                        db->insertRecord(record,this->getTableName());
                    }

                    db->closeTransaction();
                    emit finished();
                }
            }
            emit currentProgress("Import NUV file done", 100);
            emit finished();
        }
    }
}

void ImportFile::setAndLoadGBNFile(QString filename)
{
    //DEBUG_TRACE("selected file : " + filename.toUtf8());
    emit currentProgress("Starting import GBN...", 0);
    emit currentProgress("Reading GBN file...", 10);
    ImportGBN importGBN(nullptr, filename);
    int numberOfRows = importGBN.getTableNumOfRows();
    //DEBUG_TRACE("selected file : " + filename.toUtf8());
    auto db = DataBase::getInstance();

    if (db->openDatabase()) {
        QStringList typeList;
        typeList.clear();
        //emit started();
        std::vector<gbnChsRecStruct> headerStruct = importGBN.getAllChannels();
        QList<int> arrayChans = importGBN.getArrayChanNumbers();

        typeList.append("flightDate TEXT,");
        typeList.append("flightNo INTEGER,");
        typeList.append("LineType TEXT,");
        typeList.append("LineNo TEXT,");

        if (arrayChans.length() == 0) {
            for (int i = 0; i < headerStruct.size(); ++i) {
                if (!importGBN.mDupFDFNLYLN.contains(i)) {
                    if (headerStruct[i].channelType == GS_FLOAT
                            || headerStruct[i].channelType == GS_DOUBLE) {
                        typeList << headerStruct[i].channelName+" FLOAT,";
                    } else if (headerStruct[i].channelType == GS_UNSIGNED_BYTE) {
                        typeList << headerStruct[i].channelName+" TEXT,";
                    } else {
                        typeList << headerStruct[i].channelName+" INTEGER,";
                    }
                }
            }
        } else {
            for (int i = 0; i < headerStruct.size(); ++i) {
                if (!importGBN.mDupFDFNLYLN.contains(i)) {
                    if (!arrayChans.contains(i)) {
                        if (headerStruct[i].channelType == GS_FLOAT
                                || headerStruct[i].channelType == GS_DOUBLE) {
                            typeList << headerStruct[i].channelName+" FLOAT,";
                        } else if (headerStruct[i].channelType == GS_UNSIGNED_BYTE) {
                            typeList << headerStruct[i].channelName+" TEXT,";
                        } else {
                            typeList << headerStruct[i].channelName+" INTEGER,";
                        }
                    } else {
                        if(headerStruct[i].channelType == GS_FLOAT
                                || headerStruct[i].channelType == GS_DOUBLE) {
                            typeList << headerStruct[i].channelName+" FLOAT[],";
                        } else if (headerStruct[i].channelType == GS_UNSIGNED_BYTE) {
                            typeList << headerStruct[i].channelName+" TEXT,";
                        } else {
                            typeList << headerStruct[i].channelName+" INTEGER[],";
                        }
                    }
                }
            }
        }
        db->createTable(this->getTableName(),typeList);
        emit currentProgress("Inserting to database...", 40);
        db->startTransaction();
        int row = 0;
        while (row < numberOfRows) {
            QString record;
            importGBN.getOneRow(row, record);
            db->insertRecord(record, this->getTableName());
            if (row % 5000 == 0) {
                int value = (double)row/(double)numberOfRows*60+40;
                emit currentProgress("Inserting to database...", value);
            }
            row++;
        }
        db->closeTransaction();
    }
    emit currentProgress("Import GBN file done", 100);
    emit finished();
}

QStringList ImportFile::typeDeduction(QStringList &theHeader,QStringList &theData)
{
    QStringList columnList;
    DEBUG_TRACE(theData);
    DEBUG_TRACE(theHeader);
    for (int i = 0; i < theData.size(); ++i) {
        if (isString(theData[i])) {
            columnList << theHeader[i]+" TEXT,";
            continue;
        }
        if (isNumber(theData[i])) {
            columnList << theHeader[i]+" INTEGER,";
            continue;
        }
        if (isDecimal(theData[i])) {
            columnList << theHeader[i]+" FLOAT,";
            continue;
        }
        if (isDate(theData[i])) {
            columnList << theHeader[i]+" TEXT,";
            continue;
        }
        if (isAnArray(theData[i])) {
            columnList << theHeader[i]+" FLOAT[],";
            continue;
        }
        else {
            columnList << theHeader[i]+" TEXT,";
        }
    }
    return columnList;
}

bool ImportFile::isDecimal(QString& value)
{
    std::regex e ("^-?[0-9]+\.(0?[0-9])*$");      //^\d+\.(0?[0-9]|1[0-3])$

    if (std::regex_match(value.toStdString(),e))
        return true;
    return false;
}

bool ImportFile::isString(QString& value)
{
    std::regex e ("([A-Za-z])+");

    if (std::regex_search (value.toStdString(),e))
        return true;
    return false;
}

bool ImportFile::isNumber(QString& value)
{
    std::regex e ("^0$|^-?[1-9][0-9]*$");

    if (std::regex_match (value.toStdString(),e))
        return true;
    return false;
}

bool ImportFile::isDate(QString& value)
{
    std::regex e ("[0-9]{1,4}[-\/. ][0-9]{1,2}[-\/. ][0-9]{1,4}");

    if (std::regex_match (value.toStdString(),e))
        return true;
    return false;
}

bool ImportFile::isArray(QString& value)
{
    // QRegExp e ("([A-Za-z])+");   //(([A-Za-z])+\[(?:\[??[^\[]*?\])) -- abc[0]  \[(.*?)\] --[0]
    if (value.contains("[")) {
        if (value.contains("]")) {
            value.remove("]");
            QStringList items = value.split("[");
            auto it = mArray.find(items.first());
            if (it != mArray.end()) {
                if (it->second < items.last().toInt())
                    mArray.erase(it);
            }
            mArray.insert(std::make_pair(items.first(), items.last().toInt()));
        }
    }
    return false;
}

bool ImportFile::isAnArray(QString& value)
{
    if (value.contains("["))
        return true;
    return false;
}
