/**
 *  @file   ImportFile.h
 *  @brief  Import CSV/XYZ/NUV/GBN file data to database
 *  @author Rachana Kapoor
 *  @date   Dec 14,2021
 ***********************************************/

#ifndef IMPORTFILE_H
#define IMPORTFILE_H
#include <QObject>
#include "DataBase.h"
#include "TypeDefConst.h"
class ImportFile:public QObject
{
    Q_OBJECT
public:

    /// @brief This class is a singleton class having only one instance at a time
    ///
    /// @return   Instance of class Database
    ///
    static ImportFile* getInstance();

signals:
    /// @brief This signal is emmitted when the application starts reading file.
    /// @return  void
    ///
    void started();

    /// @brief This signal is emmitted when the application finished reading file.
    /// @return  void
    ///
    void finished();

    void currentProgress(QString status, int value);

public slots:

    /// @brief This function is invoked when setTable signal is emitted.
    /// It sets the table name in this class.
    /// @param file: Name of the table
    /// @return  void
    ///
    void setTableName(QString table);

    /// @brief This function is invoked when setFile signal is emitted.
    /// It sets the file in this class.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setFile(QString file);

private:

    ///Class constructor
    ImportFile();

    /// @brief This function is responsible for reading the ".csv" formatted file
    /// as well as analyzing the data.
    /// This function also formats the file data as the requirements and stores
    /// data in the database.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setAndLoadCSVFile(QString file);

    /// @brief This function is responsible for reading the ".XYZ" formatted file
    /// as well as analyzing the data.
    /// This function also formats the file data as the requirements and stores
    /// data in the database.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setAndLoadXYZFile(QString file);

    /// @brief This function is responsible for reading the ".NUV" formatted file
    /// as well as analyzing the data.
    /// This function also formats the file data as the requirements and stores
    /// data in the database.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setAndLoadNUVFile(QString file);

    /// @brief This function is responsible for reading the ".GBN" formatted file
    /// as well as analyzing the data.
    /// This function also formats the file data as the requirements and stores
    /// data in the database.
    /// @param file: Name of the file
    /// @return  void
    ///
    void setAndLoadGBNFile(QString file);

    /// @brief This function is responsible for deducing the data type of the data.
    /// It sets the data type in the list as per the header list provided.
    /// @param theHeader: List of the channels/header
    /// @param theData: List of the line data
    /// @return  combined list of header/channels and data types after investigation.
    ///
    QStringList typeDeduction(QStringList &theHeader,QStringList &theData);

    /// @brief This function checks whether the value is decimal or not
    /// @param value: data item of channel
    /// @return  True is the value is decimal else False
    ///
    bool isDecimal(QString& value);

    /// @brief This function checks whether the value is string(alphanumeric) or not
    /// @param value: data item of channel
    /// @return  True is the value is string(alphanumeric) else False
    ///
    bool isString(QString& value);

    /// @brief This function checks whether the value is number or not
    /// @param value: data item of channel
    /// @return  True is the value is number else False
    ///
    bool isNumber(QString& value);

    /// @brief This function checks whether the value is date or not
    /// @param value: data item of channel
    /// @return  True is the value is date else False
    ///
    bool isDate(QString& value);

    /// @brief This function is a getter function which returns name of the table
    /// @return  Name of the table
    ///
    QString& getTableName();

    /// @brief This function checks whether the value is array or not
    /// @param value: data item of channel
    /// @return  True is the value is date else False
    ///
    bool isArray(QString& value);
    bool isAnArray(QString& value);

    QString mTable;

    std::map<QString,int> mArray;

    QList<int> recordCopy;

    std::vector<std::pair<int,int>> mColumnCounter;



};

#endif // IMPORTFILE_H
