#include "ImportGBN.h"

ImportGBN::ImportGBN(QObject *parent, QString filePath): QObject(parent)
{

    mChanNameUpperHash.insert("FLIGHTDATE");
    mChanNameUpperHash.insert("FLIGHTNO");
    mChanNameUpperHash.insert("LINETYPE");
    mChanNameUpperHash.insert("LINENO");

    mFilePath = filePath;
    //DEBUG_TRACE("selected file : " + mFilePath.toUtf8());

    if(openGBNFile(mFilePath) == true)
    {
        //DEBUG_TRACE("selected file : " + mGbnFile.isOpen());
        mGbnFile.seek(0);
        if(checkGBNFirst17Chars() == true)
        {
            mGbnFile.seek(17);
            if(searchGBNSub() == true)
            {
                mGbnFile.seek(mGbnReadPos);
                decodeData();
            }
            else
            {mGbnFile.close();}
        }
        else
        {mGbnFile.close();}
    }
}

ImportGBN::~ImportGBN()
{
    for(int i = 0; i < mGbnAllDataRecs.size(); i++)
    {
        delete mGbnAllDataRecs[i];
    }
    if(mBinaryTypes)  delete []mBinaryTypes;
    if(mLength)  delete []mLength;
    if(mDataAddrs)  delete []mDataAddrs;
    //if(mEveryNRow)  delete []mEveryNRow;
    mGbnFile.close();

}

//QString **ImportGBN::getGBNDecodedVals() const
//{
//    return mGBNValsForDB;
//}
void ImportGBN::getOneRow(int row, QString &outPut)
{
    //QString outPutNormal;

    if(mLineArrIndex < mNewLineRowsSize
            &&row == mNewLineRows[mLineArrIndex])
    {
        QString flightDate = QString::number(mGbnLineRecs[mLineArrIndex].year) + "-"
                + QString::number(mGbnLineRecs[mLineArrIndex].month) + "-"
                + QString::number(mGbnLineRecs[mLineArrIndex].day);

        QString flightNo = QString::number(mGbnLineRecs[mLineArrIndex].flight);

        QString lineTypeStr;
        lineTypeStr = lineType2Str(mGbnLineRecs[mLineArrIndex].lineType);

        //        mLineDateStr = lineTypeStr+"-"+QString::number(mGbnLineRecs[mLineArrIndex].lineNumber)
        //                +"-"+QString::number(mGbnLineRecs[mLineArrIndex].lineVersion)+"-"+flightNo;

        mFlightDate = flightDate;
        mFlightNo = flightNo;
        mLineType = lineTypeStr;
        mLineNo = QString::number(mGbnLineRecs[mLineArrIndex].lineNumber)+"."+QString::number(mGbnLineRecs[mLineArrIndex].lineVersion);

        mLineArrIndex++;
    }

    outPut.append("'");
    outPut.append(mFlightDate);
    outPut.append("','");
    outPut.append(mFlightNo);
    outPut.append("','");
    outPut.append(mLineType);
    outPut.append("','");
    outPut.append(mLineNo);
    outPut.append("',");


    //outPut += "'" + mFlightDate + "','"+ mFlightNo + "','" + mLineType + "','" + mLineNo + "',";

    //unsigned long j = 0;
    //outPutArray.resize(mArrayChansList.size());
    for(int i = 0; i < mGbnChsRecsSize; ++i)
    {
        if(!mDupFDFNLYLN.contains(i))
        {
            if(thereIsNullChan)
            {
                if(!arrayChans.contains(i))   //Normal channel
                {
                    if(mCh2RecsIdx.find(i) != mCh2RecsIdx.end())    //This is a valid(not null) channel
                    {
                        if(row/mEveryNRow[i] >= mLength[i])   //If row is larger than data length
                        {
                            outPut.append("NULL");
                        }
                        else
                        {
                            if(row % mEveryNRow[i] != 0)
                            {
                                outPut.append("NULL");
                            }
                            else
                            {
                                int theRow = row/mEveryNRow[i];
                                switch(mBinaryTypes[i])
                                {
                                case(GS_UNSIGNED_BYTE):
                                {
                                    QString str = ((QString *)mDataAddrs[i])[theRow];
                                    if(str.isEmpty())
                                    {
                                        outPut.append("NULL");
                                    }
                                    else
                                    {
                                        outPut.append("'");
                                        outPut.append(str);
                                        outPut.append("'");
                                    }
                                    break;
                                }

                                case(GS_BYTE):
                                {
                                    int8_t val = ((int8_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_S1DM)  outPut.append("NULL");
                                    else
                                        outPut.append(QString::number(val,'g',maxDigits));
                                    break;
                                }

                                case(GS_USHORT):
                                {
                                    uint16_t val = ((uint16_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_U2DM)  outPut.append("NULL");
                                    else
                                        outPut.append(QString::number(val,'g',maxDigits));
                                    break;
                                }

                                case(GS_SHORT):
                                {
                                    int16_t val = ((int16_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_S2DM)  outPut.append("NULL");
                                    else
                                        outPut.append(QString::number(val,'g',maxDigits));
                                    break;
                                }

                                case(GS_LONG):
                                {
                                    int32_t val = ((int32_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_S4DM)  outPut.append("NULL");
                                    else
                                        outPut.append(QString::number(val,'g',maxDigits));
                                    break;
                                }

                                case(GS_FLOAT):
                                {
                                    float val = ((float *)mDataAddrs[i])[theRow];
                                    if(val == GS_R4DM)  outPut.append("NULL");
                                    else
                                        outPut.append(QString::number(val,'g',maxDigits));
                                    break;
                                }

                                case(GS_DOUBLE):
                                {
                                    double val = ((double *)mDataAddrs[i])[theRow];
                                    if(val == GS_R8DM)  outPut.append("NULL");
                                    else
                                        outPut.append(QString::number(val, 'g', maxDigits));
                                    break;
                                }
                                }
                            }
                        }
                        //j++;
                    }
                    else
                    {
                        outPut.append("NULL");
                    }
                    outPut.append(",");
                }

                else   //Array channel
                {
                    if(mCh2RecsIdx.find(i) != mCh2RecsIdx.end())    //This is a valid(not null) channel
                    {
                        if(row/mEveryNRow[i]*mGbnChsRecs[i].arrayDepth >= mLength[i])   //If row is larger than data length
                        {
                            outPut.append("NULL,");
                            //int outPutVectIndex = mArrayChansList.indexOf(i);
                            //outPutArray[outPutVectIndex] = mArrNullOutPuts[outPutVectIndex];
                        }
                        else
                        {
                            if(row % mEveryNRow[i] != 0)
                            {
                                outPut.append("NULL,");
                                //int outPutVectIndex = mArrayChansList.indexOf(i);
                                //outPutArray[outPutVectIndex] = mArrNullOutPuts[outPutVectIndex];
                            }
                            else
                            {
                                int arraySize = mGbnChsRecs[i].arrayDepth;
                                int theRow = row/mEveryNRow[i]*arraySize;
                                //int outPutVectIndex = mArrayChansList.indexOf(i);



                                switch(mBinaryTypes[i])
                                {

                                case(GS_BYTE):
                                {
                                    int8_t val = ((int8_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_S1DM) outPut.append("NULL,");

                                    else
                                    {
                                        outPut.append("'[");
                                        outPut.append(QString::number(arraySize));
                                        outPut.append(",");
                                        for(int row = theRow; row < theRow+arraySize; row++)
                                        {
                                            int8_t val = ((int8_t *)mDataAddrs[i])[row];
                                            outPut.append(QString::number(val,'g',maxDigits));
                                            outPut.append(",");
                                            //outPutArray[outPutVectIndex].push_back(QString::number(val));
                                        }
                                        outPut.chop(1);
                                        outPut.append("]',");
                                    }
                                    break;
                                }

                                case(GS_USHORT):
                                {
                                    uint16_t val = ((uint16_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_U2DM) outPut.append("NULL,");

                                    else
                                    {
                                        outPut.append("'[");
                                        outPut.append(QString::number(arraySize));
                                        outPut.append(",");
                                        for(int row = theRow; row < theRow+arraySize; row++)
                                        {
                                            uint16_t val = ((uint16_t *)mDataAddrs[i])[row];
                                            outPut.append(QString::number(val,'g',maxDigits));
                                            outPut.append(",");
                                            //outPutArray[outPutVectIndex].push_back(QString::number(val));
                                        }
                                        outPut.chop(1);
                                        outPut.append("]',");
                                    }
                                    break;
                                }

                                case(GS_SHORT):
                                {
                                    int16_t val = ((int16_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_S2DM)  outPut.append("NULL,");

                                    else
                                    {
                                        outPut.append("'[");
                                        outPut.append(QString::number(arraySize));
                                        outPut.append(",");
                                        for(int row = theRow; row < theRow+arraySize; row++)
                                        {
                                            int16_t val = ((int16_t *)mDataAddrs[i])[row];
                                            outPut.append(QString::number(val,'g',maxDigits));
                                            outPut.append(",");
                                            //outPutArray[outPutVectIndex].push_back(QString::number(val));
                                        }
                                        outPut.chop(1);
                                        outPut.append("]',");
                                    }
                                    break;
                                }

                                case(GS_LONG):
                                {
                                    int32_t val = ((int32_t *)mDataAddrs[i])[theRow];
                                    if(val == GS_S4DM)  outPut.append("NULL,");

                                    else
                                    {
                                        outPut.append("'[");
                                        outPut.append(QString::number(arraySize));
                                        outPut.append(",");
                                        for(int row = theRow; row < theRow+arraySize; row++)
                                        {
                                            int32_t val = ((int32_t *)mDataAddrs[i])[row];
                                            outPut.append(QString::number(val,'g',maxDigits));
                                            outPut.append(",");
                                            //outPutArray[outPutVectIndex].push_back(QString::number(val));
                                        }
                                        outPut.chop(1);
                                        outPut.append("]',");
                                    }
                                    break;
                                }

                                case(GS_FLOAT):
                                {
                                    float val = ((float *)mDataAddrs[i])[theRow];
                                    if(val == GS_R4DM)  outPut.append("NULL,");

                                    else
                                    {
                                        outPut.append("'[");
                                        outPut.append(QString::number(arraySize));
                                        outPut.append(",");
                                        for(int row = theRow; row < theRow+arraySize; row++)
                                        {
                                            float val = ((float *)mDataAddrs[i])[row];
                                            outPut.append(QString::number(val, 'g', maxDigits));
                                            outPut.append(",");
                                            //outPutArray[outPutVectIndex].push_back(QString::number(val, 'f', 7));
                                        }
                                        outPut.chop(1);
                                        outPut.append("]',");
                                    }
                                    break;
                                }

                                case(GS_DOUBLE):
                                {
                                    double val = ((double *)mDataAddrs[i])[theRow];
                                    if(val == GS_R8DM)  outPut.append("NULL,");

                                    else
                                    {
                                        outPut.append("'[");
                                        outPut.append(QString::number(arraySize));
                                        outPut.append(",");
                                        for(int row = theRow; row < theRow+arraySize; row++)
                                        {
                                            double val = ((double *)mDataAddrs[i])[row];
                                            outPut.append(QString::number(val, 'g', maxDigits));
                                            outPut.append(",");
                                            //outPutArray[outPutVectIndex].push_back(QString::number(val, 'f', 7));
                                        }
                                        outPut.chop(1);
                                        outPut.append("]',");
                                    }
                                    break;
                                }
                                }

                            }
                        }
                    }

                }
            }
            else
            {
                if(!arrayChans.contains(i))   //Normal channel
                {
                    if(row/mEveryNRow[i] >= mLength[i])   //If row is larger than data length
                    {
                        outPut.append("NULL");
                    }
                    else
                    {
                        if(row % mEveryNRow[i] != 0)
                        {
                            outPut.append("NULL");
                        }
                        else
                        {
                            int theRow = row/mEveryNRow[i];

                            switch(mBinaryTypes[i])
                            {
                            case(GS_UNSIGNED_BYTE):
                            {
                                QString str = ((QString *)mDataAddrs[i])[theRow];
                                if(str.isEmpty())
                                {
                                    outPut.append("NULL");
                                }
                                else
                                {
                                    outPut.append("'");
                                    outPut.append(str);
                                    outPut.append("'");
                                }
                                break;
                            }

                            case(GS_BYTE):
                            {
                                int8_t val = ((int8_t *)mDataAddrs[i])[theRow];
                                if(val == GS_S1DM)  outPut.append("NULL");
                                else
                                    outPut.append(QString::number(val,'g',maxDigits));
                                break;
                            }

                            case(GS_USHORT):
                            {
                                uint16_t val = ((uint16_t *)mDataAddrs[i])[theRow];
                                if(val == GS_U2DM)  outPut.append("NULL");
                                else
                                    outPut.append(QString::number(val,'g',maxDigits));
                                break;
                            }

                            case(GS_SHORT):
                            {
                                int16_t val = ((int16_t *)mDataAddrs[i])[theRow];
                                if(val == GS_S2DM)  outPut.append("NULL");
                                else
                                    outPut.append(QString::number(val,'g',maxDigits));
                                break;
                            }

                            case(GS_LONG):
                            {
                                int32_t val = ((int32_t *)mDataAddrs[i])[theRow];
                                if(val == GS_S4DM)  outPut.append("NULL");
                                else
                                    outPut.append(QString::number(val,'g',maxDigits));
                                break;
                            }

                            case(GS_FLOAT):
                            {
                                float val = ((float *)mDataAddrs[i])[theRow];
                                if(val == GS_R4DM)  outPut.append("NULL");
                                else
                                    outPut.append(QString::number(val, 'g', maxDigits));
                                break;
                            }

                            case(GS_DOUBLE):
                            {
                                double val = ((double *)mDataAddrs[i])[theRow];
                                if(val == GS_R8DM)  outPut.append("NULL");
                                else
                                    outPut.append(QString::number(val, 'g', maxDigits));
                                break;
                            }
                            }
                        }
                    }

                    outPut.append(",");
                }
                else   //Array channel
                {
                    if(row/mEveryNRow[i]*mGbnChsRecs[i].arrayDepth >= mLength[i])   //If row is larger than data length
                    {
                        outPut.append("NULL,");
                    }
                    else
                    {
                        if(row % mEveryNRow[i] != 0)
                        {
                            outPut.append("NULL,");
                        }
                        else
                        {
                            int arraySize = mGbnChsRecs[i].arrayDepth;
                            int theRow = row/mEveryNRow[i]*arraySize;
                            //int outPutVectIndex = mArrayChansList.indexOf(i);


                            switch(mBinaryTypes[i])
                            {

                            case(GS_BYTE):
                            {
                                int8_t val = ((int8_t *)mDataAddrs[i])[theRow];
                                if(val == GS_S1DM) outPut.append("NULL,");

                                else
                                {
                                    outPut.append("'[");
                                    outPut.append(QString::number(arraySize));
                                    outPut.append(",");

                                    for(int row = theRow; row < theRow+arraySize; row++)
                                    {
                                        int8_t val = ((int8_t *)mDataAddrs[i])[row];
                                        outPut.append(QString::number(val,'g',maxDigits));
                                        outPut.append(",");
                                    }
                                    outPut.chop(1);
                                    outPut.append("]',");
                                }
                                break;
                            }

                            case(GS_USHORT):
                            {
                                uint16_t val = ((uint16_t *)mDataAddrs[i])[theRow];
                                if(val == GS_U2DM) outPut.append("NULL,");

                                else
                                {
                                    outPut.append("'[");
                                    outPut.append(QString::number(arraySize));
                                    outPut.append(",");
                                    for(int row = theRow; row < theRow+arraySize; row++)
                                    {
                                        uint16_t val = ((uint16_t *)mDataAddrs[i])[row];
                                        outPut.append(QString::number(val,'g',maxDigits));
                                        outPut.append(",");
                                    }
                                    outPut.chop(1);
                                    outPut.append("]',");
                                }
                                break;
                            }

                            case(GS_SHORT):
                            {
                                int16_t val = ((int16_t *)mDataAddrs[i])[theRow];
                                if(val == GS_S2DM)  outPut.append("NULL,");

                                else
                                {
                                    outPut.append("'[");
                                    outPut.append(QString::number(arraySize));
                                    outPut.append(",");
                                    for(int row = theRow; row < theRow+arraySize; row++)
                                    {
                                        int16_t val = ((int16_t *)mDataAddrs[i])[row];
                                        outPut.append(QString::number(val,'g',maxDigits));
                                        outPut.append(",");
                                    }
                                    outPut.chop(1);
                                    outPut.append("]',");
                                }
                                break;
                            }

                            case(GS_LONG):
                            {
                                int32_t val = ((int32_t *)mDataAddrs[i])[theRow];
                                if(val == GS_S4DM)  outPut.append("NULL,");

                                else
                                {
                                    outPut.append("'[");
                                    outPut.append(QString::number(arraySize));
                                    outPut.append(",");
                                    for(int row = theRow; row < theRow+arraySize; row++)
                                    {
                                        int32_t val = ((int32_t *)mDataAddrs[i])[row];
                                        outPut.append(QString::number(val,'g',maxDigits));
                                        outPut.append(",");
                                    }
                                    outPut.chop(1);
                                    outPut.append("]',");
                                }
                                break;
                            }

                            case(GS_FLOAT):
                            {
                                float val = ((float *)mDataAddrs[i])[theRow];
                                if(val == GS_R4DM)  outPut.append("NULL,");

                                else
                                {
                                    outPut.append("'[");
                                    outPut.append(QString::number(arraySize));
                                    outPut.append(",");
                                    for(int row = theRow; row < theRow+arraySize; row++)
                                    {
                                        float val = ((float *)mDataAddrs[i])[row];
                                        outPut.append(QString::number(val, 'g', maxDigits));
                                        outPut.append(",");
                                    }
                                    outPut.chop(1);
                                    outPut.append("]',");
                                }
                                break;
                            }

                            case(GS_DOUBLE):
                            {
                                double val = ((double *)mDataAddrs[i])[theRow];
                                if(val == GS_R8DM)  outPut.append("NULL,");

                                else
                                {
                                    outPut.append("'[");
                                    outPut.append(QString::number(arraySize));
                                    outPut.append(",");
                                    for(int row = theRow; row < theRow+arraySize; row++)
                                    {
                                        double val = ((double *)mDataAddrs[i])[row];
                                        outPut.append(QString::number(val, 'g', maxDigits));
                                        outPut.append(",");
                                    }
                                    outPut.chop(1);
                                    outPut.append("]',");
                                }
                                break;
                            }
                            }
                        }
                    }
                }
            }
        }
    }

    outPut.chop(1);
    //return outPutNormal;
}

QList<int> ImportGBN::getArrayChanNumbers()
{
    return mArrayChansList;
}

//QString **ImportGBN::getGBNDecodedStrs() const
//{
//    return mGBNStrValsForDB;
//}

unsigned long ImportGBN::getTableNumOfRows() const
{
    return mNumberOfRows;
}

QString ImportGBN::getErrMsg() const
{
    return mErrMsg;
}

std::vector<gbnChsRecStruct> ImportGBN::getAllChannels() const
{
    return mGbnChsRecs;
}


bool ImportGBN::checkGBNFirst17Chars()
{
    QString first17Chars;
    for(int i = 0; i < 17; i++)
    {
        char theChar;
        mGbnFile.read(&theChar, 1);
        first17Chars.append(theChar);
    }
    if(first17Chars.toUpper() == "OASIS BINARY DATA")
    {
        return true;
    }
    else  return false;
}

bool ImportGBN::searchGBNSub()
{
    mGbnReadPos = 17;
    bool subFound = false;

    while(!mGbnFile.atEnd())
    {
        if(subFound) break;

        QByteArray line = mGbnFile.readLine();
        mGbnReadPos += line.size();
        if(line.contains(mSubChar) && !subFound)
        {
            subFound = true;
            mGbnReadPos -= line.size() - line.indexOf(mSubChar);
            mGbnReadPos += 1;
        }
    }

    if(subFound == false)
    {
        return false;
    }
    else  return true;
}

void ImportGBN::decodeData()
{

    int64_t oriGBNReadPos = mGbnReadPos;

    while(!mGbnFile.atEnd())
    {
        char recHeadHex;
        mGbnFile.read(&recHeadHex, 1);

        mGbnReadPos++;
        mGbnFile.seek(mGbnReadPos);

        switch(recHeadHex)
        {
        case (0x01):
        {
            extractGBNChRec();
            break;
        }

        case (0x02):
        {
            extractGBNLineRec();
            break;
        }
        case (0x03):
        {
            extractGBNDataRec();
            break;
        }
        case (0x04):
        {
            extractGBNArrChRec();
            break;
        }

        case (0x05):
        {
            extractGBNParmRec();
            break;
        }

        case (0x00):
        {
            DEBUG_TRACE("Scan GBN file done");
            mFirstTimeScanData = false;
            break;
        }
        default:
        {
            DEBUG_TRACE("Miss alignment");
            mErrMsg += "Miss alignment\n";
            return;
        }

        }
    }


    if(allocateGBNDataBuff())
    {
        mGbnReadPos = oriGBNReadPos;
        mGbnFile.seek(mGbnReadPos);

        while(!mGbnFile.atEnd())
        {
            char recHeadHex;
            mGbnFile.read(&recHeadHex, 1);

            mGbnReadPos++;
            mGbnFile.seek(mGbnReadPos);

            if(recHeadHex == 0x03)
            {
                //DEBUG_TRACE("Extracting data...");
                extractGBNDataRec();
            }
            else if(recHeadHex == 0x00)
            {
                DEBUG_TRACE("Read data done");
            }
            else if(recHeadHex == 0x01)
            {
                mGbnReadPos += 80;
                mGbnFile.seek(mGbnReadPos);
            }
            else if(recHeadHex == 0x02)
            {
                mGbnReadPos += 28;
                mGbnFile.seek(mGbnReadPos);
            }
            else if(recHeadHex == 0x04)
            {
                mGbnReadPos += 84;
                mGbnFile.seek(mGbnReadPos);
            }
            else if(recHeadHex == 0x05)
            {
                mGbnReadPos += 192;
                mGbnFile.seek(mGbnReadPos);
            }
            else if(recHeadHex != 0x01 && recHeadHex != 0x02
                    && recHeadHex != 0x04 && recHeadHex != 0x05)
            {
                DEBUG_TRACE("Miss alignment");
                mErrMsg += "Miss alignment\n";
                return;
            }
        }
    }
}

void ImportGBN::extractGBNChRec()
{

    char chNameChar;
    gbnChsRecStruct thisGBNChRec;

    for(int i = 0; i < 64; i++)
    {
        mGbnFile.read(&chNameChar, 1);

        if(chNameChar != 0)
            thisGBNChRec.channelName.append(chNameChar);

        mGbnReadPos++;
        mGbnFile.seek(mGbnReadPos);
    }

    if(thisGBNChRec.channelName.toUpper() == "FLIGHTDATE" || thisGBNChRec.channelName.toUpper() == "FLIGHTNO" ||
            thisGBNChRec.channelName.toUpper() == "LINETYPE" || thisGBNChRec.channelName.toUpper() == "LINENO")
    {
        mDupFDFNLYLN.insert(currentChanNo);
    }
    modifyChanName(thisGBNChRec.channelName);


    char readBuff[4];
    mGbnFile.read(readBuff, 4);

    memcpy(&thisGBNChRec.channelType, reinterpret_cast<unsigned char*>(readBuff), 4);
    if(thisGBNChRec.channelType < 0)
    {thisGBNChRec.channelType = GS_UNSIGNED_BYTE;}

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNChRec.displayFormat, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNChRec.displayWidth, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNChRec.displayDecimals, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnChsRecs.push_back(thisGBNChRec);

    currentChanNo++;
}

void ImportGBN::extractGBNLineRec()
{

    gbnLineRecStruct thisGBNLineRec;

    if(!mNewLineRows.empty())
    {
        int theNewLine = mNewLineRows.back() + mTheNewLineRow;
        mNewLineRows.push_back(theNewLine);
    }
    else
    {
        mNewLineRows.push_back(mTheNewLineRow);
    }

    char readBuff[4];
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.lineNumber, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.lineVersion, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.lineType, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.flight, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.year, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.month, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNLineRec.day, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnLineRecs.push_back(thisGBNLineRec);

    mTheNewLineRow = 0;
}

void ImportGBN::extractGBNDataRec()
{

    if(mFirstTimeScanData)
    {
        long channelNumber;
        int32_t binaryType;
        double fidStart;
        double fidIncrement;
        long length = 0;
        int buffSizeRatio = 1;
        char readBuffLong[4];
        char readBuffDouble[8];

        mGbnFile.read(readBuffLong, 4);
        memcpy(&channelNumber, reinterpret_cast<unsigned char*>(readBuffLong), 4);

        mGbnReadPos += 4;
        mGbnFile.seek(mGbnReadPos);
        mGbnFile.read(readBuffLong, 4);
        memcpy(&binaryType, reinterpret_cast<unsigned char*>(readBuffLong), 4);

        if(binaryType < 0)
        {buffSizeRatio = -binaryType; binaryType = GS_UNSIGNED_BYTE;}

        mGbnReadPos += 4;
        mGbnFile.seek(mGbnReadPos);
        mGbnFile.read(readBuffDouble, 8);
        memcpy(&fidStart, reinterpret_cast<unsigned char*>(readBuffDouble), 8);

        mGbnReadPos += 8;
        mGbnFile.seek(mGbnReadPos);
        mGbnFile.read(readBuffDouble, 8);
        memcpy(&fidIncrement, reinterpret_cast<unsigned char*>(readBuffDouble), 8);

        mGbnReadPos += 8;
        mGbnFile.seek(mGbnReadPos);
        mGbnFile.read(readBuffLong, 4);
        memcpy(&length, reinterpret_cast<unsigned char*>(readBuffLong), 4);

        if(length > mTheNewLineRow
                && !arrayChans.contains(currentChanNo))
        {mTheNewLineRow = length;}

        if(binaryType == GS_UNSIGNED_BYTE)
            length *= buffSizeRatio;

        mGbnReadPos += 4;
        mGbnFile.seek(mGbnReadPos);

        if(std::find(mChannelNos.begin(), mChannelNos.end(), channelNumber) == mChannelNos.end())
        {
            switch(binaryType)
            {
            case(GS_UNSIGNED_BYTE):
            {
                dataRecStruct<QString> *thisDataRecStruct = new dataRecStruct<QString>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length/buffSizeRatio;
                thisDataRecStruct->buffSizeRatio = buffSizeRatio;

                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}
                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});
                if(channelNumber != mGbnAllDataRecs.size()-1)
                {
                    thereIsNullChan=true;
                }
                mGbnReadPos += length * sizeof(unsigned char);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_BYTE):
            {
                dataRecStruct<char> *thisDataRecStruct = new dataRecStruct<char>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length;
                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}

                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});

                mGbnReadPos += length * sizeof(char);
                mGbnFile.seek(mGbnReadPos);
                break;
            }

            case(GS_USHORT):
            {
                dataRecStruct<uint16_t> *thisDataRecStruct = new dataRecStruct<uint16_t>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length;
                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}

                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});

                mGbnReadPos += length * sizeof(uint16_t);
                mGbnFile.seek(mGbnReadPos);
                break;
            }

            case(GS_SHORT):
            {
                dataRecStruct<int16_t> *thisDataRecStruct = new dataRecStruct<int16_t>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length;
                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}

                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});

                mGbnReadPos += length * sizeof(int16_t);
                mGbnFile.seek(mGbnReadPos);
                break;
            }

            case(GS_LONG):
            {
                dataRecStruct<int32_t> *thisDataRecStruct = new dataRecStruct<int32_t>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length;
                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}

                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});

                mGbnReadPos += length * sizeof(int32_t);
                mGbnFile.seek(mGbnReadPos);
                break;
            }

            case(GS_FLOAT):
            {
                dataRecStruct<float> *thisDataRecStruct = new dataRecStruct<float>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length;
                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}

                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});

                mGbnReadPos += length * sizeof(float);
                mGbnFile.seek(mGbnReadPos);
                break;
            }

            case(GS_DOUBLE):
            {
                dataRecStruct<double> *thisDataRecStruct = new dataRecStruct<double>;
                thisDataRecStruct->channelNumber = channelNumber;
                thisDataRecStruct->binaryType = binaryType;
                thisDataRecStruct->fidStart = fidStart;
                thisDataRecStruct->fidIncrement = fidIncrement;
                thisDataRecStruct->length = length;
                if(fidIncrement < mMinFidInc) {mMinFidInc = fidIncrement;}

                mGbnAllDataRecs.push_back(thisDataRecStruct);
                mChannelNos.push_back(channelNumber);
                mCh2RecsIdx.insert({channelNumber, mGbnAllDataRecs.size()-1});

                mGbnReadPos += length * sizeof(double);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            default:
                break;
            }
        }
        else
        {
            switch(binaryType)
            {
            case(GS_BYTE):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length);
                mGbnReadPos += length * sizeof(char);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_USHORT):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length);

                mGbnReadPos += length * sizeof(uint16_t);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_SHORT):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length);

                mGbnReadPos += length * sizeof(int16_t);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_LONG):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length);

                mGbnReadPos += length * sizeof(int32_t);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_FLOAT):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length);

                mGbnReadPos += length * sizeof(float);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_DOUBLE):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length);

                mGbnReadPos += length * sizeof(double);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            case(GS_UNSIGNED_BYTE):
            {
                mGbnAllDataRecs[mCh2RecsIdx[channelNumber]]->addLength(length/buffSizeRatio);

                mGbnReadPos += length * sizeof(unsigned char);
                mGbnFile.seek(mGbnReadPos);
                break;
            }
            default:
                break;

            }

        }
    }
    else
    {
        long length;
        int32_t binaryType;
        int buffSizeRatio = 1;

        long channelNumber;
        char readBuffLong[4];

        mGbnFile.read(readBuffLong, 4);
        memcpy(&channelNumber, reinterpret_cast<unsigned char*>(readBuffLong), 4);

        if(channelNumber == 22)
        {
            int i = 0;
        }
        mGbnReadPos += 4;
        mGbnFile.seek(mGbnReadPos);
        mGbnFile.read(readBuffLong, 4);
        memcpy(&binaryType, reinterpret_cast<unsigned char*>(readBuffLong), 4);
        if(binaryType < 0)
        {buffSizeRatio = -binaryType; binaryType = GS_UNSIGNED_BYTE;}


        mGbnReadPos += 20;
        mGbnFile.seek(mGbnReadPos);
        mGbnFile.read(readBuffLong, 4);
        memcpy(&length, reinterpret_cast<unsigned char*>(readBuffLong), 4);
        if(binaryType == GS_UNSIGNED_BYTE)
            length *= buffSizeRatio;

        mGbnReadPos += 4;
        mGbnFile.seek(mGbnReadPos);

        switch(binaryType)
        {
        case(GS_BYTE):
        {
            extractData<char>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        case(GS_USHORT):
        {
            extractData<uint16_t>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        case(GS_SHORT):
        {
            extractData<int16_t>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        case(GS_LONG):
        {
            extractData<int32_t>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        case(GS_FLOAT):
        {
            extractData<float>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        case(GS_DOUBLE):
        {
            extractData<double>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        case(GS_UNSIGNED_BYTE):
        {
            extractData<unsigned char>(mGbnAllDataRecs[mCh2RecsIdx[channelNumber]], length);
            break;
        }
        default:
            break;
        }

    }
}

void ImportGBN::extractGBNArrChRec()
{
    char chNameChar;
    gbnChsRecStruct thisGBNArrChRec;

    for(int i = 0; i < 64; i++)
    {
        mGbnFile.read(&chNameChar, 1);

        if(chNameChar != 0)
            thisGBNArrChRec.channelName.append(chNameChar);

        mGbnReadPos++;
        mGbnFile.seek(mGbnReadPos);
    }

    modifyChanName(thisGBNArrChRec.channelName);

    char readBuff[4];
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNArrChRec.channelType, reinterpret_cast<unsigned char*>(readBuff), 4);
    if(thisGBNArrChRec.channelType < 0)
    {thisGBNArrChRec.channelType = GS_UNSIGNED_BYTE;}


    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNArrChRec.arrayDepth, reinterpret_cast<unsigned char*>(readBuff), 4);


    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNArrChRec.displayFormat, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNArrChRec.displayWidth, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnFile.read(readBuff, 4);
    memcpy(&thisGBNArrChRec.displayDecimals, reinterpret_cast<unsigned char*>(readBuff), 4);

    mGbnReadPos += 4;
    mGbnFile.seek(mGbnReadPos);
    mGbnChsRecs.push_back(thisGBNArrChRec);

    arrayChans.insert(currentChanNo);
    currentChanNo++;
}

void ImportGBN::extractGBNParmRec()
{
    gbnParmRecStruct thisGBNParRec;

    char readBuffParm[64];
    mGbnFile.read(readBuffParm, 64);
    memcpy(&thisGBNParRec.szParm, readBuffParm, 64);

    mGbnReadPos += 64;
    mGbnFile.seek(mGbnReadPos);
    char readBuffVal[128];
    mGbnFile.read(readBuffVal, 128);
    memcpy(&thisGBNParRec.szValue, readBuffVal, 128);

    mGbnReadPos += 128;
    mGbnFile.seek(mGbnReadPos);
    mGbnParmRecs.push_back(thisGBNParRec);
}

bool ImportGBN::allocateGBNDataBuff()
{
    mGbnChsRecsSize = mGbnChsRecs.size();
    mNewLineRowsSize = mNewLineRows.size();
    MEMORYSTATUSEX status;
    status.dwLength = sizeof(status);

    const unsigned int oneGBLeft = 1000000000;

    if(mCh2RecsIdx.size() != mGbnChsRecsSize)
    {
        thereIsNullChan = true;
    }
    mBinaryTypes = new int[mGbnChsRecs.size()]();
    mLength = new int[mGbnChsRecs.size()]();
    mDataAddrs = new void*[mGbnChsRecs.size()]();
    mEveryNRow.resize(mGbnChsRecs.size());
    mEveryNRowAdjust.resize(mGbnChsRecs.size());
    //unsigned long j = 0;
    for(unsigned long i = 0; i < mGbnChsRecs.size(); i++)
    {
        if(mCh2RecsIdx.find(i) != mCh2RecsIdx.end())
        {
            {mGbnAllDataRecs[mCh2RecsIdx[i]]->allocateBuff(mGbnAllDataRecs[mCh2RecsIdx[i]]->getLength());}

            if(mNumberOfRows < mGbnAllDataRecs[mCh2RecsIdx[i]]->getLength()
                    && mGbnAllDataRecs[mCh2RecsIdx[i]]->getDataType() != GS_UNSIGNED_BYTE
                    && mGbnChsRecs[i].arrayDepth == -1)
            {
                mNumberOfRows = mGbnAllDataRecs[mCh2RecsIdx[i]]->getLength();
            }

            mBinaryTypes[i] = mGbnAllDataRecs[mCh2RecsIdx[i]]->getDataType();
            mLength[i] = mGbnAllDataRecs[mCh2RecsIdx[i]]->getLength();
            mDataAddrs[i] = mGbnAllDataRecs[mCh2RecsIdx[i]]->getData();
            if((mGbnAllDataRecs[mCh2RecsIdx[i]]->getFdInc()) == mMinFidInc)
            {
                mEveryNRowAdjust[i] = 1;
            }
            else
            {
                mEveryNRowAdjust[i] = mGbnAllDataRecs[mCh2RecsIdx[i]]->getFdInc()/mMinFidInc;
            }

            if(mEveryNRowAdjust[i] <= 0)
            {
                mEveryNRowAdjust[i] = 1;   //This should never happen
            }

            //j++;
        }
        GlobalMemoryStatusEx(&status);
        if((status.ullAvailPhys - oneGBLeft) < 0)
        {mErrMsg += "Not enough RAM space.\n"; return false;}
    }

    adjustEveryNRows();
    mNumberOfRows = mNumberOfRows*mK;

    mArrayChansList = arrayChans.values();
    std::sort(mArrayChansList.begin(), mArrayChansList.end());

    mArrNullOutPuts.resize(mArrayChansList.size());
    for(int i = 0; i < mArrayChansList.size(); i++)
    {
        mArrNullOutPuts[i].resize(mGbnChsRecs[mArrayChansList[i]].arrayDepth);
        std::fill(mArrNullOutPuts[i].begin(), mArrNullOutPuts[i].end(), "NULL");
    }
    return true;
}

bool ImportGBN::openGBNFile(QString gbnFilePath)
{
    if(!gbnFilePath.isEmpty())
    {
        mGbnFile.setFileName(gbnFilePath);
        if(mGbnFile.open(QIODevice::ReadOnly))                                                   // ReadWrite??
        {
            return true;
        }

        else
        {
            mErrMsg += mGbnFile.errorString() + "\n";
            mGbnFile.close();
            return false;
        }
    }
    else
    {
        mErrMsg += "File path empty.\n";
        return false;
    }
}

QString ImportGBN::lineType2Str(int lineType)
{
    if(lineType == 0)  return "L";
    else if(lineType == 1)  return "B";
    else if(lineType == 2)  return "T";
    else if(lineType == 3)  return "S";
    else if(lineType == 4)  return "R";
    else if(lineType == 5)  return "P";
    else if(lineType ==6)  return "D";
    else return "";
}

void ImportGBN::modifyChanName(QString &chanName)
{
    bool ok = false;
    chanName.toFloat(&ok);

    if(ok)   //Numeric chan name
    {
        chanName = "N"+chanName;
    }

    QString theChanNameUpper = chanName.toUpper();

    int suffixIndex = 0;
    while(mChanNameUpperHash.contains(theChanNameUpper))
    {
        suffixIndex++;
        theChanNameUpper = chanName.toUpper() + "_" + QString::number(suffixIndex);
    }

    if(suffixIndex != 0)
    {
        chanName = chanName + "_" + QString::number(suffixIndex);
        mChanNameUpperHash.insert(chanName.toUpper());
    }
    else
    {
        mChanNameUpperHash.insert(chanName.toUpper());
    }
}

void ImportGBN::adjustEveryNRows()
{

    bool needAdjust = false;
    for(int i = 0; i < mEveryNRowAdjust.size(); i++)
    {
        if(mEveryNRowAdjust[i] != std::rint(mEveryNRowAdjust[i]))
        {
            needAdjust = true;
            break;
        }
    }

    while(needAdjust)
    {
        int intCount = 0;
        mK++;

        for(int i = 0; i < mEveryNRowAdjust.size(); i++)
        {
            mEveryNRowAdjust[i] = mEveryNRowAdjust[i]*mK;
        }

        for(int i = 0; i < mEveryNRowAdjust.size(); i++)
        {
            if(mEveryNRowAdjust[i] == std::rint(mEveryNRowAdjust[i]))
            {
                intCount++;
            }
        }

        if(intCount == mEveryNRowAdjust.size())
        {
            needAdjust = false;
        }
    }

    std::copy(mEveryNRowAdjust.begin(), mEveryNRowAdjust.end(), mEveryNRow.begin());

}

void ImportGBN::findMaxDigits(double input, bool isFloatChan)
{
    int theDigits = 0;

    if(isFloatChan)
    {
        theDigits = numDigits(qRound(input)) + constDigits;
    }
    else
    {
        theDigits = numDigits(qRound(input));
    }

    if(theDigits > maxDigits)
    {
        maxDigits = theDigits;
    }
}

template<class T>
void ImportGBN::extractData(gbnDataRecStruct *inputStruct, int length)
{
    int buffSizeRatio = inputStruct->getBuffSizeRatio();
    if(typeid(T) == typeid(unsigned char))
    {
        char *val = new char[length]{0};
        mGbnFile.read(val, length);
        int bufIndex = 0;

        while(bufIndex < length)
        {
            int idxOneBuff = bufIndex;
            QString valStr;

            while(val[idxOneBuff] != '\0')
            {
                valStr.append(val[idxOneBuff]);
                idxOneBuff++;
            }
            inputStruct->appendData((void *)&valStr, 1);
            inputStruct->addPushPos(1);
            bufIndex += buffSizeRatio;
        }
        mGbnReadPos += length;
        mGbnFile.seek(mGbnReadPos);
        delete[] val;
    }
    else
    {
        char *valBinary = new char[length*sizeof(T)]{0};
        mGbnFile.read(valBinary, length*sizeof(T));
        T *valArray = new T[length]{0};

        memcpy(valArray, reinterpret_cast<unsigned char*>(valBinary), length*sizeof(T));

        if(testMaxDgCount < mGbnChsRecs.size())
        {
            bool isFlChan = false;
            double testVal = static_cast<double>(valArray[0]);
            if(typeid(T) == typeid(float) || typeid(T) == typeid(double))
            {
                isFlChan = true;
            }
            findMaxDigits(testVal, isFlChan);
            testMaxDgCount++;
        }

        if(mGbnChsRecs[inputStruct->getChanNumber()].displayFormat != GSF_NORMAL)
        {
            //TODO, other display formats
        }
        inputStruct->appendData((void *)valArray, length);
        inputStruct->addPushPos(length);
        mGbnReadPos += length * sizeof(T);
        mGbnFile.seek(mGbnReadPos);
        delete[] valBinary;
        delete[] valArray;
    }
}
