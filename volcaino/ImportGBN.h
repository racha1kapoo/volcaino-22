/**
 *  @file   ImportGBN.h
 *  @brief  Opens GBN files, checks, decodes GBN files and stores data.
 *  @brief  GBN file manual: https://www.manualslib.com/manual/515306/Geosoft-Oasis-Montaj-7-0.html?page=99#manual
 *  @author Shuo Li
 *  @date   2022-05-06
 ***********************************************/
#ifndef IMPORTGBN_H
#define IMPORTGBN_H
#include <QObject>
#include <QString>
#include <QFile>
#include "TypeDefConst.h"
#include "math.h"
#include <windows.h>
#include <float.h>
#include <QVariant>
#include <algorithm>
#include <map>
#include <QSet>
#include <unordered_map>
class ImportGBN: public QObject
{

public:
    ///default constructor
    ImportGBN(QObject *parent = nullptr, QString filePath = "");

    /// Class destructor, it frees heap memory
    ~ImportGBN();

//    /// @brief This function returns the decoded GBN values in 2d double table format
//    /// @return  Pointer to the starting address mGBNValsForDB
//    QString **getGBNDecodedVals() const;

    /// @brief This function returns a row of decoded GBN values in string format
    /// @param row: Row number
    /// @return A row of decoded GBN values ("A,1,NULL,123")
    void getOneRow(int row, QString &outPut);

    QList<int> getArrayChanNumbers();
    /// @brief This function returns the number of rows in mGBNValsForDB table
    /// @return The number of rows
    unsigned long getTableNumOfRows() const;

    /// @brief This function returns the error messages(for testing and debuggings)
    /// @return Error messages
    QString getErrMsg() const;

    /// @brief This function returns all channels structs.
    /// @return All channels.
    std::vector<gbnChsRecStruct> getAllChannels() const;

    QSet<int> mDupFDFNLYLN; //Duplicated FLIGHTDATE, FLIGHTNO, LINETYPE, LINENO

private:

    struct gbnDataRecStruct
    {
        virtual ~gbnDataRecStruct(){}
        virtual void allocateBuff(int size) = 0;
        virtual void appendData(void *data, int size) = 0;
        virtual void* getData() = 0;
        virtual void addLength(int length) = 0;
        virtual long getLength() = 0;
        virtual int getDataType() = 0;
        virtual long getChanNumber() = 0;
        virtual void setLength(int length) = 0;
        virtual int getBuffSizeRatio() = 0;
        virtual void setBuffSizeRatio(int ratio) = 0;
        virtual void addPushPos(int pos) = 0;
        virtual int getPushPos() = 0;
        virtual double getFdInc() = 0;
        //virtual void setSpRate(float spRate) = 0;
    };

    template<class T>
    struct dataRecStruct: public gbnDataRecStruct
    {
        virtual ~dataRecStruct()
        {
            if(chanData){delete []chanData;}
        }
        void appendData(void *Data, int size) override
        {
            if(chanData+this->pushPos)
            {
                T *dataToAppend = (T *)Data;
                if(dataToAppend+size != nullptr && dataToAppend != nullptr)
                {std::copy(dataToAppend, dataToAppend+size, chanData+this->pushPos);}
            }
        }

        void allocateBuff(int size) override
        {
            chanData = new T[size*sizeof(T)];
        }

        void *getData() override
        {
            return (void *)chanData;
        }

        void addLength(int length) override
        {
            this->length += length;
        }

        long getLength() override
        {
            return this->length;
        }

        int getDataType() override
        {
            return this->binaryType;
        }

        long getChanNumber() override
        {
            return this->channelNumber;
        }

        void setLength(int length) override
        {
            if(length != this->length)
            {this->length = length;}
        }

        int getBuffSizeRatio() override
        {
            return this->buffSizeRatio;
        }

        void setBuffSizeRatio(int ratio) override
        {
            if(ratio != this->buffSizeRatio)
            {
                this->buffSizeRatio = ratio;
            }
        }

        void addPushPos(int pos) override
        {
            this->pushPos += pos;
        }

        int getPushPos() override
        {
            return this->pushPos;
        }

        double getFdInc() override
        {
            return this->fidIncrement;
        }

//        void setSpRate(float spRate) override
//        {
//            if(this->spRate != spRate)
//            {
//                spRate = this->spRate;
//            }
//        }

        long channelNumber = 0;
        int32_t binaryType = 0;
        double fidStart = 0;
        double fidIncrement = 0;
        long length = 0;
        int buffSizeRatio = 1;
        int pushPos = 0;
        T* chanData = nullptr;
    };
    /// @brief This function checks the very first 17 characters of a GBN file, they must be "OASIS BINARY DATA",
    /// otherwise this file is invalid
    /// @return Whether this file is valid
    bool checkGBNFirst17Chars();

    /// @brief This tries to find char "SUB" (ascii code 1a), "SUB" is the seperation between the header and the binary data,
    /// if "SUB" is not found this file is invalid
    /// @return Whether this file is valid
    bool searchGBNSub();

    /// @brief This function decodes and stores data, it first extracts channel, line, parameter records & data records, when reading
    /// data records for the first time it only extracts data records informations without extracting and storing data. These
    /// informations are for allocating the 2d decoded values table. In the 2nd round it extracts data and store them. If an error happens
    /// the process stops.
    void decodeData();

    /// @brief This function extracts informations from a simple channel record
    void extractGBNChRec();

    /// @brief This function extracts informations from a line record
    void extractGBNLineRec();


    /// @brief This function firstly only extracts data records informations without extracting data. These
    /// informations are for allocating the 2d decoded values table. In the 2nd round it extracts data pushes them into 2d table.
    void extractGBNDataRec();

    /// @brief This function extracts informations from an array channel record
    void extractGBNArrChRec();

    /// @brief This function extracts informations from a parameter record
    void extractGBNParmRec();

    /// @brief This function extracts data and pushes into the 2d table
    template<class T>
    void extractData(gbnDataRecStruct *inputStruct, int length);

    /// @brief This function uses informations from the 1st round reading data records to create a 2d double type table
    /// (guarantees 1 GB free RAM space), if not enough RAM space it returns false.
    /// @return Whether there's enough RAM space.
    bool allocateGBNDataBuff();

    /// @brief This function opens a GBN file with a specific path, if an error happens it returns false.
    /// @param peiFilePath: GBN file path
    /// @return  Whether an error happened
    bool openGBNFile(QString gbnFilePath);


    QFile mGbnFile;
    QString **mGBNValsForDB;

    std::vector<gbnLineRecStruct> mGbnLineRecs;

    std::vector<gbnChsRecStruct> mGbnChsRecs;

    int mGbnChsRecsSize;

    std::vector<gbnDataRecStruct*> mGbnAllDataRecs;
    std::vector<gbnParmRecStruct> mGbnParmRecs;
    std::vector<unsigned long> mChannelNos;
    std::unordered_map<int, int> mCh2RecsIdx;
    bool thereIsNullChan = false;

    int *mBinaryTypes;
    int *mLength;
    std::vector<int>mEveryNRow;
    std::vector<double>mEveryNRowAdjust;

    void* *mDataAddrs;
    std::vector<int> mNewLineRows;
    int mNewLineRowsSize;
    int mTheNewLineRow = 0;
    QString mFlightDate;
    QString mFlightNo;
    QString mLineDateStr;
    QString mLineNo;
    QString mLineType;
    unsigned long mLineArrIndex = 0;

    unsigned long mGBNMaxRRounds = 0;
    unsigned long mGBNMaxRPerRound = 0;
    unsigned long mAvaiRow = 0;
    unsigned long mChannelCount = 0;
    unsigned long mRound = 0;
    std::vector<unsigned long> mRoundList;
    QMap<unsigned long, unsigned long> mDBPushList;
    std::vector<int64_t> mReadPosList;

    bool mFirstTimeScanData = true;
    int mReadDataCount = -1;
    int mGBNLineChanNo = -1;
    int *ucharValArray;
    int64_t mGbnReadPos = -1;

    const char mSubChar = char(26);

    QString mErrMsg;
    QString mFilePath;

    unsigned long mTableLoadCount = 0;
    unsigned long mMaxSpRatio = 0;

    unsigned long mNumberOfRows = 0;

    double mMinFidInc = DBL_MAX;

    QSet<int> arrayChans;
    int currentChanNo = 0;
    QList<int> mArrayChansList;

    std::vector<std::vector<QString>>mArrNullOutPuts;

    QString lineType2Str(int lineType);

    void modifyChanName(QString &chanName);

    void adjustEveryNRows();

    int numDigits(int number)
    {
        int digits = 0;
        if (number < 0) digits = 1; // remove this line if '-' counts as a digit
        while (number) {
            number /= 10;
            digits++;
        }
        return digits;
    }

    void findMaxDigits(double input, bool isFloatChan);

    QSet<QString> mChanNameUpperHash;

    int maxDigits = 0;

    const int constDigits = 3;

    int testMaxDgCount = 0;

    int mReadRow = 0;

    double mK = 1;


};

#endif // IMPORTGBN_H
