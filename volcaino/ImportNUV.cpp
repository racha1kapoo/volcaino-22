#include "ImportNUV.h"

ImportNUV::ImportNUV(QObject *parent, QString filePath): QObject(parent)
{

    if(openPeiFile(filePath) == true)
    {
        if(checkHeaderStructure() == true)
        {
            if(extractChannels() == true)
            {
                if(createTableForDB() == true)
                {decodeData();mPeiFile.close();}
                else
                {mPeiFile.close();}
            }
            else
            {mPeiFile.close();}
        }
        else
        {mPeiFile.close();}
    }
}

ImportNUV::~ImportNUV()
{
    if(mDBTableRows > 0)
    {
        for(unsigned int i = 0; i < mDBTableRows; i++)
        {
            delete [] mPEIValsForDB[i];
        }
        delete[] mPEIValsForDB;
    }
}

double **ImportNUV::getPEIDecodedVals() const
{
    return mPEIValsForDB;
}

std::vector<std::vector<double>> &ImportNUV::getArrayData()
{
    return mArrayData;
}

QList<nuvChStruct> ImportNUV::getAllChannels() const
{
    return mAllChans;
}

int ImportNUV::getFlightNumber() const
{
    return mFlightNo;
}

QString ImportNUV::getFlightDate() const
{
    return mFDate;
}

unsigned int ImportNUV::getTableNumOfRows() const
{
    return mDBTableRows;
}

FileType ImportNUV::getFileType() const
{
    return mFileType;
}

QString ImportNUV::getErrMsg() const
{
    return mErrMsg;
}

bool ImportNUV::openPeiFile(QString peiFilePath)
{

    if(!peiFilePath.isEmpty())
    {

        mPeiFile.setFileName(peiFilePath);
        if(mPeiFile.open(QIODevice::ReadOnly))                                                   // ReadWrite??
        {
            mPeiFileSize = mPeiFile.size();
            return true;
        }

        else
        {
            mErrMsg += mPeiFile.errorString() + "\n";
            mPeiFile.close();
            return false;
        }
    }
    else
    {
        mErrMsg += "File path empty.\n";
        return false;
    }

}

bool ImportNUV::checkHeaderStructure()
{
    bool subFound = false;
    bool endFound = false;

    while (!mPeiFile.atEnd()) {
        if(endFound && subFound) break;
        QByteArray line = mPeiFile.readLine();
        mPeiSubCharPos += line.size();
        if(!endFound){mHeaderList.append(line);}

        if(line.contains("END"))
        {
            endFound = true;
            if(mHeaderList.size() < mMinHeaderLength)
            {
                mErrMsg += "Corrupted File: File header is too short.\n";
                return false;
            }
        }

        if(line.contains(mSubChar) && !subFound)
        {
            subFound = true;
            mPeiSubCharPos -= line.size() - line.indexOf(mSubChar);
            mPeiSubCharPos += 1;
            if(mPeiSubCharPos < mMinHeaderLength)
            {
                mErrMsg += "Incorrect File Structure: File header is too short.\n";
                return false;
            }
        }
    }

    if(subFound == false)
    {mErrMsg += "Incorrect File Structure: Symbol <$1A> is missing.\n";return false;}
    if(endFound == false)
    {mErrMsg += "Incorrect File Structure: File Header: <END> is missing\n";return false;}

    if(!extractHeader()) {return false;}
    return true;
}

bool ImportNUV::extractHeader()
{

    if (mHeaderList[1].indexOf("Gamma Spectrometer (TCP/IP)") == 0) FPEISystem = "AGRS";

    for(int i = 0; i < mHeaderList.count(); i++)
    {
        if (mHeaderList[i].trimmed() == "BEGIN")
        {mFBeginPos = i;}
        else if (mHeaderList[i].trimmed() == "BEGIN,")
        {mFEndPos = i;}
        else if (mHeaderList[i].trimmed() == "END")
        {mFEndPos = i;}
        else if ((mHeaderList[i].indexOf("END") == 0) && mHeaderList[i].length() == 4)
        {mFEndPos = i;}
        else if ((mHeaderList[i].indexOf("Mission") == 0) || (mHeaderList[i].indexOf("Flight") == 0))
        {mFlightNo = (mHeaderList[i].split(':').last().trimmed()).toInt();}
        else if ((mHeaderList[i].indexOf("ZDA") == 0))
        {
            QStringList tmpList = mHeaderList[i].split(',');
            if(tmpList.length() > 4)
            {
                mFDate.clear();
                mFDate.append(tmpList[4]);
                mFDate.append("-");
                mFDate.append(tmpList[3]);
                mFDate.append("-");
                mFDate.append(tmpList[2]);
            }
        }



        else if ((mHeaderList[i].indexOf("Project") == 0))
        {FProject = mHeaderList[i].remove("Project: ");}
        else if ((mHeaderList[i].indexOf("AGIS Version") == 0))
        {
            FPEISystem = "AGIS";
            FSysWithVers = mHeaderList[i];
        }
        else if ((mHeaderList[i].indexOf("AGIS Version") == 0))
        {
            FPEISystem = "SIRIS";
            FSysWithVers = mHeaderList[i];
        }
        else if ((mHeaderList[i].indexOf("IRIS Version") == 0))
        {
            FPEISystem = "IRIS";
            FSysWithVers = mHeaderList[i];
        }
        else if ((mHeaderList[i].indexOf("Radionuclides file: ") == 0))
        {FNuclideLib = mHeaderList[i].remove("Radionuclides file: ");}
        else if ((mHeaderList[i].indexOf("Pico Envirotec Inc. File replayed: ") == 0))
        {fReplayed = mHeaderList[i].remove("Pico Envirotec Inc. File replayed: ");}
        else if ((mHeaderList[i].indexOf("handheld") > 0))
        {FPEISystem = "PGIS";}
        else if ((mHeaderList[i].indexOf("Android") > 0))
        {FPEISystem = "Foot Path";}
        else if ((mHeaderList[i].indexOf("Vers:") == 0))
        {FPEISystem = "IRIS";}
        else if ((mHeaderList[i].indexOf("Version: ") == 0))
            //{FPEISystem = mHeaderList[i].remove("Version: ");}  //TODO
        {FPEISystem = mHeaderList[i].mid(9);}  //TODO
        else if ((mHeaderList[i].indexOf("Accumulation Time") == 0))
        {
            fAccumulated = true;
            fAccumTime = i;
        }
        else if ((mHeaderList[i].indexOf("Xt orientation") == 0))  fXtalsDown = i;
        else if ((mHeaderList[i].indexOf("Xt Volume") == 0))  fXtalsDown = i;
        else if ((mHeaderList[i].indexOf("Xtals") == 0))  fXtalsDown = i;
        else if ((mHeaderList[i].indexOf("Nuclide Detection Algorithm version: ") == 0))  FNuclAlgorithm = mHeaderList[i];
        else if ((mHeaderList[i].indexOf("Nuclide detection procedure version: ") == 0))  FNuclAlgorithm = mHeaderList[i];
        else if ((mHeaderList[i].indexOf("Sampling time") == 0))  SamplTimePos = i;
        else if ((mHeaderList[i].indexOf("ROI Parameters") == 0))  nROIParamPos = i;
        else if ((mHeaderList[i].indexOf("Air Attenuation=") == 0) && nLastROIParamPos<0)  nLastROIParamPos = i;
        else if ((mHeaderList[i].indexOf("RECORDPEI definition") == 0))  nDefPos = i;
    }


    if (mFBeginPos < 0)
    {
        mErrMsg += "Incorrect File Structure: File Header: <BEGIN> is missing.\n";
        return false;
    }

    else if ((mFBeginPos>=mFEndPos) || ((mFBeginPos+1)==mFEndPos))
    {
        mErrMsg += "Incorrect File Structure: File Header: <BEGIN>, <END> Keywords problems.\n";
        return false;
    }

    mFSecCount = mPeiFileSize - mPeiSubCharPos - 1;    //TODO

    //    if(!PEIName2Date(peiFile.fileName(), FDateTime)){mErrMsg += "File name incoorect??";}


    if(fAccumulated == true)
    {
        QStringList sTmp;
        if(fAccumTime < mHeaderList.length())
            sTmp = mHeaderList[fAccumTime].split(" ", Qt::SkipEmptyParts);

        if (sTmp.count() > 2)  fAccumTime = sTmp[2].toInt();
    }
    if (fXtalsDown>=0)
    {
        if(fXtalsDown < mHeaderList.length())
            sStr = mHeaderList[fXtalsDown].mid(mHeaderList[fXtalsDown].indexOf(':')).trimmed();
        fXtalsDown = 0;

        for(int i = 1; i < sStr.count(); i++)
        {
            if (sStr[i]=='D') fXtalsDown++;
        }
        fShieldPos = sStr.indexOf('S');
    }
    if (fXtVolume>=0)
    {
        if(fXtVolume < mHeaderList.length())
            sStr = mHeaderList[fXtVolume].mid(mHeaderList[fXtVolume].indexOf(':')).trimmed();

        bool ok;
        dTmp = sStr.toDouble(&ok);
        if(!ok){fXtVolume = -1;}
        else{fXtVolume = round(dTmp*1000);}
    }
    if (SamplTimePos>=0)
    {
        if(SamplTimePos < mHeaderList.length())
            sStr = mHeaderList[SamplTimePos].mid(mHeaderList[SamplTimePos].indexOf(':')).trimmed();

        //fSamplTime = s2i(sStr, 1000);  what is s2i??  TODO
    }
    if (nROIParamPos>=0)
    {
        if(nROIParamPos < mHeaderList.length())
            sStr = mHeaderList[nROIParamPos].mid(mHeaderList[nROIParamPos].indexOf(':')).trimmed();
        QStringList sTmp;
        sTmp = sStr.split(";", Qt::SkipEmptyParts);

        for(int i = 0; i < sTmp.length(); i++)
        {
            if (sTmp[i].indexOf("AirAttn") > 0)
            {
                if(i < mHeaderList.length())
                    sStr = mHeaderList[i].mid(mHeaderList[i].indexOf('=')).trimmed();
                bool ok;
                fAirAtn = sStr.toDouble(&ok);
                if(!ok) fAirAtn = -1;
            }
            else if((sTmp[i].indexOf("Aircraft Attenuation") > 0))
            {
                sStr = sTmp[i].mid(sTmp[i].indexOf('='));
                bool ok;
                fArcAtn = sStr.toDouble(&ok);
                if(!ok){fArcAtn = -1;}
            }
        }
    }

    if (nLastROIParamPos>=0)
    {
        if(nLastROIParamPos < mHeaderList.length())
            sStr = mHeaderList[nLastROIParamPos];
        QStringList sTmp;
        sTmp = sStr.split(";", Qt::SkipEmptyParts);
        for(int i = 0; i < sTmp.length(); i++)
        {
            if (sTmp[i].indexOf("Air Attenuation") > 0)
            {
                if(i < mHeaderList.length())
                    sStr = mHeaderList[i].mid(mHeaderList[i].indexOf('=')).trimmed();
                bool ok;
                fAirAtn = sStr.toDouble(&ok);
                if(!ok) fAirAtn = -1;
            }
            else if((sTmp[i].indexOf("Aircraft Attenuation") > 0))
            {
                sStr = sTmp[i].mid(sTmp[i].indexOf('='));
                bool ok;
                fArcAtn = sStr.toDouble(&ok);
                if(!ok){fArcAtn = -1;}
            }
        }
    }

    if(FProject == "")
    {
        FProject = QFileInfo(mPeiFile).baseName();
    }
    return true;

}

bool ImportNUV::extractChannels()
{

    QStringList tmpList;
    int nextCh = 0;
    int nPos;
    int nextIdx = 0;

    //int arrayChanDataSize = 0;
    //    theWholeChStruct fNoChan;
    //    fNoChan.chName = "FlightNo";
    //    theWholeChStruct fDateChan;
    //    fDateChan.chName = "FDate";
    //    mAllChans.append(fNoChan);
    //    mAllChans.append(fDateChan);


    for(int i = mFBeginPos + 1; i < mFEndPos; i++)
    {
        nuvChStruct thisChStruct;
        tmpList = mHeaderList[i].split(",", Qt::KeepEmptyParts);
        if(tmpList.count() < 6)
        {
            mErrMsg += "Incorrect channel description: <" + mHeaderList[i] + ">\n";
            return false;
        }
        QString s1 = tmpList[0].trimmed();
        if(s1 == "")
        {
            mErrMsg += "Channel name is missing: <" + mHeaderList[i] + ">\n";
            return false;
        }

        modifyChanName(s1);
        //mChanNameHash.insert(s1);

        thisChStruct.chName = s1;
        s1 = s1.toUpper();
        if ((s1=="RECS") || (s1 == "RNUM") || (s1 == "FID") || (s1 == "FIDS")) {mFRecCh = nextCh;}
        if ((s1 == "LINE") || (s1 == "LINES"))
        {mFLineCh = nextCh;}
        s1 = tmpList[2].trimmed();

        if(s1.right(1) == "*")   //It's spectrum
        {
            mArrChanCount++;
            mArrChans.push_back(nextCh);
            thisChStruct.chStruct.isArrChan = true;
            mFileType = FileType::Spectrum;
            thisChStruct.chStruct.chType = s2PeiType(s1.left(2));
            thisChStruct.chStruct.chSps = 1;
            s1 = tmpList[1].trimmed();
            nPos = s1.indexOf("*");
            if(nPos>0)
            {
                thisChStruct.chStruct.chSps = s1.mid(s1.indexOf("*")+1).toInt(); //TODO
                s1 = s1.left(s1.indexOf("*"));
            }
            thisChStruct.chStruct.arrayLength = s1.toInt();
            //            if(s1 == "2048")   thisChStruct.chStruct.specKind = sp2048;  //TODO
            //            else if(s1 == "1024")   thisChStruct.chStruct.specKind = sp1024;
            //            else if(s1 == "512")  thisChStruct.chStruct.specKind = sp512;
            //            else if(s1 == "256")  thisChStruct.chStruct.specKind = sp256;
            //            else {thisChStruct.chStruct.specKind = spAny;}

            //thisChStruct.chStruct.chSps = s1.toInt() * thisChStruct.chStruct.spRatePerSec;   //TODO
        }
        else
        {
            thisChStruct.chStruct.chType =  s2PeiType(s1);
            //thisChStruct.chStruct.specKind = spnone;
            thisChStruct.chStruct.chSps = tmpList[1].trimmed().toInt();
        }

        if(!mSpRatesHash.contains(thisChStruct.chStruct.chSps) && thisChStruct.chStruct.chSps != 8)
        {
            mSpRatesHash.insert(thisChStruct.chStruct.chSps);
        }
        //        if(thisChStruct.chStruct.chSps > mMaxSps && !thisChStruct.chStruct.isArrChan)
        //        {mMaxSps = thisChStruct.chStruct.chSps;}
        //mTotalSps = thisChStruct.chSps;
        if (thisChStruct.chStruct.chSps == 0)
        {
            mErrMsg += "Channel sampling is incorrect: <" + mHeaderList[i] + ">\n";
            return false;
        }
        if(thisChStruct.chStruct.chType == tpErr)
        {
            mErrMsg += "Channel type is incorrect: <" + mHeaderList[i] + ">\n";
            return false;
        }

        thisChStruct.chStruct.chCoef = tmpList[3].trimmed().toDouble();
        thisChStruct.chStruct.chOfs = tmpList[4].trimmed().toDouble();
        thisChStruct.chStruct.chUnit = tmpList[5].trimmed();

        if(tmpList.count() > 6)
        {thisChStruct.chStruct.chComm = tmpList[6].trimmed();}
        else
        {thisChStruct.chStruct.chComm = "";}


        thisChStruct.chStruct.chIndex = nextIdx;
        if(thisChStruct.chStruct.isArrChan)
        {
            nextIdx += thisChStruct.chStruct.chSps*thisChStruct.chStruct.arrayLength*sizeOfType(thisChStruct.chStruct.chType);
        }
        else
        {
            nextIdx = nextIdx + thisChStruct.chStruct.chSps * sizeOfType(thisChStruct.chStruct.chType);
        }

        nextCh++;
        mAllChans.append(thisChStruct);
    }

    getMaxSpRate(mSpRatesHash);
    mFBlockSize = nextIdx;  //What's FBlockSize for?? Size of all columns in byte?
    if(mFBlockSize == 0)
    {
        mErrMsg += "Data channels not found in a file header.\n";
        return false;
    }

    mFSecCount = mFSecCount/mFBlockSize;   //FSecCount is the size of all rows in byte?

    if(mFSecCount == 0)
    {
        mErrMsg += "No records found in a data file.\n";
        return false;
    }
    mFChCount = mFEndPos - mFBeginPos - 1;
    return true;
}

ChanType ImportNUV::s2PeiType(QString input)
{
    ChanType outPut = tpUnknow;

    if(input == "as")  outPut = tpAs;
    else if(input == "by")  outPut = tpByte;
    else if(input == "db")  outPut = tpReal;
    else if(input == "fl")  outPut = tpFloat;
    else if(input == "in")  outPut = tpInt;
    else if(input == "li")  outPut = tpLong;
    else if(input == "si")  outPut = tpShort;
    else if(input == "wd")  outPut = tpWord;
    else if(input == "dw")  outPut = tpDWord;

    return outPut;
}

int ImportNUV::sizeOfType(ChanType input)
{

    int outPut = 0;

    if(input == tpDWord)  outPut = 2;  //TODO
    else if(input == tpLong64)  outPut = 8;
    else if(input == tpAs)  outPut = 1;   //TODO
    else if(input == tpByte)  outPut = 1;
    else if(input == tpShort)  outPut = 1;
    else if(input == tpWord)  outPut = 2;
    else if(input == tpInt)  outPut = 2;
    else if(input == tpLong)  outPut = 4;
    else if(input == tpFloat)  outPut = 4;
    else if(input == tpReal)  outPut = 8;

    return outPut;
}

void ImportNUV::decodeData()
{

    mFileReadPos = mPeiSubCharPos + 1;
    mPeiFile.seek(mFileReadPos);

    int secCount = 0;
    while(secCount < mFSecCount)
    {
        char *buff = new char[mFBlockSize];

        mPeiFile.read(buff, mFBlockSize);
        mOneSecBinaryData = reinterpret_cast<unsigned char*>(buff);
        mOneSecIndexInBuff = secCount;
        decodeOneSec();

        delete []buff;
        mFileReadPos += mFBlockSize;
        mPeiFile.seek(mFileReadPos);
        secCount++;
    }

}

bool ImportNUV::createTableForDB()
{

    MEMORYSTATUSEX status;
    status.dwLength = sizeof(status);
    GlobalMemoryStatusEx(&status);
    const unsigned int oneGBLeft = 1000000000;
    unsigned long long avaiSpace = status.ullAvailPhys - oneGBLeft;
    if(avaiSpace == 0)  {mErrMsg += "Not enough RAM space.\n"; return false;}

    unsigned long long avaiRow = avaiSpace / sizeof(double) / (mFChCount);

    if(avaiRow >= mFSecCount*mMaxSps)
    {
        mPEIValsForDB = new double*[mFSecCount*mMaxSps];
        for(unsigned long long i = 0; i < mFSecCount*mMaxSps; i++)
        {
            mPEIValsForDB[i] = new double[mFChCount];
            //std::fill_n(mPEIValsForDB[i], mFChCount, nullValue);
        }
        mOneSecCountInBuff = mFSecCount;
        mDBTableRows = mFSecCount*mMaxSps;

        for(int row = 0; row < mFSecCount*mMaxSps; row++)
        {
            for(int col = 0; col < mFChCount; col++)
            {
                mPEIValsForDB[row][col] = nullValue;
            }
        }
        mArrayData.resize(mArrChanCount);
        mArrReadIndex.resize(mArrChanCount);
    }


    else
    {
        mErrMsg += "Not enough RAM space.\n";
        return false;

    }

    return true;
}

void ImportNUV::decodeOneSec()
{

    bool firstTime = true;
    for(int channel = 0; channel < mFChCount; channel++)
    {
        if(!mAllChans[channel].chStruct.isArrChan)
        {
            int theChSps = mAllChans[channel].chStruct.chSps;
            ChanType theChType = mAllChans[channel].chStruct.chType;
            int theChIndex = mAllChans[channel].chStruct.chIndex;
            double theChCoef = mAllChans[channel].chStruct.chCoef;
            double theChOfs = mAllChans[channel].chStruct.chOfs;
            int insertEveryNSec = mMaxSps / theChSps;
            unsigned long long offSet = mOneSecIndexInBuff*mMaxSps;
            if(channel == mFLineCh && firstTime)
            {
                firstTime = !firstTime;
                double lineVal = extractOneSec(0, theChType, theChIndex, theChCoef, theChOfs);
                for(int i = 0; i < mMaxSps; i++)
                {
                    mPEIValsForDB[i + offSet][channel] = lineVal;
                }
            }
            else
            {
                if(mOneSecIndexInBuff == 0)
                {
                    double testVal = extractOneSec(0, theChType, theChIndex, theChCoef, theChOfs);
                    bool isFlChan = false;

                    if(theChType == ChanType::tpReal || theChType == ChanType::tpFloat)
                    {
                        isFlChan = true;
                    }
                    findMaxDigits(testVal, isFlChan);
                }

                for(int sample = 0; sample < theChSps; sample++)
                {
                    mPEIValsForDB[sample*insertEveryNSec + offSet][channel] = extractOneSec(sample, theChType, theChIndex, theChCoef, theChOfs);
                }
            }
        }
        else
        {
            int arrayLength = mAllChans[channel].chStruct.arrayLength;
            int theChSps = mAllChans[channel].chStruct.chSps;
            ChanType theChType = mAllChans[channel].chStruct.chType;
            int theChIndex = mAllChans[channel].chStruct.chIndex;
            double theChCoef = mAllChans[channel].chStruct.chCoef;
            double theChOfs = mAllChans[channel].chStruct.chOfs;

            int nthChan = mArrChans.indexOf(channel);

            if(mOneSecIndexInBuff == 0)
            {
                double testVal = extractOneSec(0, theChType, theChIndex, theChCoef, theChOfs);
                bool isFlChan = false;

                if(theChType == ChanType::tpReal || theChType == ChanType::tpFloat)
                {
                    isFlChan = true;
                }
                findMaxDigits(testVal, isFlChan);
            }

            for(int sample = 0; sample < arrayLength*theChSps; sample++)
            {
                mArrayData[nthChan].push_back(extractOneSec(sample, theChType, theChIndex, theChCoef, theChOfs));
            }
        }
    }
}

double ImportNUV::extractOneSec(int nSps, ChanType theChType, int chIndex, double chCoef, double chOfs)
{

    float flnum = 0;
    double drnum = 0;
    int16_t intnum = 0;
    uint16_t wdnum = 0;
    int32_t linum = 0;
    unsigned char bynum = 0;
    signed char sinum = 0;
    //    int32_t dwnum;
    //    int64_t l8num;
    //    double NewRes;

    //if (nSec < 0 || nSec > FAllRecs)  return false;
    switch(theChType)
    {
    case (tpByte):
    {
        bynum = mOneSecBinaryData[chIndex+nSps];
        return(bynum*chCoef+chOfs);
    }

    case (tpShort):
    {
        sinum = static_cast<char>(mOneSecBinaryData[chIndex+nSps]);
        return(sinum*chCoef+chOfs);
    }

    case (tpAs):
    {
        bynum = mOneSecBinaryData[chIndex+nSps];
        return(bynum*chCoef+chOfs);
    }

    case (tpLong):
    {
        memcpy(&linum, mOneSecBinaryData + chIndex+nSps*4, 4);
        return(linum*chCoef+chOfs);
    }

    case (tpInt):
    {
        memcpy(&intnum, mOneSecBinaryData + chIndex+nSps*2, 2);
        return(intnum*chCoef+chOfs);
    }

    case (tpWord):
    {
        memcpy(&wdnum, mOneSecBinaryData + chIndex+nSps*2, 2);
        return(wdnum*chCoef+chOfs);
    }

    case (tpFloat):
    {
        memcpy(&flnum, mOneSecBinaryData + chIndex+nSps*4, 4);
        return(flnum*chCoef+chOfs);
    }

    case (tpReal):
    {
        memcpy(&drnum, mOneSecBinaryData + chIndex+nSps*8, 8);
        return(drnum*chCoef+chOfs);
    }

    default:
        return -1;
    }
}

void ImportNUV::getMaxSpRate(QSet<int> input)
{
    QSet<int>::iterator it = input.begin();

    mMaxSps = *it;

    for (it = input.begin(); it != input.end(); ++it)
    {
        mMaxSps = (*it * mMaxSps) /
                (gcd(*it, mMaxSps));
    }

}

void ImportNUV::modifyChanName(QString &chanName)
{
    bool ok = false;
    chanName.toFloat(&ok);

    if(ok)   //Numeric chan name
    {
        chanName = "N"+chanName;
    }

    QString theChanNameUpper = chanName.toUpper();

    int suffixIndex = 0;
    while(mChanNameUpperHash.contains(theChanNameUpper))
    {
        suffixIndex++;
        theChanNameUpper = chanName.toUpper() + "_" + QString::number(suffixIndex);
    }

    if(suffixIndex != 0)
    {
        chanName = chanName + "_" + QString::number(suffixIndex);
        mChanNameUpperHash.insert(chanName.toUpper());
    }
    else
    {
        mChanNameUpperHash.insert(chanName.toUpper());
    }


}

void ImportNUV::findMaxDigits(double input, bool isFloatChan)
{
    int theDigits = 0;

    if(isFloatChan)
    {
        theDigits = numDigits(qRound(input)) + constDigits;
    }
    else
    {
        theDigits = numDigits(qRound(input));
    }

    if(theDigits > maxDigits)
    {
        maxDigits = theDigits;
    }

}

