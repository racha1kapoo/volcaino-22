/**
 *  @file   ImportNUV.h
 *  @brief  Opens PEI files, checks, decodes PEI files and stores data
 *  @author Shuo Li
 *  @date   2022-04-20
 ***********************************************/
#ifndef IMPORTNUV_H
#define IMPORTNUV_H
#include <QObject>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QMessageBox>
#include "TypeDefConst.h"
#include "math.h"
#include <windows.h>
#include <QSet>
#include <cmath>
class ImportNUV: public QObject
{
public:
    ///default constructor
    ImportNUV(QObject *parent = nullptr, QString filePath = "");

    /// Class destructor, it frees mPEIValsForDB,
    ~ImportNUV();

    /// @brief This function returns the decoded PEI values in 2d double table format
    /// @return  Pointer to the starting address mPEIValsForDB
    double **getPEIDecodedVals() const;

    std::vector<std::vector<double>> &getArrayData();

    /// @brief This function returns all channels structs.
    /// @return All channels.
    QList<nuvChStruct> getAllChannels() const;

    /// @brief This function returns the flight number.
    /// @return The flight number
    int getFlightNumber() const;

    /// @brief This function returns the flight date in string format.
    /// @return The flight date
    QString getFlightDate() const;

    /// @brief This function returns the number of rows in mPEIValsForDB table
    /// @return The number of rows
    unsigned int getTableNumOfRows() const;

    /// @brief This function returns the file type in enum type
    /// @return File type in enum type
    FileType getFileType() const;

    /// @brief This function returns the error messages(for testing and debuggings)
    /// @return Error messages
    QString getErrMsg() const;

    QVector<int> mArrChans;
    QVector<int> mArrReadIndex;
    int mMaxSps = 0;

    int maxDigits = 0;


private:
    /// @brief This function opens a PEI file with a specific path, if an error happens it returns false.
    /// @param peiFilePath: PEI file path
    /// @return  Whether an error happened
    bool openPeiFile(QString peiFilePath);

    /// @brief This function checks the PEI file's string "END" and char "SUB"(ascii code 1a),
    /// "SUB" is the seperation between the header and the binary data, "END" is the ending of the header.
    /// it also extracts some information from the PEI file header.
    /// if an error happens it returns false.
    /// @return  Whether an error happened
    bool checkHeaderStructure();

    /// @brief This function extract informations from the PEI file header, if an error happens it returns false.
    /// @return  Whether an error happened
    bool extractHeader();

    /// @brief This function extract channels informations and data from the PEI file header to put into ChStruct,
    /// if an error happens it returns false.
    /// @return  Whether an error happened
    bool extractChannels();

    /// @brief This function takes a PEI channel data type in string format and converts to ChanType enumeration type,
    /// @param PEI channel data type in string format
    /// @return  PEI channel data type in ChanType
    ChanType s2PeiType(QString input);

    /// @brief This function takes a PEI channel data type in enum type and gets its size in bytes
    /// @param PEI channel data type in enum type
    /// @return  PEI channel data type's size in bytes
    int sizeOfType(ChanType input);

    /// @brief This function reads and decodes PEI file binary data, all decoded data will be stored in mPEIValsForDB
    void decodeData();

    /// @brief This function creates the largest possible mPEIValsForDB data table(guarantees 1 GB free RAM space)
    /// If there's no availabe space for allocating mPEIValsForDB buffe, it returns false.
    /// @return  Whether an error happened
    bool createTableForDB();

    /// @brief This function takes one second record from the PEI file and decodes according to different channels and samples.
    void decodeOneSec();

    /// @brief This function takes one second record from the function decodeOneSec and other parameters, to decode to one double type value.
    /// Below is an example of PEI file data, * is channel 0 with 8 samples, - is channel 1 with 4 samples, + is channel 2 with 1 sample.
    /// Each sample consists of several bytes of bianry data. For example, a double type sample is of 8 bytes.
    /// One row is one second record, the last column is all data of channel 2, the first 8 colums are all data of channel 0.
    /// ********----+
    /// ********----+
    /// ********----+
    /// ...
    /// ********----+
    /// @param nSps: Current sample number, a channel may contain multiple samples in one record.
    /// @param *input: The starting address of one second record of binary data
    /// @param &outPut: Decoded double type value
    /// @param theChType: Channel type in enum ChanType.
    /// @param chIndex: Channel index, it determines the address offset of this channel's data with respect to this second of record.
    /// For example in the above ascii code picture, chIndex of channel one is 8*(Number of bytes of a ch0 sample).
    /// @param chCoef: Channel coefficient, it's determined by the header.
    /// @param chCoef: Channel offset, it's determined by the header.
    /// @return  The decoded value.
    double extractOneSec(int nSps, ChanType theChType, int chIndex,
                         double chCoef, double chOfs);

    int gcd(int a, int b)
    {
        if (b == 0)
            return a;
        return gcd(b, a % b);
    }

    int numDigits(int number)
    {
        int digits = 0;
        if (number < 0) digits = 1; // remove this line if '-' counts as a digit
        while (number) {
            number /= 10;
            digits++;
        }
        return digits;
    }
    void getMaxSpRate(QSet<int> input);

    void modifyChanName(QString &chanName);

    void findMaxDigits(double input, bool isFloatChan);
    QFile mPeiFile;

    uint64_t mPeiFileSize;

    unsigned int mDBTableRows = 0;

    double **mPEIValsForDB = nullptr;

    QString mErrMsg;

    QList<nuvChStruct> mAllChans;

    int mFlightNo;

    QString mFDate = "1970-01-01";

    int mPeiSubCharPos = -1;

    const int mMinHeaderLength = 20;  //TBD

    const char mSubChar = char(26);

    QStringList mHeaderList;

    int mFBeginPos = -1;
    int mFEndPos = -1;

    QString FPEISystem = "AGIS";
    QString FProject;
    QString FSysWithVers;
    QString FNuclideLib;
    QString fReplayed;
    bool fAccumulated = false;
    int fAccumTime;
    int fXtalsDown;
    QString FNuclAlgorithm;
    int SamplTimePos = -1;
    int nROIParamPos = -1;
    int nLastROIParamPos = -1;
    int nDefPos;
    QString sStr;
    int fShieldPos;
    int fXtVolume;
    double dTmp;
    int fSamplTime;
    double fAirAtn;
    double fArcAtn;

    uint64_t mFSecCount;

    int mFChCount;

    int mFLineCh;

    int mFRecCh = -1;


    FileType mFileType = FileType::Plain;

    uint64_t mFBlockSize;

    int mFileReadPos = 0;

    unsigned char *mOneSecBinaryData;
    unsigned int mOneSecCountInBuff = 0;
    unsigned int mOneSecIndexInBuff = 0;
    unsigned int mOneSecForDBCount = 0;

    int mArrayChanDataSize = 0;
    int mArrChanCount = 0;

    std::vector<std::vector<double>> mArrayData;


    //QStringList mNullArr;
    QSet<int> mSpRatesHash;

    QSet<QString> mChanNameUpperHash;

    const int constDigits = 6;


};

#endif // IMPORTNUV_H
