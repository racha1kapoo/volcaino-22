#include "LanguageSelection.h"
#include <QCoreApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QRadioButton>
#include <QGroupBox>
#include <QDir>
#include "Login.h"
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>

LanguageSelection::LanguageSelection(QWidget *parent):mLanguage("English")
{

}

LanguageSelection::~LanguageSelection()
{

}

LanguageSelection* LanguageSelection::getInstance()
{
    static LanguageSelection theInstance;
    return &theInstance;
}

void LanguageSelection::setLanguage(QString text, QTranslator &translator)
{
    DEBUG_TRACE(text);
    auto files = getTranslationFiles();
    DEBUG_TRACE(files);

    QString fileName = ":/TRANSLATION/"+text+".qm";
    mLanguage = text;
    qApp->removeTranslator(&translator);
    DEBUG_TRACE(fileName);

    if (files.contains(fileName)) {
        DEBUG_TRACE(fileName);
        DEBUG_TRACE(translator.load(fileName));
        DEBUG_TRACE(qApp->installTranslator(&translator));
        mLanguage = getLanguageName(fileName);
    }
}

QString LanguageSelection::getLanguageName(const QString &qmFile)
{
    translator.load(qmFile);
    return translator.translate("LanguageSelection", "English");
}

QStringList LanguageSelection::getTranslationFiles()
{
    QDir dir("../TRANSLATION");
    QStringList fileNames = dir.entryList(QStringList("*.qm"), QDir::Files);
    qDebug() << fileNames;
    for (QString &fileName : fileNames)
        fileName = dir.filePath(fileName);
    return fileNames;
}

QString LanguageSelection::getLanguage()
{
    return mLanguage;
}

