/**
 *  @file   DatabaseTable.h
 *  @brief  This class provides an option to select Chinese, English & Spanish
 *
 *          Procedure to generate language translation file:
 *          1. Translation files can be created by clicking on QT menu bar (Tools > External > Linguist > Update Translations (lupdate)).
 *              *.ts files will be generated under TRANSLATION folder.
 *          2. Then change the translation file generated in step 1 as per the lanugauge and then release those translation files.
 *          3. To release translation files, click on QT menu bar (Tools > External > Linguist > Release Translations (lrelease))
 *
 *  @author Rachana Kapoor
 *  @date   ‎February ‎19, ‎2022
 ***********************************************/

#ifndef LANGUAGESELECTION_H
#define LANGUAGESELECTION_H
#include <QObject>
#include "TypeDefConst.h"
#include <QWidget>
#include <QComboBox>
#include <QTranslator>

class LanguageSelection:public QWidget
{
    Q_OBJECT
public:
    ///Class constructor
    LanguageSelection(QWidget *parent = nullptr);

    ///Class destructor
    ~LanguageSelection();

    /// @brief static function
    ///
    /// @return   Instance of class LanguageSelection
    ///
    static LanguageSelection* getInstance();

    /// @brief This function sets the current language as provided by the user.
    /// @param text: language to set
    /// @param translator: an instance of QTranslator
    /// @return  void
    ///
    void setLanguage(QString text,QTranslator &translator);

    /// @brief This function returns the current language.
    /// @return QString :language
    ///
    QString getLanguage();

    /// @brief This function returns the default language (English).
    /// @return QString :language
    ///
    QString getLanguageName(const QString &qmFile);

    /// @brief This function returns the list of languages.
    /// @return QStringList :languages
    ///
    QStringList getTranslationFiles();

    QComboBox *languages;

    QTranslator translator;

    QString mLanguage;
};

#endif // LANGUAGESELECTION_H
