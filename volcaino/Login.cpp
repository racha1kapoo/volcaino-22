#include "Login.h"
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QDialog>
#include <QLineEdit>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QWidget>
#include <QCoreApplication>

Login::Login()
{
    selectLanguage=LanguageSelection::getInstance();
    this->show();
    tab->tabBar()->hide();
}

Login::~Login()
{
    dialog->close();
}

Login* Login::getInstance()
{
    static Login theInstance;
    return &theInstance;
}

void Login::show()
{
    dialog = new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("USER AUTHENTICATION"));
    dialog->setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
    dialog->setMinimumSize(250,200);

    tab = new QTabWidget(dialog);

    QVBoxLayout *VLayout = new QVBoxLayout(dialog);

    QWidget *langForm = new QWidget(dialog);
    VLayout->addWidget(langForm, Qt::AlignLeft);
    QHBoxLayout *Hlayout = new QHBoxLayout(langForm);
    languages = new QComboBox(langForm);
    Hlayout->addStretch(48);
    languages->addItem(QIcon(":/ICONS/IMAGES/chinese.png"),"Chinese");
    languages->addItem(QIcon(":/ICONS/IMAGES/english.png"),"English");
    languages->addItem(QIcon(":/ICONS/IMAGES/spanish.png"),"Spanish");

    Hlayout->addWidget(languages, Qt::AlignRight);
    connect(languages, &QComboBox::currentTextChanged, this, &Login::setTab);

    VLayout->addWidget(tab);
    {
        selectLanguage->setLanguage("Chinese", translator);

        dialogChinese = new QDialog(tab);
        tab->addTab(dialogChinese, tr("Chinese"));

        QVBoxLayout *VLayout = new QVBoxLayout(dialogChinese);
        VLayout->setAlignment(Qt::AlignCenter);
        VLayout->addSpacing(12);
        QWidget *inputForm = new QWidget(dialogChinese);
        VLayout->addWidget(inputForm);
        QLabel *user = new QLabel(tr("USER"),inputForm);
        QComboBox *typeComboBox = new QComboBox(inputForm);
        typeComboBox->addItem(tr("ADMIN"));
        typeComboBox->addItem(tr("VIEWER"));
        QLabel *pass = new QLabel(tr("PASSWORD"));
        QLineEdit *passValue = new QLineEdit(inputForm);
        QFormLayout *layout = new QFormLayout(inputForm);
        layout->addRow(user, typeComboBox);
        layout->addRow(pass, passValue);

        QWidget *actionForm = new QWidget(dialogChinese);
        QHBoxLayout *HLayout = new QHBoxLayout(actionForm);
        HLayout->addSpacing(36);

        VLayout->addWidget(actionForm);

        QPushButton *ok = new QPushButton(tr("LOGIN"), actionForm);
        QPushButton *cancel = new QPushButton(tr("CANCEL"), actionForm);
        HLayout->addWidget(ok);
        HLayout->addWidget(cancel);
        HLayout->addSpacing(36);
        dialogChinese->show();
        connect(cancel, &QPushButton::clicked, this, &Login::on_pushButton_cancel_clicked);
        connect(ok, &QPushButton::clicked, this, &Login::on_pushButton_login_clicked);
    }
    {

        selectLanguage->setLanguage("English", translator);
        dialogEnglish = new QDialog(tab);
        tab->addTab(dialogEnglish, tr("English"));

        QVBoxLayout *VLayout = new QVBoxLayout(dialogEnglish);
        VLayout->setAlignment(Qt::AlignCenter);

        VLayout->addSpacing(12);
        QWidget *inputForm = new QWidget(dialogEnglish);
        VLayout->addWidget(inputForm);
        QLabel *user = new QLabel(tr("USER"), inputForm);
        QComboBox *typeComboBox = new QComboBox(inputForm);
        typeComboBox->addItem(tr("ADMIN"));
        typeComboBox->addItem(tr("VIEWER"));
        QLabel *pass = new QLabel(tr("PASSWORD"));
        QLineEdit *passValue = new QLineEdit(inputForm);
        QFormLayout *layout = new QFormLayout(inputForm);
        layout->addRow(user, typeComboBox);
        layout->addRow(pass, passValue);

        QWidget *actionForm = new QWidget(dialogEnglish);
        QHBoxLayout *HLayout = new QHBoxLayout(actionForm);
        HLayout->addSpacing(36);

        VLayout->addWidget(actionForm);

        QPushButton *ok = new QPushButton(tr("LOGIN"), actionForm);
        QPushButton *cancel = new QPushButton(tr("CANCEL"), actionForm);
        HLayout->addWidget(ok);
        HLayout->addWidget(cancel);
        HLayout->addSpacing(36);
        dialogEnglish->show();
        connect(cancel, &QPushButton::clicked, this, &Login::on_pushButton_cancel_clicked);
        connect(ok, &QPushButton::clicked, this, &Login::on_pushButton_login_clicked);
    }
    {

        selectLanguage->setLanguage("Spanish", translator);
        dialogEnglish = new QDialog(tab);
        tab->addTab(dialogEnglish, tr("Spanish"));

        QVBoxLayout *VLayout = new QVBoxLayout(dialogEnglish);
        VLayout->setAlignment(Qt::AlignCenter);

        VLayout->addSpacing(12);
        QWidget *inputForm = new QWidget(dialogEnglish);
        VLayout->addWidget(inputForm);
        QLabel *user = new QLabel(tr("USER"), inputForm);
        QComboBox *typeComboBox = new QComboBox(inputForm);
        typeComboBox->addItem(tr("ADMIN"));
        typeComboBox->addItem(tr("VIEWER"));
        QLabel *pass = new QLabel(tr("PASSWORD"));
        QLineEdit *passValue = new QLineEdit(inputForm);
        QFormLayout *layout = new QFormLayout(inputForm);
        layout->addRow(user, typeComboBox);
        layout->addRow(pass, passValue);

        QWidget *actionForm = new QWidget(dialogEnglish);
        QHBoxLayout *HLayout = new QHBoxLayout(actionForm);
        HLayout->addSpacing(36);

        VLayout->addWidget(actionForm);

        QPushButton *ok = new QPushButton(tr("LOGIN"), actionForm);
        QPushButton *cancel = new QPushButton(tr("CANCEL"), actionForm);
        HLayout->addWidget(ok);
        HLayout->addWidget(cancel);
        HLayout->addSpacing(36);
        dialogEnglish->show();
        connect(cancel, &QPushButton::clicked, this, &Login::on_pushButton_cancel_clicked);
        connect(ok, &QPushButton::clicked, this, &Login::on_pushButton_login_clicked);
    }

    languages->setCurrentText("English");
    tab->setCurrentIndex(languages->currentIndex());
    this->setTab(selectLanguage->getLanguage());
    tab->show();

    dialog->show();
}

void Login::close()
{
    dialog->close();
}

void Login::setTab(QString text)
{
    DEBUG_TRACE(text);
    tab->setCurrentIndex(languages->currentIndex());
    selectLanguage->setLanguage(text, translator);
}

void Login::on_pushButton_login_clicked()
{
    dialog->close();
    mMainWin = new MainWindow;
    mMainWin->showWindow();
}

void Login::on_pushButton_cancel_clicked()
{
    dialog->close();
}
