/**
 *  @file   Login.h
 *  @brief  This class is reponsible for providing user authentication on the basis of user selection. (background authentication not yet implemented).
 *          User can also perform language selection in this GUI before pressing 'LOGIN' button.
 *  @author Rachana Kapoor
 *  @date   Dec 14,2021
 ***********************************************/

#ifndef LOGIN_H
#define LOGIN_H

#include <QObject>
#include <QTabWidget>
#include "mainwindow.h"
#include <QTranslator>
#include <QComboBox>
#include "LanguageSelection.h"
class Login:public QMainWindow
{
    Q_OBJECT

public:
    ///Class constructor
    Login();

    /// Class destructor
    ~Login();

    /// @brief static function
    /// @return   Instance of class Login
    ///
    static Login* getInstance();

    /// @brief This function display the login screen
    /// @return   void
    ///
    void show();

    /// @brief This function closes the login screen
    /// @return   void
    ///
    void close();


private slots:

    /// @brief This function checks  username & password of the user and
    /// opens the main window on successful login.
    /// @return  void
    ///
    void on_pushButton_login_clicked();

    /// @brief This function closes the GUI when "CANCEL" button is pressed.
    /// @return  void
    ///
    void on_pushButton_cancel_clicked();

    /// @brief This function sets the language.
    /// @param QString: Language
    /// @return  void
    ///
    void setTab(QString text);

private:
    LanguageSelection *selectLanguage;
    QTranslator translator;
    QWidget *widget;
    QDialog *dialog;
    QDialog *dialogEnglish;
    QDialog *dialogFrench;
    QDialog *dialogChinese;
    QDialog *dialogSpanish;
    QTabWidget *tab;
    QComboBox *languages;
    MainWindow *mMainWin;


};

#endif // LOGIN_H
