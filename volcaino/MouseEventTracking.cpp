#include "MouseEventTracking.h"
#include <QtGui/QPainter>
#include <QtGui/QFontMetrics>
#include <QtWidgets/QGraphicsSceneMouseEvent>
#include <QtGui/QMouseEvent>
#include <QtCharts/QChart>
#include <QTextDocument>
#include "TypeDefConst.h"
#include "ChartData.h"
MouseEventTracking::MouseEventTracking(QChart *chart):
    QGraphicsItem(chart),
    m_chart(chart),
    m_xLine(new QGraphicsLineItem(m_chart)),
    m_yLine(new QGraphicsLineItem(m_chart)),
    m_xText(new QGraphicsTextItem(m_chart)),
    m_yText(new QGraphicsTextItem(m_chart)),
    m_xLineDoubleL(new QGraphicsLineItem(m_chart)),
    m_xLineDoubleR(new QGraphicsLineItem(m_chart)),
    m_xTextDoubleL(new QGraphicsTextItem(m_chart)),
    m_xTextDoubleR(new QGraphicsTextItem(m_chart))
{
    m_xLine->setPen(QPen(Qt::black, 1));
    m_yLine->setPen(QPen(Qt::black, 1));
    //    m_xText->setZValue(11);
    //    m_yText->setZValue(11);
    m_xText->document()->setDocumentMargin(0);
    m_yText->document()->setDocumentMargin(0);
    this->setZValue(1);
    //    m_xText->setDefaultTextColor(Qt::white);
    //    m_yText->setDefaultTextColor(Qt::white);

    m_xLine->setVisible(false);
    m_yLine->setVisible(false);
    m_xText->setVisible(false);
    m_yText->setVisible(false);
    m_xLineDoubleL->setVisible(false);
    m_xLineDoubleR->setVisible(false);
    m_xTextDoubleL->setVisible(false);
    m_xTextDoubleR->setVisible(false);


}

MouseEventTracking::~MouseEventTracking()
{
}

QRectF MouseEventTracking::boundingRect() const
{
    //    QPointF anchor = mapFromParent(m_chart->mapToPosition(m_anchor));

    //    QRectF rect;
    //    rect.setLeft(qMin(m_rect.left(), anchor.x()));
    //    rect.setRight(qMax(m_rect.right(), anchor.x()));
    //    rect.setTop(qMin(m_rect.top(), anchor.y()));
    //    rect.setBottom(qMax(m_rect.bottom(), anchor.y()));

    //    return rect;
}

void MouseEventTracking::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)

    //    QPainterPath path;
    //    path.addRoundedRect(m_rect, 5, 5);

    //    QPointF anchor = mapFromParent(m_chart->mapToPosition(m_anchor));
    //    //DEBUG_TRACE(anchor);

    //    if (!m_rect.contains(anchor)) {
    //        QPointF point1, point2;

    //        // establish the position of the anchor point in relation to m_rect
    //        bool above = anchor.y() <= m_rect.top();
    //        bool aboveCenter = anchor.y() > m_rect.top() && anchor.y() <= m_rect.center().y();
    //        bool belowCenter = anchor.y() > m_rect.center().y() && anchor.y() <= m_rect.bottom();
    //        bool below = anchor.y() > m_rect.bottom();

    //        bool onLeft = anchor.x() <= m_rect.left();
    //        bool leftOfCenter = anchor.x() > m_rect.left() && anchor.x() <= m_rect.center().x();
    //        bool rightOfCenter = anchor.x() > m_rect.center().x() && anchor.x() <= m_rect.right();
    //        bool onRight = anchor.x() > m_rect.right();

    //        // get the nearest m_rect corner.
    //        qreal x = (onRight + rightOfCenter) * m_rect.width();
    //        qreal y = (below + belowCenter) * m_rect.height();
    //        bool cornerCase = (above && onLeft) || (above && onRight) || (below && onLeft) || (below && onRight);
    //        bool vertical = qAbs(anchor.x() - x) > qAbs(anchor.y() - y);

    //        qreal x1 = x + leftOfCenter * 10 - rightOfCenter * 20 + cornerCase * !vertical * (onLeft * 10 - onRight * 20);
    //        qreal y1 = y + aboveCenter * 10 - belowCenter * 20 + cornerCase * vertical * (above * 10 - below * 20);;
    //        point1.setX(x1);
    //        point1.setY(y1);

    //        qreal x2 = x + leftOfCenter * 20 - rightOfCenter * 10 + cornerCase * !vertical * (onLeft * 20 - onRight * 10);;
    //        qreal y2 = y + aboveCenter * 20 - belowCenter * 10 + cornerCase * vertical * (above * 20 - below * 10);;
    //        point2.setX(x2);
    //        point2.setY(y2);

    //        path.moveTo(point1);
    //        path.lineTo(anchor);
    //        path.lineTo(point2);
    //        path = path.simplified();

    //    }


    //    if(mEnableCrossHair){
    //        painter->setBrush(QColor(255, 255, 255));
    //        painter->drawPath(path);
    //        painter->drawText(m_textRect, m_text);
    //    }
    //    else{
    //        path.closeSubpath();
    //        path.clear();
    //        painter->drawText(m_rect,"");
    //    }

}

void MouseEventTracking::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    event->setAccepted(true);
}

void MouseEventTracking::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    //DEBUG_TRACE(event->pos());
    if (event->buttons() & Qt::LeftButton){
        setPos(mapToParent(event->pos() - event->buttonDownPos(Qt::LeftButton)));
        event->setAccepted(true);
    } else {
        event->setAccepted(false);
    }
}
void MouseEventTracking::mouseMoveEvent(QMouseEvent *event)
{
    QPoint p = event->pos();
    //DEBUG_TRACE(p);
    mouseMoveEvent(event);
}

//void MouseEventTracking::setText(const QString &text)
//{
//    m_text = text;
//    QFontMetrics metrics(m_font);
//    m_textRect = metrics.boundingRect(QRect(0, 0, 150, 150), Qt::AlignLeft, m_text);
//    m_textRect.translate(5, 5);
//    prepareGeometryChange();
//    m_rect = m_textRect.adjusted(-5, -5, 5, 5);
//}

//void MouseEventTracking::setAnchor(QPointF pos)
//{
//    m_anchor = pos;
//}


void MouseEventTracking::updateGeometry()
{
    prepareGeometryChange();
    setPos(m_chart->mapToPosition(m_anchor) + QPoint(10, -50));
}

void MouseEventTracking::setUpdateSingleIndicator(QPointF pos)
{
    singleIndPos = pos;
    QPointF position = (m_chart->mapToPosition(pos));

    if(m_xLine->isVisible())
    {
        QLineF xLine(position.x(), m_chart->plotArea().top(),
                     position.x(), m_chart->plotArea().bottom());
        m_xLine->setLine(xLine);
        QString xText = QString("%1").arg(m_chart->mapToValue(position).x());
        seriesIndex = m_chart->mapToValue(position).x();

        m_xText->setHtml(QString("<div style='background-color: #6597c8;'>") + xText + "</div>");
        m_xText->setDefaultTextColor(Qt::black);

        m_xText->setPos(position.x() - m_xText->boundingRect().width() / 2.0, m_chart->plotArea().top()-m_xText->boundingRect().height());


        //        if(mTrackerType == MouseTrackerType::PLMZeroPtSelection)
        //        {
        //            m_xText->setPos(position.x() - m_xText->boundingRect().width() / 2.0, m_chart->plotArea().top()-m_xText->boundingRect().height());
        //        }
        //        else
        //        {
        //            m_xText->setPos(position.x() - m_xText->boundingRect().width() / 2.0, m_chart->plotArea().bottom()-m_xText->boundingRect().height());
        //        }


        m_xLine->show();
        m_xText->show();
    }



    if(mTrackerType == MouseTrackerType::ProfileSingleXY)
    {
        if(m_xLine->isVisible())
        {

            QLineF yLine(m_chart->plotArea().left(), position.y(),
                         m_chart->plotArea().right(), position.y());
            m_yLine->setLine(yLine);
            QString yText = QString("%1").arg(m_chart->mapToValue(position).y());

            m_yText->setHtml(QString("<div style='background-color: #6597c8;'>") + yText + "</div>");
            m_yText->setDefaultTextColor(Qt::black);
            m_yText->setPos(m_chart->plotArea().left(), position.y() - m_yText->boundingRect().height() / 2.0);

            m_yLine->show();
            m_yText->show();
        }
    }
}

void MouseEventTracking::setMultiSingleIndicators(QPointF position, bool isPLMZeroPtCrossInd, bool isArray)
{
    //QPointF position = (m_chart->mapToPosition(pos));
    mIsArray = isArray;
    //    if(mShowMarkerTimes == 10)  mShowMarkerTimes = 0;
    //    int hueValue = 300.0 - 30.0*mShowMarkerTimes;
    //    QColor thisColor = QColor::fromHsv(hueValue,255,255);
    QColor thisColor = Qt::magenta;
    mPLMDataSelCols.push_back(thisColor);

    if(isPLMZeroPtCrossInd)  //PLMZeroPtXYInd
    {
        QLineF xLine;
        if(!isArray)
        {
            xLine = QLineF(position.x(), position.y()-10,
                           position.x(), position.y()+10);
        }
        else
        {
            xLine = QLineF(position.x(), m_chart->plotArea().top(),
                           position.x(), m_chart->plotArea().bottom());
        }

        QGraphicsLineItem *xLinePtr = new QGraphicsLineItem(m_chart);
        xLinePtr->setPen(QPen(thisColor, 3));
        xLinePtr->setLine(xLine);
        xLinePtr->show();

        mPLMDataSelCrossX.push_back(xLinePtr);

        QLineF yLine(position.x()-10, position.y(),
                     position.x()+10, position.y());
        QGraphicsLineItem *yLinePtr = new QGraphicsLineItem(m_chart);
        yLinePtr->setPen(QPen(thisColor, 3));

        if(!isArray)
        {
            yLinePtr->setLine(yLine);
            yLinePtr->show();
        }

        mPLMDataSelCrossY.push_back(yLinePtr);

    }
    else  //PLMZeroPtXInd
    {
        QLineF xLine(position.x(), m_chart->plotArea().top(),
                     position.x(), m_chart->plotArea().bottom());

        QGraphicsLineItem *xLinePtr = new QGraphicsLineItem(m_chart);
        xLinePtr->setPen(QPen(thisColor, 3));
        xLinePtr->setLine(xLine);
        xLinePtr->show();

        QGraphicsTextItem *xTextPtr = new QGraphicsTextItem(m_chart);
        xTextPtr->document()->setDocumentMargin(0);
        QString xText = QString("%1").arg(m_chart->mapToValue(position).x());
        xTextPtr->setHtml(QString("<div style='background-color: #6597c8;'>") + xText + "</div>");
        xTextPtr->setPos(position.x() - xTextPtr->boundingRect().width() / 2.0, m_chart->plotArea().top()-xTextPtr->boundingRect().height());
        xTextPtr->setDefaultTextColor(Qt::white);
        xTextPtr->show();

        mPLMRefDataSelInds.push_back(xLinePtr);
        mPLMRefDataSelTexts.push_back(xTextPtr);
    }
    //mShowMarkerTimes++;
}

void MouseEventTracking::setMultiSinglePtPos(std::vector<QPointF> point, bool isPLMZeroPtCrossInd, bool isArray)
{

    QPointF position;
    QPointF positionVal;
    //QPointF position = (m_chart->mapToPosition(pos));

    for(int i = 0; i < point.size(); i++)
    {
        position += m_chart->mapToPosition(point[i])/(double)point.size();
        positionVal += point[i]/(double)point.size();
    }

    mPLMRefDataSelPos.push_back(positionVal);
    mPLMLvlDataSelPos.push_back(positionVal);
    setMultiSingleIndicators(position, isPLMZeroPtCrossInd, isArray);

}

void MouseEventTracking::updateMultiSingleRefInds()
{
    //qDebug() << m_chart->mapToPosition(mPLMRefDataSelPos[0]);

    for(int i = 0; i < mPLMRefDataSelPos.size(); i++)
    {
        QPointF newPos = m_chart->mapToPosition(mPLMRefDataSelPos[i]);

        QLineF xLine(newPos.x(), m_chart->plotArea().top(),
                     newPos.x(), m_chart->plotArea().bottom());

        //mPLMRefDataSelInds[i]->setPen(QPen(mPLMDataSelCols[i], 3));
        mPLMRefDataSelInds[i]->setLine(xLine);
        mPLMRefDataSelTexts[i]->setX(newPos.x() - mPLMRefDataSelTexts[i]->boundingRect().width() / 2.0);

    }
}

void MouseEventTracking::updateMultiSingleLvlCross()
{
    if(!mPLMLvlDataSelPos.empty())
    {
        double xMin = dynamic_cast<QValueAxis *>(m_chart->axes(Qt::Horizontal).back())->min();
        double xMax = dynamic_cast<QValueAxis *>(m_chart->axes(Qt::Horizontal).back())->max();

        double yMin = dynamic_cast<QValueAxis *>(m_chart->axes(Qt::Vertical).back())->min();
        double yMax = dynamic_cast<QValueAxis *>(m_chart->axes(Qt::Vertical).back())->max();

        for(int i = 0; i < mPLMLvlDataSelPos.size(); i++)
        {
            double xAxisRatio = (mPLMLvlDataSelPos[i].x()-xMin)/(xMax-xMin);
            double yAxisRatio = (mPLMLvlDataSelPos[i].y()-yMin)/(yMax-yMin);

            QPointF newPos;

            newPos.setX(m_chart->plotArea().left()+m_chart->plotArea().width()*xAxisRatio);
            newPos.setY(m_chart->plotArea().bottom()-m_chart->plotArea().height()*yAxisRatio);


            //mPLMDataSelCrossX[i]->setPen(QPen(mPLMDataSelCols[i], 3));

            if(!mIsArray)
            {
                QLineF xLine(newPos.x(), newPos.y()-10,
                             newPos.x(), newPos.y()+10);

                mPLMDataSelCrossX[i]->setLine(xLine);

                QLineF yLine(newPos.x()-10, newPos.y(),
                             newPos.x()+10, newPos.y());

                mPLMDataSelCrossY[i]->setLine(yLine);
            }
            else
            {
                QLineF xLine(newPos.x(), m_chart->plotArea().top(),
                             newPos.x(), m_chart->plotArea().bottom());
                mPLMDataSelCrossX[i]->setLine(xLine);
            }
            //mPLMDataSelCrossY[i]->setPen(QPen(mPLMDataSelCols[i], 3));
        }
    }
}

void MouseEventTracking::updateIndicators()
{
    switch(mTrackerType)
    {
    case ProfileSingleXY:
    {
        //setUpdateSingleIndicator(QPointF(this->lastX, this->lastY));
        break;
    }
    case ProfileSingleX:
    {
        //setUpdateSingleIndicator(QPointF(this->lastX, this->lastY));
        break;
    }
    case ProfileDouble:
    {
        setUpdateDoubleIndicators(QPointF(this->lastXL, this->lastYL), QPointF(this->lastXR, this->lastYR));
        break;
    }
    case PLMSubInd:
    {
        setUpdateDoubleIndicators(QPointF(this->lastXL, this->lastYL), QPointF(this->lastXR, this->lastYR));
        break;
    }
        //    case PLMZeroPtXYInd:
        //    {

        //        break;
        //    }
        //    case PLMZeroPtXInd:
        //    {

        //        break;
        //    }
    default:
    {
        break;
    }
    }
}

void MouseEventTracking::setUpdateDoubleIndicators(QPointF from, QPointF to)
{
    doubleLIndPos = from;
    doubleRIndPos = to;

    QColor thisColor = Qt::blue;
    QPointF startPos = (m_chart->mapToPosition(from));
    QLineF xLineStart(startPos.x(), m_chart->plotArea().top(),
                      startPos.x(), m_chart->plotArea().bottom());

    m_xTextDoubleL->document()->setDocumentMargin(0);
    m_xLineDoubleL->setPen(QPen(thisColor, 1));
    m_xLineDoubleL->setLine(xLineStart);

    QString xTextStart = QString("%1").arg(m_chart->mapToValue(startPos).x());
    m_xTextDoubleL->setHtml(QString("<div style='background-color: #6597c8;'>") + xTextStart + "</div>");
    m_xTextDoubleL->setPos(startPos.x() - m_xTextDoubleL->boundingRect().width() / 2.0, m_chart->plotArea().top()-m_xTextDoubleL->boundingRect().height());
    m_xTextDoubleL->setDefaultTextColor(Qt::black);



    QPointF endPos = (m_chart->mapToPosition(to));
    QLineF xLineEnd(endPos.x(), m_chart->plotArea().top(),
                    endPos.x(), m_chart->plotArea().bottom());

    m_xTextDoubleR->document()->setDocumentMargin(0);
    m_xLineDoubleR->setPen(QPen(thisColor, 1));
    m_xLineDoubleR->setLine(xLineEnd);

    QString xTextEnd = QString("%1").arg(m_chart->mapToValue(endPos).x());
    m_xTextDoubleR->setHtml(QString("<div style='background-color: #6597c8;'>") + xTextEnd + "</div>");
    m_xTextDoubleR->setPos(endPos.x() - m_xTextDoubleR->boundingRect().width() / 2.0, m_chart->plotArea().top()-m_xTextDoubleR->boundingRect().height());
    m_xTextDoubleR->setDefaultTextColor(Qt::black);


    if(m_xLineDoubleL->isVisible())
    {
        m_xLineDoubleL->show();
        m_xLineDoubleR->show();
        m_xTextDoubleL->show();
        m_xTextDoubleR->show();
    }

}

void MouseEventTracking::deleteLvlTrackers(int start, int end)
{

    for(int i = mPLMLvlDataSelPos.size()-1; i >= 0; i--)
    {
        if(mPLMLvlDataSelPos[i].x() >= start && mPLMLvlDataSelPos[i].x() <= end)
        {
            mPLMDataSelCrossX[i]->hide();
            delete mPLMDataSelCrossX[i];
            mPLMDataSelCrossY[i]->hide();
            delete mPLMDataSelCrossY[i];

            mPLMLvlDataSelPos[i] = mPLMLvlDataSelPos.back();
            mPLMLvlDataSelPos.pop_back();
            mPLMDataSelCrossX[i] = mPLMDataSelCrossX.back();
            mPLMDataSelCrossX.pop_back();
            mPLMDataSelCrossY[i] = mPLMDataSelCrossY.back();
            mPLMDataSelCrossY.pop_back();

        }
    }

}

void MouseEventTracking::deleteRefTrackers(int start, int end)
{

    for(int i = mPLMRefDataSelPos.size()-1; i >= 0; i--)
    {
        if(mPLMRefDataSelPos[i].x() >= start && mPLMRefDataSelPos[i].x() <= end)
        {
            mPLMRefDataSelInds[i]->hide();
            delete mPLMRefDataSelInds[i];
            mPLMRefDataSelTexts[i]->hide();
            delete mPLMRefDataSelTexts[i];

            mPLMRefDataSelPos[i] = mPLMRefDataSelPos.back();
            mPLMRefDataSelPos.pop_back();
            mPLMRefDataSelInds[i] = mPLMRefDataSelInds.back();
            mPLMRefDataSelInds.pop_back();
            mPLMRefDataSelTexts[i] = mPLMRefDataSelTexts.back();
            mPLMRefDataSelTexts.pop_back();
        }
    }
}

void MouseEventTracking::addRangeLines(QPointF from, QPointF to)
{
    //    if(mShowMarkerTimes == 10)  mShowMarkerTimes = 0;
    //    int hueValue = 300.0 - 30.0*mShowMarkerTimes;
    //    QColor thisColor = QColor::fromHsv(hueValue,255,255);

    //    QPointF startPos = (m_chart->mapToPosition(from));

    //    QLineF xLineStart(startPos.x(), m_chart->plotArea().top(),
    //                      startPos.x(), m_chart->plotArea().bottom());

    //    QGraphicsLineItem *xLinePtrStart = new QGraphicsLineItem(m_chart);
    //    xLinePtrStart->setPen(QPen(thisColor, 3));

    //    QGraphicsTextItem *xTextPtrStart = new QGraphicsTextItem(m_chart);
    //    xTextPtrStart->document()->setDocumentMargin(0);

    //    xLinePtrStart->setLine(xLineStart);

    //    QString xTextStart = QString("%1").arg(m_chart->mapToValue(startPos).x());
    //    xTextPtrStart->setHtml(QString("<div style='background-color: #6597c8;'>") + xTextStart + "</div>");
    //    xTextPtrStart->setPos(startPos.x() - xTextPtrStart->boundingRect().width() / 2.0, m_chart->plotArea().bottom()-xTextPtrStart->boundingRect().height());
    //    xTextPtrStart->setDefaultTextColor(Qt::white);

    //    xLinePtrStart->show();
    //    xTextPtrStart->show();

    //    mLines.push_back(xLinePtrStart);
    //    mTexts.push_back(xTextPtrStart);

    //    if(from.x() != to.x())
    //    {
    //        QPointF endPos = (m_chart->mapToPosition(to));

    //        QLineF xLineEnd(endPos.x(), m_chart->plotArea().top(),
    //                        endPos.x(), m_chart->plotArea().bottom());

    //        QGraphicsLineItem *xLinePtrEnd = new QGraphicsLineItem(m_chart);
    //        xLinePtrEnd->setPen(QPen(thisColor, 3));

    //        QGraphicsTextItem *xTextPtrEnd = new QGraphicsTextItem(m_chart);
    //        xTextPtrEnd->document()->setDocumentMargin(0);

    //        xLinePtrEnd->setLine(xLineEnd);

    //        QString xTextEnd = QString("%1").arg(m_chart->mapToValue(endPos).x());
    //        xTextPtrEnd->setHtml(QString("<div style='background-color: #6597c8;'>") + xTextEnd + "</div>");
    //        xTextPtrEnd->setPos(endPos.x() - xTextPtrEnd->boundingRect().width() / 2.0, m_chart->plotArea().bottom()-xTextPtrEnd->boundingRect().height());
    //        xTextPtrEnd->setDefaultTextColor(Qt::white);

    //        mLines.push_back(xLinePtrEnd);
    //        mTexts.push_back(xTextPtrEnd);

    //        xLinePtrEnd->show();
    //        xTextPtrEnd->show();
    //    }

    //    mShowMarkerTimes++;

}

void MouseEventTracking::toggleTrackLine()
{
    if(mEnableCrossHair)
    {
        mEnableCrossHair = false;
        m_xLine->hide();
        m_xText->hide();
        m_yLine->hide();
        m_yText->hide();
    }
    else
    {
        mEnableCrossHair = true;
        mEnableIndicator = false;
        m_xLine->show();
        m_xText->show();
        m_yLine->show();
        m_yText->show();
    }
}

void MouseEventTracking::setEnableCrossHair(bool flag){
    //DEBUG_TRACE(mEnableCrossHair);
    mEnableCrossHair=flag;

    if(!mEnableCrossHair)
    {
        m_xLine->hide();
        m_xText->hide();
        m_yLine->hide();
        m_yText->hide();
    }
    else
    {
        m_xLine->show();
        m_xText->show();
        m_yLine->show();
        m_yText->show();
    }
}

void MouseEventTracking::setEnableIndicator(bool flag)
{
    mEnableIndicator=flag;
    if(!mEnableIndicator)
    {
        m_xLine->hide();
        m_xText->hide();
    }
}

void MouseEventTracking::setEnableMultiIndicators(bool flag)
{

}

void MouseEventTracking::toggleIndicator()
{
    if(mEnableIndicator)
    {
        mEnableIndicator = false;
        m_xLine->hide();
        m_xText->hide();
    }
    else
    {
        mEnableIndicator = true;
        mEnableCrossHair = false;
        m_xLine->show();
        m_xText->show();
    }
}

bool MouseEventTracking::getSingleIndVisible()
{
    return m_xLine->isVisible();
}

bool MouseEventTracking::getEnableIndicator()
{
    return mEnableIndicator;
}

void MouseEventTracking::clearTrackers()
{
    hideTrackers();

    for(int i = 0; i < mPLMDataSelCrossX.size(); i++)
    {
        delete mPLMDataSelCrossX[i];
    }
    for(int i = 0; i < mPLMDataSelCrossY.size(); i++)
    {
        delete mPLMDataSelCrossY[i];
    }

    for(int i = 0; i < mPLMRefDataSelInds.size(); i++)
    {
        delete mPLMRefDataSelInds[i];

    }
    for(int i = 0; i < mPLMRefDataSelTexts.size(); i++)
    {
        delete mPLMRefDataSelTexts[i];
    }

    mPLMDataSelCrossX.clear();
    mPLMDataSelCrossY.clear();
    mPLMRefDataSelInds.clear();
    mPLMRefDataSelTexts.clear();
    mPLMRefDataSelPos.clear();
    mPLMLvlDataSelPos.clear();
    //mPLMDataSelCols.clear();

}

void MouseEventTracking::hideTrackers()
{

    for(int i = 0; i < mPLMDataSelCrossX.size(); i++)
    {
        mPLMDataSelCrossX[i]->hide();
    }

    for(int i = 0; i < mPLMDataSelCrossY.size(); i++)
    {
        mPLMDataSelCrossY[i]->hide();
    }

    for(int i = 0; i < mPLMRefDataSelInds.size(); i++)
    {
        mPLMRefDataSelInds[i]->hide();

    }
    for(int i = 0; i < mPLMRefDataSelTexts.size(); i++)
    {
        mPLMRefDataSelTexts[i]->hide();
    }

    m_xLine->hide();
    m_yLine->hide();
    m_xText->hide();
    m_yText->hide();

    m_xLineDoubleL->hide();
    m_xLineDoubleR->hide();
    m_xTextDoubleL->hide();
    m_xTextDoubleR->hide();
}

void MouseEventTracking::adjustTrackerPos(QPointF from, QPointF to)
{
    switch(mTrackerType)
    {
    case ProfileSingleXY:
    {
        //setUpdateSingleIndicator(from);
        break;
    }
    case ProfileSingleX:
    {
        //setUpdateSingleIndicator(from);
        break;
    }
    case ProfileDouble:
    {
        setUpdateDoubleIndicators(from, to);
        break;
    }
    case PLMSubInd:
    {
        setUpdateDoubleIndicators(from, to);
        break;
    }
        //    case PLMZeroPtXYInd:
        //    {

        //        break;
        //    }
        //    case PLMZeroPtXInd:
        //    {

        //        break;
        //    }
    default:
    {
        break;
    }
    }
}

void MouseEventTracking::setTrackerType(MouseTrackerType type)
{
    mTrackerType = type;
    //hideTrackers();
    switch(mTrackerType)
    {
    case ProfileSingleXY:
    {
        //        m_xLine->show();
        //        m_yLine->show();
        //        m_xText->show();
        //        m_yText->show();
        break;
    }
    case ProfileSingleX:
    {
        bool isShow = m_xLine->isVisible();
        isShow = !isShow;

        m_xLine->setVisible(isShow);
        m_xText->setVisible(isShow);

        if(isShow)
        {
            setUpdateSingleIndicator(QPointF(-1,0));   //Initial position
        }

        break;
    }
    case ProfileSingleXShow:
    {

        m_xLine->show();
        m_xText->show();

        setUpdateSingleIndicator(QPointF(-1,0));   //Initial position


        break;
    }
    case ProfileDouble:
    {
        //        m_xLineDoubleL->show();
        //        m_xLineDoubleR->show();
        //        m_xTextDoubleL->show();
        //        m_xTextDoubleR->show();
        break;
    }
    case PLMSubInd:
    {
        //        m_xLineDoubleL->show();
        //        m_xLineDoubleR->show();
        //        m_xTextDoubleL->show();
        //        m_xTextDoubleR->show();
        break;
    }
        //    case PLMZeroPtXYInd:
        //    {
        ////        for(int i = 0; i < mLines.size(); i++)
        ////        {
        ////            mLines[i]->show();
        ////        }

        //        break;
        //    }
        //    case PLMZeroPtXInd:
        //    {
        ////        for(int i = 0; i < mLines.size(); i++)
        ////        {
        ////            mLines[i]->show();
        ////        }
        ////        for(int i = 0; i < mTexts.size(); i++)
        ////        {
        ////            mTexts[i]->show();
        ////        }
        //        break;
        //    }
    case PLMZeroPtSelection:
    {
        bool isShow = m_xLine->isVisible();
        isShow = !isShow;

        if(!isShow)
        {
            //mTrackerType = NoTrackerTypes;
        }

        m_xLine->setVisible(isShow);
        m_xText->setVisible(isShow);

        if(isShow)
        {
            setUpdateSingleIndicator(QPointF(-1,0));   //Initial position
        }

        break;
    }
    case NoTrackerTypes:
    {
        if(m_xLine->isVisible())
        {
            m_xLine->hide();
            m_xText->hide();
        }
        if(m_yLine->isVisible())
        {
            m_yLine->hide();
            m_yText->hide();
        }
        if(m_xLineDoubleL->isVisible())
        {
            m_xLineDoubleL->hide();
            m_xLineDoubleR->hide();
            m_xTextDoubleL->hide();
            m_xTextDoubleR->hide();
        }
        break;
    }
    case NoTrackerTypesProfileViewer:
    {
        if(m_xLine->isVisible())
        {
            m_xLine->hide();
            m_xText->hide();
        }
        if(m_yLine->isVisible())
        {
            m_yLine->hide();
            m_yText->hide();
        }
    }
    default:
        break;
    }
}

MouseTrackerType MouseEventTracking::getTrackerType()
{
    return mTrackerType;
}

void MouseEventTracking::saveSingleIndX(double x)
{
    singleIndX = x;
}

void MouseEventTracking::saveSingleIndY(double y)
{
    singleIndY = y;
}

double MouseEventTracking::getSingleIndX()
{
    return singleIndX;
}

double MouseEventTracking::getSingleIndY()
{
    return singleIndY;
}

QPointF MouseEventTracking::getSingleIndPos()
{
    return singleIndPos;
}

QPointF MouseEventTracking::getDoubleLIndPos()
{
    return doubleLIndPos;
}

QPointF MouseEventTracking::getDoubleRIndPos()
{
    return doubleRIndPos;
}

void MouseEventTracking::showIniDoubleIndicators()
{
    m_xLineDoubleL->setVisible(true);
    m_xLineDoubleR->setVisible(true);
    m_xTextDoubleL->setVisible(true);
    m_xTextDoubleR->setVisible(true);
}

//bool MouseEventTracking::singleIndVisible()
//{
//    if(m_xLine->isVisible() || m_yLine->isVisible())
//    {
//        return true;
//    }
//    return false;
//}

