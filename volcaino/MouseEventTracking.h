/**
 *  @file   MouseEventTracking.h
 *  @brief  This class is responsible for detecting mouse events when clicked or hovered over chart/graphics.
 *          It is also responsible for enabling or disabling track line to display the coordinates of the chart and paint
 *          X & Y values of the chart.
 *  @author Rachana Kapoor
 *  @date   ‎May ‎3, ‎2022
 ***********************************************/

#ifndef MOUSEEVENTTRACKING_H
#define MOUSEEVENTTRACKING_H
#include <QtCharts/QChartGlobal>
#include <QtWidgets/QGraphicsItem>
#include <QtGui/QFont>
#include <QPen>
#include <QtWidgets/QGraphicsItem>
#include <QMouseEvent>
#include <QObject>
#include "TypeDefConst.h"

QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class MouseEventTracking: public QGraphicsItem
{

public:
    ///Class constructor
    MouseEventTracking(QChart *parent);

    ~MouseEventTracking();

    /// @brief This function sets the X & Y values of the mouse click in the rectangular box to display.
    /// @param text: A string containing X & Y values
    /// @return  void
    ///
    //void setText(const QString &text);

    /// @brief This function saves the recent the X & Y values of the mouse click.
    /// @param point: coordinates of mouse events(click/hover)
    /// @return  void
    ///
    //void setAnchor(QPointF point);

    /// @brief This function updates the coordinates whenever change in mouse event is detected.
    /// @return  void
    ///
    void updateGeometry();  

    /// @brief This function sets  the position/size of rectangular box.
    /// @return  void
    ///
    QRectF boundingRect() const;

    /// @brief This function is responsible for plotting/painting the rectangular box whenever mouse event is detected.
    /// @param painter: an instance of QPainter class
    /// @param option: Not used
    /// @param widget: Not used
    /// @return  void
    ///
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);

    /// @brief This function is responsible for plotting the coordinates of X & Y values on XY axes whenever mouse event is detected.
    /// @param point: point coordinates
    /// @return  void
    ///
    void setUpdateSingleIndicator(QPointF point);

    void setMultiSingleIndicators(QPointF position, bool isPLMZeroPtCrossInd, bool isArray);  //PLMZeroPtXYInd, PLMZeroPtXInd, SINGLE pt
    void setMultiSinglePtPos(std::vector<QPointF> point, bool isPLMZeroPtCrossInd, bool isArray);  //PLMZeroPtXYInd, PLMZeroPtXInd, MULTI pt
    void updateMultiSingleRefInds();
    void updateMultiSingleLvlCross();

    void updateIndicators();

    void setUpdateDoubleIndicators(QPointF from, QPointF to);

    void deleteLvlTrackers(int start, int end);
    void deleteRefTrackers(int start, int end);

    void addRangeLines(QPointF from, QPointF to);

    /// @brief This function enables/disables the track line.
    /// @return  void
    ///
    void toggleTrackLine();

    /// @brief This function sets the flag to display/hide the track line.
    /// @param flag: true/false
    /// @return  void
    void setEnableCrossHair(bool flag);

    void setEnableIndicator(bool flag);

    void setEnableMultiIndicators(bool flag);

    void toggleIndicator();

    bool getSingleIndVisible();

    bool getEnableIndicator();

    double lastX;
    double lastY;

    double lastXL;
    double lastYL;
    double lastXR;
    double lastYR;

    int seriesIndex;

    void clearTrackers();

    void hideTrackers();

    void adjustTrackerPos(QPointF from, QPointF to);

    void setTrackerType(MouseTrackerType type);

    MouseTrackerType getTrackerType();

    void saveSingleIndX(double x);
    void saveSingleIndY(double y);
    double getSingleIndX();
    double getSingleIndY();
    QPointF getSingleIndPos();
    QPointF getDoubleLIndPos();
    QPointF getDoubleRIndPos();

    //bool singleIndVisible();
    void showIniDoubleIndicators();

protected:
    /// @brief This function is invoked when mouse press event is detected over chart/graphics.
    /// @return  event: An instance of QGraphicsSceneMouseEvent
    /// @return void
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    /// @brief This function is invoked when mouse move event is detected over chart/graphics.
    /// @return  event: An instance of QGraphicsSceneMouseEvent
    /// @return void
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    /// @brief An overloaded function is invoked when mouse move event is detected over chart/graphics.
    /// @return  event: An instance of QMouseEvent
    /// @return void
    void mouseMoveEvent(QMouseEvent *event);

private:
    QString m_text;
    QRectF m_textRect;
    QRectF mRect;
    QRectF m_rect;
    QPointF m_anchor;
    QFont m_font;
    QChart *m_chart;
    QGraphicsLineItem *m_xLine = nullptr, *m_yLine = nullptr;
    QGraphicsTextItem *m_xText = nullptr, *m_yText = nullptr;

    QGraphicsLineItem *m_xLineDoubleL = nullptr;
    QGraphicsLineItem *m_xLineDoubleR = nullptr;
    QGraphicsTextItem *m_xTextDoubleL = nullptr;
    QGraphicsTextItem *m_xTextDoubleR = nullptr;

    QGraphicsLineItem *m_lineItem;
    QList<QPointF> m_xList;
    QPen m_middleLinePen;
    QPoint m_middlePos;



    int mShowMarkerTimes = 0;

    std::vector<QGraphicsLineItem *> mPLMDataSelCrossX;
    std::vector<QGraphicsLineItem *> mPLMDataSelCrossY;

    std::vector<QGraphicsTextItem *> mTexts;

    std::vector<QGraphicsLineItem *> mPLMRefDataSelInds;
    std::vector<QGraphicsTextItem *> mPLMRefDataSelTexts;
    std::vector<QPointF> mPLMRefDataSelPos;
    std::vector<QPointF> mPLMLvlDataSelPos;
    std::vector<QColor> mPLMDataSelCols;

    int mSelectedColNo = 0;

    bool mEnableCrossHair = true;
    bool mEnableIndicator = false;
    bool mEnableMultiIndicators = false;

    bool mProfileSingle = false;
    bool mProfileDouble = false;
    bool mPLMSubInd = false;
    bool mPLMZeroPtXYInd = false;
    bool mPLMZeroPtYInd = false;

    MouseTrackerType mTrackerType;

    double singleIndX;
    double singleIndY;
    QPointF singleIndPos;

    double doubleLIndX;
    double doubleRIndX;
    QPointF doubleLIndPos;
    QPointF doubleRIndPos;

    std::vector<double> mMultiCrossX;
    std::vector<double> mMultiCrossY;

    std::vector<double> mMultiIndX;

    bool mIsArray;

};

#endif // MOUSEEVENTTRACKING_H
