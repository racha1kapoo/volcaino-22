#include "NPKGridding.h"

NPKGridding::NPKGridding(std::vector<double> inputPars, QString filePath)
{
    mFile.setFileName(filePath);

    mCellSize = inputPars[0];
    interpDist = inputPars[1];
    maxLoop = inputPars[2];
    trendM = inputPars[3];
    angleSearch = inputPars[4];
    searchStepSize = inputPars[5];
    autoStop = inputPars[6];

}

NPKGridding::~NPKGridding()
{
    //    delete []diffs;
    //    for(int row = 0; row < mImgHeight; row++)
    //    {
    //        delete []X[row];
    //        delete []Y[row];
    //        delete []Value[row];
    //        delete []ValCount[row];
    //        delete []trendLOG[row];
    //        delete []ValueDrvItr[row];
    //        delete []ValPrevious[row];
    //        delete []SmoothGrid[row];
    //        delete []EignVals[row];
    //        delete []EignV11[row];
    //        delete []EignV12[row];
    //        delete []LoopNewVals[row];
    //        delete []ValCountDrvItr[row];
    //        delete []multiplierCells[row];
    //        delete []iMultCells[row];

    //        for(int col = 0; col < mImgWidth; col++)
    //        {
    //            delete []Dist2RealVal[row][col];
    //            for(int k = 0; k < 8; k++)
    //            {
    //                delete []XYAdjacentCell[row][col][k];
    //            }
    //            delete []XYAdjacentCell[row][col];
    //        }
    //        delete []Dist2RealVal[row];
    //        delete []XYAdjacentCell[row];
    //    }

    //    delete []X;
    //    delete []Y;
    //    delete []Value;
    //    delete []ValCount;
    //    delete []trendLOG;
    //    delete []ValueDrvItr;
    //    delete []ValPrevious;
    //    delete []SmoothGrid;
    //    delete []EignVals;
    //    delete []EignV11;
    //    delete []EignV12;
    //    delete []LoopNewVals;
    //    delete []ValCountDrvItr;
    //    delete []multiplierCells;
    //    delete []iMultCells;
    //    delete []Dist2RealVal;
    //    delete []XYAdjacentCell;

    //    //emit statusChanged("Memory is safely deleted");
    emit enableBoxOperations(true);

}

void NPKGridding::setDataBase(QString tableName, QString gridValue, QString gridX, QString gridY)
{
    mTableName = tableName;
    mGridValue = gridValue;
    mGridX = gridX;
    mGridY = gridY;

    auto db =DataBase::getInstance();
    if (db->openDatabase())
        mSql=db->getSqlQuery();

    mReadFromDB = true;
}

std::vector<std::vector<double>> &NPKGridding::getGridData()
{
    return Value;
}

std::vector<std::vector<int> > &NPKGridding::getGridDataFlag()
{
    return ValCount;
}

void NPKGridding::loadData()
{
    if(!mReadFromDB)
    {
        if(mFile.open(QIODevice::ReadOnly))
        {
            QList<QByteArray> line;
            bool isFirstTime = true;
            mFile.readLine();

            while(!mFile.atEnd())
            {
                line = mFile.readLine().split(mSplit);

                double theX = line[xTrack].toDouble();
                double theY = line[yTrack].toDouble();
                double theVal = line[valTrack].toDouble();

                if(isFirstTime)
                {
                    mYmin = theY;
                    mYmax = theY;

                    mXmin = theX;
                    mXmax = theX;

                    mValueMax = theVal;
                    mValueMin = theVal;

                    isFirstTime = false;
                }

                if(theY > mYmax)
                {
                    mYmax = theY;
                }

                if(theY < mYmin)
                {
                    mYmin = theY;
                }

                if(theX > mXmax)
                {
                    mXmax = theX;
                }

                if(theX < mXmin)
                {
                    mXmin = theX;
                }

                if(theVal > mValueMax)
                {
                    mValueMax = theVal;
                }

                if(theVal < mValueMin)
                {
                    mValueMin = theVal;
                }
            }

            mImgWidth = (mXmax-mXmin)/(mCellSize)+1;
            mImgHeight = (mYmax-mYmin)/(mCellSize)+1;
            mRmg1Range = qAbs(mValueMax-mValueMin);

            mXmin = (int)mXmin/(int)mCellSize * (int)mCellSize;
            mYmin = (int)mYmin/(int)mCellSize * (int)mCellSize;

            allocateBuff();

            if(mValueMin < 0)
            {dcOffset = qAbs(mValueMin) + 100.0;}

            else
            {dcOffset = 100.0;}

            mFile.seek(0);
            mFile.readLine();
            while(!mFile.atEnd())
            {
                line = mFile.readLine().split(',');

                //int lineNo = line[lineTrack].toInt();
                double theX = line[xTrack].toDouble();
                double theY = line[yTrack].toDouble();
                double theVal = line[valTrack].toDouble();

                int row = (mYmax-theY)/mCellSize;
                int col = (theX - mXmin)/mCellSize;

                Value[row][col] += theVal;
                ValCount[row][col] += 1;
            }


            mFile.close();

        }
    }
    else
    {
        bool isFirstTime = true;
        QString valueQuery = "SELECT "+mGridValue+", "+mGridX+", "+mGridY+" FROM "+mTableName;
        if (mSql.exec(valueQuery)) {
            while (mSql.next()) {
                if(!mSql.value(0).isNull() && !mSql.value(1).isNull()
                        && !mSql.value(2).isNull())
                {
                    double gridValue = mSql.value(0).toDouble();
                    double gridX = mSql.value(1).toDouble();
                    double gridY = mSql.value(2).toDouble();
                    if(isFirstTime)
                    {
                        mYmin = gridY;
                        mYmax = gridY;

                        mXmin = gridX;
                        mXmax = gridX;

                        mValueMax = gridValue;
                        mValueMin = gridValue;

                        isFirstTime = false;
                    }

                    if(gridY > mYmax)
                    {
                        mYmax = gridY;
                    }

                    if(gridY < mYmin)
                    {
                        mYmin = gridY;
                    }

                    if(gridX > mXmax)
                    {
                        mXmax = gridX;
                    }

                    if(gridX < mXmin)
                    {
                        mXmin = gridX;
                    }

                    if(gridValue > mValueMax)
                    {
                        mValueMax = gridValue;
                    }

                    if(gridValue < mValueMin)
                    {
                        mValueMin = gridValue;
                    }
                }
            }
        }

        mImgWidth = (mXmax-mXmin)/(mCellSize)+1;
        mImgHeight = (mYmax-mYmin)/(mCellSize)+1;
        mRmg1Range = qAbs(mValueMax-mValueMin);

        mXmin = (int)mXmin/(int)mCellSize * (int)mCellSize;
        mYmin = (int)mYmin/(int)mCellSize * (int)mCellSize;

        allocateBuff();

        if(mValueMin < 0)
        {dcOffset = qAbs(mValueMin) + 100.0;}

        else
        {dcOffset = 100.0;}

        if (mSql.exec(valueQuery)) {
            while (mSql.next()) {
                if(!mSql.value(0).isNull() && !mSql.value(1).isNull()
                        && !mSql.value(2).isNull())
                {
                    double gridValue = mSql.value(0).toDouble();
                    double gridX = mSql.value(1).toDouble();
                    double gridY = mSql.value(2).toDouble();

                    int row = (mYmax-gridY)/mCellSize;
                    int col = (gridX - mXmin)/mCellSize;

                    Value[row][col] += gridValue;
                    ValCount[row][col] += 1;
                }
            }
        }

    }
}

void NPKGridding::prepareData()
{
    {
        //        if(mValueMin < 0)
        //        {dcOffset = qAbs(mValueMin) + 10.0;}
        //        else
        //        {dcOffset = 10.0;}


        //        for(int i = 0; i < mDataSet.size(); i++)
        //        {
        //            int row = 0;
        //            int col;

        //            if(isYmin)
        //            {row = (mDataSet[i][1] - mYmin)/mCellSize;}
        //            else
        //            {row = (mYmax - mDataSet[i][1])/mCellSize;}

        //            col = (mDataSet[i][0] - mXmin)/mCellSize;

        //            //X[row][col] += mDataSet[i][0];
        //            //Y[row][col] += mDataSet[i][1];

        //            Value[row][col] += mDataSet[i][2];

        //            ValCount[row][col]++;
        //        }


        for(int col = 0; col < mImgWidth; col++)
        {
            for(int row = 0; row < mImgHeight; row++)
            {
                if(ValCount[row][col] > 1)
                {
                    //X[row][col] = X[row][col] / ValCount[row][col];
                    //Y[row][col] = Y[row][col] / ValCount[row][col];
                    Value[row][col] = Value[row][col] / ValCount[row][col];
                    ValCount[row][col] = 1;
                }
            }
        }


        //Now go through all cells, and assign values to the ones that have no data currently, or determine if they are too far away from real data to use.
        //During this process we will also find all closest data to each cell which will be information needed when normalizing.
        for(int col = 0; col < mImgWidth; col++)
        {
            for(int row = 0; row < mImgHeight; row++)
            {
                if(ValCount[row][col] != 1)
                {
                    //X[row][col] = col*mCellSize + mXmin + mCellSize/2;
                    //Y[row][col] = row*mCellSize + mXmin + mCellSize/2;
                }


                bool searchNextDir = false;
                double closeDistVal = 0;
                double closeDist = (interpDist / mCellSize) + 1;
                int closeDistCount = 0;

                for (int k = 0; k < 8; k++)
                {

                    int loopI = 1;
                    searchNextDir = false;

                    while (searchNextDir == false)
                    {
                        if (col + xLoop[k] * loopI >= mImgWidth || col + xLoop[k] * loopI < 0) //hit an edge in x-direction
                        {
                            if (loopI == 1)
                            {
                                XYAdjacentCell[row][col][k][0] = col;
                                XYAdjacentCell[row][col][k][1] = row;
                            }
                            Dist2RealVal[row][col][k] = -1;
                            searchNextDir = true;
                        }

                        else if (row + yLoop[k] * loopI >= mImgHeight || row + yLoop[k] * loopI < 0) //hit an edge in y-direction
                        {
                            if (loopI == 1)
                            {
                                XYAdjacentCell[row][col][k][0] = col;
                                XYAdjacentCell[row][col][k][1] = row;
                            }
                            Dist2RealVal[row][col][k] = -1;
                            searchNextDir = true;
                        }

                        else if(ValCount[col+ xLoop[k] * loopI][col + xLoop[k] * loopI] == -1)
                        {
                            if(loopI == 1)
                            {
                                XYAdjacentCell[row][col][k][0] = col;
                                XYAdjacentCell[row][col][k][1] = row;
                            }
                            Dist2RealVal[row][col][k] = -1;
                            searchNextDir = true;
                        }

                        else //everything is OK at this location
                        {
                            if (loopI == 1)
                            {
                                XYAdjacentCell[row][col][k][0] = col + xLoop[k];
                                XYAdjacentCell[row][col][k][1] = row + yLoop[k];
                            }

                            if(ValCount[row + yLoop[k] * loopI][col + xLoop[k] * loopI] == 1)
                            {
                                Dist2RealVal[row][col][k] = loopI;
                                searchNextDir = true;

                                double rDistance =  loopI * sqrt(pow(xLoop[k], 2) + pow(yLoop[k], 2));

                                // Find all closest data points
                                if(closeDist > rDistance)
                                {
                                    closeDistCount = 1;
                                    closeDist = rDistance;
                                    closeDistVal = Value[row+yLoop[k]*loopI][col+xLoop[k]*loopI];
                                }
                                else if(closeDist == rDistance)
                                {
                                    closeDistCount++;
                                    closeDist = rDistance;
                                    closeDistVal += Value[row+yLoop[k]*loopI][col+xLoop[k]*loopI];
                                }
                            }
                            else
                            {
                                loopI++;
                                if(loopI>interpDist/mCellSize)
                                {
                                    Dist2RealVal[row][col][k] = -1;
                                    searchNextDir = true;
                                }
                            }
                        }
                    }

                    if(ValCount[row][col] == 1)
                    {
                        Dist2RealVal[row][col][k] = 0;
                    }
                }

                if (ValCount[row][col] != 1) //if this was not a real data cell then assign the values
                {
                    //Now need to find closest real data and assign its value to the current cell.
                    if (closeDist != (interpDist / mCellSize) + 1)
                    {
                        Value[row][col] = closeDistVal / closeDistCount;
                        ValCount[row][col] = 0;
                    }
                    else //no real data was close enough, therefore it is outside of the range.
                    {
                        Value[row][col] = -999999;
                        ValCount[row][col] = -1;
                        for (int k = 0; k < 8; k++)
                        {
                            XYAdjacentCell[row][col][k][0] = col;
                            XYAdjacentCell[row][col][k][1] = row;
                        }
                    }
                }
            }
        }
    }


    for(int col = 0; col < mImgWidth; col++)
    {
        for(int row = 0; row < mImgHeight; row++)
        {
            if(ValCount[row][col] != -1)
            {Value[row][col] += dcOffset;}
        }
    }

    mValueMin += dcOffset;
    mValueMax += dcOffset;

    {
        for(int col = 0; col < mImgWidth; col++)
        {
            for(int row = 0; row < mImgHeight; row++)
            {
                if(ValCount[row][col] != -1)
                {
                    double adjcentValues[9];

                    for(int k = 0; k < 8; k++)
                    {
                        if(Value[XYAdjacentCell[row][col][k][1]][XYAdjacentCell[row][col][k][0]] == -999999)
                        {
                            adjcentValues[k] = Value[row][col];
                            XYAdjacentCell[row][col][k][0] = col;
                            XYAdjacentCell[row][col][k][1] = row;
                        }
                        else
                        {
                            adjcentValues[k] = Value[XYAdjacentCell[row][col][k][1]][XYAdjacentCell[row][col][k][0]];
                        }
                    }
                    adjcentValues[8] = Value[row][col];

                    std::sort(std::begin(adjcentValues), std::end(adjcentValues));

                    int numAlpha = 0;
                    double sumAlpha = 0;
                    for (int k = 2; k < 7; k++)
                    {
                        sumAlpha = sumAlpha + adjcentValues[k];
                        numAlpha++;
                    }

                    double alphaDataVal = sumAlpha / numAlpha;   //estimations from 8 adjcent values
                    double difference = alphaDataVal - Value[row][col];

                    double maxV = std::ceil(interpDist / mCellSize / 2.0);

                    double distR = maxV * 2;

                    if(ValCount[row][col] == 1)
                    {
                        distR = 0;
                    }

                    else
                    {
                        for(int i = 0; i < 8; i++)
                        {
                            if(Dist2RealVal[row][col][i] != -1)
                            {
                                if(Dist2RealVal[row][col][i] < distR)
                                {
                                    distR = Dist2RealVal[row][col][i];
                                }
                            }
                        }
                    }

                    if (distR > maxV)
                    {
                        distR = maxV;
                    }

                    double newDiff = difference * distR/maxV;

                    Value[row][col] += newDiff;
                }
            }
        }
    }
}

void NPKGridding::iteration()
{
    int looping = 0;
    int currentLoop = 0;
    double previousMean = 0;
    double previousMedian = 0;

    int numStops = 0;

    while (looping == 0)
    {
        //        int time3 = QDateTime::currentMSecsSinceEpoch();
        mStatusMsg = "Iterating at " + QString::number(currentLoop) + " loop";
        emit statusChanged(mStatusMsg);
        {
            //****************************DerivV3
            if (currentLoop == 0) //this is the first loop, therefore need the data from gridedData
            {
                //                double j[100][100];
                //                std::copy(&Value[0][0], &Value[0][0]+50*50,&j[0][0]);
                //                //std::copy(&ValCount[0][0], &ValCount[0][0]+mImgWidth*mImgHeight,&ValCountDrvItr[0][0]);

                for(int col = 0; col < mImgWidth; col++)
                {
                    for(int row = 0; row < mImgHeight; row++)
                    {
                        ValueDrvItr[row][col] = Value[row][col];
                        ValCountDrvItr[row][col] = ValCount[row][col];
                    }
                }
            }
            else
            {
                //                std::copy(&LoopNewVals[0][0], &LoopNewVals[0][0]+mImgWidth*mImgHeight,&ValueDrvItr[0][0]);
                //                std::copy(&ValueDrvItr[0][0], &ValueDrvItr[0][0]+mImgWidth*mImgHeight,&ValPrevious[0][0]);
                for(int col = 0; col < mImgWidth; col++)
                {
                    for(int row = 0; row < mImgHeight; row++)
                    {
                        ValueDrvItr[row][col] = LoopNewVals[row][col];
                        ValPrevious[row][col] = ValueDrvItr[row][col];
                    }
                }
            }
        }

        {
            //Taylor expansions
            for(int col = 0; col < mImgWidth; col++)
            {
                for(int row = 0; row < mImgHeight; row++)
                {
                    if (ValCount[row][col] != -1 && Value[row][col] != -999999) //If the data is outside of the grid, then don't use it
                    {

                        double adjcentValues[9];
                        for(int i = 0; i < 8; i++)
                        {

                            if(ValueDrvItr[XYAdjacentCell[row][col][i][1]][XYAdjacentCell[row][col][i][0]] == -999999)
                            {
                                adjcentValues[i] = ValueDrvItr[row][col];
                                //                                XYAdjacentCell[row][col][i][0] = col;
                                //                                XYAdjacentCell[row][col][i][1] = row;
                            }
                            else
                            {
                                adjcentValues[i] = ValueDrvItr[XYAdjacentCell[row][col][i][1]][XYAdjacentCell[row][col][i][0]];
                            }
                        }
                        adjcentValues[8] = ValueDrvItr[row][col];


                        double fx = (adjcentValues[4] - adjcentValues[0]) / (2 * mCellSize);
                        double fy = (adjcentValues[2] - adjcentValues[6]) / (2 * mCellSize);
                        double fxx = (adjcentValues[4] - (2 * adjcentValues[8]) + adjcentValues[0]) / (mCellSize * mCellSize);
                        double fyy = (adjcentValues[2] - (2 * adjcentValues[8]) + adjcentValues[6]) / (mCellSize * mCellSize);
                        double fxy = (adjcentValues[3] - adjcentValues[1] - adjcentValues[5] + adjcentValues[7]) / (4 * mCellSize * mCellSize); //f3-f1-f9+f7

                        int numOfSuccess = 0;
                        //QVector<double> successValues;
                        double successValues[8]{};
                        for (int m = -1; m <= 1; m++)   //col
                        {
                            for (int n = -1; n <= 1; n++)   //row
                            {
                                if (m == 0 && n == 0) //center point; therefore ignore
                                {
                                }
                                else if (m + col < 0 || m + col >= mImgWidth || n + row < 0 || n + row >= mImgHeight) //make sure we don't look outside of range
                                {
                                }
                                else
                                {
                                    if(ValCount[row+n][col+m] != -1)
                                    {
                                        successValues[numOfSuccess] = (ValueDrvItr[row + n][col+m ] - (m * fx) - (n * fy) - 0.5 * ((fxx * std::pow(m, 2)) + (2 * m * n * fxy) + (fyy * std::pow(n, 2))));
                                        numOfSuccess++;
                                    }
                                }
                            }
                        }

                        if (numOfSuccess != 0)
                        {
                            if (numOfSuccess == 2) //then we need to just do an average
                            {
                                LoopNewVals[row][col] = (successValues[0] + successValues[1]) / 2; //mean
                                ValCountDrvItr[row][col] = 0;
                            }
                            else if (numOfSuccess == 1)  //just directly assign
                            {
                                LoopNewVals[row][col] = successValues[0];
                                ValCountDrvItr[row][col] = 0;
                            }
                            else //alpha trimmed mean
                            {
                                double tempQuart = numOfSuccess/4.0;
                                int quarterNum = std::ceil(tempQuart);
                                int maxQuarter = numOfSuccess - quarterNum;
                                int tempNum = 0;
                                double tempSum = 0;
                                std::sort(std::begin(successValues), std::begin(successValues)+numOfSuccess); //Sort according to the value

                                for (int p = quarterNum; p < maxQuarter; p++)
                                {
                                    tempSum += successValues[p];
                                    tempNum++;
                                }

                                LoopNewVals[row][col] = tempSum / tempNum; //alpha trimmed mean
                                ValCountDrvItr[row][col] = 0;

                                //ValueDrvItr[row][col] = LoopNewVals[row][col];
                            }
                        }

                    }
                }
            }

            for(int col = 0; col < mImgWidth; col++)
            {
                for(int row = 0; row < mImgHeight; row++)
                {
                    ValueDrvItr[row][col] = LoopNewVals[row][col];
                }
            }

        }

        {
            //***********************************

            //*************TRENDING DETERMINATION
            std::vector<double> flatEigenVal;
            flatEigenVal.resize(mImgWidth*mImgHeight);
            int EngValcount = 0;

            for(int col = 0; col < mImgWidth; col++)
            {
                for(int row = 0; row < mImgHeight; row++)
                {
                    if (ValCountDrvItr[row][col] != -1)
                    {
                        //first smooth the entire grid to remove noise issues
                        double adjcentValues[9];
                        double sumAlpha = 0;

                        for(int i = 0; i < 8; i++)
                        {
                            adjcentValues[i] = ValueDrvItr[XYAdjacentCell[row][col][i][1]][XYAdjacentCell[row][col][i][0]];
                            if (adjcentValues[i] == -999999)
                            {
                                adjcentValues[i] = ValueDrvItr[row][col];
                            }

                            sumAlpha += adjcentValues[i];
                        }
                        adjcentValues[8] = ValueDrvItr[row][col];
                        sumAlpha += adjcentValues[8];

                        SmoothGrid[row][col] = sumAlpha / 9;
                    }
                }
            }


            for(int col = 0; col < mImgWidth; col++)
            {
                for(int row = 0; row < mImgHeight; row++)
                {
                    if(ValCountDrvItr[row][col] != -1)
                    {
                        double Ix;
                        double Iy;

                        if (col == 0)
                        {
                            Ix = SmoothGrid[row][col+1] - SmoothGrid[row][col];
                        }
                        else if (col == mImgWidth - 1)
                        {
                            Ix = SmoothGrid[row][col] - SmoothGrid[row][col-1];
                        }
                        else
                        {
                            Ix = 0.5 * (SmoothGrid[row][col+1] - SmoothGrid[row][col-1]);
                        }

                        if (row == 0)
                        {
                            Iy = SmoothGrid[row+1][col] - SmoothGrid[row][col];
                        }
                        else if (row == mImgHeight - 1)
                        {
                            Iy = SmoothGrid[row][col] - SmoothGrid[row-1][col];
                        }
                        else
                        {
                            Iy = 0.5 * (SmoothGrid[row+1][col] - SmoothGrid[row-1][col]);
                        }

                        double S[2][2];
                        S[0][0] = Ix * Ix;
                        S[0][1] = Ix * Iy;
                        S[1][0] = Iy * Ix;
                        S[1][1] = Iy * Iy;

                        EignVals[row][col] =
                                0.5 * (S[0][1] + S[1][1] + std::sqrt((std::pow(S[0][0] - S[1][1], 2)) + (4 * std::pow(S[0][1], 2))));

                        flatEigenVal[EngValcount] = EignVals[row][col];
                        EngValcount++;

                        if(S[0][1] > 0)
                        {
                            if (S[0][0] == S[0][1])
                            {
                                //theta = 1/4 pi
                                double theta = 0.25 * M_PI;
                                EignV12[row][col] = cos(theta);
                                EignV11[row][col] = -1 * sin(theta);
                            }
                            else
                            {
                                double theta = atan(2 * S[0][1] / (S[0][0] - S[1][1])) / 2.0;
                                if(S[0][0] < S[1][1])
                                {
                                    EignV11[row][col] = -1 * abs(cos(theta));
                                    EignV12[row][col] = abs(sin(theta));
                                }
                                else
                                {
                                    EignV11[row][col] = abs(sin(theta));
                                    EignV12[row][col] = -1 * abs(cos(theta));
                                }
                            }
                        }

                        else if(S[0][1] == 0)
                        {
                            if(S[0][0] > S[1][1])
                            {
                                EignV11[row][col] = 0;
                                EignV12[row][col] = 1;
                            }
                            else
                            {
                                EignV11[row][col] = 1;
                                EignV12[row][col] = 0;
                            }
                        }
                        else
                        {
                            if(S[0][0] == S[1][1])
                            {
                                double theta = 0.75 * M_PI;
                                EignV12[row][col] = abs(cos(theta));
                                EignV11[row][col] = abs(sin(theta));
                            }
                            else
                            {
                                double theta = (atan(2 * S[0][1] / (S[0][0] - S[1][1])) / 2.0);
                                if(S[0][0] < S[1][1])
                                {
                                    EignV11[row][col] = -1 * abs(cos(theta));
                                    EignV12[row][col] = -1 * abs(sin(theta));
                                }
                                else
                                {
                                    EignV11[row][col] = -1 * abs(sin(theta));
                                    EignV12[row][col] = -1 * abs(cos(theta));
                                }
                            }
                        }
                    }
                }
            }


            //QVector<double> flatEigenVal;

            //            for(int col = 0; col < mImgWidth; col++)
            //            {
            //                for(int row = 0; row < mImgHeight; row++)
            //                {
            //                    if (ValCountDrvItr[row][col] != -1)
            //                    {
            //                        flatEigenVal[EngValcount] = EignVals[row][col];
            //                        EngValcount++;
            //                        //flatEigenVal.append(EignVals[row][col]);
            //                    }
            //                }
            //            }

            std::sort(flatEigenVal.begin(), flatEigenVal.end());
            if(trendM == 0)
            {
                eigenAvg = flatEigenVal[EngValcount-1]; //trendM larger = less change during normalization
            }
            if(trendM == 100)
            {
                eigenAvg = flatEigenVal[0];
            }
            else
            {
                eigenAvg = flatEigenVal[EngValcount * (100-trendM) / 100]; //therefore to make this work we take 100 - Globals.trendM
            }

            //delete []flatEigenVal;
        }

        for(int col = 0; col < mImgWidth; col++)
        {
            for(int row = 0; row < mImgHeight; row++)
            {
                if (ValCount[row][col] == -1)
                {
                    trendLOG[row][col] = 0;
                }
                else
                {
                    if (EignVals[row][col] >= eigenAvg)
                    {
                        trendLOG[row][col] = 1;
                    }
                    else
                    {
                        trendLOG[row][col] = EignVals[row][col] / eigenAvg;
                    }
                }
            }
        }


        {
            //***********************************

            //*************REAL DATA SCALING

            for(int col = 0; col < mImgWidth; col++)
            {
                for(int row = 0; row < mImgHeight; row++)
                {
                    if(ValCount[row][col] == 1)
                    {
                        multiplierCells[row][col] =
                                abs(Value[row][col]/ValueDrvItr[row][col]);

                        //iMultCells[row][col] = multiplierCells[row][col];

                    }
                }
            }

            for(int col = 0; col < mImgWidth; col++)
            {
                for(int row = 0; row < mImgHeight; row++)
                {

                    if(ValCount[row][col] == 1)
                    {
                        LoopNewVals[row][col] = Value[row][col];
                        //ValCountReRplace[row][col] = 1;
                    }


                    else if(ValCount[row][col] == -1)
                    {
                        LoopNewVals[row][col] = -999999;
                        //ValCountReRplace[row][col] = -1;
                        iMultCells[row][col] = -999999;
                    }

                    else
                    {
                        bool failed = false;
                        bool multFind = false;
                        double iMult = 0;
                        int multI = 0;
                        int multJ = 0;
                        int eigenDevi = 0;
                        double eigenSide = 1;
                        double tenD = M_PI * angleSearch / 180;

                        while (multFind == false) //have we found any multiplier
                        {
                            double origXD = 0;
                            double origYD = 0;

                            //We need to determine the direction to search. First we can just use the eigenvector result, however if that fails we must begin searching elsewhere.
                            if (eigenDevi > 0)
                            {
                                //instead let's find the direction of the eigenvector, and move the angle by theta degrees
                                double origAng = atan(EignV12[row][col] / EignV11[row][col]); //measuring from the x-axis, so range is -90 -> +90
                                double newAng = 0;

                                if (abs(EignV11[row][col]) > abs(EignV12[row][col])) //if the initial x direction is stronger, then we will first search more towards the x-axis. Also means that if it is 0 degrees, we just assume one direction.
                                {
                                    if (eigenDevi % 2 == 0) //then we are even, and therefore have already tried the x direction first
                                    {
                                        if (EignV11[row][col] > 0) //if it's positive, then we need to add the angle to move towards the y-axis
                                        {
                                            newAng = origAng + (eigenSide * tenD);
                                        }
                                        else
                                        {
                                            newAng = origAng - (eigenSide * tenD);
                                        }
                                        eigenSide++;
                                    }
                                    else //it is odd, and it is time to check the x direction
                                    {
                                        if (EignV11[row][col] > 0) //if it's positive, then we need to subtract the angle to move towards the x-axis
                                        {
                                            newAng = origAng - (eigenSide * tenD);
                                        }
                                        else
                                        {
                                            newAng = origAng + (eigenSide * tenD);
                                        }
                                    }
                                }
                                else if (abs(EignV11[row][col]) < abs(EignV12[row][col])) //if the initial y direction is stronger, then we will first search more towards the y-axis. Also means that if it is -90 or 90 degrees, we just assume one direction.
                                {
                                    if (eigenDevi % 2 == 0) //then we are even, and therefore have already tried the y direction first
                                    {
                                        if (EignV12[row][col] > 0) //if it's positive, then we need to subtract the angle to move towards the x-axis
                                        {
                                            newAng = origAng - (eigenSide * tenD);
                                        }
                                        else
                                        {
                                            newAng = origAng + (eigenSide * tenD);
                                        }
                                        eigenSide++;
                                    }
                                    else //it is odd, and it is time to check the y direction
                                    {
                                        if (EignV12[row][col] > 0) //if it's positive, then we need to add the angle to move towards the y-axis
                                        {
                                            newAng = origAng + (eigenSide * tenD);
                                        }
                                        else
                                        {
                                            newAng = origAng - (eigenSide * tenD);
                                        }
                                    }
                                }
                                else //then they are equal, and we started at 45 degrees. Arbitrarily I will say we first search more towards the x-axis. May be worthwhile to change at a later time.
                                {
                                    if (eigenDevi % 2 == 0) //then we are even, and therefore have already tried the x direction first
                                    {
                                        if (EignV11[row][col] > 0) //if it's positive, then we need to add the angle to move towards the y-axis
                                        {
                                            newAng = origAng + (eigenSide * tenD);
                                        }
                                        else
                                        {
                                            newAng = origAng - (eigenSide * tenD);
                                        }
                                        eigenSide++;
                                    }
                                    else //it is odd, and it is time to check the x direction
                                    {
                                        if (EignV11[row][col] > 0) //if it's positive, then we need to subtract the angle to move towards the x-axis
                                        {
                                            newAng = origAng - (eigenSide * tenD);
                                        }
                                        else
                                        {
                                            newAng = origAng + (eigenSide * tenD);
                                        }
                                    }
                                }

                                //now that we have a new angle, we need to give our new x and y distances
                                if (abs(newAng) < (M_PI / 4) || abs(newAng) > (3 * M_PI / 4)) //then we are a stronger x direction
                                {
                                    //normalize such that x is 1, then multiply by 1.1
                                    origXD = 1.1;
                                    origYD = abs(tan(newAng)) * 1.1;
                                    multI = 1;
                                }
                                else if (abs(newAng) == (M_PI / 4) || abs(newAng) == (3 * M_PI / 4))
                                {
                                    origXD = 1.1;
                                    origYD = 1.1;
                                    multI = 1;
                                }
                                else
                                {
                                    //normalize such that y is 1, then multiply by 1.1
                                    origXD = abs((1 / tan(newAng))) * 1.1;
                                    origYD = 1.1;
                                    multI = 1;
                                }
                                if (newAng < 0)
                                {
                                    multJ = -1;
                                }
                                else
                                {
                                    multJ = 1;
                                }
                            }

                            else
                            {
                                //normalize such that the large of the two eigenvectors is 1, then multiply by 1.1
                                if (abs(EignV11[row][col]) > abs(EignV12[row][col]))
                                {
                                    origXD = 1.1; //begin at 1.1 cells
                                    origYD = abs(EignV12[row][col]) / abs(EignV11[row][col]) * 1.1;
                                }
                                else
                                {
                                    origXD = abs(EignV11[row][col]) / abs(EignV12[row][col]) * 1.1;
                                    origYD = 1.1; //begin at 1.1 cells
                                }
                                multI = signOfVal(EignV11[row][col]);
                                multJ = signOfVal(EignV12[row][col]);
                            }

                            //Now with the x and y found, we need to look in both positive and negative directions, and find the locations of the first real data cells we run into.
                            double searchStep = 1; //start at the point where it would be in the next cell if looking directly along axes
                            bool searching = true;
                            bool searchPos = false;
                            bool posGood = false;
                            bool searchNeg = false;
                            bool negGood = false;
                            int iT1 = 0;
                            int jT1 = 0;
                            int iT1_2 = 0;
                            int jT1_2 = 0;
                            int iT1_3 = 0;
                            int jT1_3 = 0;
                            int iT2 = 0;
                            int jT2 = 0;
                            int iT2_2 = 0;
                            int jT2_2 = 0;
                            int iT2_3 = 0;
                            int jT2_3 = 0;
                            bool foundNearbyCellPos = false;
                            bool foundNearbyCellPos2 = false;
                            bool foundNearbyCellNeg = false;
                            bool foundNearbyCellNeg2 = false;

                            while (searching == true) //begin searching with current direction
                            {
                                double currentStep = 1 + (searchStepSize * searchStep);

                                double tempXD = origXD * currentStep;
                                double tempYD = origYD * currentStep;

                                //now we need to know which direction we are looking, and add the current distance to i and j (current location)
                                if (searchPos == false) //we have not found a real data cell in the positive direction yet
                                {
                                    int tempI = (multI * floor(tempXD));
                                    int tempJ = (multJ * floor(tempYD));

                                    if (col + tempI >= mImgWidth || col + tempI < 0) //hit an edge in x-direction
                                    {
                                        searchPos = true;
                                        posGood = false;
                                    }

                                    else if (row + tempJ >= mImgHeight || row + tempJ < 0) //hit an edge y-direction
                                    {
                                        searchPos = true;
                                        posGood = false;
                                    }

                                    else if (ValCount[row + tempJ][col + tempI] == -1) //outside of interpolation distance
                                    {
                                        searchPos = true;
                                        posGood = false;
                                    }

                                    else if (ValCount[row + tempJ][col + tempI] == 1)
                                    {
                                        //find out which side we came in from
                                        double XPM = (multI * tempXD) - (tempI + 0.5);
                                        double YPM = (multJ * tempYD) - (tempJ + 0.5);

                                        for (int k = 0; k < 8; k++)
                                        {
                                            if (ValCount[XYAdjacentCell[row + tempJ][col + tempI][k][1]][XYAdjacentCell[row + tempJ][col + tempI][k][0]] == 1) //this cell is real
                                            {
                                                if (XYAdjacentCell[row + tempJ][col + tempI][k][0] == (col + tempI) && XYAdjacentCell[row + tempJ][col + tempI][k][1] == (row + tempJ)) //and not a repeat value
                                                {
                                                }
                                                else
                                                {
                                                    int wayX = 0;
                                                    int wayY = 0;
                                                    //basically just determine which direction this is. Not the best way to do it, but should work.
                                                    if (k == 7 || k == 0 || k == 1)
                                                    {
                                                        wayX = -1;
                                                    }
                                                    else if (k == 3 || k == 4 || k == 5)
                                                    {
                                                        wayX = 1;
                                                    }
                                                    if (k == 1 || k == 2 || k == 3)
                                                    {
                                                        wayY = 1;
                                                    }
                                                    else if (k == 5 || k == 6 || k == 7)
                                                    {
                                                        wayY = -1;
                                                    }
                                                    if (signOfVal(wayX) == signOfVal(XPM) || wayX == 0)
                                                    {
                                                        if (signOfVal(wayY) == signOfVal(YPM) || wayY == 0)
                                                        {
                                                            //in the rare case where two are found, we need to average over all
                                                            if (foundNearbyCellPos == true)
                                                            {
                                                                iT1_3 = tempI + wayX;
                                                                jT1_3 = tempJ + wayY;
                                                                foundNearbyCellPos2 = true;
                                                            }
                                                            else
                                                            {
                                                                iT1_2 = tempI + wayX;
                                                                jT1_2 = tempJ + wayY;
                                                                foundNearbyCellPos = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        iT1 = tempI;
                                        jT1 = tempJ;
                                        searchPos = true;
                                        posGood = true;
                                    }

                                }

                                if (searchNeg == false) //we have not found a real data cell in the negative direction yet
                                {
                                    int tempI = -multI * floor(tempXD);
                                    int tempJ = -multJ * floor(tempYD);
                                    if (col + tempI >= mImgWidth || col + tempI < 0) //hit an edge in x-direction
                                    {
                                        searchNeg = true;
                                        negGood = false;
                                    }
                                    else if (row + tempJ >= mImgHeight || row + tempJ < 0) //hit an edge y-direction
                                    {
                                        searchNeg = true;
                                        negGood = false;
                                    }
                                    else if (ValCount[row + tempJ][col + tempI] == -1) //outside of interpolation distance
                                    {
                                        searchNeg = true;
                                        negGood = false;
                                    }
                                    else if (ValCount[row + tempJ][col + tempI] == 1) //if true, then we have found a real data cell
                                    {
                                        //find out which side we came in from
                                        double XPM = (-multI * tempXD) - (tempI + 0.5);
                                        double YPM = (-multJ * tempYD) - (tempJ + 0.5);

                                        for (int k = 0; k < 8; k++)
                                        {
                                            if (ValCount[XYAdjacentCell[row + tempJ][col + tempI][k][1]][XYAdjacentCell[row + tempJ][col + tempI][k][0]] == 1) //this cell is real
                                            {
                                                if (XYAdjacentCell[row + tempJ][col + tempI][k][0] == (col + tempI) && XYAdjacentCell[row + tempJ][col + tempI][k][1] == (row + tempJ)) //and not a repeat value
                                                {
                                                }
                                                else
                                                {
                                                    int wayX = 0;
                                                    int wayY = 0;
                                                    //basically just determine which direction this is. Not the best way to do it, but should work.
                                                    if (k == 7 || k == 0 || k == 1)
                                                    {
                                                        wayX = -1;
                                                    }
                                                    else if (k == 3 || k == 4 || k == 5)
                                                    {
                                                        wayX = 1;
                                                    }
                                                    if (k == 1 || k == 2 || k == 3)
                                                    {
                                                        wayY = 1;
                                                    }
                                                    else if (k == 5 || k == 6 || k == 7)
                                                    {
                                                        wayY = -1;
                                                    }
                                                    if (signOfVal(wayX) == signOfVal(XPM) || wayX == 0)
                                                    {
                                                        if (signOfVal(wayY) == signOfVal(YPM) || wayY == 0)
                                                        {
                                                            //in the rare case where two are found, we need to average over all
                                                            if (foundNearbyCellNeg == true)
                                                            {
                                                                iT2_3 = tempI + wayX;
                                                                jT2_3 = tempJ + wayY;
                                                                foundNearbyCellNeg2 = true;
                                                            }
                                                            else
                                                            {
                                                                iT2_2 = tempI + wayX;
                                                                jT2_2 = tempJ + wayY;
                                                                foundNearbyCellNeg = true;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        iT2 = tempI;
                                        jT2 = tempJ;
                                        searchNeg = true;
                                        negGood = true;
                                    }
                                }

                                if (searchPos == false || searchNeg == false) //have not yet found a real data cell in one or both directions yet
                                {
                                    searchStep++;
                                    if (searchStepSize * searchStep >= (interpDist / mCellSize) - 1) //then we have not found any real data cell within the user-defined interpolation distance
                                    {
                                        searching = false; //exit the loop
                                    }
                                }
                                else //both are true, and therefore we are done searching
                                {
                                    searching = false;
                                }
                            }


                            if (posGood == true && negGood == true) //then both directions succeeeded
                            {
                                double multiplierCPos = 0;
                                double multiplierCNeg = 0;
                                if (foundNearbyCellPos == true)
                                {// j + jT1,
                                    multiplierCPos = (multiplierCells[row + jT1][col + iT1] + multiplierCells[row + jT1_2][col + iT1_2]) / 2;
                                }
                                else if (foundNearbyCellPos2 == true)
                                {
                                    multiplierCPos = (multiplierCells[row + jT1][col + iT1] + multiplierCells[row + jT1_2][col + iT1_2] + multiplierCells[row + jT1_3][col + iT1_3]) / 3;
                                }
                                else
                                {
                                    multiplierCPos = multiplierCells[row + jT1][col + iT1];
                                }
                                if (foundNearbyCellNeg == true)
                                {
                                    multiplierCNeg = (multiplierCells[row + jT2][col + iT2] + multiplierCells[row + jT2_2][col + iT2_2]) / 2;
                                }
                                else if (foundNearbyCellNeg2 == true)
                                {
                                    multiplierCNeg = (multiplierCells[row + jT2][col + iT2] + multiplierCells[row + jT2_2][col + iT2_2] + multiplierCells[row + jT2_3][col + iT2_3]) / 3;
                                }
                                else
                                {
                                    multiplierCNeg = multiplierCells[row + jT2][col + iT2];
                                }
                                double distance1 = sqrt(pow(iT1, 2) + pow(jT1, 2));
                                double distance2 = sqrt(pow(iT2, 2) + pow(jT2, 2));
                                double totDist = distance1 + distance2;
                                double iMultT1 = distance2 * (multiplierCPos) / totDist;
                                double iMultT2 = distance1 * (multiplierCNeg) / totDist;
                                iMult = iMultT1 + iMultT2;
                                multFind = true;
                            }

                            else if (posGood == true)
                            {
                                if (foundNearbyCellPos == true)
                                {
                                    iMult = (multiplierCells[row + jT1][col + iT1] + multiplierCells[row + jT1_2][col + iT1_2]) / 2;
                                }
                                else
                                {
                                    iMult = multiplierCells[row + jT1][col + iT1];
                                }
                                multFind = true;
                            }
                            else if (negGood == true)
                            {
                                if (foundNearbyCellNeg == true)
                                {
                                    iMult = (multiplierCells[row + jT2][col + iT2] + multiplierCells[row + jT2_2][col + iT2_2]) / 2;
                                }
                                else
                                {
                                    iMult = multiplierCells[row + jT2][col + iT2];
                                }
                                multFind = true;
                            }

                            else //both directions failed. We must now start over with a new direction.
                            {
                                eigenDevi++; //this direction did not work, so search new angle
                                if ((eigenSide + 1) * angleSearch >= 180) //then we've checked every direction
                                {
                                    failed = true;
                                    multFind = true;
                                }
                            }
                        }

                        if (failed == true)
                        {
                            //ValReRplace[row][col] = -999999; //failed data
                            //ValCountReRplace[row][col] = -1; //outside of grid
                            iMultCells[row][col] = -999999;
                        }
                        else
                        {
                            iMultCells[row][col] = iMult;
                        }
                    }
                }
            }
        }


        {
            //***********************************

            //*************Smooth multiplier grid & Apply the normalization

            //if no smoothing is wanted, then just pass along the multiplier grid
            {
                //don't do anything

            }
            //            if (multiSmooth != false) //otherwise we will apply a 3x3 smoother to the grid, with a sliding effect
            //            {
            //                for (int col = 0; col < mImgWidth; col++)
            //                {
            //                    for (int row = 0; row < mImgHeight; row++)
            //                    {
            //                        if (ValCount[row][col] == -1)
            //                        {
            //                            iMultCells[row][col] = 0;
            //                        }
            //                        else
            //                        {
            //                            QVector<double> adjacent;
            //                            double sum;
            //                            for(int i= 0; i < 8; i++)
            //                            {
            //                                if(Value[row][col] == -999999)
            //                                {
            //                                    adjacent.append(iMultCells[row][col]);
            //                                }
            //                                adjacent.append(iMultCells[XYAdjacentCell[row][col][i][1]][XYAdjacentCell[row][col][i][0]]);
            //                            }


            //                            //now to enact the sliding scale, add 100 minus the smoother amount number of values of the original mutlitplier
            //                            //e.g. 1 will add 100, 100 will add only 1
            //                            for (int k = 0; k < 101 - multiSmooth; k++)
            //                            {
            //                                adjacent.append(iMultCells[row][col]);
            //                            }

            //                            for(int i = 0; i < adjacent.length(); i++)
            //                            {
            //                                sum += adjacent[i];
            //                            }

            //                            iMultCells[row][col] = sum/adjacent.length();
            //                        }
            //                        else
            //                        {
            //                            iMultCells[row][col] = 0;
            //                        }

            //                    }
            //                }
            //            }


            for (int col = 0; col < mImgWidth; col++)
            {
                for (int row = 0; row < mImgHeight; row++)
                {
                    if(ValCount[row][col] != 1 && ValCount[row][col] != -1)
                    {
                        double tempMR = ValueDrvItr[row][col] + (((ValueDrvItr[row][col] * iMultCells[row][col]) - ValueDrvItr[row][col]) * trendLOG[row][col]);

                        if (tempMR > mValueMax) //do not let the scaled data become greater than the max value found in the raw data
                        {
                            tempMR = mValueMax;
                        }
                        else if (tempMR < mValueMin) //do not let the scaled data become less than the min value (+ DC offset) found in the raw data
                        {
                            tempMR = mValueMin;
                        }

                        LoopNewVals[row][col] = tempMR;

                        //                        ValReRplace[row][col] = tempMR; //replace with scaled data
                        //                        ValueDrvItr[row][col] = tempMR;
                        //                        ValCountReRplace[row][col] = 0; //not real
                    }
                }
            }
        }
        //        if(currentLoop == 17)
        //        debugFunc();
        {

            //***********************************

            //Stopping criteria check************

            if (autoStop == true)
            {
                if (currentLoop != 0)
                {
                    //std::vector<double> theDiffs;
                    for (int row = 0; row < mImgHeight; row++)
                    {
                        for (int col = 0; col < mImgWidth; col++)
                        {
                            if (ValCount[row][col] != -1)
                            {
                                double tempVal = abs(LoopNewVals[row][col] - ValPrevious[row][col]);
                                diffs[col+row*mImgWidth] = tempVal;

                                //theDiffs.push_back(tempVal);
                            }
                        }
                    }

                    std::sort(diffs.begin(), diffs.end());
                    //std::sort(theDiffs.begin(), theDiffs.end());
                    double sum = 0;
                    for(int i = 0; i < mImgWidth*mImgHeight; i++)
                    {
                        if(diffs[0]!=0)
                        {sum += diffs[i];}
                    }

                    double diffAvg = sum/(mImgWidth*mImgHeight);
                    double diffMedi = diffs[mImgWidth*mImgHeight / 2];

                    //meandiffs[currentLoop] = diffAvg;

                    if (currentLoop != 1)
                    {
                        double meanDiff = diffAvg - previousMean;
                        double medianDiff = diffMedi - previousMedian;

                        if (meanDiff >= 0 && medianDiff >= 0)
                        {
                            numStops++;

                        }
                        else
                        {
                            previousMean = diffAvg;
                            previousMedian = diffMedi;
                        }
                    }
                    else
                    {
                        previousMean = diffAvg;
                        previousMedian = diffMedi;
                    }

                }
            }
            //***********************************
            currentLoop++;

            if (autoStop == true && numStops >= 3)
            {
                looping = 1;
            }
            if (currentLoop == maxLoop) //Hard stop of max iterations
            {
                looping = 1;
            }
        }

        //        int time4 = QDateTime::currentMSecsSinceEpoch();
        //        qDebug() << "One iteration took " << time4-time3;
    }
}

void NPKGridding::finaliseData()
{
    for (int row = 0; row < mImgHeight; row++) {
        for (int col = 0; col < mImgWidth; col++) {
            if (ValCount[row][col] != -1) {
                if (ValCount[row][col] == 0)
                    Value[row][col] = LoopNewVals[row][col];
                Value[row][col] -= dcOffset;
            }
        }
    }

    mValueMin -= dcOffset;
    mValueMax -= dcOffset;
}

void NPKGridding::paint()
{
    for (int col = 0; col < mImgWidth; col++) {
        for (int row = 0; row < mImgHeight; row++) {
            if (ValCount[row][col] != -1) {
                mNumberOfValid++;
                double hueAddIndex = ((qAbs(Value[row][col]-mValueMin))/(mRmg1Range)*4.0);
                double hueValue = 240.0 - 60.0*hueAddIndex;

                mImg.setPixelColor(col, row, QColor::fromHsv(hueValue,255,255));
            }
        }
    }

    mPixMap = QPixmap::fromImage(mImg);
}

void NPKGridding::configPixMap()
{
    //    mImgWidth = (mXmax-mXmin)/mCellSize;
    //    mImgHeight = (mYmax-mYmin)/mCellSize;
    mRmg1Range = mValueMax - mValueMin;

    mImg = QImage(mImgWidth+widthMargin, mImgHeight+heightMargin, QImage::Format_RGB32);
    mImg.fill(Qt::white);
}

void NPKGridding::allocateBuff()
{
    Value.resize(mImgHeight,std::vector<double>(mImgWidth));
    ValueDrvItr.resize(mImgHeight,std::vector<double>(mImgWidth));
    ValPrevious.resize(mImgHeight,std::vector<double>(mImgWidth));
    SmoothGrid.resize(mImgHeight,std::vector<double>(mImgWidth));
    EignVals.resize(mImgHeight,std::vector<double>(mImgWidth));
    EignV11.resize(mImgHeight,std::vector<double>(mImgWidth));
    EignV12.resize(mImgHeight,std::vector<double>(mImgWidth));
    LoopNewVals.resize(mImgHeight,std::vector<double>(mImgWidth));
    trendLOG.resize(mImgHeight,std::vector<double>(mImgWidth));
    multiplierCells.resize(mImgHeight,std::vector<double>(mImgWidth));
    iMultCells.resize(mImgHeight,std::vector<double>(mImgWidth));

    ValCount.resize(mImgHeight,std::vector<int>(mImgWidth));
    ValCountDrvItr.resize(mImgHeight,std::vector<int>(mImgWidth));
    diffs.resize(mImgWidth*mImgHeight);

    Dist2RealVal.resize(mImgHeight, std::vector<std::vector<int>>(mImgWidth, std::vector<int>(8)));
    XYAdjacentCell.resize(mImgHeight, std::vector<std::vector<std::vector<int>>>(mImgWidth, std::vector<std::vector<int>>(8, std::vector<int>(2))));

    //    trendLOG = new double*[mImgHeight]();

    //    //XDrvItr = new double*[mImgHeight];
    //    //XRReplace = new double*[mImgHeight]();
    //    //YDrvItr = new double*[mImgHeight];
    //    //YRReplace = new double*[mImgHeight]();

    //    ValueDrvItr = new double*[mImgHeight]();
    //    //ValReRplace = new double*[mImgHeight]();
    //    ValPrevious = new double*[mImgHeight];

    //    SmoothGrid = new double*[mImgHeight]();
    //    EignVals = new double*[mImgHeight]();
    //    EignV11 = new double*[mImgHeight]();
    //    EignV12 = new double*[mImgHeight]();
    //    multiplierCells = new double*[mImgHeight]();
    //    iMultCells = new double*[mImgHeight]();

    //    LoopNewVals = new double*[mImgHeight]();
    //    ValCountDrvItr = new int*[mImgHeight]();
    //    diffs = new double[mImgHeight*mImgWidth]();
    //    //ValCountReRplace = new int*[mImgHeight];

    //    Dist2RealVal = new int**[mImgHeight]();
    //    XYAdjacentCell = new int***[mImgHeight]();


    //    for(int row = 0; row < mImgHeight; row++)
    //    {
    //        X[row] = new double[mImgWidth]();
    //        Y[row] = new double[mImgWidth]();

    //        Value[row] = new double[mImgWidth]();
    //        ValCount[row] = new int[mImgWidth]();

    //        //XDrvItr[row] = new double[mImgWidth];
    //        //YDrvItr[row] = new double[mImgWidth];

    //        //XRReplace[row] = new double[mImgWidth]();
    //        //YRReplace[row] = new double[mImgWidth]();

    //        trendLOG[row] = new double[mImgWidth]();
    //        ValueDrvItr[row] = new double[mImgWidth]();
    //        //ValReRplace[row] = new double[mImgWidth]();
    //        ValPrevious[row] = new double[mImgWidth];
    //        SmoothGrid[row] = new double[mImgWidth]();
    //        EignVals[row] = new double[mImgWidth]();
    //        EignV11[row] = new double[mImgWidth]();
    //        EignV12[row] = new double[mImgWidth]();
    //        LoopNewVals[row] = new double[mImgWidth];
    //        ValCountDrvItr[row] = new int[mImgWidth];
    //        //ValCountReRplace[row] = new int[mImgWidth];

    //        multiplierCells[row] = new double[mImgWidth]();
    //        iMultCells[row] = new double[mImgWidth];

    //        Dist2RealVal[row] = new int*[mImgWidth]();
    //        XYAdjacentCell[row] = new int**[mImgWidth]();

    //        for(int col = 0; col < mImgWidth; col++)
    //        {
    //            Dist2RealVal[row][col] = new int[8]();

    //            XYAdjacentCell[row][col] = new int*[8]();
    //            for(int k = 0; k < 8; k++)
    //            {
    //                XYAdjacentCell[row][col][k] = new int[2]();
    //            }
    //        }
    //    }
}

void NPKGridding::configGridParms()
{

}

int NPKGridding::signOfVal(double input)
{
    if(input < 0)
        return -1;
    if(input > 0)
        return 1;

    else
        return 0;
}

void NPKGridding::startGridding()
{
    mStatusMsg = "Loading and preparing data...";
    emit statusChanged(mStatusMsg);
    loadData();

    int time1 = QDateTime::currentMSecsSinceEpoch();

    prepareData();
    mStatusMsg = "Loading data done, starting iterations...";
    emit statusChanged(mStatusMsg);

    configPixMap();

    iteration();

    finaliseData();

    int time2 = QDateTime::currentMSecsSinceEpoch();

    mStatusMsg = "Iteration done, painting pit map...";
    emit statusChanged(mStatusMsg);
    paint();

    if (mImg.save("../Rmg1.jpg", "JPEG")) {
        mStatusMsg = "Bitmap saved";
        emit statusChanged(mStatusMsg);
    }

    mStatusMsg = "Naperstek gridding took " + QString::number(time2-time1) + " ms";
    emit statusChanged(mStatusMsg);

    emit gridProcessDone(this);
}
