#ifndef NPKGRIDDING_H
#define NPKGRIDDING_H
#include <QObject>
#include <QWidget>
#include <QFile>
#include <QDebug>
#include <float.h>
#include <algorithm>
#include <QImage>
#include <QMap>
#include <QRgb>
#include <math.h>
#include <QtAlgorithms>
#include <QDateTime>
#include <QString>
#include "GriddingBase.h"
#include "qsqlquery.h"
#include "DataBase.h"
class NPKGridding: public GriddingBase
{
    Q_OBJECT
public:
    NPKGridding(std::vector<double> inputPars = {}, QString filePath = "");

    ~NPKGridding();

    //QString statusMsg = "";
    void setDataBase(QString tableName, QString gridValue, QString gridX, QString gridY);

    std::vector<std::vector<double>>& getGridData() override;
    std::vector<std::vector<int>>& getGridDataFlag() override;

private:
    QFile mFile;


    std::vector<std::vector<double>> mDataSet;   //X,Y,VAL

//    double mXmin;
//    double mXmax;
//    double mYmin;
//    double mYmax;
//    double mValueMax;
//    double mValueMin;
    double mRmg1Range;
    void loadData();
    void prepareData();


    void iteration();

    void finaliseData();

    void paint();

    void configPixMap();

    void allocateBuff();

    void configGridParms();

    int widthIncrease = 0;
    int widthOffset = 3;
    int widthMargin = (widthOffset+1)*2;
    int heightMargin = (widthOffset+1)*2;


    int mLineCount = 0;

//    int mImgWidth;
//    int mImgHeight;



    int mImgDecimalCounts = 0;   // -1,0,1,2,3...
    // 10,1,0.1,0.01...

    //QImage mPixImg;

    //bool sortByRowCol()
    QColor mTestColor;

    void getRGBParms(float minimum, float maximum, float value,
                     int *r, int *g, int *b)
    {
        float ratio = 2.0 * qAbs(value-minimum) / qAbs(maximum - minimum);

        //float halfmax = qAbs((maximum - minimum)) / 2.0;

        *b = int(std::max(0.0, 255.0*(1.0 - ratio)));
        *r = int(std::max(0.0, 255.0*(ratio - 1.0)));
        *g = 255 - *b - *r;
    }
    struct cellData
    {


    };


    std::vector<std::vector<double>>Value;
    std::vector<std::vector<double>>ValueDrvItr;
    //double **ValReRplace;
    std::vector<std::vector<double>>ValPrevious;

    std::vector<std::vector<double>>SmoothGrid;
    std::vector<std::vector<double>>EignVals;
    std::vector<std::vector<double>>EignV11;
    std::vector<std::vector<double>>EignV12;
    std::vector<std::vector<double>>LoopNewVals;
    std::vector<std::vector<double>>trendLOG;

    std::vector<std::vector<double>>multiplierCells;
    std::vector<std::vector<double>>iMultCells;

    std::vector<std::vector<int>>ValCount;
    std::vector<std::vector<int>>ValCountDrvItr;
    std::vector<double>diffs;
    //int **ValCountReRplace;

    std::vector<std::vector<std::vector<int>>>Dist2RealVal;
    std::vector<std::vector<std::vector<std::vector<int>>>>XYAdjacentCell;


    int xLoop[8] = { -1, -1, 0, 1, 1, 1, 0, -1 };
    int yLoop[8] = { 0, 1, 1, 1, 0, -1, -1, -1 };

    bool isYmin = true;
    //float mCellSize = 50;
    double interpDist = 200; //metres away that will be interpolated
    int maxLoop = 75;
    int trendM = 100;
    int multiSmooth = 0;
    double angleSearch = 10;
    double searchStepSize = 0.25;

    const int xTrack = 1;
    const int yTrack = 2;
    const int valTrack = 3;

    char mSplit = ',';

    int signOfVal(double input);

    double dcOffset;

    bool autoStop = true;

    double eigenAvg;

    QString mTableName;
    QString mGridValue;
    QString mGridX;
    QString mGridY;

     QSqlQuery mSql;

     bool mReadFromDB = false;

public slots:
    void startGridding();

signals:
};

#endif // NPKGRIDDING_H
