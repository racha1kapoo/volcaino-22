#include <QThread>
#include "ProfileLevelingDialog.h"
#include "ui_ProfileLevelingDialog.h"

ProfileLevelingDialog::ProfileLevelingDialog(QString qsTable, QWidget *parent)
    : QDialog(parent), ui(new Ui::ProfileLevelingDialog)
{
    ui->setupUi(this);
    setWindowFlags(this->windowFlags() | Qt::WindowMinMaxButtonsHint);
    mqsTable = qsTable;

    InitUI();
    if (mInterpolator == nullptr)
        mInterpolator = new Interpolator();

    ui->toolButton->setPopupMode(QToolButton::InstantPopup);
    ui->toolButton->setToolButtonStyle(Qt::ToolButtonTextOnly);

    QMenu *toolMenu = new QMenu(ui->toolButton);
    QAction *akimaAct = new QAction(tr("Akima"), toolMenu);
    toolMenu->addAction(akimaAct);
    connect(akimaAct, &QAction::triggered, this, &ProfileLevelingDialog::drawAkimaSpline);
    QAction *linearAct = new QAction(tr("Linear"), toolMenu);
    toolMenu->addAction(linearAct);
    connect(linearAct, &QAction::triggered, this, &ProfileLevelingDialog::drawLinearSpline);
    QAction *rationalAct = new QAction(tr("Rational"), toolMenu);
    toolMenu->addAction(rationalAct);
    connect(rationalAct, &QAction::triggered, this, &ProfileLevelingDialog::drawRationalSpline);
    QAction *pCHIPAct = new QAction(tr("PCHIP"), toolMenu);
    toolMenu->addAction(pCHIPAct);
    connect(pCHIPAct, &QAction::triggered, this, &ProfileLevelingDialog::drawPchipSpline);

    ui->toolButton->setMenu(toolMenu);

    mBar = new ProgressBar(this);
    disconnect(this, &ProfileLevelingDialog::currentProgress, mBar, &ProgressBar::updateProgress);
    connect(this, &ProfileLevelingDialog::currentProgress, mBar, &ProgressBar::updateProgress);
}

ProfileLevelingDialog::~ProfileLevelingDialog()
{
    delete ui;
}

std::vector<QMap<double, double>> &ProfileLevelingDialog::getAssumedZeroes()
{
    return mSelectedVals;
}

void ProfileLevelingDialog::InitUI()
{
    setWindowTitle(tr("Profile Leveling"));
    showMaximized();
    if (!DataBase::getInstance()->openDatabase())
        return;

    mDb = DataBase::getInstance();
    mSql = mDb->getSqlQuery();
    UpdateComboBoxData();

    //channel to level
    mpChart = new ChartData();

    mpMouse = new MouseEventTracking(mpChart);
    mpChartView = new ShowChartView(mpChart);

    //Reference channel
    mpReferenceChart = new ChartData();
    mpReferenceChartView = new ShowChartView(mpReferenceChart);
    //mpReferenceChartView->setChartType(ChartType::PLMRefNormal);
    mpMouseRef = new MouseEventTracking(mpReferenceChart);
    mpReferenceChartView->setMouseTracker(mpMouseRef);
    //mpReferenceChartView->setChanContextMenu();
    //Zoomed Channel
    //    mpSubChart = new ChartData;
    //    mpSubChartView = new ShowChartView(mpSubChart);
    //    mpMouseSub = new MouseEventTracking(mpSubChart);
    //this->layout()->addWidget(qMainSplitter);

    ChartData *ptrChart = new ChartData;
    mpSubChartView = new ShowChartView(ptrChart);

    mSnippetChartLayout = new QVBoxLayout(ui->widget_2);
    mSnippetChartLayout->addWidget(mpSubChartView);
    QVBoxLayout* p = new QVBoxLayout(this);
    this->setLayout(p);

    qVerticalSplitter = new QSplitter(Qt::Vertical);
    //mChartLayout = new QVBoxLayout(ui->ChannelViewer);
    qVerticalSplitter->addWidget(mpReferenceChartView);
    qVerticalSplitter->addWidget(mpChartView);

    qHorizontalSplitter = new QSplitter(Qt::Horizontal);
    qHorizontalSplitter->addWidget(ui->FlightSelectorWidget);
    qHorizontalSplitter->addWidget(ui->widget_2);
    qHorizontalSplitter->setStretchFactor(0,0);
    qHorizontalSplitter->setStretchFactor(1,3);


    qMainSplitter = new QSplitter(Qt::Vertical);
    qMainSplitter->addWidget(qHorizontalSplitter);
    qMainSplitter->addWidget(qVerticalSplitter);
    qMainSplitter->setStretchFactor(0,0);
    qMainSplitter->setStretchFactor(1,3);
    CreateMenuBar();
    ConnectSignalsToSlots();
}

void ProfileLevelingDialog::CreateMenuBar()
{
    QMenuBar *toolBar = new QMenuBar(this);

    QMenu *FileMenu =new QMenu("Actions");
    QMenu *SelectionPointsMenu = new QMenu("Selection Points");

    QAction *action1 = new QAction("Export...");
    SelectionPointsMenu->addAction(action1);
    connect(action1, &QAction::triggered, this, &ProfileLevelingDialog::SaveSelectedPoints);

    QAction *action2 = new QAction("Import...");
    SelectionPointsMenu->addAction(action2);
    FileMenu->addMenu(SelectionPointsMenu);
    connect(action2, &QAction::triggered, this, &ProfileLevelingDialog::ImportSelectedPoints);

    QMenu *DatabaseMenu = new QMenu("Database");

    QMenu *SaveChannel = new QMenu("Correct and Save Channel");
    QAction* AkimaSplineChannel = new QAction("Akima");
    connect(AkimaSplineChannel, &QAction::triggered, this, &ProfileLevelingDialog::SaveAkimaChannel);
    SaveChannel->addAction(AkimaSplineChannel);
    QAction* LinearSplineChannel = new QAction("Linear");
    connect(LinearSplineChannel, &QAction::triggered, this, &ProfileLevelingDialog::SaveLinearChannel);
    SaveChannel->addAction(LinearSplineChannel);

    QAction* RationalSplineChannel = new QAction("Rational");
    connect(RationalSplineChannel, &QAction::triggered, this, &ProfileLevelingDialog::SaveRationalChannel);
    SaveChannel->addAction(RationalSplineChannel);
    QAction* PCHIPSplineChannel = new QAction("PCHIP");
    connect(PCHIPSplineChannel, &QAction::triggered, this, &ProfileLevelingDialog::SavePCHIPChannel);
    SaveChannel->addAction(PCHIPSplineChannel);

    DatabaseMenu->addMenu(SaveChannel);

    FileMenu->addMenu(DatabaseMenu);

    QAction *action3 = new QAction("Import OffSet File..");
    FileMenu->addAction(action3);
    connect(action3, &QAction::triggered, this, &ProfileLevelingDialog::ImportOffSetFile);

    toolBar->addMenu(FileMenu);
    layout()->setMenuBar(toolBar);
    layout()->addWidget(qMainSplitter);
}

void ProfileLevelingDialog::ConnectSignalsToSlots()
{
    connect(ui->FlightNoCB, SIGNAL(currentIndexChanged(int)), this, SLOT(OnFlightNoChanged()));
    connect(ui->LineNoCB, SIGNAL(currentIndexChanged(int)), this, SLOT(OnLineNoChanged()));
    connect(ui->LineTypeCB, SIGNAL(currentIndexChanged(int)), this, SLOT(OnLineTypeChanged()));

    connect(ui->PlotPB, &QPushButton::clicked, this, &ProfileLevelingDialog::OnPlotChannel);
}

void ProfileLevelingDialog::OnFlightNoChanged()
{
    QString qsFlightNo = ui->FlightNoCB->currentText();
    QString sQuery = "Select lineNo,lineType from "+mqsTable+"where flightNo ="+qsFlightNo;
    if (!mSql.exec(sQuery))
        return;
    while (mSql.next()) {
        QString LineNo = mSql.value(0).toString();
        QString LineType = mSql.value(1).toString();
        ui->LineNoCB->addItem(LineNo);
        ui->LineTypeCB->addItem(LineType);
    }
}

void ProfileLevelingDialog::UpdateComboBoxData()
{
    QStringList currentlist =  mDb->getHeaderDetails(mqsTable);
    ui->ChannelCB->addItems(currentlist);
    ui->RefChannelCB->addItems(currentlist);

    QStringList QFlightNo = mDb->getFlightNo(mqsTable);
    ui->FlightNoCB->addItems(QFlightNo);
    ui->LineNoCB->addItem("all");
    ui->LineTypeCB->addItem("all");
    OnFlightNoChanged();
}

void ProfileLevelingDialog::OnPlotChannel()
{
    QString qsFlightNo = ui->FlightNoCB->currentText();
    QString qsChannel = ui->ChannelCB->currentText();
    QString qsRefChannel = ui->RefChannelCB->currentText();

    mSelectedVals.clear();
    if (mpSubChartView != nullptr) {
        mSnippetChartLayout->removeWidget(mpSubChartView);
        delete mpSubChartView;
        mpSubChartView = nullptr;
    }


    mpMouse->clearTrackers();
    mpChart->removeAllSeries();

    mpMouseRef->clearTrackers();
    mpReferenceChart->removeAllSeries();

    //mpReferenceChart->setShowLegend(false);

    //auto pMousePt = new MouseEventTracking(mpChart);

    if (mArrayList.find(ui->ChannelCB->currentIndex())!=mArrayList.end()) {
        //found an array;
        // plot Array

        //populating the chart
        int chanIndex = distance(mArrayList.begin(), mArrayList.find(ui->ChannelCB->currentIndex()));
        mArraySize = mChartLoadData->getArraySize(chanIndex);
        int rowCount = mChartLoadData->getRowCount(chanIndex);

        mpChart->setShowLegend(false);

        QFont font("Times", 8, QFont::Normal);
        mpChart->legend()->setFont(font);
        mpChart->populateArrayData(mChartLoadData, mArraySize, chanIndex,0,rowCount);

        mpChartView->setMouseTracker(mpMouse);
        mpChartView->setChartType(ChartType::PLMDataArray);

        mpChartView->setArrayCount(mArraySize);

        mpChartView->setTitle(qsChannel);

        mbArray = true;

        mpChartView->setRubberBand(QChartView::HorizontalRubberBand);
    } else {
        //populating the chart Data
        QString sQuery = "Select "+qsChannel +","+qsRefChannel+" from "+mqsTable+" where flightNo ="+qsFlightNo;
        if (ui->LineNoCB->currentIndex() != 0)
            sQuery += " AND LineNo ="+ui->LineNoCB->currentText();
        if (ui->LineTypeCB->currentIndex() != 0)
            sQuery += " AND LineType ="+ui->LineTypeCB->currentText();
        mpChart->removeAllSeries();
        mpChart->populateSingleChannelDataFromQuery(sQuery);

        mpChartView->setChartType(ChartType::PLMDataNormal);


        mpChartView->setTitle(qsChannel);
        mpChartView->setMouseTracker(mpMouse);
        //mpChartView->setChanContextMenu();

        mpChartView->setRubberBand(QChartView::HorizontalRubberBand);
        mbArray = false;
    }

    if(mArrayList.find(ui->RefChannelCB->currentIndex())!=mArrayList.end())
    {
        int chanIndex = distance(mArrayList.begin(), mArrayList.find(ui->RefChannelCB->currentIndex()));
        int arraySize = mChartLoadData->getArraySize(chanIndex);
        int rowCount = mChartLoadData->getRowCount(chanIndex);

        mpReferenceChart->setShowLegend(false);

        QFont font("Times", 8, QFont::Normal);
        mpReferenceChart->legend()->setFont(font);
        mpReferenceChart->populateArrayData(mChartLoadData, arraySize, chanIndex,0,rowCount);

        mpReferenceChartView->setChartType(ChartType::PLMRefArray);
        mpReferenceChartView->setArrayCount(arraySize);

        //mpReferenceChartView->setChanContextMenu();
        mpReferenceChartView->setTitle(qsRefChannel);

        mpReferenceChartView->setRubberBand(QChartView::HorizontalRubberBand);
    }
    else
    {
        QString sQuery = "Select "+qsRefChannel+" from "+mqsTable+" where flightNo ="+qsFlightNo;
        if (ui->LineNoCB->currentIndex() !=0)
            sQuery+=" AND LineNo ="+ui->LineNoCB->currentText();
        if (ui->LineTypeCB->currentIndex() !=0)
            sQuery+=" AND LineType ="+ui->LineTypeCB->currentText();

        mpReferenceChartView->setChartType(ChartType::PLMRefNormal);
        mpReferenceChart->populateSingleChannelDataFromQuery(sQuery);
        mpReferenceChartView->setTitle(qsRefChannel);

        //mpReferenceChartView->setChanContextMenu();
        mpReferenceChartView->setRubberBand(QChartView::HorizontalRubberBand);
    }


    alignXAxis();
    QApplication::sendPostedEvents();
    mpChartView->setChanContextMenu();
    mpReferenceChartView->setChanContextMenu();

    //connect(mpChart,&ChartData::plotAreaChanged, this,&ProfileLevelingDialog::getIniPlotAreasWidth,Qt::UniqueConnection);
    connect(mpChartView, &ShowChartView::plmZoomY, this, &ProfileLevelingDialog::allSaveAxis, Qt::UniqueConnection);
    connect(mpReferenceChartView, &ShowChartView::plmZoomY, this, &ProfileLevelingDialog::allSaveAxis, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::scrollDone, this, &ProfileLevelingDialog::saveAxisScroll, Qt::UniqueConnection);
    connect(mpReferenceChartView, &ShowChartView::scrollDone, this, &ProfileLevelingDialog::saveAxisScroll, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::seriesValuesSelects, this, &ProfileLevelingDialog::ShowSubGraph, Qt::UniqueConnection);
    connect(mpReferenceChartView, &ShowChartView::seriesValuesSelects, this, &ProfileLevelingDialog::ShowSubGraph, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::scrollSignal, this, &ProfileLevelingDialog::allScroll, Qt::UniqueConnection);

    connect(mpReferenceChartView, &ShowChartView::scrollSignal, this, &ProfileLevelingDialog::allScroll, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::thisZoomReset, this, &ProfileLevelingDialog::allZoomReset, Qt::UniqueConnection);

    connect(mpReferenceChartView, &ShowChartView::thisZoomReset, this, &ProfileLevelingDialog::allZoomReset, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::backToLast, this, &ProfileLevelingDialog::allGoBack, Qt::UniqueConnection);

    connect(mpReferenceChartView, &ShowChartView::backToLast, this, &ProfileLevelingDialog::allGoBack, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::zoomInX, this, &ProfileLevelingDialog::allZoomInX, Qt::UniqueConnection);

    connect(mpReferenceChartView, &ShowChartView::zoomInX, this, &ProfileLevelingDialog::allZoomInX, Qt::UniqueConnection);

    connect(this, &ProfileLevelingDialog::allScroll, this, &ProfileLevelingDialog::chartScroll, Qt::UniqueConnection);

    connect(this, &ProfileLevelingDialog::allZoomReset, this, &ProfileLevelingDialog::chartReset, Qt::UniqueConnection);

    connect(mpChartView,&ShowChartView::thisRubberBandChanged, this, &ProfileLevelingDialog::allRubberBandChanged, Qt::UniqueConnection);

    connect(mpReferenceChartView,&ShowChartView::thisRubberBandChanged, this, &ProfileLevelingDialog::allRubberBandChanged, Qt::UniqueConnection);

    connect(this, &ProfileLevelingDialog::allRubberBandChanged, this, &ProfileLevelingDialog::zoomInX, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::deletePoints, this, &ProfileLevelingDialog::deleteSelectedVals, Qt::UniqueConnection);

    connect(this, &ProfileLevelingDialog::allGoBack, this,&ProfileLevelingDialog::revert, Qt::UniqueConnection);
    connect(this, &ProfileLevelingDialog::allSaveAxis, this,&ProfileLevelingDialog::saveAxis, Qt::UniqueConnection);

    connect(this, &ProfileLevelingDialog::allZoomInX, this,&ProfileLevelingDialog::allZoomInAlongX, Qt::UniqueConnection);

    connect(mpChartView, &ShowChartView::setXYRanges, this, &ProfileLevelingDialog::setYRange, Qt::UniqueConnection);

    connect(mpReferenceChartView, &ShowChartView::setXYRanges, this, &ProfileLevelingDialog::setYRange, Qt::UniqueConnection);
    connect(this, &ProfileLevelingDialog::allSetXYRanges, this,&ProfileLevelingDialog::setXRange, Qt::UniqueConnection);

    connect(this, &ProfileLevelingDialog::allGoBack, this, &ProfileLevelingDialog::revert, Qt::UniqueConnection);
    connect(this, &ProfileLevelingDialog::allSaveAxis, this, &ProfileLevelingDialog::saveAxis, Qt::UniqueConnection);


    saveIniAxisRanges();
}

void ProfileLevelingDialog::OnLineNoChanged()
{
    QString qsFlightNo = ui->FlightNoCB->currentText();
    QString sQuery = "Select DISTINCT lineType from "+mqsTable+"where flightNo ="+qsFlightNo;
    if (ui->LineNoCB->currentIndex() != 0)
        sQuery+=" AND lineNo ="+ui->LineNoCB->currentText();

    if (!mSql.exec(sQuery))
        return;
    ui->LineTypeCB->clear();
    ui->LineTypeCB->addItem("All");
    while (mSql.next()) {
        QString LineType = mSql.value(1).toString();
        ui->LineTypeCB->addItem(LineType);
    }
}

void ProfileLevelingDialog::OnLineTypeChanged()
{
}

void ProfileLevelingDialog::adjustTrackLine(MouseEventTracking *track)
{
}

void ProfileLevelingDialog::ShowSubGraph(int fromX, int toX)
{
    if (mpSubChartView != nullptr) {
        mSnippetChartLayout->removeWidget(mpSubChartView);
        delete mpSubChartView;
        mpSubChartView = nullptr;
    }

    QPointF from(fromX, 0);
    QPointF to(toX, 0);

    mpChartView->getMouseTracker()->showIniDoubleIndicators();
    mpReferenceChartView->getMouseTracker()->showIniDoubleIndicators();
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpChartView->update();
    mpReferenceChartView->update();

    ChartData *ptrChart = new ChartData;
    ShowChartView *ptrChartView = new ShowChartView(ptrChart);
    MouseEventTracking *ptrTracker = new MouseEventTracking(ptrChart);

    mpSubChart = ptrChart;
    mpSubChartView = ptrChartView;
    mpSubMouse = ptrTracker;

    QString qsFlightNo = ui->FlightNoCB->currentText();
    QString qsChannel = ui->ChannelCB->currentText();

    if (!mbArray) {
        mpSubChart->legend()->hide();
        QString sQuery = "Select "+qsChannel +" from "+mqsTable+" where flightNo ="+qsFlightNo;
        if (ui->LineNoCB->currentIndex() != 0)
            sQuery += " AND LineNo ="+ui->LineNoCB->currentText();
        if (ui->LineTypeCB->currentIndex() != 0)
            sQuery += " AND LineType ="+ui->LineTypeCB->currentText();

        if (!mSql.exec(sQuery))
            return;
        QLineSeries* mSubSeries = new QLineSeries;
        mSubSeries->setUseOpenGL();
        int count = 0;
        while (mSql.next()) {
            bool bVal = mSql.value(0).isNull();
            if (!bVal) {
                if (count >= fromX && count <= toX)
                    mSubSeries->append(count, mSql.value(0).toReal());
            }
            count++;
        }

        mpSubChart->addSeries(mSubSeries);
        mpSubChart->setLineseries(mSubSeries);
        mpSubChartView->setChartType(ChartType::PLMSubNormal);
    } else {
        int chanIndex = distance(mArrayList.begin(),mArrayList.find(ui->ChannelCB->currentIndex()));
        int arraySize = mChartLoadData->getArraySize(chanIndex);
        mpSubChart->populateArrayData(mChartLoadData,arraySize,chanIndex,fromX,toX);
        //mpSubChart->setShowLegend(true);
        mpSubChart->legend()->hide();

        mpSubChartView->setChartType(ChartType::PLMSubArray);

        mpSubChartView->setArrayCount(arraySize);
    }
    mpSubChartView->setMouseTracker(mpSubMouse);
    mpSubChartView->setChanContextMenu();
    mpSubChart->setIniAxis();

    mSnippetChartLayout->addWidget(mpSubChartView);
    connect(mpSubChartView, &ShowChartView::seriesValuesSelects, this, &ProfileLevelingDialog::getSelectedVals);
}

void ProfileLevelingDialog::deleteSelectedVals(int startIndex, int endIndex)
{
    for (int i = 0; i < mSelectedVals.size(); i++) {
        QMap<double, double>::iterator itr;
        QMap<double, double> thisMap = mSelectedVals[i];
        for (itr = thisMap.begin(); itr != thisMap.end(); ++itr) {
            if (itr.key() >= startIndex && itr.key() <= endIndex)
                mSelectedVals[i].remove(itr.key());
        }
    }
    mpMouse->deleteLvlTrackers(startIndex, endIndex);
    mpMouseRef->deleteRefTrackers(startIndex, endIndex);
}

void ProfileLevelingDialog::getSelectedVals(int startIndex, int endIndex)
{
    //std::vector<double> thisSelectedVals;
    //thisSelectedVals.push_back(startIndex);
    //thisSelectedVals.push_back(endIndex);

    std::vector<QPointF> selectionPts;
    std::vector<std::vector<QPointF>> selectionArrPts;
    std::vector<int> nullPts(0);
    //double ave = 0;
    double range = (endIndex - startIndex) + 1;
    double seriesSize = mbArray? mArraySize: 1;
    selectionArrPts.resize(seriesSize);
    nullPts.resize(seriesSize);
    //double totalCount = range * seriesSize;
    int nullValCount = 0;
    mSelectedVals.resize(seriesSize);

    for (int index = startIndex; index <= endIndex; index++) {
        if (seriesSize != 1) {
            for (int seriesNo = 0; seriesNo < seriesSize; seriesNo++) {
                if (!mpChart->getValidArrayChanData()[seriesNo][index]) {
                    nullPts[seriesNo]++;
                } else {
                    selectionArrPts[seriesNo].push_back(QPointF(index, mpChart->getArrayChanData()[seriesNo][index]));
                    //qDebug() << QPointF(index, mpChart->getArrayChanData()[seriesNo][index]);
                }
            }
        } else {
            if (mpChart->getNormalChanData()[index] == nullValue) {
                nullValCount++;
            } else {
                selectionPts.push_back(QPointF(index, mpChart->getNormalChanData()[index]));
            }
        }
    }

    if (seriesSize == 1) {
        if (nullValCount == range && range == 1) {  //Only one selection pt is null
            QString str = "Null selected, switched to the nearest valid value";
            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Prompt"));
            msgBox.setText(str);
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
            int ret = msgBox.exec();

            switch (ret) {
            case QMessageBox::Ok:
                msgBox.close();
                break;
            default:
                break;
            }

            selectionPts.clear();
            //thisSelectedVals.clear();
            int leftIndex = startIndex;
            int rightIndex = startIndex;
            int firstValid = 0;

            while (1) {
                leftIndex -= 1;
                rightIndex += 1;

                if (mpChart->getNormalChanData()[leftIndex] != nullValue) {
                    firstValid = leftIndex;
                    break;
                }

                if (mpChart->getNormalChanData()[rightIndex] != nullValue) {
                    firstValid = rightIndex;
                    break;
                }
            }
            selectionPts.push_back(QPointF(firstValid, mpChart->getNormalChanData()[firstValid]));

            mSelectedVals[0].insert(selectionPts[0].x(), selectionPts[0].y());

            mpChartView->getMouseTracker()->setMultiSinglePtPos(selectionPts, true, false);
            mpReferenceChartView->getMouseTracker()->setMultiSinglePtPos(selectionPts, false, false);

            mpChartView->update();
            mpReferenceChartView->update();
        } else if (nullValCount >= range-1 && range > 1) {
            QString str = "Nulls selected, please select again";
            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Prompt"));
            msgBox.setText(str);
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
            int ret = msgBox.exec();

            switch (ret) {
            case QMessageBox::Ok:
                msgBox.close();
                break;
            default:
                break;
            }
        } else {
            QPointF position;
            for (int i = 0; i < selectionPts.size(); i++)
                position += selectionPts[i]/(double)selectionPts.size();

            mSelectedVals[0].insert(position.x(),position.y());

            mpChartView->getMouseTracker()->setMultiSinglePtPos(selectionPts, true, false);
            mpReferenceChartView->getMouseTracker()->setMultiSinglePtPos(selectionPts, false, false);

            mpChartView->update();
            mpReferenceChartView->update();
        }
    } else {
        bool showError = false;
        for (int i : nullPts) {
            if (i == range) {
                selectionArrPts.clear();
                showError = true;
                break;
            }
        }
        if (showError) {  //Only one selection pt is null
            QString str = "Nulls selected, please select again";
            QMessageBox msgBox;
            msgBox.setWindowTitle(tr("Prompt"));
            msgBox.setText(str);
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
            int ret = msgBox.exec();

            switch (ret) {
            case QMessageBox::Ok:
                msgBox.close();
                break;
            default:
                break;
            }
        } else {
            for (int series = 0; series < seriesSize; series++) {
                QPointF position(0,0);
                double selPtsSize = selectionArrPts[series].size();
                for (int index = 0; index < selPtsSize; index++)
                    position += selectionArrPts[series][index]/selPtsSize;
                //
                mSelectedVals[series].insert(position.x(),position.y());
            }
            //qDebug() << mSelectedVals;
            double aveIndex = (startIndex + endIndex)/2.0;

            QPointF avePt(aveIndex, 0);
            std::vector<QPointF> avePts;
            avePts.push_back(avePt);

            mpChartView->getMouseTracker()->setMultiSinglePtPos(avePts, true, true);
            mpReferenceChartView->getMouseTracker()->setMultiSinglePtPos(avePts, false, true);

            mpChartView->update();
            mpReferenceChartView->update();
        }
    }
}

void ProfileLevelingDialog::setMembers(ChanMenuOpData* pdata, std::map<int,QString> ArrayList)
{
    mChartLoadData = pdata;
    mArrayList = ArrayList;
}

void ProfileLevelingDialog::saveIniAxisRanges()
{
    mpChart->saveAxisIniRangesPLM();
    mpReferenceChart->saveAxisIniRangesPLM();
}

void ProfileLevelingDialog::drawAkimaSpline()
{
    if (mSelectedVals.empty() || mSelectedVals.at(0).count() < 4) {
        QString str = "Please select atleast 4 points to interpolate";
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Prompt"));
        msgBox.setText(str);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Ok:
            msgBox.close();
            break;
        default:
            break;
        }
        return;
    }
    if (mInterpolator)
        mInterpolator->setData(mqsTable, mSelectedVals);
    if (mAkimaSpline != nullptr)
        mpChart->removeSeries(mAkimaSpline);
    mAkimaSpline = mInterpolator->akimaInterp();
    mpChart->addSeries(mAkimaSpline);
    mpChart->setSplineSeries(mAkimaSpline);
    mpChart->setIniAxis();
}

void ProfileLevelingDialog::drawLinearSpline()
{
    if (mSelectedVals.empty() || mSelectedVals.at(0).count() < 4) {
        QString str = "Please select atleast 4 points to interpolate";
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Prompt"));
        msgBox.setText(str);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Ok:
            msgBox.close();
            break;
        default:
            break;
        }
        return;
    }
    if (mInterpolator)
        mInterpolator->setData(mqsTable, mSelectedVals);
    if (linSpline != nullptr)
        mpChart->removeSeries(linSpline);
    linSpline = mInterpolator->linearInterp();
    mpChart->addSeries(linSpline);
    mpChart->setLineseries(linSpline);
    mpChart->setIniAxis();
}

void ProfileLevelingDialog::drawRationalSpline()
{
    if (mSelectedVals.empty() || mSelectedVals.at(0).count() < 4) {
        QString str = "Please select atleast 4 points to interpolate";
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Prompt"));
        msgBox.setText(str);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Ok:
            msgBox.close();
            break;
        default:
            break;
        }
        return;
    }
    if(mInterpolator)
        mInterpolator->setData(mqsTable, mSelectedVals);
    if (mRationalSpline != nullptr)
        mpChart->removeSeries(mRationalSpline);
    mRationalSpline = mInterpolator->rationalInterp();
    mpChart->addSeries(mRationalSpline);
    mpChart->setSplineSeries(mRationalSpline);
    mpChart->setIniAxis();
}

void ProfileLevelingDialog::drawPchipSpline()
{
    if (mSelectedVals.empty() || mSelectedVals.at(0).count() < 4) {
        QString str = "Please select atleast 4 points to interpolate";
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Prompt"));
        msgBox.setText(str);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Ok:
            msgBox.close();
            break;
        default:
            break;
        }
        return;
    }
    if(mInterpolator)
        mInterpolator->setData(mqsTable, mSelectedVals);
    if (mPCHIPSpline != nullptr)
        mpChart->removeSeries(mPCHIPSpline);
    mPCHIPSpline = mInterpolator->pCHIPInterp();
    mpChart->addSeries(mPCHIPSpline);
    mpChart->setSplineSeries(mPCHIPSpline);
    mpChart->setIniAxis();
}

void ProfileLevelingDialog::alignXAxis()
{
    QApplication::sendPostedEvents();

    if (mpChart->plotArea().left() > mpReferenceChart->plotArea().left()) {
        int marginDiff = mpChart->plotArea().left()-mpReferenceChart->plotArea().left();
        QMargins margin = mpReferenceChart->margins();
        margin.setLeft(margin.left()+marginDiff);
        mpReferenceChart->setMargins(margin);
    } else {
        int marginDiff = mpReferenceChart->plotArea().left()-mpChart->plotArea().left();
        QMargins margin = mpChart->margins();
        margin.setLeft(margin.left()+marginDiff);
        mpChart->setMargins(margin);
    }


    int rowCount = DataBase::getInstance()->getRowCount(mqsTable);

    mpChart->axes(Qt::Horizontal).back()->setMax(rowCount);
    mpReferenceChart->axes(Qt::Horizontal).back()->setMax(rowCount);

    mpChartView->update();
    mpReferenceChartView->update();

}

void ProfileLevelingDialog::reAlignXAxis()
{
    if (mpChart->plotArea().left() != mpReferenceChart->plotArea().left()) {
        QMargins margin = mpChart->margins();
        margin.setLeft(20);
        mpChart->setMargins(margin);

        QMargins marginRef = mpReferenceChart->margins();
        marginRef.setLeft(20);
        mpReferenceChart->setMargins(marginRef);

        QApplication::sendPostedEvents();

        if (mpChart->plotArea().left() > mpReferenceChart->plotArea().left()) {
            int marginDiff = mpChart->plotArea().left()-mpReferenceChart->plotArea().left();
            QMargins margin = mpReferenceChart->margins();
            margin.setLeft(margin.left()+marginDiff);
            mpReferenceChart->setMargins(margin);
        } else {
            int marginDiff = mpReferenceChart->plotArea().left()-mpChart->plotArea().left();
            QMargins margin = mpChart->margins();
            margin.setLeft(margin.left()+marginDiff);
            mpChart->setMargins(margin);
        }

        mpChartView->update();
        mpReferenceChartView->update();
    }

}

void ProfileLevelingDialog::singleChannelCorrection()
{
    std::vector<double> orgData = mpChart->getNormalChanData();
    std::vector<double> finalData;

    for (size_t i = 0; i < orgData.size(); i++) {
        double val;
        if (orgData[i] != nullValue) {
            val = orgData[i] - mData[0][i];
        } else {
            val = NAN;
        }
        finalData.push_back(val);
    }

    int mTableRows = DataBase::getInstance()->getRowCount(mqsTable);
    mSql = DataBase::getInstance()->getSqlQuery();

    QString newChanName = ui->ChannelCB->currentText() + mInterpName;
    QStringList typeList;
    typeList << newChanName+" FLOAT,";

    DataBase::getInstance()->createTable("TMPR", typeList);

    emit currentProgress("Inserting to database...", 0);
    emit currentProgress("Inserting to database...", 10);
    QCoreApplication::processEvents();


    DataBase::getInstance()->startTransaction();
    for (int i = 0; i < mTableRows; i++) {
        if (!std::isnan(finalData[i])) {
            QString rec = QString::number(finalData[i], 'f', 4);
            DataBase::getInstance()->insertRecord(rec,"TMPR");
        } else {
            QString blnk = "NAN";
            DataBase::getInstance()->insertRecord(blnk,"TMPR");
        }
    }
    emit currentProgress("Inserting to database...", 70);
    QCoreApplication::processEvents();

    DataBase::getInstance()->closeTransaction();

    if(DataBase::getInstance()->getHeaderDetails(mqsTable).contains(newChanName))
        DataBase::getInstance()->deleteColumn(mqsTable, newChanName);

    DataBase::getInstance()->addColumn(mqsTable, newChanName, "FLOAT", false, "0");

    QString query = "UPDATE "+mqsTable+" SET "+newChanName+" =(SELECT "+newChanName+" FROM TMPR WHERE TMPR.rowid = "+mqsTable+".rowid)";

    DataBase::getInstance()->startTransaction();

    if(!mSql.exec(query))
        qDebug() << "Error happened";

    DataBase::getInstance()->closeTransaction();

    QString query2 = "DROP TABLE TMPR";

    if (!mSql.exec(query2))
        qDebug() << "Error happened";

    emit currentProgress("Corrected Channel Saved", 100);

    emit correctionDone();
}

void ProfileLevelingDialog::multiChannelCorrection()
{
    std::vector<std::vector<double>> orgData = mpChart->getArrayChanData();
    std::vector<std::vector<double>> finalData;
    bool bUseOffSet = (mvArrayOffSet.size() == orgData.size());
    for (size_t i = 0; i < orgData.at(0).size(); i++) { //outer loop 70000 times
        std::vector<double> vecData;
        finalData.push_back(vecData);
        for (size_t j = 0; j < orgData.size(); j++) { //inner loop 27 times
            double val;
            if (orgData[j][i] != nullValue) {
                val = orgData[j][i] - mData[j][i] ;
                if(bUseOffSet)
                    val+=mvArrayOffSet[j];

            } else {
                val = NAN;
            }
            finalData.at(i).push_back(val);
        }
    }

    mSql = DataBase::getInstance()->getSqlQuery();

    QString newChanName = ui->ChannelCB->currentText() + mInterpName;
    QStringList typeList;
    typeList << newChanName+" FLOAT[],";

    DataBase::getInstance()->createTable("TMPR", typeList);
    emit currentProgress("Inserting to database...", 0);
    emit currentProgress("Inserting to database...", 10);
    QCoreApplication::processEvents();

    qDebug() << "Size of final array" << finalData.size() << "inside is: " << finalData.at(0).size();

    DataBase::getInstance()->startTransaction();
    for (size_t i = 0; i < finalData.size(); i++) {
        QString rec;
        if (!std::isnan(finalData.at(i).at(0))) {
            rec.append("'[").append(QString::number(finalData.at(0).size()).append(','));
            for (size_t j = 0; j < finalData.at(0).size(); j++) {
                rec += QString::number(finalData[i][j], 'f', 4);
                rec += ",";
            }
            rec.chop(1);
            rec = rec + "]'";
            DataBase::getInstance()->insertRecord(rec, "TMPR");
        } else {
            QString blnk = "NAN";
            DataBase::getInstance()->insertRecord(blnk,"TMPR");
        }
    }
    emit currentProgress("Inserting to database...", 70);
    QCoreApplication::processEvents();

    DataBase::getInstance()->closeTransaction();

    if (DataBase::getInstance()->getHeaderDetails(mqsTable).contains(newChanName))
        DataBase::getInstance()->deleteColumn(mqsTable, newChanName);

    DataBase::getInstance()->addColumn(mqsTable, newChanName, "FLOAT[]", false, "0");

    QString query = "UPDATE "+mqsTable+" SET "+newChanName+" =(SELECT "+newChanName+" FROM TMPR WHERE TMPR.rowid = "+mqsTable+".rowid)";

    DataBase::getInstance()->startTransaction();

    if (!mSql.exec(query))
        qDebug() << "Error happened";

    DataBase::getInstance()->closeTransaction();

    QString query2 = "DROP TABLE TMPR";

    if (!mSql.exec(query2))
        qDebug() << "Error happened";

    emit currentProgress("Corrected Channel Saved", 100);

    emit correctionDone();
}

void ProfileLevelingDialog::levelChartScroll(double dx, double dy)
{
    //    double ratio = mpChart->plotArea().width()/mIniPlotAreaWidth;
    //    mpChartView->move(dx*ratio, dy);
    mpChartView->move(dx, dy);

    QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpChartView->getMouseTracker()->updateMultiSingleLvlCross();

}

void ProfileLevelingDialog::refChartScroll(double dx, double dy)
{
    //double ratio = mpReferenceChart->plotArea().width()/mpChart->plotArea().width();
    mpReferenceChartView->move(dx, dy);

    QPointF from = mpReferenceChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpReferenceChartView->getMouseTracker()->getDoubleRIndPos();
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpReferenceChartView->getMouseTracker()->updateMultiSingleRefInds();
    mpReferenceChartView->update();
}

void ProfileLevelingDialog::chartReset()
{
    levelChartReset();
    refChartReset();

    reAlignXAxis();
}

void ProfileLevelingDialog::levelChartReset()
{
    mpChart->setAxisIniRanges();
    QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpChartView->getMouseTracker()->updateMultiSingleLvlCross();

    mpChart->getXAxisRanges(updateType::reset);
    mpChart->getYAxisRanges(updateType::reset);

    mpChartView->update();
}

void ProfileLevelingDialog::refChartReset()
{
    mpReferenceChart->setAxisIniRanges();
    QPointF from = mpReferenceChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpReferenceChartView->getMouseTracker()->getDoubleRIndPos();
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpReferenceChartView->getMouseTracker()->updateMultiSingleRefInds();

    mpReferenceChart->getXAxisRanges(updateType::reset);
    mpReferenceChart->getYAxisRanges(updateType::reset);

    mpReferenceChartView->update();
}

void ProfileLevelingDialog::chartScroll(double dx, double dy)
{
   levelChartScroll(dx,dy);
   refChartScroll(dx,dy);

   if (dy != 0)
       reAlignXAxis();
}

void ProfileLevelingDialog::getIniPlotAreasWidth()
{
    mIniPlotAreaWidth = mpChart->plotArea().width();
}

void ProfileLevelingDialog::saveAxis()
{
    //double xMinLvl = dynamic_cast<QValueAxis *>(mpChart->axes(Qt::Horizontal).back())->min();
    //double xMaxLvl = dynamic_cast<QValueAxis *>(mpChart->axes(Qt::Horizontal).back())->max();

    //mpReferenceChart->axes(Qt::Horizontal).back()->setRange(xMinLvl, xMaxLvl);
    reAlignXAxis();

    mpChart->getXAxisRanges(updateType::update);
    mpChart->getYAxisRanges(updateType::update);
    mpReferenceChart->getXAxisRanges(updateType::update);
    mpReferenceChart->getYAxisRanges(updateType::update);
}

void ProfileLevelingDialog::zoomInX(QChartView *view, QPointF fp, QPointF tp, int startIndex, int endIndex, bool isYMode)
{
    mpChartView->zoomInFunc(view, fp, tp, startIndex,  endIndex, isYMode);
    mpReferenceChartView->zoomInFunc(view, fp, tp, startIndex,  endIndex, isYMode);

    QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);

    mpChartView->getMouseTracker()->updateMultiSingleLvlCross();
    mpReferenceChartView->getMouseTracker()->updateMultiSingleRefInds();

    mpChart->getXAxisRanges(updateType::update);
    mpChart->getYAxisRanges(updateType::update);
    mpReferenceChart->getXAxisRanges(updateType::update);
    mpReferenceChart->getYAxisRanges(updateType::update);

    mpChartView->update();
    mpReferenceChartView->update();
}

void ProfileLevelingDialog::setYRange(qreal xmin, qreal xmax, qreal ymin, qreal ymax)
{
    if(ymin != -1 && ymax != -1)
    {
        mpChart->axes(Qt::Vertical).back()->setMin(ymin);
        mpChart->axes(Qt::Vertical).back()->setMax(ymax);
    }
    reAlignXAxis();
    emit this->allSetXYRanges(xmin,xmax);

}

void ProfileLevelingDialog::setXRange(qreal xmin, qreal xmax)
{
    if(xmin != -1 && xmax != -1)
    {
        mpChart->axes(Qt::Horizontal).back()->setMin(xmin);
        mpChart->axes(Qt::Horizontal).back()->setMax(xmax);

        mpReferenceChart->axes(Qt::Horizontal).back()->setMin(xmin);
        mpReferenceChart->axes(Qt::Horizontal).back()->setMax(xmax);
    }

    QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);

    mpChartView->getMouseTracker()->updateMultiSingleLvlCross();
    mpReferenceChartView->getMouseTracker()->updateMultiSingleRefInds();

    mpChart->getXAxisRanges(updateType::update);
    mpChart->getYAxisRanges(updateType::update);
    mpReferenceChart->getXAxisRanges(updateType::update);
    mpReferenceChart->getYAxisRanges(updateType::update);

    mpChartView->update();
    mpReferenceChartView->update();
}

void ProfileLevelingDialog::allZoomInAlongX(qreal factor, qreal xcenter)
{
    mpChartView->zoomAlongX(factor, xcenter);
    mpReferenceChartView->zoomAlongX(factor, xcenter);

    QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);

    mpChartView->getMouseTracker()->updateMultiSingleLvlCross();
    mpReferenceChartView->getMouseTracker()->updateMultiSingleRefInds();

    mpChartView->update();
    mpReferenceChartView->update();

}

void ProfileLevelingDialog::revert()
{
    mpChart->getXAxisRanges(updateType::revert);
    mpChart->setXAxisRanges();
    mpChart->getYAxisRanges(updateType::revert);
    mpChart->setYAxisRanges();

    mpReferenceChart->getXAxisRanges(updateType::revert);
    mpReferenceChart->setXAxisRanges();
    mpReferenceChart->getYAxisRanges(updateType::revert);
    mpReferenceChart->setYAxisRanges();

    QPointF from = mpChartView->getMouseTracker()->getDoubleLIndPos();
    QPointF to = mpChartView->getMouseTracker()->getDoubleRIndPos();
    mpChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);
    mpReferenceChartView->getMouseTracker()->setUpdateDoubleIndicators(from, to);

    mpChartView->getMouseTracker()->updateMultiSingleLvlCross();
    mpReferenceChartView->getMouseTracker()->updateMultiSingleRefInds();

    reAlignXAxis();
    //mpChart->update();
    //mpReferenceChart->update();
}

void ProfileLevelingDialog::saveAxisScroll()
{
    mpChart->getXAxisRanges(updateType::update);
    mpChart->getYAxisRanges(updateType::update);

    mpReferenceChart->getXAxisRanges(updateType::update);
    mpReferenceChart->getYAxisRanges(updateType::update);
}

#define DETAILS_STR "Details"
#define TABLENAME_STR "Table Name"
#define CHANNELNAME_STR "Channel Name"
#define ISARRAY_STR "IsArray"
#define ASSUMEDZEROS_STR "Selected Points"

void ProfileLevelingDialog::SaveSelectedPoints()
{
    QString qsFlightNo = ui->FlightNoCB->currentText();
    QString qsChannel = ui->ChannelCB->currentText();
    QString qsRefChannel = ui->RefChannelCB->currentText();
    QString selectedFilter;
    QString qsJsonFileName = QFileDialog::getSaveFileName(this,"Please select a folder to save the selected points","","*.json,*.JSON",&selectedFilter,QFileDialog::ShowDirsOnly);
    QFile file(qsJsonFileName);
    if (!file.open(QIODevice::ReadWrite)) {
        qDebug() << "File open error";
    } else {
        qDebug() <<"File opened!";
    }

    // Clear the original content in the file
    file.resize(0);
    //QJsonArray jsonArray;
    //write the details of the system
    QJsonObject content;
    QJsonObject detailsObject;
    detailsObject.insert(TABLENAME_STR, mqsTable);
    detailsObject.insert(CHANNELNAME_STR, qsChannel);
    detailsObject.insert(ISARRAY_STR, QJsonValue(mbArray));
    content.insert(DETAILS_STR, detailsObject);
    //jsonArray.append(TableDetailObject);

    QJsonObject qPoints;
    QJsonArray pArrayObject;
    double seriesSize = mbArray? mArraySize: 1;
    for (int j = 0; j < seriesSize; j++) {
        int i = 0;
        QJsonObject pointObject;
        for (double val : mSelectedVals.at(j).keys()) {
            pointObject.insert("X",QJsonValue(val));
            pointObject.insert("Y",mSelectedVals.at(j).value(val));
            QString qsSelectedPoint = "SelectedPoint";
            qsSelectedPoint+=QString::number(i).rightJustified(2, '0');
            qPoints.insert(qsSelectedPoint,pointObject);
            i++;
        }
        qPoints.insert("TotalPointCount",i);
        pArrayObject.append(qPoints);
    }
    content.insert(ASSUMEDZEROS_STR,pArrayObject);
    //jsonArray.append(qAssumedZeros);
    QJsonDocument jsonDoc;
    jsonDoc.setObject(content);
    file.write(jsonDoc.toJson());
    file.close();
    qDebug() << "Write to file";
}

void ProfileLevelingDialog::ImportSelectedPoints()
{
    QString qsJsonFileName = QFileDialog::getOpenFileName(this,"Please select a file to import","","*.json;*.JSON");
    QFile file(qsJsonFileName);
    if (file.open(QIODevice::ReadOnly)) {
        QByteArray bytes = file.readAll();
        file.close();

        QJsonParseError jsonError;
        QJsonDocument document = QJsonDocument::fromJson( bytes, &jsonError );
        if (jsonError.error != QJsonParseError::NoError) {
            qDebug() << "fromJson failed:";
            return ;
        }
        QString qSChannelName = "";
        bool isArray;
        QString qsTabelName = "";
        if (document.isObject()) {
            QJsonObject jsonObj = document.object();
            QJsonObject Detailsobj = jsonObj.value(DETAILS_STR).toObject();
            QStringList keys = Detailsobj.keys();
            for (auto key: keys) {
                if (key == CHANNELNAME_STR) {
                    auto value = Detailsobj.take( key );
                    if (value.isString())
                        qSChannelName =value.toString();
                } else if (key == TABLENAME_STR) {
                    auto value = Detailsobj.take( key );
                    if (value.isString())
                        qsTabelName =value.toString();
                } else if (key == ISARRAY_STR) {
                    auto value = Detailsobj.take( key );
                    if (value.isBool())
                        isArray =  value.toBool();
                } else {
                    qDebug()<<"Invalid Key";
                }
            }
            bool bCoumputePoints = false;
            if (qSChannelName != ui->ChannelCB->currentText()) {
                bCoumputePoints = true;
                isArray = mbArray;
            }
            int seriesSize = 1;
            mSelectedVals.clear();
            if (isArray) {
                int chanIndex = distance(mArrayList.begin(), mArrayList.find(ui->ChannelCB->currentIndex()));
                seriesSize = mChartLoadData->getArraySize(chanIndex);
            }
            mSelectedVals.resize(seriesSize);
            std::vector<QMap<double, double>> recompute;
            recompute.resize(seriesSize);
            QJsonArray AssumedZerosObj =jsonObj.value(ASSUMEDZEROS_STR).toArray();
            for (int i = 0; i < AssumedZerosObj.size(); i++) {
                QJsonObject arrayObject =AssumedZerosObj[i].toObject();
                keys = arrayObject.keys();
                int j = 0;

                for (auto key: keys) {
                    if (key.contains("SelectedPoint")) {
                        QString qsSelectedPoint = "SelectedPoint";
                        qsSelectedPoint += QString::number(j).rightJustified(2, '0');
                        j++;
                        if (key == qsSelectedPoint ) {
                            QJsonObject selctedpointObject =arrayObject.take(key).toObject();
                            double x,y;
                            for (auto cord: selctedpointObject.keys()) {
                                if (cord == "X") {
                                    auto val = selctedpointObject.take(cord);
                                    if (val.isDouble())
                                        x = val.toDouble();
                                } else if (cord == "Y") {
                                    auto val = selctedpointObject.take(cord);
                                    if (val.isDouble())
                                        y = val.toDouble();
                                }
                            }

                            if (!bCoumputePoints) {
                                mSelectedVals[i].insert(x,y);
                            } else {
                                if (i<seriesSize)
                                    recompute[i].insert(x,0);
                            }
                        }
                    }
                }
            }
            //plot the points
            //for(int i = 0;i<seriesSize;i++)
            //{
            if (!bCoumputePoints) {
                for (double val : mSelectedVals.at(0).keys()) {
                    std::vector<QPointF> avePts;
                    QPointF avePt(val,isArray?0:mSelectedVals.at(0).value(val));
                    avePts.push_back(avePt);
                    mpChartView->getMouseTracker()->setMultiSinglePtPos(avePts, true, isArray);
                    mpReferenceChartView->getMouseTracker()->setMultiSinglePtPos(avePts, false, isArray);
                }
                //}
                mpChartView->update();
                mpReferenceChartView->update();
            } else {
                int mTableRows = DataBase::getInstance()->getRowCount(mqsTable);
                for (double val : recompute.at(0).keys()) {
                    int startIndex = val-20;
                    int endIndex = val +20;
                    if (startIndex<0)
                        startIndex = 0;
                    if (endIndex>mTableRows)
                        endIndex = mTableRows;

                    getSelectedVals(startIndex,endIndex);
                }
            }
        }
    }
}

#define ARRSIZE_STR "ARRAY_SIZE"
void ProfileLevelingDialog::ImportOffSetFile()
{
    QString qsJsonFileName = QFileDialog::getOpenFileName(this,"Please select a file to import","","*.json;*.JSON");
    QFile file(qsJsonFileName);

    if (file.open(QIODevice::ReadOnly)) {
        QByteArray bytes = file.readAll();
        file.close();

        QJsonParseError jsonError;
        QJsonDocument document = QJsonDocument::fromJson( bytes, &jsonError );
        if (jsonError.error != QJsonParseError::NoError) {
            qDebug() << "fromJson failed:";
            return ;
        }
        if (document.isObject()) {
            QJsonObject jsonObj = document.object();
            QJsonObject Detailsobj = jsonObj.value(DETAILS_STR).toObject();
            QStringList keys = Detailsobj.keys();
            int iArrSize = 0;
            if( keys.contains(ARRSIZE_STR) )
            {
                auto value = Detailsobj.take(ARRSIZE_STR);
                 iArrSize =value.toInt();
            }

            if(iArrSize == 0)
                return ;
            int counter = 0;
            mvArrayOffSet.clear();
            mvArrayOffSet.resize(iArrSize);
            for (auto key: keys) {
                QString qsArrayNumber = QString::number(counter).rightJustified(2, '0');

                if(key == qsArrayNumber )
                {
                       auto value = Detailsobj.take(qsArrayNumber);
                       if(value.isDouble())
                        mvArrayOffSet[counter] = value.toDouble();
                       else
                        mvArrayOffSet[counter] = value.toInt();
                }
                counter++;
               }
        }
    }

}
void ProfileLevelingDialog::SaveAkimaChannel()
{
    CorrectAndSaveChannelUtl("Akima");
}

void ProfileLevelingDialog::SaveLinearChannel()
{
    CorrectAndSaveChannelUtl("Linear");
}

void  ProfileLevelingDialog::SaveRationalChannel()
{
    CorrectAndSaveChannelUtl("Rational");
}

void ProfileLevelingDialog::SavePCHIPChannel()
{
    CorrectAndSaveChannelUtl("PCHIP");
}

//void ProfileLevelingDialog::CorrectAndSaveChannel()
//{
//    //ask the user the name of the channel and the interpolation
//    //CorrectAndSaveChannelUtl("PCHIP");
//}
void ProfileLevelingDialog::CorrectAndSaveChannelUtl(QString qsInterpolatorName)
{
    mInterpName = qsInterpolatorName;
    if (qsInterpolatorName == "Akima") {
        mData = mInterpolator->getAkimaSplineData();
    } else if (qsInterpolatorName == "Linear") {
        mData = mInterpolator->getLinearSplineData();
    } else if (qsInterpolatorName == "Rational") {
        mData = mInterpolator->getRationalSplineData();
    } else {
        mData = mInterpolator->getPCHIPSplineData();
    }

    if (mData.empty()) {
        emit currentProgress("No interpolated data", 0);
        emit currentProgress("No interpolated data", -1);
    }
    if (mData.size() == 1) {
        QThread* thread = new QThread();
        this->moveToThread(thread);

        disconnect(this, &ProfileLevelingDialog::currentProgress, mBar, &ProgressBar::updateProgress);
        connect(this, &ProfileLevelingDialog::currentProgress, mBar, &ProgressBar::updateProgress);

        connect(thread, &QThread::finished, thread, &QObject::deleteLater);
        connect(thread, &QThread::finished, this, [this]() {
            this->moveToThread(QApplication::instance()->thread());
        });
        connect(this, &ProfileLevelingDialog::correctionDone, thread, &QThread::quit);
        connect(thread, &QThread::started, this, &ProfileLevelingDialog::singleChannelCorrection);
        thread->start();
    } else if (mData.size() > 1) {
        QThread* thread = new QThread();
        this->moveToThread(thread);

        disconnect(this, &ProfileLevelingDialog::currentProgress, mBar, &ProgressBar::updateProgress);
        connect(this, &ProfileLevelingDialog::currentProgress, mBar, &ProgressBar::updateProgress);

        connect(thread, &QThread::finished, thread, &QObject::deleteLater);
        connect(thread, &QThread::finished, this, [this]() {
            this->moveToThread(QApplication::instance()->thread());
        });
        connect(this, &ProfileLevelingDialog::correctionDone, thread, &QThread::quit);
        connect(thread, &QThread::started, this, &ProfileLevelingDialog::multiChannelCorrection);
        thread->start();
    } else {
        QString str = "Please interpolate before attempting to write non-existent correction";
        QMessageBox msgBox;
        msgBox.setWindowTitle(tr("Prompt"));
        msgBox.setText(str);
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
        int ret = msgBox.exec();

        switch (ret) {
        case QMessageBox::Ok:
            msgBox.close();
            break;
        default:
            break;
        }
        return;
    }
}
