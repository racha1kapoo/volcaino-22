#ifndef PROFILELEVELINGDIALOG_H
#define PROFILELEVELINGDIALOG_H

#include <QDialog>
#include <QSqlQuery>
#include <QtCharts/QChartView>
#include <QtCharts/QtCharts>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QGraphicsScene>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCore/qglobal.h>
#include <QtWidgets/QGraphicsTextItem>
#include <QMap>
#include <QThread>
#include "MouseEventTracking.h"
#include "TableViewOperation.h"
#include "ShowChartView.h"
#include "DataBase.h"
#include "interpolator.h"
#include "ProgressBar.h"

namespace Ui {
class ProfileLevelingDialog;
}

class ProfileLevelingDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProfileLevelingDialog(QString mTable, QWidget *parent = nullptr);
    ~ProfileLevelingDialog();

    std::vector<QMap<double, double>> &getAssumedZeroes();
    void setMembers(ChanMenuOpData *pdata, std::map<int, QString> ArrayList);

    enum INTERPOLATORTYPE
    {
        AKIMA,
        LINEAR,
        RATIONAL,
        PCIHP
    };

signals:
    void allZoomIn();
    void allZoomOut();
    void allZoomReset();
    void allGoBack();
    void allEnableTrackLine();
    void allAdjustTrackLine();
    void allScroll(double dx, double dy);
    void allRubberBandChanged(QChartView *view, QPointF fp, QPointF tp, int startIndex, int endIndex, bool isYMode);
    void allSaveAxis();
    void currentProgress(QString status, int value);
    void allZoomInX(qreal factor, qreal xcenter);
    void allSetXYRanges(qreal xmin, qreal xmax);
    ///
    /// @brief correctionDone signals to the application that writing of corrected channel to the DB table is done
    ///
    void correctionDone();

private slots:
    void OnFlightNoChanged();
    void OnLineNoChanged();
    void OnLineTypeChanged();
    void UpdateComboBoxData();
    void OnPlotChannel();
    void adjustTrackLine(MouseEventTracking *);
    void ShowSubGraph(int startIndex, int endIndex);
    void getSelectedVals(int startIndex, int endIndex);
    ///
    /// @brief singleChannelCorrection corrects and writes data to DB table for a single channel
    ///
    void singleChannelCorrection();
    ///
    /// @brief multiChannelCorrection corrects and writes data to DB table for array channels
    ///
    void multiChannelCorrection();
    void deleteSelectedVals(int startIndex, int endIndex);
    void SaveSelectedPoints();
    void ImportSelectedPoints();
    void ImportOffSetFile();
    //void CorrectAndSaveChannel();

    void levelChartScroll(double dx, double dy);
    void refChartScroll(double dx, double dy);

    void chartReset();
    void levelChartReset();
    void refChartReset();

    void chartScroll(double dx, double dy);

    void getIniPlotAreasWidth();

    void zoomInX(QChartView *view, QPointF fp, QPointF tp, int startIndex, int endIndex, bool isYMode);

    void setYRange(qreal xmin, qreal xmax, qreal ymin, qreal ymax);
    void setXRange(qreal xmin, qreal xmax);

    void allZoomInAlongX(qreal factor, qreal xcenter);

    void revert();
    void saveAxis();
    void SaveAkimaChannel();
    void SaveLinearChannel();
    void SaveRationalChannel();
    void SavePCHIPChannel();

private:
    void InitUI();
    void ConnectSignalsToSlots();
    void CreateMenuBar();
    ///
    /// @brief drawAkimaSpline draws the modified akima spline to the chart
    ///
    void drawAkimaSpline();
    ///
    /// @brief drawLinearSpline draws the linear spline to the chart
    ///
    void drawLinearSpline();
    ///
    /// @brief drawRationalSpline draws the rational spline to the chart
    ///
    void drawRationalSpline();
    ///
    /// @brief drawPchipSpline draws the PCHIP spline to the chart
    ///
    void drawPchipSpline();
    ///
    /// @brief CorrectAndSaveChannelUtl decides whether the correction is for a single or an array channel
    /// @param qsInterpolatorName is the name of the interpolation method chosen
    ///
    void CorrectAndSaveChannelUtl(QString qsInterpolatorName);

    void alignXAxis();
    void reAlignXAxis();
    void saveAxisScroll();

    QSqlQuery mSql;
    DataBase *mDb;
    QString mqsTable;

    ChartData *mpChart;

    ShowChartView *mpChartView;

    QSplineSeries *mAkimaSpline = nullptr;
    QSplineSeries *mRationalSpline = nullptr;
    QSplineSeries *mPCHIPSpline = nullptr;
    QLineSeries *linSpline = nullptr;

    QLineSeries *mSeries;

    QValueAxis *mAxisY;

    QValueAxis *mAxisX;

    Ui::ProfileLevelingDialog *ui;

    QVBoxLayout* mChartLayout = nullptr;

    MouseEventTracking *mpSubMouse = nullptr;
    ChartData *mpSubChart = nullptr;
    ShowChartView *mpSubChartView = nullptr;

    ChartData *mpReferenceChart;


    ShowChartView *mpReferenceChartView;

    QLineSeries *mReferenceSeries;

    std::map<int,QString> mArrayList;

    ChanMenuOpData *mChartLoadData = nullptr;

    QThread *mThread = nullptr;
    bool mbArray = false;

    int mArraySize;

    //std::vector<QPointF> mSelectedVals;
    std::vector<QMap<double, double>> mSelectedVals;

    MouseEventTracking *mpMouse = nullptr;
    MouseEventTracking *mpMouseRef = nullptr;
    MouseEventTracking *mpMouseSub = nullptr;

    Interpolator *mInterpolator = nullptr;
    QVBoxLayout *mSnippetChartLayout = nullptr;

    void saveIniAxisRanges();
    void resetIniAxisRanges();

    double mIniPlotAreaWidth = -1;

    ProgressBar *mBar = nullptr;

    QString mInterpName;
    std::vector<std::vector<double>> mData;
    std::vector<long> mvArrayOffSet;
	
	QSplitter* qVerticalSplitter;
    QSplitter* qHorizontalSplitter;
    QSplitter* qMainSplitter;
//    double mXiniMin;
//    double mXiniMax;
//    double mYiniMin;
//    double mYiniMax;

//    double mXiniRefMin;
//    double mXiniRefMax;
//    double mYiniRefMin;
//    double mYiniRefMax;
};

#endif // PROFILELEVELINGDIALOG_H
