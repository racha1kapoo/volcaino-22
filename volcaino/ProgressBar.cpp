#include "ProgressBar.h"
#include <QTimer>
ProgressBar::ProgressBar(QWidget *parent): QDialog(parent)
{
    this->setFixedHeight(100);
    this->setFixedWidth(400);
    //this->move(parent->geometry().center());
    this->setWindowFlag(Qt::FramelessWindowHint);
    QVBoxLayout *vLayoutWhole = new QVBoxLayout;
    setLayout(vLayoutWhole);

    mText = new QLabel(this);
    mBar = new QProgressBar(this);

    mBar->setStyleSheet(QString("QProgressBar::chunk {background-color: blue;}"));
    mBar->setTextVisible(false);

    vLayoutWhole->addWidget(mText);
    vLayoutWhole->addWidget(mBar);

    mParent = parent;
}

void ProgressBar::setText(QString text)
{
    if (text != mTextStr) {
        mTextStr = text;
        mText->setText(mTextStr);
    }
}

void ProgressBar::setValue(int value)
{
    if (value != mValue) {
        mValue = value;
        mBar->setValue(mValue);
    }
}

void ProgressBar::updateProgress(QString status, int value)
{
    if (value == 0) {
        this->show();
        this->move(mParent->geometry().center().x() - this->width()/2,
                mParent->geometry().center().y() - this->height()/2);
    }

    if (value == 100) {
        QTimer *timer = new QTimer(this);
        connect(timer, &QTimer::timeout, this, [this, timer]() {
            timer->deleteLater();
            this->hide();
        });
        timer->start(800);
    }

    if(value == -1)
    {
        QTimer *timer = new QTimer(this);
        connect(timer, &QTimer::timeout, this,
                [this,timer](){timer->deleteLater();this->hide();});
        timer->start(800);
    }
    this->setText(status);
    this->setValue(value);
}
