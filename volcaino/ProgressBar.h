#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H
#include <QDialog>
#include <QWidget>
#include <QObject>
#include <QProgressBar>
#include <QLabel>
#include <QVBoxLayout>
class ProgressBar: public QDialog
{
    Q_OBJECT
public:
    ProgressBar(QWidget *parent = nullptr);

    void setText(QString text);
    void setValue(int value);

public slots:
    void updateProgress(QString status, int value);
private:
    QLabel *mText;

    QProgressBar *mBar;

    QWidget *mParent;

    QString mTextStr;
    int mValue;
};

#endif // PROGRESSBAR_H
