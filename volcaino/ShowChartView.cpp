#include "ShowChartView.h"
#include <QtCharts/QChartView>
#include <QtCharts/QtCharts>
#include <QtGui/QResizeEvent>
#include <QtWidgets/QGraphicsScene>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtWidgets/QGraphicsTextItem>
#include <QtGui/QMouseEvent>

ShowChartView::ShowChartView(QChart *chart, QWidget *parent) :
    QChartView(chart, parent),
    m_isTouching(false)
{
    //mIsArrayChan = isArrayChan;

    setRubberBand(QChartView::RectangleRubberBand);

    m_middleLinePen.setStyle(Qt::DashLine);
    m_middleLinePen.setColor(Qt::red);
    m_showLabel = new QLabel(this);
    m_showLabel->hide();
    m_showLabel->setFixedSize(120, 60);
    m_showLabel->setWindowFlag(Qt::ToolTip);
    m_showLabel->setFocusPolicy(Qt::NoFocus);

    rubberBand = this->findChild<QRubberBand *>();
    //rubberBand->setVisible(true);
    //rubberBand->installEventFilter(this);

    mTimer = new QTimer(this);
    //connect(mTimer, &QTimer::timeout, this, &ShowChartView::updateTrackLine);
    connect(mTimer, &QTimer::timeout, this, &ShowChartView::showAllArrays);
    mTimerKey = new QTimer(this);
    connect(mTimerKey, &QTimer::timeout, this, [this](){scrollSpeed = 80;mTimerKey->stop();});
    mTimerKeyRelease = new QTimer(this);

    mChart = dynamic_cast<ChartData *>(chart);

    //this->setChanContextMenu();
    this->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    this->setMinimumSize(700, 150);
    this->setMouseTracking(true);
    this->setRenderHint(QPainter::Antialiasing);

    //m_isLevelingProfile = false;
    //    connect(mChart, &ChartData::plotAreaChanged, this,
    //            [this]()
    //    {
    //        QPointF from = this->getMouseTracker()->getDoubleLIndPos();
    //        QPointF to = this->getMouseTracker()->getDoubleRIndPos();
    //        this->getMouseTracker()->setUpdateDoubleIndicators(from, to);

    //        QPointF singlePos = this->getMouseTracker()->getSingleIndPos();
    //        this->getMouseTracker()->setUpdateSingleIndicator(singlePos);

    //        this->getMouseTracker()->updateMultiSingleRefInds();
    //        this->getMouseTracker()->updateMultiSingleLvlCross();
    //    });
}

ShowChartView::~ShowChartView()
{
    if (mChart) {
        delete mChart;
        mChart = nullptr;
    }
}

bool ShowChartView::viewportEvent(QEvent *event)
{
    if (event->type() == QEvent::TouchBegin) {

        m_isTouching = true;

        chart()->setAnimationOptions(QChart::NoAnimation);
    }

    if (event->type() == QEvent::Resize) {
        //mTimer->start(5);
    }

    return QChartView::viewportEvent(event);
}

void ShowChartView::mousePressEvent(QMouseEvent *event)
{
    if (m_isTouching)
        return;

    if (event->button() == Qt::MiddleButton) {
        mIsPanMode = !mIsPanMode;

        if (mIsPanMode) {
            emit scrollStart();
            viewport()->setCursor(Qt::SizeHorCursor);
            mPreviousPos = QPointF(event->x(), event->y());
        } else {
            emit scrollDone();
            viewport()->setCursor(Qt::ArrowCursor);
        }
    }
    QChartView::mousePressEvent(event);
}

void ShowChartView::wheelEvent(QWheelEvent *event)
{
    if (this->hasFocus()) {
        if (event->angleDelta().y() > 0) // up Wheel
        {
            emit zoomInX(1.1, event->position().x() - chart()->plotArea().x());
            //zoomAlongX(1.1, event->position().x() - chart()->plotArea().x());
        }
        else if (event->angleDelta().y() < 0) //down Wheel
        {
            emit zoomInX(0.9, event->position().x() - chart()->plotArea().x());
            //zoomAlongX(0.9, event->position().x() - chart()->plotArea().x());
        }
    }
}

void ShowChartView::mouseMoveEvent(QMouseEvent *event)
{
    if (m_isTouching)
        return;

    if(mIsPanMode)
    {
        double dx = event->x() - mPreviousPos.x();
        //double dy = event->y() - mPreviousPos.y();

        mPreviousPos.setX(event->x());
        //mPreviousPos.setY(event->y());


        emit scrollSignal(-dx, 0);

    }


    if(mTrack)
    {
        if(mTrack->getTrackerType() == MouseTrackerType::ProfileSingleX
                || mTrack->getTrackerType() == MouseTrackerType::ProfileSingleXShow)
        {
            if(!this->hasFocus()) this->setFocus();
            double index = mChart->mapToValue(event->pos()).x();
            QPointF point(index, 0);

            mTrack->setUpdateSingleIndicator(point);
        }
        else if(mTrack->getTrackerType() == MouseTrackerType::PLMZeroPtSelection)
        {
            if(!this->hasFocus()) this->setFocus();
            double index = mChart->mapToValue(event->pos()).x();
            QPointF point(index, 0);

            mTrack->setUpdateSingleIndicator(point);
        }
    }


    QChartView::mouseMoveEvent(event);
}

void ShowChartView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::RightButton) {
        createContextMenu(event->pos());
        return; //event doesn't go further
    }

    if (rubberBand->isVisible()) {
        QPointF fp = (rubberBand->geometry().topLeft());
        QPointF tp = (rubberBand->geometry().bottomRight());

        double startIndex = mChart->mapToValue(fp).x();
        double endIndex = mChart->mapToValue(tp).x();

        QRectF rec(fp, tp);
        rubberBand->hide();

        if (rec.width() > 10 && rec.height() > 10) {
            if (mMode == rubberMode::zoomYMode) {
                if (mIsPLM) {
                    rec.setLeft(mChart->plotArea().left());
                    rec.setRight(mChart->plotArea().right());
                    mChart->zoomIn(rec);
                    if (mChartType == ChartType::PLMDataNormal
                            || mChartType == ChartType::PLMDataArray
                            || mChartType == ChartType::PLMRefNormal
                            || mChartType == ChartType::PLMRefArray) {
                        if (mChartType == ChartType::PLMDataNormal
                                || mChartType == ChartType::PLMDataArray) {
                            getMouseTracker()->updateMultiSingleLvlCross();
                        }
                        emit plmZoomY();
                    }
                } else {
                    fp.setX(mChart->plotArea().x());
                    tp.setX(mChart->plotArea().x()+mChart->plotArea().width());

                    emit thisRubberBandChanged(this, fp, tp, startIndex, endIndex, true);
                }
            } else if(mMode == rubberMode::selectRangeMode) {
                emit seriesValuesSelects(startIndex, endIndex);
            } else if (mMode == rubberMode::deleteRangeMode) {
                emit deletePoints(startIndex, endIndex);
            } else {  //zoomAll, zoomX
                if (mChartType == ChartType::PLMSubNormal || mChartType == ChartType::PLMSubArray) {
                    mChart->zoomIn(QRectF(fp,tp));
                } else if (mChartType == ChartType::PLMDataNormal || mChartType == ChartType::PLMDataArray) {
                    emit thisRubberBandChanged(this, fp, tp, startIndex, endIndex, false);
                } else {
                    emit thisRubberBandChanged(this, fp, tp, startIndex, endIndex, false);
                }
            }
        }
    }

    if (m_isTouching)
        m_isTouching = false;

    //chart()->setAnimationOptions(QChart::SeriesAnimations);

    QChartView::mouseReleaseEvent(event);
}


void ShowChartView::keyPressEvent(QKeyEvent *event)
{
    //QCoreApplication::processEvents();
    switch (event->key())
    {
    case Qt::Key_Plus:
        chart()->zoomIn();
        break;
    case Qt::Key_Minus:
        chart()->zoomOut();
        break;
    case Qt::Key_Escape:
    {
        if(mChartType == ChartType::PLMSubNormal
                || mChartType == ChartType::PLMSubArray)
        {chart()->zoomReset();}

        else emit thisZoomReset();
        //chart()->zoomReset();
        break;
    }
    case Qt::Key_Left:
//        if(!mTimerKey->isActive())
//        {
//            //keyPressed = true;
//            mTimerKey->start(2000);
//        }
        emit scrollSignal(-scrollSpeed,0);
        //qDebug() << scrollSpeed;
        break;
    case Qt::Key_Right:
//        if(!mTimerKey->isActive())
//        {
//            //keyPressed = true;
//            mTimerKey->start(2000);
//        }
        emit scrollSignal(scrollSpeed,0);
        break;
    case Qt::Key_Up:
//        if(!mTimerKey->isActive())
//        {
//            //keyPressed = true;
//            mTimerKey->start(2000);
//        }
        emit scrollSignal(0,20);
        break;
    case Qt::Key_Down:
//        if(!mTimerKey->isActive())
//        {
//            //keyPressed = true;
//            mTimerKey->start(2000);
//        }
        emit scrollSignal(0,-20);
        break;
    case Qt::Key_D:
    {
        if(mTrack->getTrackerType() == MouseTrackerType::ProfileSingleXY
                || mTrack->getTrackerType() == MouseTrackerType::ProfileSingleX
                || mTrack->getTrackerType() == MouseTrackerType::ProfileSingleXShow)
        {
            emit seriesValSelect(mTrack->seriesIndex);
        }
        else if(mTrack->getTrackerType() == MouseTrackerType::PLMZeroPtSelection)
        {
            emit seriesValuesSelects(mTrack->seriesIndex, mTrack->seriesIndex);
        }
        //        else if(mTrack->getEnableIndicator())
        //        {
        //            if(!m_isLevelingProfile && mIsArrayChan)
        //                emit seriesValSelect(mTrack->seriesIndex);
        //            else
        //                emit seriesValuesSelects(mTrack->lastX, mTrack->lastX);
        //        }
        break;
    }
    default:
        QGraphicsView::keyPressEvent(event);
        break;
    }
}

void ShowChartView::keyReleaseEvent(QKeyEvent *event)
{
//   if(event->key() == Qt::Key_Left || event->key() == Qt::Key_Right
//      || event->key() == Qt::Key_Up || event->key() == Qt::Key_Down)
//   {
//       mTimerKeyRelease->start(100);
//       connect(mTimerKeyRelease, &QTimer::timeout, this,
//               [this]()
//               {
//                qDebug() << "Key release!";
//                scrollSpeed = 20;
//                mTimerKeyRelease->stop();
//                mTimerKey->stop();
//              });

//   }
}

void ShowChartView::createContextMenu(const QPoint &pos)
{
    menu = new QMenu(this);

    switch(mChartType)
    {
    case ProfileNormal:
    {
        menu->addAction(reset);
        menu->addAction(goBack);
        menu->addAction(zoomAll);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(setY);
        menu->addAction(trackL);
        menu->addAction(selectTableRange);
        menu->addAction(exit);
        break;
    }
    case ProfileArray:
    {
        menu->addAction(reset);
        menu->addAction(goBack);
        menu->addAction(zoomAll);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(setY);
        menu->addAction(dispAll);
        menu->addAction(hideArr);
        menu->addAction(highlightArr);
        menu->addAction(trackL);
        menu->addAction(selectTableRange);
        menu->addAction(exit);
        break;
    }
    case PLMSubNormal:
    {
        menu->addAction(reset);
        menu->addAction(zoomAll);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(selectPLMDataRange);
        menu->addAction(selectPLMDataSingle);
        break;
    }
    case PLMSubArray:
    {
        menu->addAction(reset);
        menu->addAction(zoomAll);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(dispAll);
        menu->addAction(hideArr);
        menu->addAction(highlightArr);
        menu->addAction(selectPLMDataRange);
        menu->addAction(selectPLMDataSingle);
        break;
    }
    case PLMDataNormal:
    {
        menu->addAction(reset);
        menu->addAction(goBack);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(setY);
        menu->addAction(deletePt);
        menu->addAction(selectPLMSub);
        break;
    }
    case PLMDataArray:
    {
        menu->addAction(reset);
        menu->addAction(goBack);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(setY);
        menu->addAction(dispAll);
        menu->addAction(hideArr);
        menu->addAction(highlightArr);
        menu->addAction(deletePt);
        menu->addAction(selectPLMSub);
        break;
    }
    case PLMRefNormal:
    {
        menu->addAction(reset);
        menu->addAction(goBack);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(setY);
        menu->addAction(selectPLMSub);
        break;
    }
    case PLMRefArray:
    {
        menu->addAction(reset);
        menu->addAction(goBack);
        menu->addAction(zoomX);
        menu->addAction(zoomY);
        menu->addAction(setY);
        menu->addAction(dispAll);
        menu->addAction(hideArr);
        menu->addAction(highlightArr);
        menu->addAction(selectPLMSub);
        break;
    }
    default:
        break;
    }

    menu->exec(QCursor::pos());
}

void ShowChartView::zoomAlongX(qreal factor, qreal xcenter)
{
    QRectF rect = mChart->plotArea();
    qreal widthOriginal = rect.width();
    rect.setWidth(widthOriginal / factor);
    qreal centerScale = (xcenter / widthOriginal);

    qreal leftOffset = (xcenter - (rect.width() * centerScale) );

    rect.moveLeft(rect.x() + leftOffset);
    mChart->zoomIn(rect);
}

void ShowChartView::setRubberMode(rubberMode mode)
{
    mMode = mode;
}

void ShowChartView::setChartType(ChartType type)
{
    mChartType = type;

    if (mChartType == ChartType::PLMSubNormal
            || mChartType == ChartType::PLMSubArray
            || mChartType == ChartType::PLMDataNormal
            || mChartType == ChartType::PLMDataArray
            || mChartType == ChartType::PLMRefNormal
            || mChartType == ChartType::PLMRefArray) {
        mIsPLM = true;
        mMode= rubberMode::selectRangeMode;
        setRubberBand(QChartView::HorizontalRubberBand);
    } else {
        mIsPLM = false;
    }
}

ChartType ShowChartView::getChartType()
{
    return mChartType;
}


void ShowChartView::setChanContextMenu()
{
    reset = new QAction(tr("Reset"),this);
    trackL = new QAction(tr("TrackLine"),this);
    goBack = new QAction(tr("Last"),this);
    dispAll = new QAction(tr("DisplayAll"),this);
    hideArr = new QAction(tr("Hide"),this);
    zoomAll = new QAction(tr("ZoomAll"),this);
    zoomX = new QAction(tr("ZoomX"),this);
    zoomY = new QAction(tr("ZoomY"),this);
    selectTableRange = new QAction(tr("SelectRange"),this);
    selectPLMDataRange = new QAction(tr("SelectMulti"),this);
    selectPLMDataSingle = new QAction(tr("SelectSingle"),this);
    selectPLMSub = new QAction(tr("SubChart"),this);
    highlightArr = new QAction(tr("Highlight"),this);
    deletePt = new QAction(tr("DeletePts"),this);
    setY = new QAction(tr("SetXYRange"),this);
    exit = new QAction(tr("Close"),this);


    switch(mChartType)
    {
    case ProfileNormal:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            QPointF previous = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(previous);
            emit this->thisZoomReset();
        });
        connect(trackL, &QAction::triggered, this, [this]()
        {
            this->getMouseTracker()->setTrackerType(MouseTrackerType::ProfileSingleX);
        });
        connect(goBack, &QAction::triggered, this, [this]()
        {
            emit this->backToLast();
        });
        connect(zoomAll, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomAll";
            mChart->setTitle(title);
            mMode= rubberMode::zoomAllMode;
            this->setRubberBand(QChartView::RectangleRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypesProfileViewer);

        });
        connect(zoomX, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomX";
            mChart->setTitle(title);
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypesProfileViewer);

        });

        connect(zoomY, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomY";
            mChart->setTitle(title);
            mMode=rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypesProfileViewer);

        });
        connect(selectTableRange, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - Select Range";
            mChart->setTitle(title);
            this->getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);

        });
        connect(setY, &QAction::triggered, this, &ShowChartView::setYDialog);

        connect(exit, &QAction::triggered, this, [this](){this->deleteLater();emit chartDeleted();});
        break;
    }
    case ProfileArray:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            QPointF previous = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(previous);
            emit this->thisZoomReset();
        });
        connect(trackL, &QAction::triggered, this, [this]()
        {
            this->getMouseTracker()->setTrackerType(MouseTrackerType::ProfileSingleX);
        });
        connect(goBack, &QAction::triggered, this, [this]()
        {
            emit this->backToLast();
        });

        connect(dispAll, &QAction::triggered, this, [this]()
        {
            int count = mChart->series().length();
            for(int i = 0; i < count; i++)
            {
                mChart->series().at(i)->show();
            }
        });

        connect(hideArr, &QAction::triggered, this, &ShowChartView::hideArrDialog);
        connect(highlightArr, &QAction::triggered, this, &ShowChartView::highlightArrDialog);

        connect(zoomAll, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomAll";
            mChart->setTitle(title);
            mMode= rubberMode::zoomAllMode;
            this->setRubberBand(QChartView::RectangleRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypesProfileViewer);

        });

        connect(zoomX, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomX";
            mChart->setTitle(title);
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypesProfileViewer);

        });

        connect(zoomY, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomY";
            mChart->setTitle(title);
            mMode=rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypesProfileViewer);

        });
        connect(selectTableRange, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - Select Range";
            mChart->setTitle(title);
            this->getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);

        });
        connect(setY, &QAction::triggered, this, &ShowChartView::setYDialog);
        connect(exit, &QAction::triggered, this, [this](){this->deleteLater();});
        break;
    }
    case PLMSubNormal:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            mChart->zoomReset();
            QPointF previous = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(previous);
        });

        connect(zoomAll, &QAction::triggered, this, [this]()
        {
            mMode= rubberMode::zoomAllMode;
            this->setRubberBand(QChartView::RectangleRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);
        });

        connect(zoomX, &QAction::triggered, this, [this]()
        {
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(zoomY, &QAction::triggered, this, [this]()
        {
            mMode= rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(selectPLMDataRange, &QAction::triggered, this, [this]()
        {
            this->getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(selectPLMDataSingle, &QAction::triggered, this, [this]()
        {
            this->getMouseTracker()->setTrackerType(MouseTrackerType::PLMZeroPtSelection);
            //this->setRubberBand(QChartView::HorizontalRubberBand);
        });

        break;
    }
    case PLMSubArray:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            mChart->zoomReset();
            QPointF previous = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(previous);
        });

        connect(zoomAll, &QAction::triggered, this, [this]()
        {
            mMode= rubberMode::zoomAllMode;
            this->setRubberBand(QChartView::RectangleRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(zoomX, &QAction::triggered, this, [this]()
        {
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(zoomY, &QAction::triggered, this, [this]()
        {
            mMode= rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(dispAll, &QAction::triggered, this, [this]()
        {
            int count = mChart->series().length();
            for(int i = 0; i < count; i++)
            {
                mChart->series().at(i)->show();
            }
        });

        connect(hideArr, &QAction::triggered, this, &ShowChartView::hideArrDialog);
        connect(highlightArr, &QAction::triggered, this, &ShowChartView::highlightArrDialog);

        connect(selectPLMDataRange, &QAction::triggered, this, [this]()
        {
            this->getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
            getMouseTracker()->setTrackerType(MouseTrackerType::NoTrackerTypes);

        });

        connect(selectPLMDataSingle, &QAction::triggered, this, [this]()
        {
            this->getMouseTracker()->setTrackerType(MouseTrackerType::PLMZeroPtSelection);
            //mMode= rubberMode::selectRangeMode;
            //this->setRubberBand(QChartView::HorizontalRubberBand);
        });

        break;
    }
    case PLMDataNormal:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            emit this->thisZoomReset();
        });
        connect(goBack, &QAction::triggered, this, [this]()
        {
            emit this->backToLast();
        });
        connect(zoomX, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomX";
            mChart->setTitle(title);
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(zoomY, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomY";
            mChart->setTitle(title);
            mMode= rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
        });
        connect(deletePt, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - Delete Pts";
            mChart->setTitle(title);
            mMode= rubberMode::deleteRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(selectPLMSub, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - SelectSub";
            mChart->setTitle(title);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(setY, &QAction::triggered, this, &ShowChartView::setYDialog);

        break;
    }
    case PLMDataArray:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            emit this->thisZoomReset();

        });
        connect(goBack, &QAction::triggered, this, [this]()
        {
            emit this->backToLast();
        });
        connect(zoomX, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomX";
            mChart->setTitle(title);
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(zoomY, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomY";
            mChart->setTitle(title);
            mMode= rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
        });
        connect(dispAll, &QAction::triggered, this, [this]()
        {
            int count = mChart->series().length();
            for(int i = 0; i < count; i++)
            {
                mChart->series().at(i)->show();
            }
        });

        connect(hideArr, &QAction::triggered, this, &ShowChartView::hideArrDialog);
        connect(highlightArr, &QAction::triggered, this, &ShowChartView::highlightArrDialog);
        connect(deletePt, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - Delete Pts";
            mChart->setTitle(title);
            mMode= rubberMode::deleteRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(selectPLMSub, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - SelectSub";
            mChart->setTitle(title);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(setY, &QAction::triggered, this, &ShowChartView::setYDialog);

        break;
    }
    case PLMRefNormal:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            emit this->thisZoomReset();
        });
        connect(goBack, &QAction::triggered, this, [this]()
        {
            emit this->backToLast();
        });
        connect(zoomX, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomX";
            mChart->setTitle(title);
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(zoomY, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomY";
            mChart->setTitle(title);
            mMode= rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
        });
        connect(selectPLMSub, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - SelectSub";
            mChart->setTitle(title);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(setY, &QAction::triggered, this, &ShowChartView::setYDialog);

        break;
    }
    case PLMRefArray:
    {
        connect(reset, &QAction::triggered, this, [this]()
        {
            emit this->thisZoomReset();
        });
        connect(goBack, &QAction::triggered, this, [this]()
        {
            emit this->backToLast();
        });
        connect(zoomX, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomX";
            mChart->setTitle(title);
            mMode= rubberMode::zoomXMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(dispAll, &QAction::triggered, this, [this]()
        {
            int count = mChart->series().length();
            for(int i = 0; i < count; i++)
            {
                mChart->series().at(i)->show();
            }
        });

        connect(hideArr, &QAction::triggered, this, &ShowChartView::hideArrDialog);
        connect(highlightArr, &QAction::triggered, this, &ShowChartView::highlightArrDialog);
        connect(zoomY, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - ZoomY";
            mChart->setTitle(title);
            mMode= rubberMode::zoomYMode;
            this->setRubberBand(QChartView::VerticalRubberBand);
        });
        connect(selectPLMSub, &QAction::triggered, this, [this]()
        {
            QString title=mTitle+" - SelectSub";
            mChart->setTitle(title);
            mMode= rubberMode::selectRangeMode;
            this->setRubberBand(QChartView::HorizontalRubberBand);
        });
        connect(setY, &QAction::triggered, this, &ShowChartView::setYDialog);

        break;
    }
    default:
        break;
    }
}

void ShowChartView::setTitle(QString title)
{
    QFont font;
    font.setPixelSize(18);
    mChart->setTitleFont(font);

    mTitle = title;

    if(mChartType == ChartType::PLMDataArray || mChartType == ChartType::PLMDataNormal ||
            mChartType == ChartType::PLMRefArray || mChartType == ChartType::PLMRefNormal)
    {
        title+=" - SelectSub";
    }
    else
    {
        title+=" - ZoomAll";
    }

    mChart->setTitle(title);
}


void ShowChartView::setMouseTracker(MouseEventTracking *mouseTracking)
{
    mTrack = mouseTracking;
}

MouseEventTracking *ShowChartView::getMouseTracker()
{
    return mTrack;
}


ChartData *ShowChartView::getChart()
{
    return mChart;
}

void ShowChartView::setSelectedCol(int col)
{
    mSelectedCol = col;
}

void ShowChartView::zoomInFunc(QChartView *view, QPointF fp, QPointF tp, double startIndex, double endIndex, bool isYMode)
{
    if (isYMode) {
        if (view == this) {
            mChart->zoomIn(QRectF(fp,tp));

            QPointF singlePos = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(singlePos);
        }
    } else {
        if (view == this) {
            fp.setX(mChart->mapToPosition(QPointF(startIndex,0)).x());
            tp.setX(mChart->mapToPosition(QPointF(endIndex,0)).x());


            if (mMode == rubberMode::zoomXMode) {
                fp.setY(mChart->plotArea().y());
                tp.setY(mChart->plotArea().y()+mChart->plotArea().height());
            }

            mChart->zoomIn(QRectF(fp,tp));
            QPointF from = getMouseTracker()->getDoubleLIndPos();
            QPointF to = getMouseTracker()->getDoubleRIndPos();
            getMouseTracker()->setUpdateDoubleIndicators(from, to);

            QPointF singlePos = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(singlePos);
        } else {
            fp.setX(mChart->mapToPosition(QPointF(startIndex,0)).x());
            tp.setX(mChart->mapToPosition(QPointF(endIndex,0)).x());

            fp.setY(mChart->plotArea().y());
            tp.setY(mChart->plotArea().y()+mChart->plotArea().height());

            mChart->zoomIn(QRectF(fp,tp));

            QPointF from = getMouseTracker()->getDoubleLIndPos();
            QPointF to = getMouseTracker()->getDoubleRIndPos();
            getMouseTracker()->setUpdateDoubleIndicators(from, to);

            QPointF singlePos = getMouseTracker()->getSingleIndPos();
            getMouseTracker()->setUpdateSingleIndicator(singlePos);
        }
    }

    mChart->setIniAxis();
}

void ShowChartView::updateTrackLine()
{
    mTimer->stop();
    if (mTrack)
        mTrack->setUpdateSingleIndicator(QPointF(mTrack->lastX, mTrack->lastY));
}

void ShowChartView::showAllArrays()
{
    mTimer->stop();
    mHighLightCount++;

    if (mHighLightCount % 2 != 0) {
        for (int i = 0; i < mHighlightArrs.size(); i++)
            mChart->getArrayLineSeries()[mHighlightArrs[i]]->show();
        if (mHighLightCount < 7)
            mTimer->start(800);
    } else {
        for (int i = 0; i < mHighlightArrs.size(); i++) {
            mChart->getArrayLineSeries()[mHighlightArrs[i]]->hide();
            //mChart->legend()->markers(mChart->getArrayLineSeries()[mHighlightArrs[i]])[mHighlightArrs[i]]->setVisible(true);
            //qDebug() << mChart->legend()->markers(mChart->getArrayLineSeries()[mHighlightArrs[i]])[mHighlightArrs[i]];
        }

        if (mHighLightCount < 7)
            mTimer->start(300);
    }
}

void ShowChartView::hideArrDialog()
{
    int count = mChart->series().length();
    QDialog *hideArr = new QDialog(this);
    hideArr->setAttribute(Qt::WA_DeleteOnClose);
    //hideArr->setWindowFlags(Qt::FramelessWindowHint);
    hideArr->setStyleSheet("background-color:white;");

    QVBoxLayout *vLayout = new QVBoxLayout(hideArr);
    QString chartTitle = mChart->title();
    QLabel *lable = nullptr;
    if(chartTitle.isEmpty())
    {
        lable = new QLabel("Select array numbers to hide",hideArr);
    }
    else
    {
        lable = new QLabel(chartTitle+": Select array numbers to hide",hideArr);
    }
    QLineEdit *lineEdit = new QLineEdit(hideArr);

    lineEdit->setFixedWidth(300);

    QHBoxLayout *hLayout = new QHBoxLayout;

    QPushButton *hide = new QPushButton(hideArr);
    hide->setFixedHeight(lineEdit->height());
    hide->setFixedWidth(hide->height()*3);
    hide->setText("Hide");
    connect(hide, &QPushButton::clicked, this,
            [lineEdit,hideArr,count,this]()
    {
        if(!lineEdit->text().isEmpty()){

            bool ok = false;
            bool status = true;

            QStringList arrs = lineEdit->text().split(",");
            QList<int> arrayNos;
            for(QString str: arrs)
            {
                //int arrayNo;
                if(str.contains("~"))
                {
                    QStringList arrs = str.split("~");
                    int min = arrs[0].toInt(&ok);
                    if(!ok)  status = false;
                    int max = arrs[1].toInt(&ok);
                    if(!ok)  status = false;

                    if(min < 1 || max < 1 || max < min)  status = false;
                    if(min-1>mArrayCount||max-1>mArrayCount) status = false;
                    for(int i = min; i <= max; i++)
                    {
                        arrayNos.append(i);
                    }

                }
                else
                {
                    int arrayNo = str.toInt(&ok);
                    if(!ok)  status = false;

                    if(arrayNo < 1 || arrayNo-1>mArrayCount)  status = false;
                    arrayNos.append(arrayNo);
                }

            }

            if(status)
            {
                for(int arrayNo: arrayNos)
                {
                    if(arrayNo > 0 && arrayNo <= mArrayCount)
                        mChart->series().at(arrayNo-1)->hide();
                }
                hideArr->close();
            }
            else
            {
                QApplication::beep();
            }
        };
    });


    QPushButton *cancel = new QPushButton(hideArr);
    cancel->setFixedHeight(lineEdit->height());
    cancel->setFixedWidth(cancel->height()*3);
    cancel->setText("Cancel");
    connect(cancel, &QPushButton::clicked, this, [hideArr](){hideArr->close();});


    hLayout->addWidget(hide);
    hLayout->addWidget(cancel);

    vLayout->addWidget(lable);
    vLayout->addWidget(lineEdit);
    vLayout->addLayout(hLayout);

    hideArr->setMinimumWidth(lineEdit->width());
    hideArr->setMinimumHeight(lineEdit->height());
    hideArr->move(hideArr->parentWidget()->x()+hideArr->parentWidget()->width()/2,
                  hideArr->parentWidget()->y()+hideArr->parentWidget()->height()/2);

    QPointF global = this->mapToGlobal(QPoint(0,0));
    hideArr->move(global.x(), global.y());
    hideArr->show();

}

void ShowChartView::setYDialog()
{
    //int count = mChart->series().length();
    QDialog *setY = new QDialog(this);
    setY->setAttribute(Qt::WA_DeleteOnClose);
    //hideArr->setWindowFlags(Qt::FramelessWindowHint);
    setY->setStyleSheet("background-color:white;");

    QVBoxLayout *vLayout = new QVBoxLayout(setY);
    QString chartTitle = mChart->title();

    QLabel *lableX = new QLabel("Set X range",setY);
    QLineEdit *lineEditX = new QLineEdit(setY);
    lineEditX->setFixedWidth(300);

    QLabel *lableY = new QLabel("Set Y range",setY);
    QLineEdit *lineEditY = new QLineEdit(setY);
    lineEditY->setFixedWidth(300);


    QHBoxLayout *hLayout = new QHBoxLayout;

    QPushButton *select = new QPushButton(setY);
    select->setFixedHeight(lineEditX->height());
    select->setFixedWidth(setY->height()*3);
    select->setText("Set");

    connect(select, &QPushButton::clicked, this,[this,lineEditX,lineEditY,setY]()
    {
        double xMin = -1, xMax = -1, yMin = -1, yMax = -1;

        if(!lineEditX->text().isEmpty())
        {
            QString max = lineEditX->text().split(",")[1];
            QString min = lineEditX->text().split(",")[0];

            xMin = min.toDouble();
            xMax = max.toDouble();
            //mChart->axes(Qt::Horizontal).back()->setRange(xMin, xMax);

            QPointF from = mTrack->getDoubleLIndPos();
            QPointF to = mTrack->getDoubleRIndPos();
            mTrack->setUpdateDoubleIndicators(from, to);

            QPointF singlePos = mTrack->getSingleIndPos();
            mTrack->setUpdateSingleIndicator(singlePos);

            //mChart->getYAxisRanges(updateType::update);
        }

        if(!lineEditY->text().isEmpty())
        {
            QString max = lineEditY->text().split(",")[1];
            QString min = lineEditY->text().split(",")[0];

            yMin = min.toDouble();
            yMax = max.toDouble();

            //mChart->axes(Qt::Vertical).back()->setRange(min.toDouble(), max.toDouble());

            QPointF from = mTrack->getDoubleLIndPos();
            QPointF to = mTrack->getDoubleRIndPos();
            mTrack->setUpdateDoubleIndicators(from, to);

            QPointF singlePos = mTrack->getSingleIndPos();
            mTrack->setUpdateSingleIndicator(singlePos);

            //mChart->getYAxisRanges(updateType::update);

        }
        setY->close();
        emit setXYRanges(xMin, xMax, yMin, yMax);
    });

    QPushButton *cancel = new QPushButton(setY);
    cancel->setFixedHeight(lineEditX->height());
    cancel->setFixedWidth(cancel->height()*3);
    cancel->setText("Cancel");
    connect(cancel, &QPushButton::clicked, this, [setY](){setY->close();});

    hLayout->addWidget(select);
    hLayout->addWidget(cancel);

    vLayout->addWidget(lableX);
    vLayout->addWidget(lineEditX);
    vLayout->addWidget(lableY);
    vLayout->addWidget(lineEditY);
    vLayout->addLayout(hLayout);

    setY->setMinimumWidth(lineEditX->width());
    setY->setMinimumHeight(lineEditX->height());
    setY->move(setY->parentWidget()->x()+setY->parentWidget()->width()/2,
               setY->parentWidget()->y()+setY->parentWidget()->height()/2);

    QPointF global = this->mapToGlobal(QPoint(0,0));
    setY->move(global.x(), global.y());
    setY->show();
}

void ShowChartView::highlightArrDialog()
{
    int count = mChart->series().length();
    QDialog *highlightArr = new QDialog(this);
    highlightArr->setAttribute(Qt::WA_DeleteOnClose);
    highlightArr->setStyleSheet("background-color:white;");

    QVBoxLayout *vLayout = new QVBoxLayout(highlightArr);
    QString chartTitle = mChart->title();
    QLabel *lable;

    if (chartTitle.isEmpty()) {
        lable = new QLabel("Select array numbers to highlight",highlightArr);
    } else {
        lable = new QLabel(chartTitle+": Select array numbers to highlight",highlightArr);
    }

    QLineEdit *lineEdit = new QLineEdit(highlightArr);

    lineEdit->setFixedWidth(300);

    QHBoxLayout *hLayout = new QHBoxLayout;

    QPushButton *hide = new QPushButton(highlightArr);
    hide->setFixedHeight(lineEdit->height());
    hide->setFixedWidth(hide->height()*3);
    hide->setText("Show");

    mHighLightCount = 0;
    mHighlightArrs.clear();

    connect(hide, &QPushButton::clicked, this,
            [lineEdit,highlightArr,this]()
    {
        //mChart->legend()->hide();
        if (!lineEdit->text().isEmpty()) {
            QStringList arrs = lineEdit->text().split(",");
            for (QString str: arrs) {
                int arrayNo = str.toInt();
                if (arrayNo > 0 && arrayNo <= mArrayCount)
                    mHighlightArrs.push_back(arrayNo-1);
            }

            //mChart->legend()->hide();
            for (int i = 0; i < mHighlightArrs.size(); i++) {
                mChart->getArrayLineSeries()[mHighlightArrs[i]]->hide();
                //mChart->legend()->markers(mChart->getArrayLineSeries()[mHighlightArrs[i]])[mHighlightArrs[i]]->setVisible(true);
            }
        };
        mTimer->start(300);
        highlightArr->close();
    });

    QPushButton *cancel = new QPushButton(highlightArr);
    cancel->setFixedHeight(lineEdit->height());
    cancel->setFixedWidth(cancel->height()*3);
    cancel->setText("Cancel");
    connect(cancel, &QPushButton::clicked, this, [highlightArr]() {
        highlightArr->close();
    });

    hLayout->addWidget(hide);
    hLayout->addWidget(cancel);

    vLayout->addWidget(lable);
    vLayout->addWidget(lineEdit);
    vLayout->addLayout(hLayout);

    highlightArr->setMinimumWidth(lineEdit->width());
    highlightArr->setMinimumHeight(lineEdit->height());

    QPointF global = this->mapToGlobal(QPoint(0,0));
    highlightArr->move(global.x(), global.y());

    highlightArr->show();
}

void ShowChartView::move(double dx, double dy)
{
    mChart->scroll(dx, dy);
}

void ShowChartView::selectOnChart(int col, int row)
{
    if (col == mSelectedCol) {
        QPointF selPt(row, 0);
        mTrack->setTrackerType(MouseTrackerType::ProfileSingleXShow);
        mTrack->setUpdateSingleIndicator(selPt);
    }
}

void ShowChartView::selectMultiCellsOnChart(int col, int startRow, int endRow)
{
    if (col == mSelectedCol) {
        QPointF pointStart(startRow, 0);
        QPointF pointEnd(endRow, 0);
        mTrack->showIniDoubleIndicators();
        mTrack->setUpdateDoubleIndicators(pointStart, pointEnd);
        //mTrack->addRangeLines(pointStart, pointEnd);
    }
}

void ShowChartView::clearChartViewTrackers()
{
    mTrack->clearTrackers();
}
