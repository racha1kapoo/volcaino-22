/**
 *  @file   ShowChartView.h
 *  @brief  This class is responsible for plotting chart, setting its properties and defining functions when any key press is detected.
 *          X & Y values of the chart.
 *  @author Rachana Kapoor
 *  @date   ‎May ‎3, ‎2022
 ***********************************************/

#ifndef SHOWCHARTVIEW_H
#define SHOWCHARTVIEW_H
#include <QtCharts/QChartView>
#include <QtWidgets/QGraphicsView>
#include <QtCharts/QChartGlobal>
#include "ChannelMenuOperation.h"
#include "TableViewOperation.h"
#include <QPen>
#include <QLabel>
#include <QMenu>
#include <QAction>
#include <QTimer>
#include "ChartData.h"
QT_BEGIN_NAMESPACE
class QGraphicsSceneMouseEvent;
QT_END_NAMESPACE

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class ShowChartView:public QChartView
{
    Q_OBJECT

public:
    ///Class constructor
    ShowChartView(QChart *chart, QWidget *parent = 0);


    ~ShowChartView();


    void setChanContextMenu();

    void setTitle(QString title);


    void setMouseTracker(MouseEventTracking *);
    MouseEventTracking *getMouseTracker();


    //void setIsLevelingProfile(bool bVal);


    ChartData *getChart();


    //void setIsArrayChannel(bool bVal) {mIsArrayChan = bVal;}
    void setArrayCount(int iCount){mArrayCount=iCount;}

    void setSelectedCol(int col);

    void setRubberMode(rubberMode mode);

    void setChartType(ChartType type);

    ChartType getChartType();

    void zoomAlongX(qreal factor, qreal xcenter);


public slots:

    void zoomInFunc(QChartView *view, QPointF fp, QPointF tp, double startIndex, double endIndex, bool isYMode);

    void updateTrackLine();

    void showAllArrays();

    void hideArrDialog();


    void highlightArrDialog();

    void move(double dx, double dy);

    void selectOnChart(int col, int row);

    void selectMultiCellsOnChart(int col, int startRow, int endRow);

    void clearChartViewTrackers();

    void setYDialog();

protected:
    /// @brief An overloaded function is invoked when mouse move event is detected over chart/graphics.
    /// @param  event: An instance of QMouseEvent
    /// @return void
    void mouseMoveEvent(QMouseEvent *event) override;

    /// @brief This function sets the animation of the chart whenever event is detected.
    /// @param  event: An instance of QEvent
    /// @return void
    bool viewportEvent(QEvent *event) override;


    /// @brief An overloaded function is invoked when mouse press event is detected over chart/graphics.
    /// @param  event: An instance of QMouseEvent
    /// @return void
    void mousePressEvent(QMouseEvent *event) override;

    void wheelEvent(QWheelEvent* event) override;


    //void  mouseDoubleClickEvent(QMouseEvent *event) override;

    /// @brief An overloaded function is invoked when mouse release event is detected over chart/graphics.
    /// @param  event: An instance of QMouseEvent
    /// @return void
    void mouseReleaseEvent(QMouseEvent *event) override;

    /// @brief An overloaded function is invoked when key press event is detected over chart/graphics.
    /// @param  event: An instance of QKeyEvent
    /// @return void
    void keyPressEvent(QKeyEvent *event) override;

    void keyReleaseEvent(QKeyEvent *event) override;

private:

    void createContextMenu(const QPoint &pos);

    bool m_isTouching;
    QTimer *m_timer;
    QLabel *m_showLabel;
    QGraphicsLineItem *m_lineItem;
    QList<QPointF> m_xList;
    QPen m_middleLinePen;
    QPoint m_middlePos;
    QMenu *menu;
    QAction *reset;
    QAction *trackL;
    QAction *goBack;
    QAction *dispAll;
    QAction *hideArr;
    QAction *zoomAll;
    QAction *zoomX;
    QAction *zoomY;
    QAction *selectTableRange;
    QAction *selectPLMDataRange;
    QAction *selectPLMDataSingle;
    QAction *selectPLMSub;
    QAction *highlightArr;
    QAction *deletePt;
    QAction *setY;
    QAction *exit;

    ChartData *mChart = nullptr;
    QRubberBand *rubberBand = nullptr;

    MouseEventTracking *mTrack = nullptr;

    QTimer *mTimer;

    QTimer *mTimerKey;
    QTimer *mTimerKeyRelease;
    bool keyPressed = false;
    bool mIsPLM = false;
    bool mIsPLMSubChart = false;

    int mRubberX;
    int mRubberY;

    bool isZoomX = false;
    bool isZoomY = false;
    bool isSelect = false;

    rubberMode mMode;

    QString mTitle;

    //bool mIsArrayChan = false;

    int mArrayCount = 0;

    bool mIsPanMode = false;

    QPointF mPreviousPos;

    QList<int> mHighlightArrs;

    int mSelectedCol = 0;

    int mHighLightCount = 0;

    ChartType mChartType;

    int scrollSpeed = 60;
signals:
    void enableTrackLine();
    void displayAll();

    void thisZoomIn();
    //void thisZoomOut();
    void thisZoomReset();

    void backToLast();
    void plmZoomY();

    void thisRubberBandChanged(ShowChartView *view, QPointF fp, QPointF tp, double startIndex, double endIndex, bool isYMode);
    void chartClose();

    void seriesValSelect(int index);

    void seriesValuesSelects(int startIndex, int endIndex);

    void deletePoints(int startIndex, int endIndex);

    void scrollSignal(double dx, double dy);

    void scrollStart();
    void scrollDone();

    void chartDeleted();

    void zoomInX(qreal factor, qreal xcenter);

    void setXYRanges(qreal xmin, qreal xmax, qreal ymin, qreal ymax);
};

#endif // SHOWCHARTVIEW_H
