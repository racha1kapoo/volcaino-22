/**
 *  @file   SqlDataTableModel.h
 *  @brief  This class acts as a interface for executing SQL statements and traversing the result set.It is built on top of the lower-level QSqlQuery
 *          and can be used to provide data to view classes such as QTableView. The model is read-only by default. To make it read-write, you must subclass
 *          it and reimplement setData() and flags().
 *  @author Rachana Kapoor
 *  @date   ‎May ‎3, ‎2022
 ***********************************************/

#ifndef SQLDATATABLEMODEL_H
#define SQLDATATABLEMODEL_H
#include <QObject>
#include <QSqlQueryModel>


class SqlDataTableModel: public QSqlQueryModel
{
    Q_OBJECT
    const QString stateName = "state";
    const QString positionName = "position";

public:
    /// @brief This function returns the column count.
    /// @param  parent: QModelIndex of parent
    /// @return int: count of columns
    //int columnCount(const QModelIndex &parent = QModelIndex()) const;

    /// @brief This function returns the data in QVariant data format.
    /// @param  index: QModelIndex of cell/item
    /// @param  role: Item/cell data role
    /// @return QVariant: data in Qvariant format
    //QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    /// @brief This function returns the header data in QVariant data format.
    /// @param  section: header index
    /// @param  orientation: Horizontal/Vertical
    /// @param  role: Item data role
    /// @return QVariant: data in Qvariant format
    //QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

    /// @brief This function returns the flag/setting of the selected cell/item.
    /// @param  index: QModelIndex of cell/item
    /// @return Qt::ItemFlags: properties of cell
    //Qt::ItemFlags flags(const QModelIndex &index) const;

    /// @brief This function sets the data of the selected cell/item.
    /// @param  index: QModelIndex of cell/item
    /// @param  value: Data to set
    /// @param  role: Item data role
    /// @return bool: success/fail flag
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
private:
    int max_position;

    int index_position;
    int index_state;

};

#endif // SQLTABLEMODEL_H
