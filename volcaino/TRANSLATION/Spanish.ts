<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>ArrayChannelProfileViewer</name>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="35"/>
        <source>Channel</source>
        <translation>Canal</translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="41"/>
        <source>Line</source>
        <translation>Línea</translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="65"/>
        <source>Scaling</source>
        <translation>Escala</translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="82"/>
        <source>Displayed Windows</source>
        <translation>Ventanas mostradas</translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="104"/>
        <source>Selected Data Value</source>
        <translation>Valor de los datos seleccionados</translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="132"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="147"/>
        <source>X Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="156"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ArrayChannelProfileViewer.cpp" line="179"/>
        <source>Row</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChanDataCalculator</name>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="36"/>
        <source>Calculate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="45"/>
        <source>Expression:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="52"/>
        <location filename="../ChanDataCalculator.cpp" line="752"/>
        <source>Operators ⏷</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="53"/>
        <source>Insert Channel Variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="253"/>
        <source>Math</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="255"/>
        <source>Logical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="257"/>
        <source>Special</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="259"/>
        <source>Trig</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="261"/>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="263"/>
        <source>Filters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="272"/>
        <source>Operators and Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="300"/>
        <source>Assign channels: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="306"/>
        <source>Filename:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="308"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="309"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="323"/>
        <source>Select table: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="400"/>
        <location filename="../ChanDataCalculator.cpp" line="446"/>
        <source>Save calculation file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="401"/>
        <location filename="../ChanDataCalculator.cpp" line="446"/>
        <location filename="../ChanDataCalculator.cpp" line="509"/>
        <source>*.txt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="508"/>
        <source>Open a calculation file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChanDataCalculator.cpp" line="747"/>
        <source>Operators ⏶</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChannelMenuOperation</name>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="61"/>
        <source>DISPLAY CHANNELS</source>
        <translation>CANALES DE VISUALIZACIÓN</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="77"/>
        <location filename="../ChannelMenuOperation.cpp" line="139"/>
        <location filename="../ChannelMenuOperation.cpp" line="312"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="94"/>
        <source>None of the Channels is hidden.</source>
        <translation>Ninguno de los canales está oculto.</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="114"/>
        <source>NEW CHANNEL</source>
        <translation>NUEVO CANAL</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="117"/>
        <location filename="../ChannelMenuOperation.cpp" line="245"/>
        <source>Channel Details</source>
        <translation>Detalles del canal</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="118"/>
        <location filename="../ChannelMenuOperation.cpp" line="246"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="120"/>
        <location filename="../ChannelMenuOperation.cpp" line="248"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="122"/>
        <location filename="../ChannelMenuOperation.cpp" line="250"/>
        <source>INTEGER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="123"/>
        <location filename="../ChannelMenuOperation.cpp" line="251"/>
        <source>TEXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="124"/>
        <location filename="../ChannelMenuOperation.cpp" line="252"/>
        <source>DOUBLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="125"/>
        <location filename="../ChannelMenuOperation.cpp" line="253"/>
        <source>FLOAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="126"/>
        <location filename="../ChannelMenuOperation.cpp" line="254"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="129"/>
        <location filename="../ChannelMenuOperation.cpp" line="257"/>
        <source>Not Null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="140"/>
        <location filename="../ChannelMenuOperation.cpp" line="313"/>
        <source>CANCEL</source>
        <translation>CANCELAR</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="199"/>
        <source>Do you want to delete channel </source>
        <translation>¿Desea eliminar el canal </translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="202"/>
        <source>DELETE CHANNEL</source>
        <translation>BORRAR CANAL</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="242"/>
        <source>COPY CHANNEL</source>
        <translation>CANAL DE COPIA</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="260"/>
        <source>Channel List</source>
        <translation>Lista de canales</translation>
    </message>
    <message>
        <location filename="../ChannelMenuOperation.cpp" line="279"/>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
</context>
<context>
    <name>DatabaseTable</name>
    <message>
        <location filename="../DatabaseTable.ui" line="20"/>
        <source>VOLCAINO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DatabaseTable.ui" line="54"/>
        <source>DATABASE TABLE LIST</source>
        <translation>LISTA DE TABLAS DE LA BASE DE DATOS </translation>
    </message>
    <message>
        <location filename="../DatabaseTable.ui" line="66"/>
        <source>Select Table Name from the List</source>
        <translation>Seleccione el nombre de la tabla en la lista </translation>
    </message>
    <message>
        <location filename="../DatabaseTable.ui" line="111"/>
        <source>CANCEL</source>
        <translation>CANCELAR</translation>
    </message>
</context>
<context>
    <name>ExportToFile</name>
    <message>
        <location filename="../ExportToFile.cpp" line="56"/>
        <source>TABLE LIST TO EXPORT</source>
        <translation>LISTA DE TABLAS PARA EXPORTAR</translation>
    </message>
    <message>
        <location filename="../ExportToFile.cpp" line="70"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportToFile.cpp" line="138"/>
        <location filename="../ExportToFile.cpp" line="314"/>
        <source>Database is successfully exported to </source>
        <translation>La base de datos se exporta con éxito a </translation>
    </message>
</context>
<context>
    <name>GriddingToolBox</name>
    <message>
        <location filename="../GridToolBox.cpp" line="6"/>
        <source>Gridding ToolBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="10"/>
        <source>Select an input file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="12"/>
        <source>Select an output file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="20"/>
        <source>Data to grid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="21"/>
        <source>Output grid:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="22"/>
        <source>Gridding method:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="23"/>
        <source>Cell size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="24"/>
        <source>Error grid file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="27"/>
        <source>Max Interp Distance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="29"/>
        <source>Max Iteration Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="31"/>
        <source>Trend Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="33"/>
        <source>Search Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="35"/>
        <source>Search Step Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="37"/>
        <source>Auto Stop?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="85"/>
        <source>Restore Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="86"/>
        <source>Start Gridding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="87"/>
        <source>Export GRID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="88"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="170"/>
        <location filename="../GridToolBox.cpp" line="211"/>
        <source>Open a file to grid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../GridToolBox.cpp" line="212"/>
        <source>*.GRD</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LanguageSelection</name>
    <message>
        <location filename="../LanguageSelection.cpp" line="51"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../Login.cpp" line="31"/>
        <source>USER AUTHENTICATION</source>
        <translation>USUARIO AUTENTICACIÓN</translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="57"/>
        <source>Chinese</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="64"/>
        <location filename="../Login.cpp" line="102"/>
        <location filename="../Login.cpp" line="140"/>
        <source>USER</source>
        <translation>USUARIO</translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="66"/>
        <location filename="../Login.cpp" line="104"/>
        <location filename="../Login.cpp" line="142"/>
        <source>ADMIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="67"/>
        <location filename="../Login.cpp" line="105"/>
        <location filename="../Login.cpp" line="143"/>
        <source>VIEWER</source>
        <translation>VISOR</translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="68"/>
        <location filename="../Login.cpp" line="106"/>
        <location filename="../Login.cpp" line="144"/>
        <source>PASSWORD</source>
        <translation>CONTRASEÑA</translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="80"/>
        <location filename="../Login.cpp" line="118"/>
        <location filename="../Login.cpp" line="156"/>
        <source>LOGIN</source>
        <translation>Acceso</translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="81"/>
        <location filename="../Login.cpp" line="119"/>
        <location filename="../Login.cpp" line="157"/>
        <source>CANCEL</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="93"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Login.cpp" line="131"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="110"/>
        <source>&amp;File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="112"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="113"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="114"/>
        <source>Database</source>
        <translation>Base de datos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="115"/>
        <source>Map</source>
        <translation>Mapa</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="116"/>
        <source>ArcGIS Tools</source>
        <translation>ArcGIS Herramientas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="117"/>
        <source>Coordinates</source>
        <translation>Coordenadas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <source>Database Tools</source>
        <translation>Herramientas de la base de datos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="119"/>
        <source>Grid and Image</source>
        <translation>Rejilla e imagen</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="120"/>
        <source>Map Tools</source>
        <translation>Herramientas del mapa</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="121"/>
        <source>Section Tools</source>
        <translation>Sección Herramientas</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="122"/>
        <source>3D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="123"/>
        <source>Seek Data</source>
        <translation>Buscar datos</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="124"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="125"/>
        <source>Help</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="128"/>
        <source>Grid Tool Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="132"/>
        <source>Channel Math</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>&amp;IMPORT</source>
        <translation>IMPORTAR</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="137"/>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>&amp;CSV TYPE</source>
        <translation >CSV TIPO</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="140"/>
        <location filename="../mainwindow.cpp" line="172"/>
        <source>&amp;XYZ TYPE</source>
        <translation>XYZ TIPO</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="143"/>
        <source>&amp;NUV TYPE</source>
        <translation>NUV TIPO</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="147"/>
        <location filename="../mainwindow.cpp" line="175"/>
        <source>&amp;GBN TYPE</source>
        <translation>GBN TIPO</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="167"/>
        <source>&amp;EXPORT</source>
        <translation>EXPORTACIÓN</translation>
    </message>
</context>
<context>
    <name>TableViewOperation</name>
    <message>
        <location filename="../TableViewOperation.cpp" line="119"/>
        <source>Enter Channel Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="120"/>
        <source>Channel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="367"/>
        <source>List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="374"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="383"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="391"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="399"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="406"/>
        <source>Hide All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="412"/>
        <source>Display All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="418"/>
        <source>View Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="424"/>
        <source>Edit Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TableViewOperation.cpp" line="430"/>
        <source>Array Viewer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
