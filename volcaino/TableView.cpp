#include "TableView.h"
#include <QRubberBand>

TableView::TableView(QWidget *parent): QWidget(parent)
{
    //setStyleSheet("background-color:blue;");
    mSplitter = new QSplitter(this);
    mSplitter->setOrientation(Qt::Vertical);

    //QRubberBand *band = new QRubberBand(QRubberBand::Rectangle, this);
}

TableView* TableView::getInstance()
{
    static TableView theInstance;
    return &theInstance;
}

void TableView::setTableViewOperation(TableViewOperation *tableView)
{
    mSplitter->setGeometry(this->geometry());
    //mSplitter->setMinimumSize(this->size()/6);
    mTableViewOp = tableView;
    mTableViewOp->setParent(mSplitter);
    mSplitter->addWidget(mTableViewOp);
}

TableViewOperation *TableView::getTableViewOperation()
{
    return mTableViewOp;
}

void TableView::itemClicked(QTreeWidgetItem *item, int column)
{
    setWindowTitle(item->text(column));
    mTableViewOp->itemClicked(item, column);
}

void TableView::openTable(QString qsTableName)
{
    mTableViewOp->openTable(qsTableName);
}

void TableView::keyPressEvent(QKeyEvent *event)
{
    if (event->type() == QKeyEvent::KeyPress) {
//        if(event->matches(QKeySequence::Find))
//        {
//            if(!thereIsFindChan)
//            {
//                thereIsFindChan = true;
//                QDialog *findChan = new QDialog(this);
//                findChan->setAttribute(Qt::WA_DeleteOnClose);
//                findChan->setWindowFlags(Qt::FramelessWindowHint);
//                findChan->setStyleSheet("background-color:white;");

//                QVBoxLayout *vLayout = new QVBoxLayout(findChan);
//                QLabel *lable = new QLabel("Find a Channel",findChan);

//                QHBoxLayout *hLayout = new QHBoxLayout;
//                QLineEdit *lineEdit = new QLineEdit(findChan);
//                lineEdit->setFixedWidth(250);
//                QPushButton *search = new QPushButton(findChan);
//                search->setFixedHeight(lineEdit->height());
//                search->setFixedWidth(search->height()*3);
//                search->setText("Find");
//                connect(search, &QPushButton::clicked, this, [this,lineEdit]()
//                {
//                    this->mTableViewOp->searchChanName(lineEdit->displayText());
//                }
//                );

//                QPushButton *close = new QPushButton(findChan);
//                close->setFixedHeight(lineEdit->height());
//                close->setFixedWidth(close->height()*3);
//                close->setText("Close");
//                connect(close, &QPushButton::clicked, this, [this,findChan](){findChan->close();thereIsFindChan = false;});

//                hLayout->addWidget(lineEdit);
//                hLayout->addWidget(search);
//                hLayout->addWidget(close);

//                vLayout->addWidget(lable);
//                vLayout->addLayout(hLayout);

//                findChan->setMinimumWidth(lineEdit->width()+search->width()*2);
//                findChan->setMinimumHeight(lineEdit->height());

//                //findChan->move(mTableViewOp->horizontalHeader()->x(), mTableViewOp->horizontalHeader()->y());


//                findChan->move(this->rect().left(), this->rect().bottom()-findChan->height()*2.5);
//                //findChan->raise();
//                findChan->show();
//            }
//        }
    }
}

void TableView::closeEvent(QCloseEvent *event)
{
    QThread *thread = mTableViewOp->getChanMenuOp()->getThread();
    if (thread == nullptr) {//no fetching data thread running, can safely delete widget
        this->deleteLater();
    } else {
        mTableViewOp->getChanMenuOp()->getChanMenuOpData()->setAbort(true);
        connect(thread, &QThread::finished, this, &TableView::deleteLater);
    }
}
