#ifndef TABLEVIEW_H
#define TABLEVIEW_H
#include <QWidget>
#include <QTableView>
#include <QSplitter>
#include <QTreeWidgetItem>
#include "TableViewOperation.h"
#include <QThread>
class TableView: public QWidget
{
public:
    TableView(QWidget *parent = nullptr);

    static TableView *getInstance();

    void setTableViewOperation(TableViewOperation *tableView);
    TableViewOperation* getTableViewOperation();

    void itemClicked(QTreeWidgetItem *item,int column);

    void openTable(QString qsTableName);

protected:
  virtual void  keyPressEvent(QKeyEvent *event);
  virtual void closeEvent (QCloseEvent *event);
private:
    TableViewOperation *mTableViewOp = nullptr;
    QSplitter *mSplitter = nullptr;

    int m = 0;

};


#endif // TABLEVIEW_H
