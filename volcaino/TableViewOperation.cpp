#include "TableViewOperation.h"
#include <QSqlDatabase>
#include <QMdiSubWindow>
#include <QScrollBar>
#include <QHeaderView>
#include <QInputDialog>
#include <QAbstractItemModel>
#include <QVBoxLayout>
#include <QAbstractItemModel>
#include <QTableView>
#include "mainwindow.h"
#include <QMenu>
#include <QSignalMapper>
#include <QAbstractButton>
#include <QTranslator>
#include <QStyleFactory>
#include <QSqlQueryModel>
#include "DataBase.h"
#include "LanguageSelection.h"
#include <QSortFilterProxyModel>
#include "qabstractitemmodel.h"
#include "ArrayChannelProfileViewer.h"
//#include <QRubberBand>
TableViewOperation::TableViewOperation()
    : mRow(1000), mColumn(0), mCount(0), mMin(1), mMax(10000),
    mNoOfPages(0), mRemRecord(0), mPageCount(0), mNoOfRows(0), mTable("N/A"),
    mFilterColumn(""), mFilterFlag(false), mSelectedChan(1), mHiddenChannels{}, mClick(false)
{
    QTranslator translator;

    auto selectLanguage = LanguageSelection::getInstance();
    selectLanguage->setLanguage(selectLanguage->getLanguage(), translator);
    //    auto db =DataBase::getInstance();
    //    if(db->openDatabase()){
    //        mSql=db->getSqlQuery();
    //    }
    //     QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    //    //db.setDatabaseName("../DATABASE/VOLCAINO.db");
    //    db.setDatabaseName("VOLCAINO.db");
    //    modal=new QSqlTableModel();
    //    sqlModal=new SqlDataTableModel;
    //    this->setModel(sqlModal);

    //    mSql=QSqlQuery("",db);
    //    DEBUG_TRACE(db.isOpen());

    // QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    //db.setDatabaseName("../DATABASE/VOLCAINO.db");
    //db.setDatabaseName("VOLCAINO.db");
    //    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    //    //db.setDatabaseName("../DATABASE/VOLCAINO.db");
    //    db.setDatabaseName("VOLCAINO.db");
    //    modal=new QSqlTableModel();
    //sqlModal=new SqlDataTableModel;
    //    mSql=QSqlQuery("",db);
    //    auto db = DataBase::getInstance();
    auto dbQ = DataBase::getInstance()->getSqlQuery();
    mSql = QSqlQuery(dbQ);
    setAlternatingRowColors(true);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setEditTriggers(QAbstractItemView::DoubleClicked);
    this->setEditTriggers(QAbstractItemView::AllEditTriggers);
    this->horizontalHeader()->setStyleSheet("QHeaderView::section { background-color:lightBlue }");
    this->horizontalHeader()->setSectionsMovable(true);

    this->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    this->verticalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);

    this->setChanContextMenu();

    this->addAction(newChan);

    this->addAction(delChan);

    this->addAction(copyChan);

    this->addAction(hideChan);

    this->addAction(findValue);

    connect(this->horizontalHeader(), &QHeaderView::sectionDoubleClicked, this, &TableViewOperation::renameChannel);
    connect(this->horizontalHeader(), &QHeaderView::sectionPressed, this, &TableViewOperation::setChannel);

    connect(this->horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &TableViewOperation::createContextMenu);
    connect(this->verticalHeader(), &QHeaderView::customContextMenuRequested, this, &TableViewOperation::createContextMenu);
    connect(this, &QTableView::customContextMenuRequested, this, &TableViewOperation::createCellContextMenu);

    connect(this, &TableViewOperation::doubleClicked, this, [this](const QModelIndex &index) {
        this->setEditTriggers(QAbstractItemView::AllEditTriggers);
        //DEBUG_TRACE(this->model()->flags(index));
        //DEBUG_TRACE(index);
        this->model()->flags(index).setFlag(Qt::ItemIsEditable, true);
        //DEBUG_TRACE(this->model()->flags(index));
        emit cellSelected(index.column(), index.row());

    }, Qt::QueuedConnection);

    this->setContextMenuPolicy(Qt::CustomContextMenu);

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

TableViewOperation::~TableViewOperation()
{
}

TableViewOperation* TableViewOperation::getInstance()
{
    static TableViewOperation theInstance;
    return &theInstance;
}

void TableViewOperation::setMDIArea(QMdiArea *mdiArea)
{
    mMdiArea = mdiArea;
}

void TableViewOperation::setTableName(QString table)
{
    DEBUG_TRACE(table);
    mTable = table;
    this->setWindowTitle(mTable);
    mChanList = DataBase::getInstance()->getHeaderDetails(mTable);
}

void TableViewOperation::renameChannel(int channel)
{
    DEBUG_TRACE(this->model()->headerData(this->getSelectedChannel(), Qt::Horizontal).toString());
    mClick = true;
    QString oldHeader = this->model()->headerData(this->getSelectedChannel(), Qt::Horizontal).toString();
    bool ok;

    QString text = QInputDialog::getText(this, tr("Enter Channel Name"),
                                         tr("Channel:"), QLineEdit::Normal,
                                         oldHeader, &ok,(this->windowFlags() & ~Qt::WindowContextHelpButtonHint| Qt::FramelessWindowHint));
    if (ok && !text.isEmpty())
        this->model()->setHeaderData(this->getSelectedChannel(), Qt::Horizontal, tr(text.toUtf8()));
    QString query = "";
    query = "ALTER TABLE "+mTable+" RENAME COLUMN "+oldHeader +" TO "+ text;
    if (mSql.exec(query)) {
        DEBUG_TRACE(query);
        this->setColumn();
    }
}

void TableViewOperation::filterChannel(int channel)
{
    //QString header=this->horizontalHeaderItem(channel)->text();
    mFilterColumn.clear();
    //    if((header.contains("FLIGHT")||header.contains("LINE"))&&!(header.contains("TYPE"))&&!(header.contains("DATE"))){
    //        mFilterFlag=true;
    //        mFilterColumn=header;
    //        QString query="";
    //        query="SELECT distinct "+header+" FROM "+mTable+" order by "+header+";";
    //        DEBUG_TRACE(query);
    //        QStringList list;
    //        if(mSql.exec(query)){
    //            while(mSql.next()){
    //                list.append(mSql.value(0).toString());
    //            }
    //        }
    //        QDialog *dialog=new QDialog(this);
    //        dialog->setWindowTitle("FILTER DATA");
    //        dialog->setMaximumHeight(150);
    //        dialog->setMaximumWidth(200);
    //        dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    //        QVBoxLayout* layout = new QVBoxLayout;
    //        dialog->setLayout(layout);

    //        QListWidget * widget = new QListWidget;

    //        QLabel *lineLabel = new QLabel(dialog);

    //        layout->addWidget(lineLabel);
    //        layout->addWidget(widget);
    //        for(int i = 0; i != list.size(); ++i)
    //            (new QListWidgetItem(widget))->setText(list[i]);

    //        lineLabel->setText(header);
    //        if(list.size()==1){
    //            widget->setEnabled(false);
    //            mFilterFlag=false;
    //            this->showData();
    //        }
    //        dialog->show();
    //        connect (widget, &QListWidget::itemClicked, this, &TableViewOperation::setFilter) ;
    //    }

}

void TableViewOperation::setFilter(QListWidgetItem *item)
{
    QString query = "";
    query = "SELECT count(*) from "+mTable+" where "+ mFilterColumn+"='"+item->text()+"';";
    if (mSql.exec(query)) {
        mSql.first();
        mCount = mSql.value(0).toInt();
    }

    query = "";
    query = "SELECT ROWID,* from "+mTable+" where "+ mFilterColumn+"='"+item->text()+"';";
    DEBUG_TRACE(query);
}

void TableViewOperation::setSliderData()
{
    QString query = "";
    query = "SELECT ROWID,* FROM "+mTable+" WHERE ROWID BETWEEN "+QString::number(mMin)+" AND "+QString::number(mMax)+";";

    if (!mSql.exec(query)) {
        DEBUG_TRACE(query);
    } else {
        DEBUG_TRACE(mSql.size());
    }
}

void TableViewOperation::setRow()
{
    if (mSql.exec("SELECT count(*) FROM "+mTable+";")) {
        mSql.first();
        mCount = mSql.value(0).toInt();
    }

    if (mRow > mCount) {
        mRow = mCount;
        mMax = mCount;
    }
    DEBUG_TRACE(mCount);
}

void TableViewOperation::setColumn()
{
    mColumnNames.clear();
    mColumnNames.append("ROWID");
    QString query = "SELECT * FROM PRAGMA_TABLE_INFO('"+mTable+"');";
    DataBase::getInstance()->startTransaction();
    if (mSql.exec(query)) {
        while (mSql.next())
            mColumnNames.append(mSql.value(1).toString());
    }
    //    DataBase::getInstance()->closeTransaction();

    for (int i = 0; i < mColumnNames.size(); ++i) {
        if (mColumnNames[i].contains("flight",Qt::CaseInsensitive)) {
            DEBUG_TRACE(mColumnNames[i]);
            if (!mColumnNames[i].contains("date",Qt::CaseInsensitive))
                flightColName = mColumnNames[i];
        }
        if (mColumnNames[i].contains("line",Qt::CaseInsensitive)) {
            DEBUG_TRACE(mColumnNames[i]);
            if (!mColumnNames[i].contains("type",Qt::CaseInsensitive))
                lineColName = mColumnNames[i];
        }
    }
    DEBUG_TRACE(mColumnNames);
    DEBUG_TRACE(flightColName);
    mColumn = mColumnNames.size();

    if (flightColName == "")
        flightColName = lineColName;

    query = "";
    query = "SELECT DISTINCT "+flightColName+" FROM "+mTable+" ORDER BY "+flightColName+";";
    //    DataBase::getInstance()->startTransaction();
    if (!mSql.exec(query)) {
        DEBUG_TRACE(query);
    } else {
        DEBUG_TRACE(query);
        while (mSql.next())
            mFlights.append(mSql.value(0).toString().toUpper());
    }
    DataBase::getInstance()->closeTransaction();

    flightNo = mFlights[0];
}

void TableViewOperation::itemClicked(QTreeWidgetItem *item, int column)
{
    openTable(item->text(column));
    mpChannel->setChanMenuOpData(this);
}

void TableViewOperation::openTable(QString qsTableName)
{
    this->setTableName(qsTableName);
    this->setColumn();
    this->setRow();
    this->setDataType();
    setMaxData(flightColName, flightNo);
    //qobject_cast<QWidget *>(this->parent()->parent())->raise();
}

void TableViewOperation::searchChanName(QString chanName)
{
    if (chanName.isEmpty()) {
        QApplication::beep();
    } else {
        if (chanName != mSerachName) {  //Search for a new chan name
            mNoWhereFound = true;
            for (QString theChanName: mChanList) {
                if (theChanName.toUpper().contains(chanName.toUpper())) {
                    mNoWhereFound = false;
                    break;
                }
            }

            if (!mNoWhereFound) {
                mSerachName = chanName;
                matchIndex = 0;
                while (!mChanList[matchIndex].toUpper().contains(chanName.toUpper()))
                    matchIndex++;
                this->setFocus();
                QModelIndex modelIndex = this->model()->index(0, matchIndex + 1);
                this->scrollTo(modelIndex);
                this->selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);
            } else {
                QApplication::beep();
            }
        } else {
            if (!mNoWhereFound) {
                matchIndex++;
                if (matchIndex == mChanList.size())
                    matchIndex = 0;
                while (!mChanList[matchIndex].toUpper().contains(chanName.toUpper())) {
                    matchIndex++;
                    if (matchIndex == mChanList.size())
                        matchIndex = 0;
                }

                this->setFocus();
                QModelIndex modelIndex = this->model()->index(0, matchIndex + 1);
                this->scrollTo(modelIndex);
                this->selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);
            } else {
                QApplication::beep();
            }
        }
    }
}

ChannelMenuOperation *TableViewOperation::getChanMenuOp()
{
    return mpChannel;
}

void TableViewOperation::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton && mIsMultiSelectMode) {
        QModelIndexList selIndexList = this->selectionModel()->selectedIndexes();
        QModelIndex selFirst = selIndexList.first();
        QModelIndex selLast = selIndexList.last();
        //        qDebug() << selFirst.column();
        //        qDebug() << selFirst.row();
        //        qDebug() << selLast.row();
        emit multiCellsSelected(selFirst.column(), selFirst.row(), selLast.row());
    }
}

void TableViewOperation::keyPressEvent(QKeyEvent *event)
{
    if (event->type() == QKeyEvent::KeyPress) {
        if (event->key() == Qt::Key_Tab) {
            if (!mIsMultiSelectMode) {
                mIsMultiSelectMode = true;
                this->clearSelection();
                viewport()->setCursor(Qt::SizeVerCursor);
                setSelectionMode(QAbstractItemView::MultiSelection);
            } else {
                mIsMultiSelectMode = false;
                emit clearTrackers();
                viewport()->setCursor(Qt::ArrowCursor);
                setSelectionMode(QAbstractItemView::SingleSelection);
            }
        }

        if (event->key() == Qt::Key_Escape) {
            emit clearTrackers();
            this->clearSelection();
        }

        if (event->matches(QKeySequence::Find)) {
            if (!thereIsFindChan) {
                thereIsFindChan = true;
                QDialog *findChan = new QDialog(this->parentWidget()->parentWidget());
                findChan->setAttribute(Qt::WA_DeleteOnClose);
                findChan->setWindowFlags(Qt::FramelessWindowHint);
                findChan->setStyleSheet("background-color:white;");

                QVBoxLayout *vLayout = new QVBoxLayout(findChan);
                QLabel *lable = new QLabel("Find a Channel",findChan);

                QHBoxLayout *hLayout = new QHBoxLayout;
                QLineEdit *lineEdit = new QLineEdit(findChan);
                lineEdit->setFixedWidth(250);
                QPushButton *search = new QPushButton(findChan);
                search->setFixedHeight(lineEdit->height());
                search->setFixedWidth(search->height()*3);
                search->setText("Find");
                connect(search, &QPushButton::clicked, this, [this,lineEdit]() {
                    this->searchChanName(lineEdit->displayText());
                });

                QPushButton *close = new QPushButton(findChan);
                close->setFixedHeight(lineEdit->height());
                close->setFixedWidth(close->height()*3);
                close->setText("Close");
                connect(close, &QPushButton::clicked, this, [this,findChan]() {
                    findChan->close();
                    thereIsFindChan = false;
                });

                hLayout->addWidget(lineEdit);
                hLayout->addWidget(search);
                hLayout->addWidget(close);

                vLayout->addWidget(lable);
                vLayout->addLayout(hLayout);

                findChan->setMinimumWidth(lineEdit->width()+search->width()*2);
                findChan->setMinimumHeight(lineEdit->height());

                //findChan->move(mTableViewOp->horizontalHeader()->x(), mTableViewOp->horizontalHeader()->y());

                findChan->move(this->rect().left(), this->rect().bottom()-findChan->height()*2.5);
                //findChan->raise();
                findChan->show();
            }
        }
    }
}

void TableViewOperation::showData()
{
    DEBUG_TRACE(mMax);
    if (mMax > 0) {
        QString query = "";
        query = "SELECT ROWID,* FROM "+mTable+";";

        if (!mSql.exec(query)) {
            DEBUG_TRACE(query);
        } else {
            DEBUG_TRACE(query);
            DEBUG_TRACE(mSql.size());

            mNoOfPages = mCount / 10000;
            mRemRecord = mCount % 10000;
            DEBUG_TRACE(mNoOfPages);
            DEBUG_TRACE(mRemRecord);

            setRowsToFetch(0);
        }
        this->show();
    }
}

void TableViewOperation::display(int min, int max)
{
    DEBUG_TRACE(mPageCount);
    QString query = "";
    query = "SELECT ROWID,* FROM "+mTable+" WHERE ROWID BETWEEN "+QString::number(min)+" AND "+QString::number(max)+";";
    if (!mSql.exec(query)) {
        DEBUG_TRACE(query);
    } else {
        DEBUG_TRACE(query);
        DEBUG_TRACE(mSql.size());

        for (int index = min; index < (max); ++index) {
            mSql.next();
            QStringList columnData;
        }
    }
    mNoOfRows = mNoOfRows+10000;
}

void TableViewOperation::setRowsToFetch(int row)
{
    if (!mFilterFlag)
        DEBUG_TRACE(mFilterFlag);

    if (mPageCount != mNoOfPages+1) {
        mPageCount++;
        if (mPageCount <= mNoOfPages) {
            display(mNoOfRows, mNoOfRows+10000);
        } else {
            display(mNoOfRows,mNoOfRows+mRemRecord);
        }
    }
}

void TableViewOperation::createContextMenu(const QPoint &pos)
{
    qDebug()<<__func__;
    qDebug()<<pos<<":::::"<<this->columnAt(pos.x());

    this->setChannel(this->columnAt(pos.x()));
    menu = new QMenu(this);
    menu->addAction(listChan);
    menu->addAction(newChan);
    menu->addAction(delChan);
    menu->addAction(copyChan);
    menu->addAction(hideChan);
    menu->addAction(hideAllChan);
    menu->addAction(displayAllChan);
    menu->addAction(viewProfile);
    menu->addAction(editChannel);
    menu->addAction(arrayChannel);
    menu->addAction(findValue);
    menu->exec(QCursor::pos());
}

void TableViewOperation::createCellContextMenu(const QPoint &pos)
{
    QItemSelectionModel *select = this->selectionModel();
    qDebug() << select->selectedIndexes();

    mSelectedChan = this->columnAt(pos.x());
    //QModelIndex modelIndex = this->model()->index(this->columnAt(pos.y()),mSelectedChan);
    //this->selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);

    menu = new QMenu(this);
    menu->addAction(copy);
    menu->addAction(cut);
    menu->addAction(paste);
    menu->addAction(deleteCell);
    menu->exec(QCursor::pos());
}

void TableViewOperation::setChanContextMenu()
{
    DEBUG_TRACE("");

    mpChannel = new ChannelMenuOperation(this);

    listChan = new QAction(tr("List..."), this);
    listChan->setIcon(QIcon("../IMAGES/list.svg"));
    QSignalMapper *signalMapperDisplay = new QSignalMapper(this);
    connect(listChan, SIGNAL(triggered()), signalMapperDisplay, SLOT(map()));
    signalMapperDisplay->setMapping(listChan, this);
    connect(signalMapperDisplay, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::listChan);

    newChan = new QAction(tr("New..."), this);
    newChan->setIcon(QIcon("../IMAGES/new-document.svg"));
    newChan->setShortcut(QKeySequence::New);
    QSignalMapper *signalMapperNew = new QSignalMapper(this);
    connect(newChan, SIGNAL(triggered()), signalMapperNew, SLOT(map()));
    signalMapperNew->setMapping(newChan, this);
    connect(signalMapperNew, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::newChan);

    delChan = new QAction(tr("&Delete"),this);
    delChan->setIcon(QIcon("../IMAGES/delete-file.svg"));
    delChan->setShortcut(QKeySequence::Delete);
    QSignalMapper *signalMapperDel = new QSignalMapper(this);
    connect(delChan, SIGNAL(triggered()), signalMapperDel, SLOT(map()));
    signalMapperDel->setMapping(delChan, this);
    connect(signalMapperDel, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::deleteChan);

    copyChan = new QAction(tr("Copy"),this);
    copyChan->setIcon(QIcon("../IMAGES/copy.svg"));
    copyChan->setShortcut(QKeySequence::Copy);
    QSignalMapper *signalMapperCopy = new QSignalMapper(this);
    connect(copyChan, SIGNAL(triggered()), signalMapperCopy, SLOT(map()));
    signalMapperCopy->setMapping(copyChan, this);
    connect(signalMapperCopy, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::copyCreate);

    hideChan = new QAction(tr("Hide"), this);
    hideChan->setShortcut(QKeySequence(Qt::Key_Space));
    QSignalMapper *signalMapperHide = new QSignalMapper(this);
    connect(hideChan, SIGNAL(triggered()), signalMapperHide, SLOT(map()));
    signalMapperHide->setMapping(hideChan, this);
    connect(signalMapperHide, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::hide);

    hideAllChan = new QAction(tr("Hide All"), this);
    QSignalMapper *signalMapperHideAll = new QSignalMapper(this);
    connect(hideAllChan, SIGNAL(triggered()), signalMapperHideAll, SLOT(map()));
    signalMapperHideAll->setMapping(hideAllChan, this);
    connect(signalMapperHideAll, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::hideAll);

    displayAllChan = new QAction(tr("Display All"), this);
    QSignalMapper *signalMapperDisplayAll = new QSignalMapper(this);
    connect(displayAllChan, SIGNAL(triggered()), signalMapperDisplayAll, SLOT(map()));
    signalMapperDisplayAll->setMapping(displayAllChan, this) ;
    connect(signalMapperDisplayAll, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::displayAll);

    viewProfile = new QAction(tr("View Profile"), this);
    QSignalMapper *signalMapperDisplayProfile = new QSignalMapper(this);
    connect(viewProfile, SIGNAL(triggered()), signalMapperDisplayProfile, SLOT(map()));
    signalMapperDisplayProfile->setMapping(viewProfile, this);
    connect(signalMapperDisplayProfile, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::displayProfile);
    viewProfile->setEnabled(false);

    editChannel = new QAction(tr("Edit Channel"), this);
    QSignalMapper *signalMapperEditChannel = new QSignalMapper(this);
    connect(editChannel, SIGNAL(triggered()), signalMapperEditChannel, SLOT(map()));
    signalMapperEditChannel->setMapping(editChannel, this);
    connect(signalMapperEditChannel, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::editChannel);

    arrayChannel = new QAction(tr("Array Viewer"), this);
    QSignalMapper *signalMapperArrayChannel = new QSignalMapper(this);
    connect(arrayChannel, SIGNAL(triggered()), signalMapperArrayChannel, SLOT(map()));
    signalMapperArrayChannel->setMapping(arrayChannel, this) ;
    connect(signalMapperArrayChannel, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::arrayViewChannel);

    findValue = new QAction(tr("Find Value"), this);
    //hideChan->setShortcut(QKeySequence(Qt::Key_Space));
    QSignalMapper *signalMapperFind = new QSignalMapper(this);
    connect(findValue, SIGNAL(triggered()), signalMapperFind, SLOT(map()));
    signalMapperFind->setMapping(findValue, this);
    connect(signalMapperFind, &QSignalMapper::mappedWidget, mpChannel, &ChannelMenuOperation::find);


    copy = new QAction(tr("Copy"), this);
    connect(copy, &QAction::triggered, this, &TableViewOperation::copyCells);

    cut = new QAction(tr("Cut"), this);
    connect(cut, &QAction::triggered, this, &TableViewOperation::cutCells);

    paste = new QAction(tr("Paste"), this);
    connect(paste, &QAction::triggered, this, &TableViewOperation::pasteCells);

    deleteCell = new QAction(tr("Delete"), this);
    connect(deleteCell, &QAction::triggered, this, &TableViewOperation::deleteCells);
}

void TableViewOperation::setChannel(int index)
{
    QModelIndex modelIndex = this->model()->index(0, index);
    this->selectionModel()->select(modelIndex, QItemSelectionModel::ClearAndSelect);

    DEBUG_TRACE(this->currentIndex().column());

    if (mSelectedChan != index)
        mSelectedChan = index;
    DEBUG_TRACE(mSelectedChan);

    auto colName = this->model()->headerData(mSelectedChan, Qt::Horizontal).toString();
    QStringList flights;
    if (colName.contains(flightColName)) {
        menuValues = new QMenu(this);

        DEBUG_TRACE(colName);

        QSignalMapper *mapper = new QSignalMapper(this);
        for (int i = 0; i < mFlights.size(); i++) {
            QAction* labelAction = new QAction(mFlights[i], this);
            labelAction->setToolTip("Trace Marker " + mFlights[i]);
            labelAction->setStatusTip("Trace Marker " + mFlights[i]);
            menuValues->addAction(labelAction);
            connect(labelAction, SIGNAL(triggered()), mapper, SLOT(map()));
            mapper->setMapping(labelAction, i);
        }
        connect(mapper, &QSignalMapper::mappedInt, this, &TableViewOperation::setFlightFilter);
        menuValues->exec(QCursor::pos());
    }
}

void TableViewOperation::setFlightFilter(int value)
{
    DEBUG_TRACE(value);
    flightNo = mFlights[value];
    setMaxData(flightColName, flightNo);
}

QString& TableViewOperation::getTableName()
{
    DEBUG_TRACE(mTable);
    return mTable;
}

int& TableViewOperation::getSelectedChannel()
{
    return mSelectedChan;
}

QStringList& TableViewOperation::getChannelList()
{
    return mColumnNames;
}

void TableViewOperation::refreshTable()
{
    DEBUG_TRACE(mSelectedChan);
    DEBUG_TRACE("Start time: " << QDateTime::currentDateTime());
    this->setColumn();
    setMaxData(flightColName, flightNo);
    setDataType();
    DEBUG_TRACE("End time: " << QDateTime::currentDateTime());
}

void TableViewOperation::setChannelToHiddenState(int& channel)
{
    if (!mHiddenChannels.contains(channel)) {
        mHiddenChannels.append(channel);
        if (!this->isColumnHidden(channel))
            this->setColumnHidden(channel, true);
    }
}

void TableViewOperation::setChannelToDisplayState(const int& channel)
{
    if (mHiddenChannels.contains(channel)) {
        if (this->isColumnHidden(channel))
            this->setColumnHidden(channel, false);
        mHiddenChannels.removeOne(channel);
    }
}

void TableViewOperation::copyCells()
{
    copyCutVals.clear();
    mSelIndexs.clear();

    QItemSelectionModel *select = this->selectionModel();
    mSelIndexs = select->selectedIndexes();

    int dataSize = mSelIndexs.size();
    copyCutVals.resize(dataSize);
    for (int i = 0; i < dataSize; i++) {
        if (mSelIndexs[i].data().isNull()) {
            copyCutVals[i] = nullValue;
        } else {
            copyCutVals[i] = mSelIndexs[i].data().toDouble();
        }
    }

    colRange = select->selection().first().right() - select->selection().first().left()+1;
    mIsMultiSelectMode = false;
    setSelectionMode(QAbstractItemView::SingleSelection);
    viewport()->setCursor(Qt::ArrowCursor);
}

void TableViewOperation::cutCells()
{
    copyCells();
    isCut = true;
}

void TableViewOperation::pasteCells()
{
    if (!copyCutVals.empty()) {
        QItemSelectionModel *select = this->selectionModel();
        int firstRow = select->selection().first().top();
        int firstCol = mSelectedChan;

        QModelIndexList modelIndexPaste;

        //leftChan = mSelectedChan;
        int valSize = copyCutVals.size();
        int thisRow = firstRow;
        int prevRow = firstRow;
        int thisCol = firstCol;
        int prevCol = firstCol;

        for (int i = 0; i < valSize; i++) {
            if (i % 50 == 0 && i != 0)
                QCoreApplication::processEvents();
            int diffRow = 0;
            int diffCol = 0;

            if (i > 0) {
                diffRow = mSelIndexs[i].row() - mSelIndexs[i-1].row();
                diffCol = mSelIndexs[i].column() - mSelIndexs[i-1].column();
            }

            thisRow = prevRow + diffRow;
            thisCol = prevCol + diffCol;

            QModelIndex index = this->model()->index(thisRow, thisCol);
            modelIndexPaste.append(index);

            QString thisColName = this->model()->headerData(thisCol, Qt::Horizontal).toString();
            QString rowIdStr = QString::number(thisRow + 1);
            QString valStr;
            if (copyCutVals[i] == nullValue) {
                valStr = "NULL";
            } else {
                valStr = QString::number(copyCutVals[i]);
            }

            QString query = "UPDATE "+mTable+" SET "+thisColName+" = "+valStr+" WHERE rowid = "+rowIdStr;
            mSql.exec(query);

            prevRow = thisRow;
            prevCol = thisCol;
        }

        if (isCut) {
            for (int i = 0; i < valSize; i++) {
                if (!mSelIndexs[i].data().isNull()) {
                    QString thisColName = this->model()->headerData(mSelIndexs[i].column(),Qt::Horizontal).toString();//colNamesDel[i%colRange];
                    int thisRow = mSelIndexs[i].row()+1;
                    QString rowIdStr = QString::number(thisRow);

                    QString query = "UPDATE "+mTable+" SET "+thisColName+" = NULL WHERE rowid = "+rowIdStr;
                    mSql.exec(query);
                }
            }
        }

        sqlModal->setQuery(mSql);
        this->setModel(sqlModal);
        this->refreshTable();

        QModelIndex modelIndex = this->model()->index(firstRow,firstCol);
        this->scrollTo(modelIndex, ScrollHint::PositionAtCenter);

        int pasteSize = modelIndexPaste.size();
        for (int i = 0; i < pasteSize; i++)
            this->selectionModel()->select(modelIndexPaste[i], QItemSelectionModel::Select);

        isCut = false;
    }
}

void TableViewOperation::deleteCells()
{
    QItemSelectionModel *select = this->selectionModel();
    QModelIndexList delIndexs = select->selectedIndexes();

    int delSize = delIndexs.size();
    for (int i = 0; i < delSize; i++) {
        if (!delIndexs[i].data().isNull()) {
            QString thisColName = this->model()->headerData(delIndexs[i].column(), Qt::Horizontal).toString();//colNamesDel[i%colRange];
            int thisRow = delIndexs[i].row() + 1;
            QString rowIdStr = QString::number(thisRow);

            QString query = "UPDATE "+mTable+" SET "+thisColName+" = NULL WHERE rowid = "+rowIdStr;
            mSql.exec(query);
        }
    }

    sqlModal->setQuery(mSql);
    this->setModel(sqlModal);
    this->refreshTable();

    QModelIndex modelIndex = this->model()->index(delIndexs[0].row(), delIndexs[0].column());
    this->scrollTo(modelIndex, ScrollHint::PositionAtCenter);
    QModelIndex modelIndexDelete;

    for (int i = 0; i < delSize; i++) {
        modelIndexDelete = this->model()->index(delIndexs[i].row(), delIndexs[i].column());
        this->selectionModel()->select(modelIndexDelete, QItemSelectionModel::Select);
    }

    mIsMultiSelectMode = false;
    setSelectionMode(QAbstractItemView::SingleSelection);
    viewport()->setCursor(Qt::ArrowCursor);
}

QList<int>& TableViewOperation::getHiddenChannelList()
{
    return mHiddenChannels;
}

QMdiArea * TableViewOperation::getMDIArea()
{
    return mMdiArea;
}

void TableViewOperation::setMaxData(QString colName,QString flight)
{
    DEBUG_TRACE(mDataType); // is 0 now

    //SqlDataTableModel * sqlModal = new SqlDataTableModel;
    sqlModal = new QSqlQueryModel;
    // auto profileView =ArrayChannelProfileViewer::getInstance();

    sqlModal->clear();

    QString query = "";
    if (mDataType > 0) {
        QList<int> list;
        QLabel label;
        label.setText("click");
        query = "SELECT ROWID,* FROM "+mTable+" WHERE "+colName+"='"+flight+"' ORDER BY ROWID;";

        DataBase::getInstance()->startTransaction();

        if (mSql.exec(query))
            DEBUG_TRACE(query);

        sqlModal->setQuery(mSql);

        while (sqlModal->canFetchMore())
            sqlModal->fetchMore();
        DataBase::getInstance()->closeTransaction();
        this->setModel(sqlModal);

        this->showMaximized();

        //        query="SELECT * FROM PRAGMA_TABLE_INFO('"+mTable+"') where type LIKE '%[]%';";
        //        if(mSql.exec(query)){
        //            while (mSql.next())
        //                mArrayList.insert(std::pair<int,QString>(mSql.value(0).toInt(),mSql.value(1).toString()));
        //        }
    } else {
        query = "SELECT ROWID,* FROM "+mTable+" WHERE "+colName+"='"+flight+"' ORDER BY ROWID;";
        DataBase::getInstance()->startTransaction();
        if (mSql.exec(query)) {
            DEBUG_TRACE(query);
        } else {
            DEBUG_TRACE(mSql.lastError().text());
        }

        sqlModal->setQuery(mSql);

        while (sqlModal->canFetchMore())
            sqlModal->fetchMore();

        DataBase::getInstance()->closeTransaction();
        // sqlModal->submit();
        this->setModel(sqlModal);

        this->showMaximized();
    }

    //this->model()->setData();

    //    connect(this,&TableViewOperation::doubleClicked,[this](const QModelIndex &index){

    //        DEBUG_TRACE("HELLO");
    //    });
}

void TableViewOperation::setDataType()
{
    mArrayList.clear();
    mDataType = 0;
    QString queryArr = "SELECT * FROM PRAGMA_TABLE_INFO('"+mTable+"') where type LIKE '%[]%';"; // get channels that are array channels (data starts with [])

    DataBase::getInstance()->startTransaction();

    if (mSql.exec(queryArr)) {
        while (mSql.next()) {
            mArrayNoList.push_back(mSql.value(0).toInt());
            mArrayList.insert(std::pair<int, QString>(mSql.value(0).toInt(), mSql.value(1).toString()));
            mDataType = 1;//mSql.value(0).toInt();
        }
    }
    DataBase::getInstance()->closeTransaction();

    //DEBUG_TRACE(mDataType);
}

unsigned int TableViewOperation::getDataType()
{
    return mDataType;
}

std::map<int,QString> TableViewOperation::getArrayList()
{
    return mArrayList;
}

QList<int> TableViewOperation::getArrayNoList()
{
    return mArrayNoList;
}
