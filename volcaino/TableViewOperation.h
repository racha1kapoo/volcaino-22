/**
 *  @file   TableViewOperation.h
 *  @brief  This class displays the database data in tabular form and applies operations like creating context menu,
 *          setting channels and applying operations to it.
 *  @author Rachana Kapoor
 *  @date   Dec 14,2021
 ***********************************************/

#ifndef TABLEVIEWOPERATION_H
#define TABLEVIEWOPERATION_H

#include <QTableWidget>
#include <QMdiArea>
#include <QListWidget>
#include <QSqlQuery>
#include <QPoint>
#include <map>
#include <QSqlTableModel>
#include "ChannelMenuOperation.h"
#include "TypeDefConst.h"
#include "SqlDataTableModel.h"
#include <QSortFilterProxyModel>

class ChannelMenuOperation;

class TableViewOperation:public QTableView
{
    Q_OBJECT
public:

    ///default constructor
    TableViewOperation();

    /// Class destructor
    ~TableViewOperation();

    /// @brief static function
    ///
    /// @return   Instance of class TableViewOperation
    ///
    static TableViewOperation* getInstance();

    /// @brief This function sets the class data members when table name is selected.
    /// @return  void
    ///
    void itemClicked(QTreeWidgetItem *item,int column);

    /// @brief This function shows the table selected in tabular form.
    /// @return  void
    ///
    void showData();

    /// @brief This function sets appends table with more data when mouse scroll is detected.
    /// @return  void
    ///
    void setSliderData();

    /// @brief This function sets the table name.
    /// @return  void
    ///
    void setTableName(QString table);

    /// @brief This function sets the no of rows to fetch.
    /// @return  void
    ///
    void setRow();

    /// @brief This function sets the column names.
    /// @return  void
    ///
    void setColumn();

    /// @brief This function adds the another subwindow.
    /// @return  void
    ///
    void setMDIArea(QMdiArea *mdiArea);

    /// @brief This function renames the channel name.
    /// @return  void
    ///
    void renameChannel(int channel);

    /// @brief This function filters the flight and Line data as per flight/light No selected.
    /// @return  void
    ///
    void filterChannel(int channel);

    /// @brief This function display the data as per range set on mouse scroll event.
    /// @return  void
    ///
    void display(int min,int max);

    /// @brief This function sets the no of rows to fetch from database.
    /// @return  void
    ///
    void setRowsToFetch(int row);

    /// @brief This function retrieves the filtered data from database and displays it.
    /// @param item: Item selected from the list
    /// @return  void
    ///
    void setFilter(QListWidgetItem* item);

    /// @brief This function creates a context menu on mouse's right click and invokes the function
    /// depending on the selected menu option.
    /// @param pos: position of the mouse when right click pressed
    /// @return  void
    ///
    void setChanContextMenu();

    /// @brief This function is responsible for getting the valid table name.
    /// @return  table: Name of the table
    ///
    QString& getTableName();

    /// @brief This function is responsible for getting the index of the selected channel.
    /// @return  index: index of the channel
    ///
    int& getSelectedChannel();

    /// @brief This function is responsible for getting the list of the channels in a table.
    /// @return  list: list of the channels
    ///
    QStringList& getChannelList();

    /// @brief This function is responsible for refreshing the table data.
    /// @return  void
    ///
    void refreshTable();

    /// @brief This function is responsible for getting the list of the hidden channels.
    /// @return  list: list of the hidden channels
    ///
    QList<int>& getHiddenChannelList();

    /// @brief This function creates the context menu whenever right mouse click detected on the header item.
    /// @param pos: position of the mouse click
    /// @return  void
    void createContextMenu(const QPoint &pos);

    void createCellContextMenu(const QPoint &pos);

    /// @brief This function returns an instance of QMdiArea.
    /// @return  QMdiArea
    QMdiArea * getMDIArea();

    /// @brief This function sets the data in the table as per flight column name and flight no.
    /// @param colName: Flight name
    /// @param flight: Flight no
    /// @return  void
    void setMaxData(QString colName,QString flight);

    /// @brief This function sets the flight filter as per the provided flight no.
    /// @param value: Flight no
    /// @return  void
    void setFlightFilter(int value);

    /// @brief This function sets the table data type whether array or not.
    /// @return  void
    ///
    void setDataType();


    /// @brief This function sets the table data type.
    /// @return mDataType
    ///
    unsigned int getDataType();

    /// @brief This function return the array column and column name as map.
    /// @return  mArrayList
    ///
    std::map<int,QString> getArrayList();

    QList<int> getArrayNoList();

    void openTable(QString qsTableName);

    void searchChanName(QString chanName);

    QAction *viewProfile;

    ChannelMenuOperation *getChanMenuOp();


signals:
    void cellSelected(int col, int row);
    void multiCellsSelected(int col, int startRow, int endRow);
    void clearTrackers();
public slots:

    /// @brief This function is responsible for setting the selected channel index.
    /// @param index: index of the channel
    /// @return  void
    ///
    void setChannel(int index);

    /// @brief This function is responsible for hiding the selected channel.
    /// @param channel: index of the channel
    /// @return  void
    ///
    void setChannelToHiddenState(int& channel);

    /// @brief This function is responsible for displaying the hidden channel.
    /// @param index: index of the channel
    /// @return  void
    ///
    void setChannelToDisplayState(const int& channel);

private:

    void copyCells();
    void cutCells();
    void pasteCells();
    void deleteCells();

    virtual void mouseReleaseEvent(QMouseEvent *event) override;

    virtual void keyPressEvent(QKeyEvent *event) override;

    QMdiArea *mMdiArea;

    QSqlQuery mSql;

    unsigned int mRow;

    unsigned int mColumn;

    unsigned int mCount;

    unsigned int mMin;

    unsigned int mMax;

    int mNoOfPages;

    int mRemRecord;

    int mPageCount;

    int mNoOfRows;

    QString mTable;

    QString mFilterColumn;

    QStringList mColumnNames;

    QTableWidget *tableWidget;

    bool mFilterFlag;

    int mSelectedChan;


    QList<int> mHiddenChannels;

    bool mClick;

    QMenu *menu;
    QMenu *menuValues;

    QAction *listChan;
    QAction *newChan;
    QAction *delChan;
    QAction *copyChan;
    QAction *hideChan;
    QAction *hideAllChan;
    QAction *displayAllChan;
    QAction *editChannel;
    QAction *arrayChannel;
    QAction *findValue;

    QAction *copy;
    QAction *cut;
    QAction *paste;
    QAction *deleteCell;


    QSqlTableModel *modal;
    QSqlQueryModel *sqlModal;

    QThread *thread;
    QTimer *timer;

    QAction *actionAll;
    QSortFilterProxyModel *proxyModel;

    QString flightColName;
    QString flightNo;
    QStringList mFlights;
    QString lineColName;

    unsigned int mDataType;

    std::map<int,QString>  mArrayList;

    QList<int> mArrayNoList;

    QString mSerachName;

    bool mNoWhereFound = true;

    int matchIndex = 0;

    QStringList mChanList;

    ChannelMenuOperation *mpChannel = nullptr;

    QMutex mutex;

    bool mIsMultiSelectMode = false;

    std::vector<double> copyCutVals;
    QModelIndexList mSelIndexs;

    //int rowRange;
    int colRange;

    bool thereIsFindChan = false;

    bool isCut = false;

};



#endif // TABLEVIEWOPERATION_H
