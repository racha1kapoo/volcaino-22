/**
 *  @file   TypeDefConst.h
 *  @brief  This files contains constants, enums, type defs, structures etc
 *  @author Rachana Kapoor
 *  @date   ‎February ‎23, ‎2022
 ***********************************************/

#ifndef TYPEDEFCONST_H
#define TYPEDEFCONST_H
#include <QDebug>

///@brief DEBUG is defined for the enabling the debug statements.
/// Comment the below line (#define DEBUG) to disable the debug statements
#define DEBUG

#ifdef DEBUG
#define DEBUG_TRACE(log) qDebug()<< __FILE__ << __LINE__ <<"::::" <<__FUNCTION__ << log
#else
#define DEBUG_TRACE(log)
#endif


#define GSF_NORMAL 0 // Normal decimal number format
#define GSF_EXP 1  //Exponential floating point(1.2E+23)
#define GSF_TIME 2  // Time (HH:MM:SS.SSSS)
#define GSF_DATE 3  // Date (YYYY/MM/DD)
#define GSF_GGRAPH 4  // Geographical (DEG.MM.SS.SSS)

#define GS_BYTE  0   // signed byte
#define GS_USHORT  1    // unsigned 2-byte integer
#define GS_SHORT   2    // signed 2-byte integer
#define GS_LONG    3    // signed 4-byte integer
#define GS_FLOAT   4    // 4-byte floating point
#define GS_DOUBLE  5    // 8-byte floating point
#define GS_UNSIGNED_BYTE  6
#define LINE_NORMAL 0
#define LINE_BASE 1
#define LINE_TIE 2
#define LINE_TEST 3
#define LINE_TREND 4
#define LINE_SPECIAL 5
#define LINE_RANDOM 6
#define DIRECT 0
#define BDR 1
#define NPK 2

#define NDI_ASCII 0
#define NDI_INT 1
#define NDI_DB 2

#define GS_U1DM (char) 255U
#define GS_S1DM (signed char) -127
#define GS_U2DM (unsigned short) 65535U
#define GS_S2DM (short) -32767
#define GS_S4DM (long) -2147483647L
#define GS_R8DM (double) -1.0E32
#define GS_R4DM (float)(GS_R8DM)
#define nullValue GS_R8DM

struct SqliteColumninfo {
    int columnId;
    QString columnName;
    QString type;
    int NotNull;
    QString defaultVal;
    int pKey=0;
};

enum ChanType
{
    tpUnknow = -1,
    tpDWord,
    tpLong64,
    tpAs,
    tpByte,
    tpShort,
    tpWord,
    tpInt,
    tpLong,
    tpFloat,
    tpReal,
    tpErr
};

enum FileType
{
    Plain,
    Spectrum,
    EM
};

struct nuvChStruct
{
    QString chName = "";

    struct theChStruct
    {
        QString chUnit;
        ChanType chType;
        QString chComm;

        //int arrSpRate = 0;
        int arrayLength = 0;
        int chSps = 0;
        double chCoef;
        double chOfs;
        int chIndex;

        bool isArrChan = false;
    }chStruct;
};

struct gbnLineRecStruct
{
    long lineNumber;
    long lineVersion;
    long lineType;
    long flight;
    long year;
    long month;
    long day;
};

struct gbnChsRecStruct
{
    QString channelName;
    long channelType = 0;
    long arrayDepth = -1;
    long displayFormat = 0;
    long displayWidth = 0;
    long displayDecimals = 0;
};

struct gbnParmRecStruct
{
    char szParm[64];
    char szValue[128];
};

enum FileExtType
{
    CSV,
    XYZ,
    NUV,
    gbn
};

enum updateType
{
    update,
    revert,
    reset,
};

enum rubberMode
{
    zoomAllMode,
    zoomXMode,
    zoomYMode,
    selectRangeMode,
    deleteRangeMode
};

enum ChartType
{
    OtherChartTypes = -1,
    ProfileNormal,
    ProfileArray,
    PLMSubNormal,
    PLMSubArray,
    PLMDataNormal,
    PLMDataArray,
    PLMRefNormal,
    PLMRefArray
};

enum MouseTrackerType
{
    NoTrackerTypesProfileViewer = -2,
    NoTrackerTypes = -1,
    ProfileSingleXY,
    ProfileSingleX,
    ProfileSingleXShow,
    ProfileDouble,
    PLMSubInd,
    PLMZeroPtSelection,
    PLMZeroPtCrossInd,
    PLMZeroPtXRefInd
};

#endif // TYPEDEFCONST_H
