QT += core widgets charts
QT       += sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
#CONFIG += static

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ArrayChannelProfileViewer.cpp \
    BitMapViewer.cpp \
    BiDrGridding.cpp \
    ChanCalDataHandling.cpp \
    ChanDataCalculator.cpp \
    ChanMenuOpData.cpp \
    ChannelMenuOperation.cpp \
    ChartData.cpp \
    DesignManager.cpp \
    ProfileLevelingDialog.cpp \
    ProgressBar.cpp \
    TableView.cpp \
    channelselector.cpp \
    DataBase.cpp \
    DatabaseTable.cpp \
    ExportGBN.cpp \
    ExportNDI.cpp \
    ExportToFile.cpp \
    GridToolBox.cpp \
    GriddingBase.cpp \
    ImportFile.cpp \
    ImportGBN.cpp \
    ImportNUV.cpp \
    LanguageSelection.cpp \
    Login.cpp \
    MouseEventTracking.cpp \
    NPKGridding.cpp \
    ShowChartView.cpp \
    SqlDataTableModel.cpp \
    TableViewOperation.cpp \
    WindowLayout.cpp \
    interpolator.cpp \
    main.cpp \
    mainwindow.cpp \
    mergedialog.cpp

HEADERS += \
    ArrayChannelProfileViewer.h \
    BitMapViewer.h \
    BiDrGridding.h \
    ChanCalDataHandling.h \
    ChanDataCalculator.h \
    ChanMenuOpData.h \
    ChannelMenuOperation.h \
    ChartData.h \
    DesignManager.h \
    ProfileLevelingDialog.h \
    ProgressBar.h \
    TableView.h \
    channelselector.h \
    DataBase.h \
    DatabaseTable.h \
    ExportGBN.h \
    ExportNDI.h \
    ExportToFile.h \
    GridToolBox.h \
    GriddingBase.h \
    ImportFile.h \
    ImportGBN.h \
    ImportNUV.h \
    LanguageSelection.h \
    Login.h \
    MouseEventTracking.h \
    NPKGridding.h \
    ShowChartView.h \
    SqlDataTableModel.h \
    TableViewOperation.h \
    TypeDefConst.h \
    WindowLayout.h \
    interpolator.h \
    mainwindow.h \
    mergedialog.h

FORMS += \
    ProfileLevelingDialog.ui \
    channelselector.ui\
    DatabaseTable.ui \
    mainwindow.ui \
    mergedialog.ui

OTHER_FILES+

CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

INCLUDEPATH += C:/boost-dir

TRANSLATIONS += TRANSLATION/Chinese.ts \
                TRANSLATION/English.ts \
                TRANSLATION/Spanish.ts
