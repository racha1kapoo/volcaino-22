#include "WindowLayout.h"

WindowLayout::WindowLayout(QWidget *parent, const QMargins &margins, int spacing)
    : QLayout(parent)
{
    setContentsMargins(margins);
    setSpacing(spacing);
}

WindowLayout::WindowLayout(int spacing)
{
    setSpacing(spacing);
}

WindowLayout::~WindowLayout()
{
    QLayoutItem *l;
    while ((l = takeAt(0)))
        delete l;
}

void WindowLayout::addItem(QLayoutItem *item)
{
    add(item, Left);
}

void WindowLayout::addWidget(QWidget *widget, Position position)
{
    add(new QWidgetItem(widget), position);
}

Qt::Orientations WindowLayout::expandingDirections() const
{
    return Qt::Horizontal | Qt::Vertical;
}

bool WindowLayout::hasHeightForWidth() const
{
    return false;
}

int WindowLayout::count() const
{
    return mList.size();
}

QLayoutItem *WindowLayout::itemAt(int index) const
{
    ItemWrapper *wrapper = mList.value(index);
    return wrapper ? wrapper->item : nullptr;
}

QSize WindowLayout::minimumSize() const
{
    return calculateSize(MinimumSize);
}

void WindowLayout::setGeometry(const QRect &rect)
{
    ItemWrapper *center = nullptr;
    int RightWidth = 0;
    int LeftWidth = 0;
    int TopHeight = 0;
    int BottomHeight = 0;
    int centerHeight = 0;
    int i;

    QLayout::setGeometry(rect);

    for (i = 0; i < mList.size(); ++i) {
        ItemWrapper *wrapper = mList.at(i);
        QLayoutItem *item = wrapper->item;
        Position position = wrapper->position;

        if (position == Top) {
            item->setGeometry(QRect(rect.x(), TopHeight, rect.width(),
                                    item->sizeHint().height()));

            TopHeight += item->geometry().height() + spacing();
        } else if (position == Bottom) {
            item->setGeometry(QRect(item->geometry().x(),
                                    item->geometry().y(), rect.width(),
                                    item->sizeHint().height()));

            BottomHeight += item->geometry().height() + spacing();

            item->setGeometry(QRect(rect.x(),
                              rect.y() + rect.height() - BottomHeight + spacing(),
                              item->geometry().width(),
                              item->geometry().height()));
        } else if (position == Center) {
            center = wrapper;
        }
    }

    centerHeight = rect.height() - TopHeight - BottomHeight;

    for (i = 0; i < mList.size(); ++i) {
        ItemWrapper *wrapper = mList.at(i);
        QLayoutItem *item = wrapper->item;
        Position position = wrapper->position;

        if (position == Left) {
            item->setGeometry(QRect(rect.x() + LeftWidth, TopHeight,
                                    item->sizeHint().width(), centerHeight));

            LeftWidth += item->geometry().width() + spacing();
        } else if (position == Right) {
            item->setGeometry(QRect(item->geometry().x(), item->geometry().y(),
                                    item->sizeHint().width(), centerHeight));

            RightWidth += item->geometry().width() + spacing();

            item->setGeometry(QRect(
                              rect.x() + rect.width() - RightWidth + spacing(),
                              TopHeight, item->geometry().width(),
                              item->geometry().height()));
        }
    }

    if (center)
        center->item->setGeometry(QRect(LeftWidth, TopHeight,
                                        rect.width() - RightWidth - LeftWidth,
                                        centerHeight));
}

QSize WindowLayout::sizeHint() const
{
    return calculateSize(SizeHint);
}

QLayoutItem *WindowLayout::takeAt(int index)
{
    if (index >= 0 && index < mList.size()) {
        ItemWrapper *layoutStruct = mList.takeAt(index);
        return layoutStruct->item;
    }
    return nullptr;
}

void WindowLayout::add(QLayoutItem *item, Position position)
{
    mList.append(new ItemWrapper(item, position));
}

QSize WindowLayout::calculateSize(SizeType sizeType) const
{
    QSize totalSize;

    for (int i = 0; i < mList.size(); ++i) {
        ItemWrapper *wrapper = mList.at(i);
        Position position = wrapper->position;
        QSize itemSize;

        if (sizeType == MinimumSize)
            itemSize = wrapper->item->minimumSize();
        else // (sizeType == SizeHint)
            itemSize = wrapper->item->sizeHint();

        if (position == Top || position == Bottom || position == Center)
            totalSize.rheight() += itemSize.height();

        if (position == Left || position == Right || position == Center)
            totalSize.rwidth() += itemSize.width();
    }
    return totalSize;
}
