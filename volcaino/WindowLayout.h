/**
 *  @file   WindowLayout.h
 *  @brief  This class is responsible for setting the layout of the main window.
 *  @author Rachana Kapoor
 *  @date   January ‎25, ‎2022
 ***********************************************/

#ifndef WINDOWLAYOUT_H
#define WINDOWLAYOUT_H

#include <QLayout>
#include <QRect>

class WindowLayout : public QLayout
{
public:
    enum Position { Left, Top, Bottom, Right, Center };

    ///Class constructor
    explicit WindowLayout(QWidget *parent, const QMargins &margins = QMargins(), int spacing = -1);
    WindowLayout(int spacing = -1);

     /// Class destructor
    ~WindowLayout();

    /// @brief This function adds the layout item to the left .
    /// @param item: layout
    /// @return void
    ///
    void addItem(QLayoutItem *item) override;


    /// @brief This function adds the widget to the layout depending on the position passed.
    /// @param widget: type of widget eg: QFrame
    /// @param position: postion of the widget
    /// @return  void
    ///
    void addWidget(QWidget *widget, Position position);

    /// @brief This function returns the orientation whether horizontal/vertical.
    /// @return  Qt::Orientations
    ///
    Qt::Orientations expandingDirections() const override;

    /// @brief This function returns false if layout has height for width.
    /// @return  false
    ///
    bool hasHeightForWidth() const override;

    /// @brief This function returns the no of layouts.
    /// @return  integer
    ///
    int count() const override;

    /// @brief This function returns the layout at passed index.
    /// @param index: index of layout
    /// @return  QLayoutItem
    ///
    QLayoutItem *itemAt(int index) const override;

    /// @brief This function returns the min size of the layout.
    /// @return  QSize (width x height)
    ///
    QSize minimumSize() const override;

    /// @brief This function sets the geometry of the layout.
    /// @param rect: dimension of rectangle (x, y ,width, height)
    /// @return  void
    ///
    void setGeometry(const QRect &rect) override;

    /// @brief This function calls the private member function calculateSize.
    /// @return  QSize (width x height)
    ///
    QSize sizeHint() const override;

    /// @brief This function returns the layout item.
    /// @param index: index of layout
    /// @return  QLayoutItem
    ///
    QLayoutItem *takeAt(int index) override;

    /// @brief This function adds the layout to the list.
    /// @param item: layout
    /// @param position: position of layout item
    /// @return  void
    ///
    void add(QLayoutItem *item, Position position);

private:
    struct ItemWrapper
    {
        ItemWrapper(QLayoutItem *i, Position p) {
            item = i;
            position = p;
        }

        QLayoutItem *item;
        Position position;
    };

    enum SizeType { MinimumSize, SizeHint };


    /// @brief This function calculates and returns the size of the layout.
    /// @return  QSize (width x height)
    ///
    QSize calculateSize(SizeType sizeType) const;

    QList<ItemWrapper *> mList;
};

#endif // WINDOWLAYOUT_H
