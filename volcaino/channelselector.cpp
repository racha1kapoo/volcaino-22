#include <QTreeWidgetItem>
#include <QCheckBox>
#include <QComboBox>

#include "DataBase.h"
#include "channelselector.h"
#include "qdatetime.h"
#include "ui_channelselector.h"

ChannelSelector::ChannelSelector(QString TableTobeMerged,QString CurrentTable,QString SyncChannel,InterpolateData interpolate,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChannelSelector)
{
    ui->setupUi(this);
    mTableToBeMerged = TableTobeMerged;
    mCurrentTable = CurrentTable;
    mSyncChannel = SyncChannel;
    miInterpolate = interpolate;

    InitUI();
}

ChannelSelector::~ChannelSelector()
{
    delete ui;
}
void ChannelSelector::InitUI()
{
    if(!DataBase::getInstance()->openDatabase())
        return;
    mDb = DataBase::getInstance();
    mSql = mDb->getSqlQuery();
    QString qsHeading = "Please use this dialog to merge the channels from \'"+ mTableToBeMerged
            +"\' to the exisitng or new  channels in \'"
            + mCurrentTable + "\' using the synchronization channel \'"
            +mSyncChannel+"\'" ;
    ui->ChannelSelectorLabel->setText(qsHeading);
    ui->ChannelSelectorLabel->setWordWrap(true);

    ui->ChannelSelectorTableWidget->setColumnCount(channelSelectorColumns::COL_SIZE);
    QStringList headers;
    headers << "Merge Database" << "Current Database";
    ui->ChannelSelectorTableWidget->setHorizontalHeaderLabels(headers);
    ui->ChannelSelectorTableWidget->horizontalHeader()->setStretchLastSection(true);
    ConnectSignalsToSlots();
    setWindowTitle("Merge Channels");
    setModal(true);
}
void ChannelSelector::ConnectSignalsToSlots()
{
    connect(ui->InsertPushButton,SIGNAL(clicked()),this,SLOT(insertRow()));
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(OnOk()));
}
void ChannelSelector::insertRow()
{
    QStringList currentlist =  mDb->getHeaderDetails(mCurrentTable);
    QStringList columnsToBeMerged = mDb->getHeaderDetails(mTableToBeMerged);
    int i = ui->ChannelSelectorTableWidget->rowCount();
    ui->ChannelSelectorTableWidget->setRowCount(i+1);

    QComboBox* pTobeMerged = new QComboBox();
    pTobeMerged->addItems(columnsToBeMerged);
    ui->ChannelSelectorTableWidget->setCellWidget(i,channelSelectorColumns::COL_TOBEMERGED,pTobeMerged);

    QComboBox* cb = new QComboBox();
    cb->addItems(currentlist);
    cb->setEditable(true);
    ui->ChannelSelectorTableWidget->setCellWidget(i,channelSelectorColumns::COL_EXISTING,cb);

}
void ChannelSelector::OnOk()
{
    QStringList currentlist =  mDb->getHeaderDetails(mCurrentTable);
    std::map<QString,SqliteColumninfo> allChansInfo;
    allChansInfo = mDb->getDbTableInfo(mTableToBeMerged);


    QMap<QString,QString> mappedChannels;
    QList<SqliteColumninfo> vColumnsToBeAdded;
    int no_of_rows = ui->ChannelSelectorTableWidget->rowCount();
    for (int i =0 ;i<no_of_rows;i++)
    {
        QComboBox* bBox = qobject_cast< QComboBox * >(ui->ChannelSelectorTableWidget->cellWidget(i,channelSelectorColumns::COL_TOBEMERGED));
        QComboBox* bExistingBox =  qobject_cast< QComboBox * >(ui->ChannelSelectorTableWidget->cellWidget(i,channelSelectorColumns::COL_EXISTING));
        if(!currentlist.contains(bExistingBox->currentText()) )
        {
            auto iter =allChansInfo.find(bBox->currentText().toUpper());
            SqliteColumninfo colInfo;
            colInfo.columnId=1;
            colInfo.columnName=bExistingBox->currentText();
            colInfo.type=iter->second.type;
            colInfo.NotNull=iter->second.NotNull;
            colInfo.defaultVal=iter->second.defaultVal;
            colInfo.pKey=iter->second.pKey;
            vColumnsToBeAdded.push_back(colInfo);
        }
        mappedChannels.insert(bBox->currentText(), bExistingBox->currentText());

    }
    //add new columns if user wants to copy the data.
    if(!vColumnsToBeAdded.empty())
    {
        for(SqliteColumninfo newColumn:vColumnsToBeAdded)
        {
            mDb->addColumn(mCurrentTable,newColumn.columnName,newColumn.type,newColumn.NotNull,newColumn.defaultVal);
        }
    }
    interpolateTable();
    QMap<QString,QString>::iterator itDB;
    int counter = 0;
    QString tempTable = "CREATE TABLE tempTable AS Select t.syncchannel as syncchannel";
    QString sQuery = "UPDATE "+mCurrentTable+" SET ";
    QString temp ;
    for (itDB = mappedChannels.begin(); itDB != mappedChannels.end(); itDB++)
    {

        QString toBeMergedChannel = itDB.key();
        QString existingChannel = itDB.value();
        temp = toBeMergedChannel;

        tempTable +=",t."+toBeMergedChannel+" as target"+QString::number(counter);
        if(counter>0)
            sQuery+=",";

        sQuery += existingChannel+"=(SELECT target"+QString::number(counter)+" FROM tempTable WHERE tempTable.rowid = "+mCurrentTable+".rowid)";
        counter++;
    }
    tempTable +=" FROM "+ mTableToBeMerged+" as t,"+ mCurrentTable +" as a WHERE a.syncchannel = t.syncchannel ;";
    mDb->executeQuery(tempTable);
    mDb->executeQuery(sQuery);

    QString dropTempTableQuery = "DROP TABLE tempTable";
    mDb->executeQuery(dropTempTableQuery);

    mDb->deleteColumn(mCurrentTable,"syncchannel");
    mDb->deleteColumn(mTableToBeMerged,"syncchannel");
}
void ChannelSelector::interpolateTable()
{
    //create a sync channel based on interpolation of single or both

    std::map<QString,SqliteColumninfo> allChansInfo;
    allChansInfo = mDb->getDbTableInfo(mTableToBeMerged);
    auto iter =allChansInfo.find(mSyncChannel.toUpper());
    SqliteColumninfo colInfo;
    colInfo.columnId=iter->second.columnId;
    colInfo.columnName="syncchannel";
    colInfo.type=iter->second.type;
    colInfo.NotNull=iter->second.NotNull;
    colInfo.defaultVal=iter->second.defaultVal;
    colInfo.pKey=iter->second.pKey;

    mDb->addColumn(mTableToBeMerged,colInfo.columnName,colInfo.type,colInfo.NotNull,colInfo.defaultVal);
    mDb->addColumn(mCurrentTable,colInfo.columnName,colInfo.type,colInfo.NotNull,colInfo.defaultVal);

    if(miInterpolate == CURRENTDB)
    {
        UpdateTempSyncChannelInExistingDB();
        InterpolateCurrentDB();
    }
    else if (miInterpolate == EXISTINGDB)
    {
        InterpolateExistingDB();
        UpdateTempSyncChannelInCurrentDB();
    }
    else if(miInterpolate == ALLDB )
    {
        InterpolateExistingDB();
        InterpolateCurrentDB();
    }
    else
    {
        UpdateTempSyncChannelInExistingDB();
        UpdateTempSyncChannelInCurrentDB();
    }
}
void ChannelSelector::InterpolateCurrentDB()
{
    InterpolateDBUtl(mCurrentTable);
}
void ChannelSelector::InterpolateExistingDB()
{
    InterpolateDBUtl(mTableToBeMerged);
}
void ChannelSelector::InterpolateDBUtl(QString qsTable)
{
   // int mTableRows = mDb->getRowCount(qsTable);
    mDb->startTransaction();
    std::map<QString,SqliteColumninfo> allChansInfo;
    allChansInfo = mDb->getDbTableInfo(qsTable);
    auto iter =allChansInfo.find(mSyncChannel.toUpper());
    QStringList typeList;
    typeList<<"syncchannel "+iter->second.type+",";
    mDb->createTable("TMP",typeList);
    QString allvaluesQuery="SELECT " +mSyncChannel +" FROM "+qsTable;
    double prev = 0;
    if (mSql.exec(allvaluesQuery))
    {
        while (mSql.next())
        {
            double val = mSql.value(0).toDouble();
            if(val == 0)
            {
                prev +=0.1;
                val = prev;
            }
            else
                prev = val;

            QString rec = QString::number(val, 'f', 4);
            mDb->insertRecord(rec,"TMP");
        }
    }
    QString  sQuery1 = "UPDATE "+qsTable+" SET syncchannel=(SELECT syncchannel FROM TMP WHERE "+ qsTable +".rowid = TMP.rowid)";
    mDb->executeQuery(sQuery1);

    QString query = "DROP TABLE TMP";
    mDb->executeQuery(query);
    mDb->closeTransaction();
}
void ChannelSelector::UpdateTempSyncChannelInExistingDB()
{
    UpdateTempSyncChannelInDBUtl(mTableToBeMerged);
}
void ChannelSelector::UpdateTempSyncChannelInCurrentDB()
{
    UpdateTempSyncChannelInDBUtl(mCurrentTable);
}
void ChannelSelector::UpdateTempSyncChannelInDBUtl(QString Table)
{
    mDb->copyColumn(Table,"syncchannel",mSyncChannel);
}

void ChannelSelector::GetChanAllVals(QString chanName,QString mTableName , double *outPuts)
{
    QString query="SELECT " +chanName +" FROM "+mTableName;
    int index = 0;

    if (mSql.exec(query))
    {
        while (mSql.next())
        {
            outPuts[index] = mSql.value(0).toDouble();
            index++;
        }
    }
}
