#ifndef CHANNELSELECTOR_H
#define CHANNELSELECTOR_H

#include <QDialog>
#include <QSqlQuery>
#include "DataBase.h"

namespace Ui {
class ChannelSelector;
}

class ChannelSelector : public QDialog
{
    Q_OBJECT

public:
    enum channelSelectorColumns
    {
        COL_TOBEMERGED,
        COL_EXISTING,
        COL_SIZE
    };

    enum InterpolateData
    {
        CURRENTDB,
        EXISTINGDB,
        ALLDB,
        NONE
    };
    /// @brief This parameterised constructor is responsible for creating a Channel selector Dialog
    /// @param TableTobeMerged:Name of the table to be merged
    /// @param CurrentTable:Name of the Current table
    /// @param SyncChannel: Name of Synchronization channel
    /// @param data: An object of the interpolation data
    /// @param parent: an object of QWidget that can be treated as parent
    explicit ChannelSelector(QString TableTobeMerged,QString CurrentTable,QString SyncChannel,InterpolateData data,
                             QWidget *parent = nullptr);
    /// @brief This is a destructor
    ~ChannelSelector();


private:
    /// @brief This function helps in setting up the initial UI of the Channel selector Dialog along with the
    /// various members required for other functions
    /// @return void
     void InitUI();
     /// @brief This function helps in setting up connections between various UI elements and Slots
     /// @return void
     void ConnectSignalsToSlots();

     //interpolate functions
     /// @brief This Function is used to interpolate the table.
     /// @return void
     void interpolateTable();
     //utility functions for interpolation
     /// @brief This function gets the channel values from the table.
     /// @param chanName:Name of the channel
     /// @param mTableName:Name of the Current table
     /// @param outPuts: an array of values from the table.
     /// @return void
     void GetChanAllVals(QString chanName,QString mTableName , double *outPuts);
     /// @brief This Function is used to interpolate the current table.
     /// @return void
     void InterpolateCurrentDB();
     /// @brief This Function is used to interpolate the table to be merged.
     /// @return void
     void InterpolateExistingDB();
     /// @brief This Function is a utility function used to interpolate the given table.
     /// @param TableName: Name of the table to be interpolated
     /// @return void
     void InterpolateDBUtl(QString Table);

     /// @brief This Function is used to add the interpolated sync channel to the table to be merged
     /// @return void
     void UpdateTempSyncChannelInExistingDB();
     /// @brief This Function is used to add the interpolated sync channel to the current table
     /// @return void
     void UpdateTempSyncChannelInCurrentDB();
     /// @brief This is a utility function used to add the interpolated sync channel to the given table
     /// @param:
     /// @return void
     void UpdateTempSyncChannelInDBUtl(QString Table);
public slots:
     void insertRow();
     void OnOk();
private:
    Ui::ChannelSelector *ui;
    QString mTableToBeMerged;
    QString mCurrentTable;
    QString mSyncChannel;
    InterpolateData miInterpolate;
    QSqlQuery mSql;
    DataBase* mDb;
};

#endif // CHANNELSELECTOR_H
