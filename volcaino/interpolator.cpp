#include "interpolator.h"

Interpolator::Interpolator()
{

}

Interpolator::Interpolator(QString tableName, std::vector<QMap<double, double>> data)
{
    setData(tableName, data);
}

void Interpolator::setData(QString tableName, std::vector<QMap<double, double>> data)
{
    mTableRows = DataBase::getInstance()->getRowCount(tableName);
    mX.clear();
    mY.clear();
    for (size_t i = 0; i < data.size(); i++) {
        std::vector<double> vecX;
        std::vector<double> vecY;
        for (double val : data.at(i).keys()) {
            vecX.push_back(val);
            vecY.push_back(data.at(i).value(val));
        }
        mX.push_back(vecX);
        mY.push_back(vecY);
    }
    mBeg = mX.at(0).front() + 1; // because value of double (x.front()) could be 13.5, int will consider this to be 13 -> if we evaluate spline(13) -> it doesn't exist -> crash.
    mEnd = mX.at(0).back();

    qDebug() << "vector of maps:" << data;
    qDebug() << "x:" << mX << "y:" << mY;
}

QtCharts::QSplineSeries *Interpolator::akimaInterp()
{
    mAkimaSplineData.clear();
    std::vector<std::vector<double>> mXcp = mX;
    std::vector<std::vector<double>> mYcp = mY;
    QtCharts::QSplineSeries *akimaSpline = new QtCharts::QSplineSeries;

    using boost::math::interpolators::makima;
    for (size_t i = 0; i < mX.size(); i++) {
        std::vector<double> vecData(mTableRows, 0);
        mAkimaSplineData.push_back(vecData);
        auto spline = makima<std::vector<double>>(std::move(mXcp.at(i)), std::move(mYcp.at(i)));
        for (int j = mBeg; j < mEnd; j++) {
            mAkimaSplineData.at(i).at(j) = spline(j);
            if (i == 0)
                akimaSpline->append(j, spline(j));
        }
    }

    return akimaSpline;
}

QtCharts::QLineSeries *Interpolator::linearInterp()
{
    mLinearSplineData.clear();
    QtCharts::QLineSeries *linearSeries = new QtCharts::QLineSeries;

    for (size_t i = 0; i < mX.size(); i++) {
        std::vector<double> vecData(mTableRows, 0);
        mLinearSplineData.push_back(vecData);
        for (size_t j = 0; j < mX.at(i).size() - 1; j++) {
            double x0 = mX.at(i).at(j);
            double y0 = mY.at(i).at(j);
            double x1 = mX.at(i).at(j + 1);
            double y1 = mY.at(i).at(j + 1);
            if (i == 0)
                linearSeries->append(x0, y0);
            mLinearSplineData.at(i).at(x0) = y0;
            for (int k = x0 + 1; k < x1; k++) {
                double val = y0 + ((y1 - y0) / (x1 - x0)) * (k - x0);
                mLinearSplineData.at(i).at(k) = val;
                if (i == 0)
                    linearSeries->append(k, val);
            }
        }
        if (i == 0)
            linearSeries->append(mX.back().back(), mY.back().back());
    }

    return linearSeries;
}

QtCharts::QSplineSeries *Interpolator::rationalInterp()
{
    mRationalSplineData.clear();
    std::vector<std::vector<double>> mXcp = mX;
    std::vector<std::vector<double>> mYcp = mY;
    QtCharts::QSplineSeries *rationalSpline = new QtCharts::QSplineSeries;

    using boost::math::interpolators::barycentric_rational;
    for (size_t i = 0; i < mX.size(); i++) {
        std::vector<double> vecData(mTableRows, 0);
        mRationalSplineData.push_back(vecData);
        auto spline = barycentric_rational<double>(mXcp.at(i).data(), mYcp.at(i).data(), mYcp.at(i).size(), 1);
        for (int j = mBeg; j < mEnd; j++) {
            mRationalSplineData.at(i).at(j) = spline(j);
            if (i == 0)
                rationalSpline->append(j, spline(j));
        }
    }

    return rationalSpline;
}

QtCharts::QSplineSeries *Interpolator::pCHIPInterp()
{
    mPCHIPSplineData.clear();
    std::vector<std::vector<double>> mXcp = mX;
    std::vector<std::vector<double>> mYcp = mY;
    QtCharts::QSplineSeries *pCHIPSpline = new QtCharts::QSplineSeries;

    using boost::math::interpolators::pchip;
    for (size_t i = 0; i < mX.size(); i++) {
        std::vector<double> vecData(mTableRows, 0);
        mPCHIPSplineData.push_back(vecData);
        auto spline = pchip<std::vector<double>>(std::move(mXcp.at(i)), std::move(mYcp.at(i)));
        for (int j = mBeg; j < mEnd; j++) {
            mPCHIPSplineData.at(i).at(j) = spline(j);
            if (i == 0)
                pCHIPSpline->append(j, spline(j));
        }
    }

    return pCHIPSpline;
}

std::vector<std::vector<double> > Interpolator::getAkimaSplineData()
{
    return mAkimaSplineData;
}

std::vector<std::vector<double> > Interpolator::getLinearSplineData()
{
    return mLinearSplineData;
}

std::vector<std::vector<double> > Interpolator::getRationalSplineData()
{
    return mRationalSplineData;
}

std::vector<std::vector<double> > Interpolator::getPCHIPSplineData()
{
    return mPCHIPSplineData;
}
