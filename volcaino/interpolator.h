#ifndef INTERPOLATOR_H
#define INTERPOLATOR_H

#include <QDebug>
#include <QSqlQuery>
#include <QPointF>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include "DataBase.h"
#include <boost/math/interpolators/makima.hpp>
#include <boost/math/interpolators/barycentric_rational.hpp>
#include <boost/math/interpolators/pchip.hpp>

class Interpolator
{
public:
    ///
    /// @brief Interpolator constructor function with no required parameters
    ///
    explicit Interpolator();
    ///
    /// @brief Interpolator constructor with parameters required
    /// @param tableName is the name of the open DB table in the main application
    /// @param data is a vector of QMaps, each QMap holding X and Y values of selected points as 'key and value' pairs
    ///
    explicit Interpolator(QString tableName, std::vector<QMap<double, double>> data);
    ///
    /// @brief setData sets the data of the selected points of the channel to internal data structures for further processing
    /// @param tableName is the name of the open DB table in the main application
    /// @param data is a vector of QMaps, each QMap holding X and Y values of selected points as 'key and value' pairs
    ///
    void setData(QString tableName, std::vector<QMap<double, double>> data);
    ///
    /// @brief akimaInterp interpolates given data using the modified Akima method
    /// @return corrected QSplineSeries pointer following the Akima method
    ///
    QtCharts::QSplineSeries *akimaInterp();
    ///
    /// @brief linearInterp interpolates given data using the linear method
    /// @return corrected QLineSeries pointer following the linear method
    ///
    QtCharts::QLineSeries *linearInterp();
    ///
    /// @brief rationalInterp interpolates given data using the rational method
    /// @return corrected QSplineSeries pointer following the rational method
    ///
    QtCharts::QSplineSeries *rationalInterp();
    ///
    /// @brief pCHIPInterp interpolates given data using the PCHIP method
    /// @return corrected QSplineSeries pointer following the PCHIP method
    ///
    QtCharts::QSplineSeries *pCHIPInterp();
    ///
    /// @brief getAkimaSplineData holds every Y value on the corrected Akima spline
    /// @return vector of vectors, holidng Y values of each corrected channel
    ///
    std::vector<std::vector<double>> getAkimaSplineData();
    ///
    /// @brief getLinearSplineData holds every Y value on the corrected linear spline
    /// @return vector of vectors, holidng Y values of each corrected channel
    ///
    std::vector<std::vector<double>> getLinearSplineData();
    ///
    /// @brief getRationalSplineData holds every Y value on the corrected rational spline
    /// @return vector of vectors, holidng Y values of each corrected channel
    ///
    std::vector<std::vector<double>> getRationalSplineData();
    ///
    /// @brief getPCHIPSplineData holds every Y value on the corrected PCHIP spline
    /// @return vector of vectors, holidng Y values of each corrected channel
    ///
    std::vector<std::vector<double>> getPCHIPSplineData();

private:
    std::vector<std::vector<double>> mX;
    std::vector<std::vector<double>> mY;
    std::vector<std::vector<double>> mAkimaSplineData;
    std::vector<std::vector<double>> mLinearSplineData;
    std::vector<std::vector<double>> mRationalSplineData;
    std::vector<std::vector<double>> mPCHIPSplineData;
    int mBeg;
    int mEnd;
    int mTableRows;
};

#endif // INTERPOLATOR_H
