#include "Login.h"
#include "DataBase.h"
#include <QApplication>
#include <QLocale>
#include "TypeDefConst.h"
#include <QStyleFactory>
#include <QSqlTableModel>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setStyle(QStyleFactory::create("Fusion"));
    app.setAttribute(Qt::AA_EnableHighDpiScaling);

    DataBase::getInstance()->createDatabase("MainThreadConnection");

    Login login;

    return app.exec();
}
