﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMovie>
#include "DatabaseTable.h"
#include "ImportFile.h"
#include "TypeDefConst.h"
#include <QSplitter>
#include <QTableWidget>
#include <QListWidget>
#include <QTextEdit>
#include <QMdiSubWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QPushButton>
#include "LanguageSelection.h"
#include <QTranslator>
#include <QSignalMapper>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include "ExportToFile.h"
#include "mergedialog.h"
#include "ChanMenuOpData.h"
#include "ProfileLevelingDialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), stopped{false}, mainWindow(new QWidget), mWindowList{}
{
    auto selectLanguage = LanguageSelection::getInstance();
    selectLanguage->setLanguage(selectLanguage->getLanguage(),translator);
    ui->setupUi(this);
    this->setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
    this->setWindowTitle("VOLCAINO");

    chanCal = nullptr;

    thread = new QThread(this);
    timer = new QTimer();

    layout = new WindowLayout;

    //mainWindow->setMinimumSize(this->size());

    //layout->addWidget(mainWindow, WindowLayout::Center);

    mdiArea = new QMdiArea(this);
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    mpSideWin = new QFrame;
    mpSideWin->setFrameStyle(QFrame::Box | QFrame::Raised);
    mpSideWin->setMinimumSize(this->width(),this->height());

    mpSideBar = new QFrame;
    mpSideBar->setFrameStyle(QFrame::Box | QFrame::Raised);
    mpSideBar->setMinimumSize(this->width()/10,this->height());

    mpToolBar = new QFrame;
    mpToolBar->setFrameStyle(QFrame::Box | QFrame::Raised);
    mpToolBar->setMinimumSize(this->width(),this->height()/8);

    DEBUG_TRACE(this->width());
    DEBUG_TRACE(this->height());

    layout->addWidget(mpSideBar, WindowLayout::Left);

    layout->addWidget(mpSideWin, WindowLayout::Left);

    layout->addWidget(mpToolBar, WindowLayout::Top);

    layout->addWidget(mdiArea, WindowLayout::Center);

    ui->centralwidget->setLayout(layout);

    this->createMenubar();

    this->createSidePane();

    this->createToolbar();

    mBar = new ProgressBar(this);

    connect(ImportFile::getInstance(),&ImportFile::currentProgress,mBar, &ProgressBar::updateProgress);
    connect(ImportFile::getInstance(),&ImportFile::finished,this,&MainWindow::stopMessage);

    //connect(ExportToFile::getInstance(),&ExportToFile::currentProgress,mBar, &ProgressBar::updateProgress);
    ExportToFile::getInstance()->setProgressBar(mBar);

    connect(this, &MainWindow::expInfoRdy, ExportToFile::getInstance(), &ExportToFile::getExpInfo);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createMenubar()
{
    this->stopped = false;

    QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
    QMenu *editMenu = menuBar()->addMenu(tr("Edit"));
    QMenu *settingMenu = menuBar()->addMenu(tr("Settings"));
    QMenu *dbMenu = menuBar()->addMenu(tr("Database"));
    QMenu *mapMenu = menuBar()->addMenu(tr("Map"));
    QMenu *aGISMenu = menuBar()->addMenu(tr("ArcGIS Tools"));
    QMenu *coordMenu = menuBar()->addMenu(tr("Coordinates"));
    QMenu *dbToolMenu = menuBar()->addMenu(tr("Database Tools"));
    QMenu *gridImageMenu = menuBar()->addMenu(tr("Grid and Image"));
    QMenu *mapToolMenu = menuBar()->addMenu(tr("Map Tools"));
    QMenu *secToolMenu = menuBar()->addMenu(tr("Section Tools"));
    QMenu *threeDMenu = menuBar()->addMenu(tr("3D"));
    QMenu *seekDataMenu = menuBar()->addMenu(tr("Seek Data"));
    QMenu *winMenu = menuBar()->addMenu(tr("Window"));
    QMenu *helpMenu = menuBar()->addMenu(tr("Help"));

    QAction *gridData = new QAction(tr("Grid Tool Box"), this);
    gridImageMenu->addAction(gridData);

    QAction *chanMath = new QAction(QIcon(":/ICONS/IMAGES/channelMath.png"), tr("Channel Math"), this);
    dbToolMenu->addAction(chanMath);

    QAction *channelMerge = new QAction(tr("&Merge Channels"), this);
    fileMenu->addAction(channelMerge);
    fileMenu->addSeparator();

    QMenu *import = fileMenu->addMenu(tr("&Import"));

    QAction *openCSV = new QAction(tr("&CSV Type"), this);
    import->addAction(openCSV);

    QAction *openXYZ = new QAction(tr("&XYZ Type"), this);
    import->addAction(openXYZ);

    QAction *openNUV = new QAction(tr("&NUV Type"), this);
    import->addAction(openNUV);

    QAction *openGBN = new QAction(tr("&GBN Type"), this);
    import->addAction(openGBN);

    QAction *profileLevelingAction = new QAction(tr("&Profile Leveling"), this);
    dbToolMenu->addAction(profileLevelingAction);
    dbToolMenu->addSeparator();
    connect(channelMerge, SIGNAL(triggered()), this , SLOT(CreateProject()));
    connect(profileLevelingAction, SIGNAL(triggered()), this, SLOT(ProfileLeveling()));

    connect(openCSV, SIGNAL(triggered()), DatabaseTable::getInstance(), SLOT(selectCSVFile()));
    connect(openXYZ, SIGNAL(triggered()), DatabaseTable::getInstance(), SLOT(selectXYZFile()));
    connect(openNUV, SIGNAL(triggered()), DatabaseTable::getInstance(), SLOT(selectNUVFile()));
    connect(openGBN, SIGNAL(triggered()), DatabaseTable::getInstance(), SLOT(selectGBNFile()));
    // connect(DatabaseTable::getInstance(),&DatabaseTable::setTable,this,&MainWindow::SetTable);

    connect(gridData, &QAction::triggered, this, &MainWindow::showGrid);

    connect(chanMath, &QAction::triggered, this, &MainWindow::showMathCalcWindow);

    QMenu *exportMenu = fileMenu->addMenu(tr("&Export"));

    QAction *exportCSV = new QAction(tr("&CSV Type"), this);
    exportMenu->addAction(exportCSV);

    QAction *exportXYZ = new QAction(tr("&XYZ Type"), this);
    exportMenu->addAction(exportXYZ);

    QAction *exportNUV = new QAction(tr("&NUV Type"), this);
    exportMenu->addAction(exportNUV);

    QAction *exportGBN = new QAction(tr("&GBN Type"), this);
    exportMenu->addAction(exportGBN);

    QSignalMapper* signalMapperCSV = new QSignalMapper(this);
    connect(exportCSV, SIGNAL(triggered()), signalMapperCSV, SLOT(map()));
    signalMapperCSV->setMapping(exportCSV, FileExtType::CSV);
    connect(signalMapperCSV, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    QSignalMapper* signalMapperXYZ = new QSignalMapper(this);
    connect(exportXYZ, SIGNAL(triggered()), signalMapperXYZ, SLOT(map()));
    signalMapperXYZ->setMapping(exportXYZ, FileExtType::XYZ);
    connect(signalMapperXYZ, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    QSignalMapper* signalMapperGBN = new QSignalMapper(this);
    connect(exportGBN, SIGNAL(triggered()), signalMapperGBN, SLOT(map()));
    signalMapperGBN->setMapping(exportGBN, FileExtType::gbn);
    connect(signalMapperGBN, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    QSignalMapper* signalMapperNUV = new QSignalMapper(this);
    connect(exportNUV, SIGNAL(triggered()), signalMapperNUV, SLOT(map()));
    signalMapperNUV->setMapping(exportNUV, FileExtType::NUV);
    connect(signalMapperNUV, &QSignalMapper::mappedInt ,ExportToFile::getInstance(), &ExportToFile::populateTableList);
}

void MainWindow::displayMessage()
{
    if (!stopped) {
        mpDialog->setWindowTitle("Alert!!!!!");
        mpDialog->setMaximumHeight(200);
        mpDialog->setMaximumWidth(200);
        mpDialog->setWindowFlags(mpDialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        QLabel *processLabel = new QLabel(mpDialog);
        processLabel->setGeometry(0, 0, 200, 200);
        QMovie *movie = new QMovie("C:/Users/rachana.kapoor/Documents/VOLCAINO/IMAGES/pleasewait.gif", QByteArray(), processLabel);
        processLabel->setMovie(movie);
        movie->stop();
        mpDialog->show();
        movie->start();
        DEBUG_TRACE(movie->state());
    }
}

void MainWindow::stopMessage()
{
    this->stopped = true;
    //mpDialog->close();
    mOldList.clear();
    mOldList = getDatabaseList();
    this->refereshDatabaseList();
}

void MainWindow::createSidePane()
{
    QWidget* tab = new QWidget(mpSideWin);

    mpSideDBButton = new QPushButton(mpSideBar);
    mpSideDBButton->setText("<");
    mpSideDBButton->setMaximumSize(30, 30);

    QStringList list = this->getDatabaseList();

    QVBoxLayout *tabLayout = new QVBoxLayout;
    tab->setLayout(tabLayout);

    mpSideTab = new QWidget;

    tabLayout->addWidget(mpSideTab);

    mTreeWidget = new QTreeWidget(mpSideWin);
    mTreeWidget->setMinimumSize(mpSideWin->size());
    mTreeWidget->setHeaderLabel("DATABASE");

    mTreeWidget->setWindowIcon(QIcon(":/ICONS/IMAGES/db.png"));
    mTreeWidget->model()->setHeaderData(0, Qt::Horizontal, QVariant::fromValue(QIcon(":/ICONS/IMAGES/db.png")), Qt::DecorationRole);

    mTreeWidget->setColumnCount(1);
    QList<QTreeWidgetItem *> items;
    for (int i = 0; i < list.size(); ++i)
        items.append(new QTreeWidgetItem(static_cast<QTreeWidget *>(nullptr), QStringList(QString(list[i]))));
    mTreeWidget->insertTopLevelItems(0, items);

    mTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(mTreeWidget, &QTreeWidget::customContextMenuRequested, this, &MainWindow::createContextMenu);
    connect(mTreeWidget, &QTreeWidget::itemClicked, this, &MainWindow::itemClicked);
    connect(mpSideDBButton, &QPushButton::clicked, this, &MainWindow::setHideUnhide);
}

void MainWindow::createContextMenu(const QPoint &pos)
{
    QMenu *menuTree = new QMenu(mTreeWidget);

    QAction *deleteAct = new QAction(tr("Delete"));
    menuTree->addAction(deleteAct);
    connect(deleteAct, &QAction::triggered, this, &MainWindow::deleteTable);

    QMenu *exportMenu = menuTree->addMenu(tr("Export"));

    QAction *exportCSV = new QAction(tr("CSV"));
    exportMenu->addAction(exportCSV);
    QSignalMapper* signalMapperCSV = new QSignalMapper(this);
    connect(exportCSV, SIGNAL(triggered()), signalMapperCSV, SLOT(map()));
    signalMapperCSV->setMapping(exportCSV, FileExtType::CSV);
    connect(signalMapperCSV, &QSignalMapper::mappedInt, this, &MainWindow::routeExportInfo);

    QAction *exportXYZ = new QAction(tr("XYZ"));
    exportMenu->addAction(exportXYZ);
    QSignalMapper* signalMapperXYZ = new QSignalMapper(this);
    connect(exportXYZ, SIGNAL(triggered()), signalMapperXYZ, SLOT(map()));
    signalMapperXYZ->setMapping(exportXYZ, FileExtType::XYZ);
    connect(signalMapperXYZ, &QSignalMapper::mappedInt, this, &MainWindow::routeExportInfo);

    QAction *exportNUV = new QAction(tr("NUV"));
    exportMenu->addAction(exportNUV);
    QSignalMapper* signalMapperNUV = new QSignalMapper(this);
    connect(exportNUV, SIGNAL(triggered()), signalMapperNUV, SLOT(map()));
    signalMapperNUV->setMapping(exportNUV, FileExtType::NUV);
    connect(signalMapperNUV, &QSignalMapper::mappedInt, this, &MainWindow::routeExportInfo);

    QAction *exportGBN = new QAction(tr("GBN"));
    exportMenu->addAction(exportGBN);
    QSignalMapper* signalMapperGBN = new QSignalMapper(this);
    connect(exportGBN, SIGNAL(triggered()), signalMapperGBN, SLOT(map()));
    signalMapperGBN->setMapping(exportGBN, FileExtType::gbn);
    connect(signalMapperGBN, &QSignalMapper::mappedInt, this, &MainWindow::routeExportInfo);

    menuTree->exec(QCursor::pos());
}

void MainWindow::routeExportInfo(int value)
{
    emit expInfoRdy(value, mTreeWidget->selectedItems().at(0)->text(0));
}

void MainWindow::deleteTable()
{
    QString tablename = mTreeWidget->selectedItems().at(0)->text(0);
    QString query = "DROP TABLE "+tablename;
    DataBase::getInstance()->getSqlQuery().exec(query);

    this->refereshDatabaseList();

    for (int i = 0; i < mdiArea->subWindowList().count(); i++) {
        if (mdiArea->subWindowList().at(i)->windowTitle() == tablename)
            mdiArea->subWindowList().at(i)->close();
    }
    delete mViews[tablename];
}

void MainWindow::showWindow()
{
    auto selectLanguage = LanguageSelection::getInstance();
    selectLanguage->setLanguage(selectLanguage->getLanguage(), translator);
    DEBUG_TRACE(selectLanguage);
    this->showMaximized();
    mpSize = new QSize(mdiArea->width(), mdiArea->height());
}

void MainWindow::itemClicked(QTreeWidgetItem *item,int column)
{
    bool hasit = false;
    if (mCanSelect) {
        if (mdiArea->subWindowList().count() == 0) {
            mCanSelect = false;
            auto *child = createMdiChild();
            child->itemClicked(item,column);
            tableview = child->getTableViewOperation();
            if (child->getTableViewOperation()->getChanMenuOp()->getChanMenuOpData()) {
                connect(child->getTableViewOperation()->getChanMenuOp()->getChanMenuOpData(), &ChanMenuOpData::fetchDBDone, this, [this]() {
                    mCanSelect = true;
                });
            } else {
                mCanSelect = true;
            }

            mViews.insert(item->text(column), tableview);
        } else {
            for (int i = 0; i < mdiArea->subWindowList().count(); i++) {
                if (mdiArea->subWindowList().at(i)->windowTitle() == item->text(column)) {
                    hasit = true;
                    if (chanCal != nullptr && chanCal->isVisible()) {
                        QString str = "Changing the current DB will close the Channel Calculator, are you sure?";
                        QMessageBox msgBox;
                        msgBox.setWindowTitle(tr("Prompt"));
                        msgBox.setText(str);
                        msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
                        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
                        msgBox.setDefaultButton(QMessageBox::No);
                        int ret = msgBox.exec();

                        switch (ret) {
                        case QMessageBox::Yes:
                            if (mdiArea->activeSubWindow()->windowTitle() == item->text(column)) {
                                break;
                            } else {
                                tableview = mViews[item->text(column)];
                                mdiArea->setActiveSubWindow(mdiArea->subWindowList().at(i));
                                chanCal->close();
                                break;
                            }
                        case QMessageBox::No:
                            msgBox.close();
                            break;
                        default:
                            break;
                        }
                    } else {
                        if (mdiArea->activeSubWindow()->windowTitle() == item->text(column)) {
                            break;
                        } else {
                            tableview = mViews[item->text(column)];
                            mdiArea->setActiveSubWindow(mdiArea->subWindowList().at(i));
                            break;
                        }
                    }
                }
            }
            if (!hasit) {
                auto *child = createMdiChild();
                child->itemClicked(item, column);
                tableview = child->getTableViewOperation();
                if (child->getTableViewOperation()->getChanMenuOp()->getChanMenuOpData()) {
                    connect(child->getTableViewOperation()->getChanMenuOp()->getChanMenuOpData(), &ChanMenuOpData::fetchDBDone, this, [this]() {
                        mCanSelect = true;
                    });
                } else {
                    mCanSelect = true;
                }

                tableview = child->getTableViewOperation();
                mViews.insert(item->text(column), tableview);
            }
        }
    }
}

TableView *MainWindow::createMdiChild()
{
    TableViewOperation *childTableViewOp = new TableViewOperation;
    TableView *child = new TableView(mdiArea);
    //child->setAttribute(Qt::WA_DeleteOnClose);
    mdiArea->addSubWindow(child);

    child->showMaximized();
    child->setMinimumSize(this->size()/6);
    child->setTableViewOperation(childTableViewOp);

    return child;
}

void MainWindow::SetTable(QString tableName)
{
    m_sTableToOpen = tableName;
}

void MainWindow::OpenTable(QString tableName)
{
    auto *child = createMdiChild();
    child->openTable(tableName);
}

void MainWindow::setHideUnhide(bool checked)
{
    if (mpSideTab->isVisible() == true) {
        mpSideTab->setVisible(false);
        mpSideWin->setMinimumSize(0, this->height());
        mpSideDBButton->setText(">");
    } else {
        mpSideTab->setVisible(true);
        mpSideWin->setMinimumSize(this->width()/6.4, this->height());
        mpSideDBButton->setText("<");
    }
}

void MainWindow::createToolbar()
{
    auto csvImport = new QPushButton(mpToolBar);
    csvImport->setMinimumSize(30, 30);

    csvImport->setIcon(QIcon(":/ICONS/IMAGES/CSV-import.svg"));

    csvImport->setIconSize(QSize(30, 30));
    //csvImport->setStyleSheet("QPushButton{border-radius:5px;border: 1px solid #345781;}") ;
    csvImport->move(10, 3);

    auto csvExport = new QPushButton(mpToolBar);
    csvExport->setMinimumSize(30, 30);

    csvExport->setIcon(QIcon(":/ICONS/IMAGES/CSV-export.svg"));

    csvExport->setIconSize(QSize(30, 30));
    //csvExport->setStyleSheet("QPushButton{border-radius:5px;border: 1px solid #345781;}") ;
    csvExport->move(50, 3);

    auto gbnImport = new QPushButton(mpToolBar);
    gbnImport->setMinimumSize(30, 30);

    gbnImport->setIcon(QIcon(":/ICONS/IMAGES/GBN-import.svg"));

    gbnImport->setIconSize(QSize(30, 30));
    gbnImport->move(90, 3);

    auto gbnExport = new QPushButton(mpToolBar);
    gbnExport->setMinimumSize(30, 30);

    gbnExport->setIcon(QIcon(":/ICONS/IMAGES/GBN-export.svg"));

    gbnExport->setIconSize(QSize(30, 30));
    gbnExport->move(130, 3);

    auto xyzImport = new QPushButton(mpToolBar);
    xyzImport->setMinimumSize(30, 30);

    xyzImport->setIcon(QIcon(":/ICONS/IMAGES/XYZ-import.svg"));

    xyzImport->setIconSize(QSize(30, 30));
    xyzImport->move(170, 3);

    auto xyzExport = new QPushButton(mpToolBar);
    xyzExport->setMinimumSize(30, 30);

    xyzExport->setIcon(QIcon(":/ICONS/IMAGES/XYZ-export.svg"));

    xyzExport->setIconSize(QSize(30, 30));
    xyzExport->move(210, 3);

    auto nuvImport = new QPushButton(mpToolBar);
    nuvImport->setMinimumSize(30, 30);

    nuvImport->setIcon(QIcon(":/ICONS/IMAGES/NUV-import.svg"));

    nuvImport->setIconSize(QSize(30, 30));
    nuvImport->move(250, 3);

    auto nuvExport = new QPushButton(mpToolBar);
    nuvExport->setMinimumSize(30, 30);
    nuvExport->setIcon(QIcon(":/new/prefix2/IMAGES/NUV-import.svg"));
    nuvExport->setIconSize(QSize(30, 30));
    nuvExport->move(290, 3);

    auto gridImage = new QPushButton(mpToolBar);
    gridImage->setMinimumSize(30, 30);

    gridImage->setIcon(QIcon(":/ICONS/IMAGES/gridImage-colored.png"));

    gridImage->setIconSize(QSize(30, 30));
    gridImage->move(330, 3);

    auto channelMath = new QPushButton(mpToolBar);
    channelMath->setMinimumSize(30, 30);

    channelMath->setIcon(QIcon(":/ICONS/IMAGES/channelMath.png"));

    channelMath->setIconSize(QSize(30, 30));
    channelMath->move(370, 3);

    connect(csvImport, SIGNAL(clicked()), DatabaseTable::getInstance(), SLOT(selectCSVFile()));
    connect(xyzImport, SIGNAL(clicked()), DatabaseTable::getInstance(), SLOT(selectXYZFile()));
    connect(nuvImport, SIGNAL(clicked()), DatabaseTable::getInstance(), SLOT(selectNUVFile()));
    connect(gbnImport, SIGNAL(clicked()), DatabaseTable::getInstance(), SLOT(selectGBNFile()));

    QSignalMapper *signalMapperCSV = new QSignalMapper(this);
    connect(csvExport, SIGNAL(clicked()), signalMapperCSV, SLOT(map()));
    signalMapperCSV->setMapping(csvExport, FileExtType::CSV);
    connect(signalMapperCSV, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    QSignalMapper *signalMapperXYZ = new QSignalMapper(this);
    connect(xyzExport, SIGNAL(clicked()), signalMapperXYZ, SLOT(map()));
    signalMapperXYZ->setMapping(xyzExport, FileExtType::XYZ);
    connect(signalMapperXYZ, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    QSignalMapper *signalMapperGBN = new QSignalMapper(this);
    connect(gbnExport, SIGNAL(clicked()), signalMapperGBN, SLOT(map()));
    signalMapperGBN->setMapping(gbnExport, FileExtType::gbn);
    connect(signalMapperGBN, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    QSignalMapper *signalMapperNUV = new QSignalMapper(this);
    connect(nuvExport, SIGNAL(clicked()), signalMapperNUV, SLOT(map()));
    signalMapperNUV->setMapping(nuvExport, FileExtType::NUV);
    connect(signalMapperNUV, &QSignalMapper::mappedInt, ExportToFile::getInstance(), &ExportToFile::populateTableList);

    connect(gridImage, &QPushButton::clicked, [this]() {
        this->showGrid();
    });

    connect(channelMath, &QPushButton::clicked, [this]() {
        this->showMathCalcWindow();
    });
}

QStringList MainWindow::getDatabaseList()
{
    QStringList list;
    auto db = DataBase::getInstance();
    if (db->openDatabase())
        list = db->getTableList();
    return list;
}

void MainWindow::showGrid(){
    if (DataBase::getInstance() && !mdiArea->subWindowList().isEmpty()) {
        DEBUG_TRACE(mdiArea->activeSubWindow()->windowTitle());
        mActiveTable = mdiArea->activeSubWindow()->windowTitle();
        mGrid = new GriddingToolBox(this, mActiveTable);
        mGrid->show();
    } else {
        ShowOpenTableErrorMessage();
    }
}

void MainWindow::showMathCalcWindow(){
    if (!exists && !mdiArea->subWindowList().isEmpty()) {
        DEBUG_TRACE(mdiArea->activeSubWindow()->windowTitle());
        mPrevTable = mdiArea->activeSubWindow()->windowTitle();
        chanCal = new ChanDataCalculator(this, mPrevTable);
        connect(chanCal, &ChanDataCalculator::calcDone, this, &MainWindow::refTable);
        chanCal->show();
        exists = true;
    } else if (DataBase::getInstance() && !mdiArea->subWindowList().isEmpty()) {
        mActiveTable = mdiArea->activeSubWindow()->windowTitle();
        if (mActiveTable != mPrevTable) {
            exists = false;
            showMathCalcWindow();
        } else {
            chanCal->show();
        }
    } else {
        ShowOpenTableErrorMessage();
    }
}

void MainWindow::refereshDatabaseList()
{
    QStringList list = this->getDatabaseList();
    mTreeWidget->clear();
    QList<QTreeWidgetItem *> items;
    for (int i = 0; i < list.size(); ++i)
        items.append(new QTreeWidgetItem(static_cast<QTreeWidget *>(nullptr), QStringList(QString(list[i]))));
    mTreeWidget->insertTopLevelItems(0, items);
    // if(!m_sTableToOpen.isEmpty() && list.contains(m_sTableToOpen))
    //   this->OpenTable(m_sTableToOpen);
    disconnect(mTreeWidget, &QTreeWidget::itemClicked, this, &MainWindow::itemClicked);
    connect(mTreeWidget, &QTreeWidget::itemClicked, this, &MainWindow::itemClicked);
}

void MainWindow::CreateProject()
{
    //QWizard* qw = new QWizard(this);
    // qw->setWizardStyle(QWizard::ModernStyle);
    //qw->show();
    if (DataBase::getInstance() && !mdiArea->subWindowList().isEmpty()) {
        DEBUG_TRACE(mdiArea->activeSubWindow()->windowTitle());
        mActiveTable=mdiArea->activeSubWindow()->windowTitle();
        MergeDialog *pMergeDialog = new MergeDialog(mActiveTable, this);
        pMergeDialog->exec();
    } else {
        ShowOpenTableErrorMessage();
    }
}

void MainWindow::ProfileLeveling()
{
    if (DataBase::getInstance() && !mdiArea->subWindowList().isEmpty()) {
        DEBUG_TRACE(mdiArea->activeSubWindow()->windowTitle());
        mActiveTable=mdiArea->activeSubWindow()->windowTitle();
        ProfileLevelingDialog *pDialog = new ProfileLevelingDialog(mActiveTable, this);
        pDialog->setMembers(mViews[mActiveTable]->getChanMenuOp()->getChanMenuOpData(), mViews[mActiveTable]->getArrayList());
        pDialog->exec();
    } else {
        ShowOpenTableErrorMessage();
    }
}

void MainWindow::ShowOpenTableErrorMessage()
{
    QString str = "Select a table from the Database first";
    QMessageBox msgBox;
    msgBox.setWindowTitle(tr("Prompt"));
    msgBox.setText(str);
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setWindowIcon(QIcon(":/ICONS/IMAGES/icon.svg"));
    int ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Ok:
        msgBox.close();
        break;
    default:
        break;
    }
}

void MainWindow::refTable()
{
    mViews[mdiArea->activeSubWindow()->windowTitle()]->refreshTable();
}
