/**
 *  @file   mainwindow.h
 *  @brief  This is the mainwindow class which is responsible to creating/setting layout of the main screen. It is also responsible for creating menu bar, tool bar, project explorer etc.
 *          It also sets the area to open/dispaly multiple database tables at a time.
 *  @author Rachana Kapoor
 *  @date   Dec 14,2021
 ***********************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QProgressDialog>
#include <QMessageBox>
#include <QDebug>
#include <QMdiArea>
#include <QListWidget>
#include "TableViewOperation.h"
#include "TableView.h"
#include "WindowLayout.h"
#include "TypeDefConst.h"
#include <QTranslator>
#include <QDockWidget>
#include <QThread>
#include <QTimer>
#include <QList>
#include <QMap>
#include "ChanDataCalculator.h"
#include "ChanCalDataHandling.h"
#include "GridToolBox.h"
#include "ProgressBar.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    ///Class constructor
    explicit MainWindow(QWidget *parent = nullptr);

    /// Class destructor
    ~MainWindow();

public slots:
    /// @brief This function is responsible for creating the menu bar and
    /// assigning actions to be taken when pressed.
    /// @return  void
    ///
    void createMenubar();

    /// @brief This function displays the progress wheel if the application
    /// is busy executing a certain task.
    /// @return  void
    ///
    void displayMessage();

    /// @brief This function closes the progress wheel when the ongoing task is completed.
    /// @return  void
    ///
    void stopMessage();

    /// @brief This function shows the list of database tables.
    /// @return  void
    ///
    void createSidePane();

    /// @brief This function shows the main window GUI.
    /// @return  void
    ///
    void showWindow();

    /// @brief This function creates a new table window
    /// @return TableViewOperation: An instance of a TableViewOperation
    ///
    TableView* createMdiChild();

    /// @brief This function is responsible for creating the tool bar items and
    /// assigning actions to be taken when pressed.
    /// @return  void
    ///
    void createToolbar();

    void SetTable(QString sTableName);
    void ShowOpenTableErrorMessage();
    ///
    /// @brief refTable refreshes the active DB table
    ///
    void refTable();

    /// @brief This function is called when database table is selected from the list of tables.
    /// @param item: An instance of QTreeWidgetItem
    /// @param column: index of a column
    /// @return  void
    ///
    void itemClicked(QTreeWidgetItem *item,int column);

    /// @brief This function hides/unhides the side pane/project explorer.
    /// @param bool: true/false (hides/unhides)
    /// @return void
    ///
    void setHideUnhide(bool checked);

private slots:
    /// @brief This function is responsible for checking whether the database table is opened or not. If the
    /// database table is opened, it will allow user to perform channel math functionality otherwise it will display an error.
    /// @return void
    ///
    void showMathCalcWindow();

    void showGrid();

    /// @brief This function returns a list of database tables.
    /// @return  QStringList: a list of tables
    ///
    QStringList getDatabaseList();

    /// @brief This function refresh the database list when user successfully imported any database file.
    /// @return void
    ///
    void refereshDatabaseList();

    void OpenTable(QString sTableName);

    void CreateProject();

    /// @brief This Function is used to delete a table
    /// @return void
    void deleteTable();

    /// @brief This Function is used to create a context menu on a point.
    /// @param Qpoint: provides a Qpoint containg the X and Y cordinates.
    /// @return void
    void createContextMenu(const QPoint &pos);

    void routeExportInfo(int value);

    /// @brief This Function is used to open the profile leveling dialog.
    /// @return void
    void ProfileLeveling();

signals:
    void expInfoRdy(int value, QString name);

private:
    QTranslator translator;

    Ui::MainWindow *ui;

    QDialog *mpDialog;

    bool stopped;
    bool exists = false;
    bool mCanSelect = true;

    QMdiArea *mdiArea;

    QSize *mpSize;

    QFrame *mpSideWin;
    QFrame *mpSideBar;
    QFrame *mpToolBar;

    QPushButton *mpSideDBButton;

    QWidget *mpSideTab;
    QWidget *mainWindow;

    WindowLayout *layout;

    QDockWidget *mpTable;
    QDockWidget *mpProfile;

    TableViewOperation *mpTableView;
    TableViewOperation *child;
    TableViewOperation *tableview;

    QThread *thread;

    QTimer *timer;

    QStringList mWindowList;
    QStringList mOldList;

    ChanDataCalculator *chanCal = nullptr;

    QString mActiveTable;
    QString mPrevTable;
    QString m_sTableToOpen;

    GriddingToolBox *mGrid;

    QTreeWidget *mTreeWidget;

    QMap<QString, TableViewOperation*> mViews;

    ProgressBar *mBar;
};

#endif // MAINWINDOW_H
