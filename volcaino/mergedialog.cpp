#include "mergedialog.h"

#include "ImportFile.h"
#include "ui_mergedialog.h"
#include "LanguageSelection.h"
#include <QTranslator>

MergeDialog::MergeDialog(QString tableName,QWidget *parent)
    : QDialog(parent), ui(new Ui::MergeDialog)
{
    QTranslator translator;
    auto selectLanguage=LanguageSelection::getInstance();
    selectLanguage->setLanguage(selectLanguage->getLanguage(),translator);
    ui->setupUi(this);
    mCurrentTable = tableName;
    InitUI();
    setModal(true);
}

MergeDialog::~MergeDialog()
{
    delete ui;
}

void MergeDialog::InitUI()
{
    if (DataBase::getInstance()->openDatabase())
        DEBUG_TRACE("List of Tables: " << DataBase::getInstance()->getTableList());

    ui->CurrentDatabaseCb->setVisible(false);
    QString currentText = ui->CurrentDatabaseLabel->text();
    currentText = currentText+" : " + mCurrentTable;
    ui->CurrentDatabaseLabel->setText(currentText);
    QStringList list = DataBase::getInstance()->getTableList();
    for (int i = 0; i < list.size(); ++i) {
        if (list[i] != mCurrentTable)
            ui->toBeMergedComboBox->addItem(list[i]);
    }
    UpdateOptions();
    UpdateListofSynchronizationChannel();
    ui->RenameChannelCb->setVisible(false);
    QString qsHeading ="Use this dialog to merge database into the current database "
                       "based on a common synchronization channel" ;

    ui->TitleLabel->setText(qsHeading);
    ui->TitleLabel->setWordWrap(true);
    setWindowTitle("Merge Channels");
    ConnectSignalsToSlots();
}

void MergeDialog:: updateImportVisibilty()
{
    ui->ImportLineEdit->setEnabled(ui->ImportRB->isChecked());
    ui->browseTB->setEnabled(ui->ImportRB->isChecked());
    ui->importLabel->setEnabled(ui->ImportRB->isChecked());
}

void MergeDialog:: updateDatabaseSelectVisibility()
{
    ui->toBeMergedComboBox->setEnabled(ui->UseExistingDatabaseRB->isChecked());
    ui->existingDatabaseLabel->setEnabled(ui->UseExistingDatabaseRB->isChecked());
}

void MergeDialog::ConnectSignalsToSlots()
{
    connect(ui->ImportRB, SIGNAL(clicked()), this, SLOT(UpdateOptions()));
    connect(ui->UseExistingDatabaseRB, SIGNAL(clicked()), this, SLOT(UpdateOptions()));
    connect(ui->browseTB, SIGNAL(clicked()), this, SLOT(OnBrowseButtonClicked()));
    connect(ui->InterpolationGB, SIGNAL(toggled(bool)), this, SLOT(OnInterpolationGbToggled()));
    connect(this, SIGNAL(accepted()), this, SLOT(OnOk()));
}

void MergeDialog::UpdateOptions()
{
    updateImportVisibilty();
    updateDatabaseSelectVisibility();
}

void MergeDialog::UpdateListofSynchronizationChannel()
{
    if (!DataBase::getInstance()->openDatabase())
        return;

   QStringList list = DataBase::getInstance()->getHeaderDetails(mCurrentTable);
   ui->SynchChannelCB->addItems(list);

}

void MergeDialog::OnBrowseButtonClicked()
{
    QString theInputFilePath =
            QFileDialog::getOpenFileName(this,
                                         tr("Open a file to merge"), QDir().dirName().append("../")
                                         ,("*.NUV;*.PEI;*.NDI;;*.GBN;;*.CSV;;*.XYZ"));

    QString inputPathTrimmed;

    if (!theInputFilePath.isEmpty())
       ui->ImportLineEdit->setText(theInputFilePath);
}

QString MergeDialog::GetMergeTable()
{
    return mMergeTable;
}

QString MergeDialog::GetTableFile()
{
    return mImportFilePath;
}

bool MergeDialog::CanRenameCommonColumns()
{
    return bRename;
}

QString MergeDialog::GetSynchronizationChannel()
{
    return mSynchronizationChannel;
}

void MergeDialog:: OnInterpolationGbToggled()
{
    bool bIsChecked = ui->InterpolationGB->isChecked();
    ui->CurrentDBRb->setEnabled(bIsChecked);
    ui->CurrentDBRb->setChecked(true);

    ui->ExistingDBRb->setEnabled(bIsChecked);
    ui->AllRb->setEnabled(bIsChecked);
}

ChannelSelector::InterpolateData MergeDialog::GetInterpolationValue()
{
     if (!ui->InterpolationGB->isChecked()) {
        return ChannelSelector::InterpolateData::NONE;
     } else {
        if (ui->CurrentDBRb->isChecked())
              return ChannelSelector::InterpolateData::CURRENTDB;
        if (ui->ExistingDBRb->isChecked())
              return ChannelSelector::InterpolateData::EXISTINGDB;
        if (ui->AllRb->isChecked())
              return ChannelSelector::InterpolateData::ALLDB;
     }

     return ChannelSelector::InterpolateData::NONE;
}

void MergeDialog::OnOk()
{
    mSynchronizationChannel = ui->SynchChannelCB->currentText();
    if (ui->ImportRB->isChecked()) {
        mImportFilePath = ui->ImportLineEdit->text();
        QString table = "";
        table = mImportFilePath.split('/').last();
        table.chop(4);
        table.remove(" ");
        ImportFile::getInstance()->setTableName(table);
        ImportFile::getInstance()->setFile(mImportFilePath);
        mMergeTable = table;
    } else {
        mMergeTable = ui->toBeMergedComboBox->currentText();
    }

     ChannelSelector *dialog = new ChannelSelector(mMergeTable, mCurrentTable, mSynchronizationChannel, GetInterpolationValue(), this->parentWidget());
     dialog->show();
}
