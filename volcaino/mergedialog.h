#ifndef MERGEDIALOG_H
#define MERGEDIALOG_H

#include <QDialog>
#include "DataBase.h"
#include "channelselector.h"
#include "TableViewOperation.h"

namespace Ui {
class MergeDialog;
}

class MergeDialog : public QDialog
{
    Q_OBJECT

public:
    /// @brief This parameterised constructor is responsible for creating the Merge Dialog
    /// @param tableName:Name of the current table
    /// @param parent: an object of QWidget that can be treated as parent
    explicit MergeDialog(QString tableName,QWidget *parent = nullptr);
     /// @brief This is a destructor
    ~MergeDialog();
private:
    /// @brief This function helps in setting up the initial UI of the Merge Dialog along with the
    /// various members required for other functions
    /// @return void
    void InitUI();
    /// @brief This function helps in setting up connections between various UI elements and Slots
    /// @return void
    void ConnectSignalsToSlots();
    /// @brief This function updates the list of synchronization channels from the current table.
    /// @return void
    void UpdateListofSynchronizationChannel();

public:
    /// @brief This function returns the name of the table to be merged.
    /// @return Name of the table to be merged
    QString GetMergeTable();
    /// @brief This function retunrs a path to the file that defines the table to be merged.
    /// @return QString
    QString GetTableFile();
    /// @brief This function lets us know whether we can rename common columns
    /// @return bool
    bool    CanRenameCommonColumns();
    /// @brief This function returns synchronization channel name
    /// @return QString
    QString GetSynchronizationChannel();
    /// @brief This function lets us know interpolation need to be done on which tables.
    /// @return Enum of interpoolation to be done on which table.
    ChannelSelector::InterpolateData GetInterpolationValue();
public slots:
    void updateImportVisibilty();
    void updateDatabaseSelectVisibility();
    void UpdateOptions();
private slots:
    void OnBrowseButtonClicked();
    void OnOk();
    void OnInterpolationGbToggled();
private:
    Ui::MergeDialog *ui;
    QString mCurrentTable;

    QString mMergeTable;
    QString mImportFilePath;
    QString mSynchronizationChannel;
    bool    bRename;

};

#endif // MERGEDIALOG_H
