#pragma once

#include <fstream>
#include <string_view>

// Geosoft
#include <windows.h>
#include <gxcpp_geogx.h>

#include "main.h"

/*
* This class needs serious refactoring!
* 
* It was implemented quickly for proof-of-concept and to not stall project.
* 
* Some things to keep in mind, right now ModifyVVChannel does most of the heavy lifting.
* It reads a line from the gdb and passes it back to main. There might be a better way to go 
* about implementing this. Also, consider decoupling it from main.
*/

using namespace geosoft::gx::geogx;

namespace geo {
	class gx {
	private:
		static void UNICODE_TO_UTF8(const wchar_t* pwUnicode, char* pcUTF8, long lUTF8Size);
		static void ModifyVVChannel(GXDBPtr db, std::string_view tableName);
	public:
		static void do_geosoft_stuff(std::wstring importFileName, std::string_view tableName);
	};
	inline void gx::UNICODE_TO_UTF8(const wchar_t* pwUnicode, char* pcUTF8, long lUTF8Size)
	{
		long lConv;
		lConv = WideCharToMultiByte(CP_UTF8, 0, pwUnicode, (int)wcslen(pwUnicode), pcUTF8, lUTF8Size, NULL, NULL);
		pcUTF8[max(0, min(lUTF8Size - 1, lConv))] = '\0';
	}
	inline void gx::ModifyVVChannel(GXDBPtr db, std::string_view tableName)
	{
		gx_string_type channelName = gx_string_type(gx_string_literal("rawmag"));
		auto dataSymbol = db->find_chan(channelName);
		channelName = gx_string_type(gx_string_literal("Xnutm09wgs84"));
		auto xSymbol = db->find_chan(channelName);
		channelName = gx_string_type(gx_string_literal("Ynutm09wgs84"));
		auto ySymbol = db->find_chan(channelName);

		// lock the channel for modification
		db->lock_symb(dataSymbol, DB_LOCK_READWRITE, DB_WAIT_INFINITY);
		db->lock_symb(xSymbol, DB_LOCK_READWRITE, DB_WAIT_INFINITY);
		db->lock_symb(ySymbol, DB_LOCK_READWRITE, DB_WAIT_INFINITY);

		// create VV to hold the channel data
		GXVVPtr vv1 = GXVV::create(GS_REAL, 16386);
		GXVVPtr vv2 = GXVV::create(GS_REAL, 16386);
		GXVVPtr vv3 = GXVV::create(GS_REAL, 16386);

		int count = 0;

		// loop through all lines to process the channel data
		for (int32_t curLineSymb = db->first_sel_line(); db->is_line_valid(curLineSymb); curLineSymb = db->next_sel_line(curLineSymb))
		{
			//read all data from the channel in the current line
			db->get_chan_vv(curLineSymb, dataSymbol, vv1);
			db->get_chan_vv(curLineSymb, xSymbol, vv2);
			db->get_chan_vv(curLineSymb, ySymbol, vv3);

			//number of rows for the channel
			int32_t nrows1 = vv1->length();
			int32_t nrows2 = vv2->length();
			int32_t nrows3 = vv3->length();

			//get the retrieved data as an array
			std::vector<double> channelArray1 = vv1->get_data<double>(0, nrows1);
			std::vector<double> channelArray2 = vv2->get_data<double>(0, nrows2);
			std::vector<double> channelArray3 = vv3->get_data<double>(0, nrows3);

			//modify data (add some value, as an example)
			for (int i = 0; i < nrows1; i++) {
				//std::cout << channelArray[i] << std::endl;
				write_gbn_to_db(count, channelArray2[i], channelArray3[i], channelArray1[i], tableName);
				count++;
			}
		}
	}
	inline void gx::do_geosoft_stuff(std::wstring importFileName, std::string_view tableName)
	{
#if 0
		// one example
		GX_OBJECT_PTR pGeo;
		auto ctx = GXContext::create_internal(pGeo);
#else
		// another example
		// Must create this object for remainder of code to work
		GXContextPtr ctx = GXContext::create(std::wstring(L"ChanAdd"), std::wstring(L"Geosoft Inc."));
#endif
	
		gx_string_type databaseName = gx_string_type(gx_string_literal("test.gdb"));
		gx_string_type gbnFileName = gx_string_type(importFileName.data());
		gx_string_type userName = gx_string_type(gx_string_literal("SUPER"));
		gx_string_type password = gx_string_type(gx_string_literal(""));
	
		GXDB::create(databaseName, 200, 50, 20000, 10, 10, userName, password);
		auto db = GXDB::open(databaseName, userName, password);
		GXDU::import_gbn(db, gbnFileName);
	
		//****************** do stuff ***********************
		if (db->check() != 0) {
			std::cout << "Database not ok!\n" << std::endl;
		}
	
		// get list of channels
		GXLSTPtr channelsList = GXLST::create(4096);
		db->chan_lst(channelsList);
	
		// open file to export list of channels
		//std::ofstream channelsListFile;
		//gx_string_type channelsListFileName = databaseName + gx_string_literal(".channel_list.utf8.txt");
		//channelsListFile.open(channelsListFileName, std::ios_base::trunc | std::ios_base::binary);
	
		// file with the list of channels is in UTF8
		//char utf8_signature[] = "\xEF\xBB\xBF";
		//channelsListFile << utf8_signature;
	
		auto size = channelsList->size();
		for (auto i = 0; i < size; i++) {
			gx_string_type name;
			int32_t symb;
			channelsList->gt_symb_item(i, name, symb);
	
			gx_string_type text =
				gx_string_literal("Channel[") + std::to_wstring(i) + gx_string_literal("]: ") +
				gx_string_literal("symbol = ") + std::to_wstring(symb) +
				gx_string_literal("   name = '") + name + gx_string_literal("'\r\n");
	
			//convert to UTF8
			char utf8[1024];
			UNICODE_TO_UTF8(text.c_str(), utf8, sizeof(utf8));
	
			//std::cout << utf8 << std::endl;
			//channelsListFile << utf8;
		}
	
		for (int32_t curLineSymb = db->first_sel_line(); db->is_line_valid(curLineSymb); curLineSymb = db->next_sel_line(curLineSymb))
		{
			gx_string_type name;
			db->get_symb_name(curLineSymb, name);
	
			gx_string_type text =
				gx_string_literal("  Line '") + name + gx_string_literal("' ") +
				gx_string_literal(" symbol = ") + std::to_wstring(curLineSymb) + gx_string_literal("\n");
	
			char utf8[1024];
			UNICODE_TO_UTF8(text.c_str(), utf8, sizeof(utf8));
			//std::cout << utf8 << std::endl;
		}

		gx_string_type channelName = gx_string_type(gx_string_literal("rawmag"));
		auto channelSymbol = db->find_chan(channelName);
	
		// modify the channel (just, as an example, add value to all its elements)
		int32_t nx = db->get_col_va(channelSymbol);
		if (nx > 1)
		{
			// VA channel
			//wprintf(L"VA channel '%s' found with %d columns", channelName.c_str(), nx);
			//ModifyVAChannel(db, channelSymbol, addValue);
		}
		else
		{
			// VV channel
			//wprintf(L"VV channel '%s' found", channelName.c_str());
			ModifyVVChannel(db, tableName);
		}

		// Discard changes so temp db doesn't bloat
		db->discard();
	}
}