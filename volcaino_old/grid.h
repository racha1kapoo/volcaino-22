#pragma once

#include <cmath>
#include <fstream>
#include <iostream>
#include <limits>
#include <sstream>
#include <utility>
#include <vector>

namespace volc {
	template <typename T>
	class grid {
	public:
		typedef std::vector<std::vector<T>> grid_t;

		typedef enum {
			GRID_ADD = 1,
			GRID_SUBTRACT,
			GRID_MULTIPLY,
			GRID_DIVIDE,
			GRID_POWER,
			GRID_LOG,
			GRID_LN,
			GRID_EXP,
			GRID_COS,
			GRID_SIN,
			GRID_TAN,
			GRID_CLIP,
		} user_math_operations_t;

		enum columns {
			INDEX_COLUMN = 0,
			EASTING_COLUMN = 1,
			NORTHING_COLUMN = 2,
			MEASUREMENT_COLUMN = 3,

			COLUMN_COUNT,
		};

		grid();
		grid(int rows, int cols);
		grid(const grid_t& values);
		grid<T> inv_dist_weighted_interp(int p, int windowSize) const;
		grid<T> binning_2d(int cellHeight, int cellWidth) const;
		grid<T> minimum_curvature(T T1, T omega) const;
		grid_t get_grid() const;
		void min_max();
		std::pair<int, int> get_size() const;

		grid<T> grid_add(const T scalar) const;
		grid<T> grid_subtract(const T scalar) const;
		grid<T> grid_multiply(const T scalar) const;
		grid<T> grid_divide(const T scalar) const;
		grid<T> grid_power(const T scalar) const;
		grid<T> grid_log() const;
		grid<T> grid_ln() const;
		grid<T> grid_exp() const;
		grid<T> grid_cos() const;
		grid<T> grid_sin() const;
		grid<T> grid_tan() const;
		grid<T> grid_clip(T min, T max) const;
	private:
		grid_t g;

		T minX = (std::numeric_limits<T>::max)();
		T maxX = (std::numeric_limits<T>::min)();
		T minY = (std::numeric_limits<T>::max)();
		T maxY = (std::numeric_limits<T>::min)();

		static T calculate_weight(int p, int rowOffset, int colOffset);
	};

	template<typename T>
	inline grid<T>::grid(int rows, int cols)
	{
		std::vector<T> gridRow(cols, 0);
		grid_t grid(rows, gridRow);
		g = grid;
	}
	template<typename T>
	inline grid<T>::grid(const grid_t& values) : g{ values } {}
	template<typename T>
	inline grid<T>::grid()
	{
		const int DEFAULT_SIZE = 0;
		std::vector<T> gridRow(DEFAULT_SIZE, 0);
		grid_t grid(DEFAULT_SIZE, gridRow);
		g = grid;
	}
	template<typename T>
	inline std::pair<int, int> grid<T>::get_size() const
	{
		if (g.empty()) {
			return std::make_pair(0, 0);
		}
		else {
			return std::make_pair((int)g.size(), (int)g.at(0).size());
		}
	}
	template<typename T>
	inline grid<T> grid<T>::inv_dist_weighted_interp(int p, int windowSize) const
	{
		std::vector<std::pair<int, int>> mask;

		auto padding = windowSize / 2;
		//auto padding = ((windowSize / 2) % 2) == 0 ? (windowSize / 2) : (windowSize / 2) - 1;

		for (int i = -padding; i <= padding; i++) {
			for (int j = -padding; j <= padding; j++) {
				if (i != 0 || j != 0) {
					mask.push_back(std::make_pair(i, j));
				}
			}
		}

		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();
		int cols = (int)g.at(0).size();
		std::vector<T> neighbours(mask.size());
		std::vector<T> weights(mask.size());

		for (int row = padding; row < rows - padding; row++) {
			for (int col = padding; col < cols - padding; col++) {
				//if (binnedData.at(row).at(col) == 0) {
				for (size_t i = 0; i < mask.size(); i++) {
					T weight = calculate_weight(p, mask.at(i).first, mask.at(i).second);
					neighbours.at(i) = weight * g.at(row + mask.at(i).first).at(col + mask.at(i).second);
					weights.at(i) = weight;
				}

#if 1
				T weightedNeighbourhoodSum = 0;
				T weightSum = 0;
				int count = 0;

				for (size_t i = 0; i < mask.size(); i++) {
					if (neighbours.at(i) != 0) {
						weightedNeighbourhoodSum += neighbours.at(i);
						weightSum += weights.at(i);
						count++;
					}
				}

				//for (auto it : neighbours) {
				//	if (it != 0) {
				//		weightedNeighbourhoodSum += it;
				//		count++;
				//		it = 0;
				//	}
				//}

				if (count > 0) {
					//result.at(row).at(col) = weightedNeighbourhoodSum / count;
					result.at(row).at(col) = weightedNeighbourhoodSum / weightSum;
				}
#else
				// new method
				constexpr type_t EPS = 0.000000001;
				type_t weightedNeighbourhoodSum = std::accumulate(neighbours.begin(), neighbours.end(), 0);
				type_t sumOfWeights = std::accumulate(weights.begin(), weights.end(), 0);
				result.at(row).at(col) = weightedNeighbourhoodSum / (sumOfWeights + EPS);
#endif
				//}
				//else {
				//	result.at(row).at(col) = binnedData.at(row).at(col);
				//}
			}
		}

		grid<T> localGrid(result);
		return localGrid;
	}
	template<typename T>
	inline grid<T> grid<T>::binning_2d(int cellHeight, int cellWidth) const
	{
		// determine ranges
		int rows = (int)std::round((maxX - minX) / cellHeight);
		int cols = (int)std::round((maxY - minY) / cellWidth);

		struct data
		{
			T measured = 0;
			int count = 0;
		};

		std::vector<data> preRow(cols, { 0.0, 0 });
		std::vector<std::vector<data>> preData(rows, preRow);
		std::vector<T> resultRow(cols, 0);
		grid_t result(rows, resultRow);

		for (size_t val = 0; val < g.size(); val++) {
			int rowMatch = 0;
			int colMatch = 0;

			T rangeX = (maxX - minX) / rows;
			T rangeY = (maxY - minY) / cols;
			rowMatch = (int)(std::floor(g.at(val).at(EASTING_COLUMN) - minX) / rangeX);
			colMatch = (int)(std::floor(g.at(val).at(NORTHING_COLUMN) - minY) / rangeY);

			preData.at(rowMatch).at(colMatch).measured += g.at(val).at(MEASUREMENT_COLUMN);
			preData.at(rowMatch).at(colMatch).count++;

			result.at(rowMatch).at(colMatch) = preData.at(rowMatch).at(colMatch).measured / preData.at(rowMatch).at(colMatch).count;
		}

		grid<T> localGrid(result);
		return localGrid;
	}
	template<typename T>
	inline grid<T> grid<T>::minimum_curvature(T T1, T omega) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();
		int cols = (int)g.at(0).size();

		T zTwoZero;
		T zNegTwoZero;
		T zZeroTwo;
		T zZeroNegTwo;
		T zOneOne;
		T zNegOneOne;
		T zOneNegOne;
		T zNegOneNegOne;
		T zOneZero;
		T zNegOneZero;
		T zZeroOne;
		T zZeroNegOne;

		T zNew;
		T zOld;

		const T alpha = 1;

		T val = 0;
		T avg = 1;
		int epoch = 0;
		const int MAX_EPOCHS = 100;
		T x0;
		T x1;
		T x2;

#if 1
		x0 = ((6 + 8 * pow(alpha, 2) + 6 * pow(alpha, 4)) * (1 - T1) + (2 * (1 + pow(alpha, 2)) * T1));
		x0 *= -1;
		x0 = 1.0 / x0;
		//std::cout << x0 << std::endl;	// should equal -0.05 if alpha = 1 and T1 = 0

		int padding = 2;
		for (int i = 0; i < MAX_EPOCHS; i++) {
			for (int row = 0; row < rows - padding; row++) {
				for (int col = 0; col < cols - padding; col++) {
					if (row == 0 || row == 1) {
						zTwoZero = 0;
					}
					else {
						zTwoZero = g.at(row - 2).at(col);
					}
					zNegTwoZero = g.at(row + 2).at(col);
					zZeroTwo = g.at(row).at(col + 2);
					if (col == 0 || col == 1) {
						zZeroNegTwo = 0;
					}
					else {
						zZeroNegTwo = g.at(row).at(col - 2);
					}
					if (row == 0) {
						zOneOne = 0;
					}
					else {
						zOneOne = g.at(row - 1).at(col + 1);
					}
					zNegOneOne = g.at(row + 1).at(col + 1);
					if (row == 0 || col == 0) {
						zOneNegOne = 0;
					}
					else {
						zOneNegOne = g.at(row - 1).at(col - 1);
					}
					if (col == 0) {
						zNegOneNegOne = 0;
					}
					else {
						zNegOneNegOne = g.at(row + 1).at(col - 1);
					}
					if (row == 0) {
						zOneZero = 0;
					}
					else {
						zOneZero = g.at(row - 1).at(col);
					}
					if (row == 0) {
						zNegOneZero = 0;
					}
					else {
						zNegOneZero = g.at(row - 1).at(col);
					}
					zZeroOne = g.at(row).at(col + 1);
					if (col == 0) {
						zZeroNegOne = 0;
					}
					else {
						zZeroNegOne = g.at(row).at(col - 1);
					}

					x1 = (1 - T1) * (zTwoZero + zNegTwoZero + pow(alpha, 4) * (zZeroTwo + zZeroNegTwo) + 2 * pow(alpha, 2) * (zOneOne + zNegOneOne + zOneNegOne + zNegOneNegOne));
					x2 = (4 * (1 + pow(alpha, 2)) * (1 - T1) + T1) * (zOneZero + zNegOneZero + pow(alpha, 2) * (zZeroOne + zZeroNegOne));
					zNew = x0 * (x1 - x2);
					zOld = result.at(row).at(col);
					result.at(row).at(col) = (1 - omega) * zOld + omega * zNew;

					val += abs(zNew - zOld);
				}
			}
			avg = val / (rows * cols);
			//std::cout << avg << std::endl;
			val = 0;
			epoch++;
		}
#else
		T eps = 0.01;

		type_t xi = 0;
		type_t alphaEta = 0;
		type_t eta = 0;

		T x3;
		T x4;

		type_t b5 = (2 * (1 + pow(alpha, 2))) / ((xi + alphaEta) * (1 + xi + alphaEta));
		type_t b4 = 1 - 0.5 * b5 * (eta + pow(eta, 2));
		type_t b3 = alphaEta * (1 + xi) * b5 - 2 * b4;
		type_t b2 = alphaEta * (1 + xi) * b5 - b3;
		type_t b1 = xi * b5 + b4 - b2;
		type_t bk = b1 + b2 + b3 + b4 + b5;

		x0 = T1 * bk - 2 * (1 - T1) * ((1 + pow(alpha, 4) - (1 + pow(alpha, 2) * bk)));
		x0 = 1.0 / x0;

		int padding = 2;
		for (int i = 0; i < MAX_EPOCHS; i++) {
			for (int row = padding; row < rows - padding; row++) {
				for (int col = padding; col < cols - padding; col++) {
					zTwoZero = binnedData.at(row - 2).at(col);
					zNegTwoZero = binnedData.at(row + 2).at(col);
					zZeroTwo = binnedData.at(row).at(col + 2);
					zZeroNegTwo = binnedData.at(row).at(col - 2);
					zOneOne = binnedData.at(row - 1).at(col + 1);
					zNegOneOne = binnedData.at(row + 1).at(col + 1);
					zOneNegOne = binnedData.at(row - 1).at(col - 1);
					zNegOneNegOne = binnedData.at(row + 1).at(col - 1);
					zOneZero = binnedData.at(row - 1).at(col);
					zNegOneZero = binnedData.at(row + 1).at(col);
					zZeroOne = binnedData.at(row).at(col + 1);
					zZeroNegOne = binnedData.at(row).at(col - 1);

					x1 = zTwoZero + zNegTwoZero + pow(alpha, 4) * (zZeroTwo + zZeroNegTwo);
					x2 = 2 * pow(alpha, 2) * (zOneOne + zOneNegOne + zNegOneOne + zNegOneNegOne);
					x3 = 2 * (1 + pow(alpha, 2)) * (zOneZero + zNegOneZero + pow(alpha, 2) * (zZeroOne + zZeroNegOne) + bk);	// missing bkzk
					x4 = T1 * bk;	// missing bkzk

					zNew = x0 * ((1 - T1) * (x1 + x2 - x3 - x4));
					zOld = result.at(row).at(col);
					result.at(row).at(col) = (1 - omega) * zOld + omega + zNew;

					val += abs(zNew - zOld);
				}
			}
			avg = val / (rows * cols);
			std::cout << avg << std::endl;
			val = 0;
			epoch++;
		}
#endif

		grid<T> localGrid(result);
		return localGrid;
	}
	template<typename T>
	inline std::vector<std::vector<T>> grid<T>::get_grid() const
	{
		return g;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_add(const T scalar) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = g.at(row).at(MEASUREMENT_COLUMN) + scalar;
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_subtract(const T scalar) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = g.at(row).at(MEASUREMENT_COLUMN) - scalar;
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_multiply(const T scalar) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = g.at(row).at(MEASUREMENT_COLUMN) * scalar;
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_divide(const T scalar) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = g.at(row).at(MEASUREMENT_COLUMN) / scalar;
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_power(const T scalar) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = pow(g.at(row).at(MEASUREMENT_COLUMN), scalar);
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_log() const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = log(g.at(row).at(MEASUREMENT_COLUMN));
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_ln() const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = log(g.at(row).at(MEASUREMENT_COLUMN));
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_exp() const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = exp(g.at(row).at(MEASUREMENT_COLUMN));
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_cos() const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = cos(g.at(row).at(MEASUREMENT_COLUMN));
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_sin() const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = sin(g.at(row).at(MEASUREMENT_COLUMN));
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_tan() const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();

		for (int row = 0; row < rows; row++) {
			result.at(row).at(INDEX_COLUMN) = g.at(row).at(INDEX_COLUMN);
			result.at(row).at(EASTING_COLUMN) = g.at(row).at(EASTING_COLUMN);
			result.at(row).at(NORTHING_COLUMN) = g.at(row).at(NORTHING_COLUMN);
			result.at(row).at(MEASUREMENT_COLUMN) = tan(g.at(row).at(MEASUREMENT_COLUMN));
		}

		return result;
	}
	template<typename T>
	inline grid<T> grid<T>::grid_clip(T min, T max) const
	{
		std::vector<T> resRow(g.at(0).size(), 0);
		grid_t result(g.size(), resRow);

		int rows = (int)g.size();
		int cols = (int)g.at(0).size();

		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				if (g.at(row).at(col) < min) {
					result.at(row).at(col) = min;
				}
				else {
					result.at(row).at(col) = g.at(row).at(col);
				}

				if (g.at(row).at(col) > max) {
					result.at(row).at(col) = max;
				}
				else {
					result.at(row).at(col) = g.at(row).at(col);
				}
			}
		}

		return result;
	}
	template<typename T>
	inline T grid<T>::calculate_weight(int p, int rowOffset, int colOffset)
	{
		const int cellSize = 25;
		const int POWER = 2;
		T result;

		result = sqrt(pow((0 + cellSize * rowOffset), POWER) + pow((0 + cellSize * colOffset), POWER));
		result = pow(result, p);
		result = 1 / result;

		return result;
	}
	template<typename T>
	inline void grid<T>::min_max() {
		for (size_t i = 0; i < g.size(); i++) {
			if (g.at(i).at(EASTING_COLUMN) < minX) {
				minX = g.at(i).at(EASTING_COLUMN);
			}
			if (g.at(i).at(EASTING_COLUMN) > maxX) {
				maxX = g.at(i).at(EASTING_COLUMN);
			}
			if (g.at(i).at(NORTHING_COLUMN) < minY) {
				minY = g.at(i).at(NORTHING_COLUMN);
			}
			if (g.at(i).at(NORTHING_COLUMN) > maxY) {
				maxY = g.at(i).at(NORTHING_COLUMN);
			}
		}
	}
}