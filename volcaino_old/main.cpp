#include <filesystem>
#include <iostream>
#include <sstream>
#include <string_view>

#include "db.h"
#include "geosoft.h"
#include "grid.h"
#include "main.h"

using namespace database;
using namespace geo;
using namespace volc;

static void write_csv(std::string_view filename, const grid<type_t>::grid_t& grid);
static std::vector<std::vector<type_t>> read_csv_write_to_table(std::string_view filename, std::string_view tableName);
static void create_new_project(std::string_view projectName);
static std::vector<std::string> list_existing_projects(void);
static std::vector<std::vector<type_t>> bin_table(std::string_view tableName);
static std::vector<std::string> list_existing_workspaces(std::string_view projectName);
static void new_table(std::string_view projectName, std::string_view columnName);
static void write_new_dataset(const grid<type_t>& rawDataset, std::string_view projectName, std::string_view columnName);
static void do_math(grid<type_t> rawDataset, std::string_view projectName);
static grid<type_t> create_mesh(grid<type_t> rawDataset, int cellWidth, int cellHeight);
static grid<type_t>::grid_t open_dataset(std::string_view tableName);

static grid<type_t>::grid_t read_csv_write_to_table(std::string_view fileName, std::string_view tableName)
{
	std::vector<type_t> inputRow(grid<type_t>::COLUMN_COUNT, 0);
	grid<type_t>::grid_t result(0, inputRow);

	// Create an input filestream
	std::ifstream myFile(fileName);

	// Make sure the file is open
	if (!myFile.is_open()) throw std::runtime_error("Could not open file");

	// Helper vars
	std::string line, colname;
	type_t val;

	// Read the column names
	if (myFile.good())
	{
		// Extract the first line in the file
		std::getline(myFile, line);

		// Create a stringstream from line
		std::stringstream ss(line);

		// Extract each column name
		while (std::getline(ss, colname, ',')) {

			// Initialize and add <colname, int vector> pairs to result
			//result.push_back({ colname, std::vector<int> {} });
		}
	}

	// Read data, line by line
	while (std::getline(myFile, line))
	{
		int idx = 0;
		type_t easting = 0;
		type_t northing = 0;
		type_t mag = 0;

		std::stringstream ss(line);
		int colIdx = 0;

		while (ss >> val) {
			inputRow.at(colIdx) = val;

			if (colIdx == 0) {
				idx = (int)val;
			}
			else if (colIdx == 1) {
				easting = val;
			}
			else if (colIdx == 2) {
				northing = val;
			}
			else if (colIdx == 3) {
				mag = val;
			}

			if (ss.peek() == ',') ss.ignore();

			colIdx++;
		}

		result.push_back(inputRow);

		db<type_t>::insert_into_dataset_table(idx, easting, northing, mag, tableName);
	}

	myFile.close();

	return result;
}

static void write_csv(std::string_view filename, const grid<type_t>::grid_t& grid)
{
	// Create an input filestream
	std::ofstream myFile(filename);

	// Make sure the file is open
	if (!myFile.is_open()) throw std::runtime_error("Could not open file");

	for (size_t i = 0; i < grid.size(); i++) {
		for (size_t j = 0; j < grid.at(i).size(); j++) {
			myFile << grid.at(i).at(j) << ",";
		}
		myFile << "\n";
	}

	myFile.close();
}

static std::vector<std::vector<type_t>> bin_table(std::string_view tableName) {
	std::vector<type_t> inputRow(grid<type_t>::COLUMN_COUNT, 0);
	grid<type_t>::grid_t result(0, inputRow);

	std::string qry1 = "select idx, easting, northing, mag from '";
	std::string qry2 = "';";
	std::ostringstream out;
	out << qry1 << tableName << qry2;
	std::string q = out.str();
	const char* qry = q.c_str();

	sqlite3_stmt* statement;
	int rc = sqlite3_prepare_v2(db<type_t>::get_db_handle(), qry, -1, &statement, NULL);
	if (rc == SQLITE_OK) {
		while (sqlite3_step(statement) != SQLITE_DONE) {
			for (int col = 0; col < grid<type_t>::COLUMN_COUNT; col++) {
				int colType = sqlite3_column_type(statement, col);
				switch (colType)
				{
				case SQLITE_INTEGER:
					inputRow.at(col) = sqlite3_column_int(statement, col);
					break;
				case SQLITE_FLOAT:
					inputRow.at(col) = sqlite3_column_double(statement, col);
					break;
				default:
					break;
				}
			}
			result.push_back(inputRow);
		}
	}

	rc = sqlite3_finalize(statement);

	return result;
}

static void import_dataset(std::string fileName, std::string tableName) {
	// Create the table
	//const std::string tableName = "datasets_raw";
	//const std::string tableName = "project_data_demo";
	char* zErrMsg = 0;
	//const char query[] = "create table datasets_raw(idx INTEGER, easting REAL, northing REAL, mag REAL);";
	std::string qry1 = "CREATE TABLE IF NOT EXISTS ";
	std::ostringstream out;
	out << qry1 << tableName << "(idx INTEGER, easting REAL, northing REAL, mag REAL);";
	std::string q = out.str();
	const char* qry = q.c_str();

	int rc = sqlite3_exec(db<type_t>::get_db_handle(), qry, NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK) {
		std::cout << "Error executing: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
	}

	db<type_t>::use_transaction(true);
	// Read the CSV file into the table
	read_csv_write_to_table(fileName, tableName);
	db<type_t>::use_transaction(false);
}

static grid<type_t>::grid_t open_dataset(std::string_view tableName) {
	// read the table into 2d vector
	grid<type_t>::grid_t result = bin_table(tableName);

	return result;
}

static grid<type_t> create_mesh(grid<type_t> rawDataset, int cellWidth, int cellHeight) {
	grid<type_t> result;

	rawDataset.min_max();
	result = rawDataset.binning_2d(cellHeight, cellWidth);

	return result;
}

grid<type_t> interpolate(grid<type_t> binnedData, int p, int windowSize) {
	grid<type_t> result;
	//int p = 2;
	//int windowSize = 5;
	result = binnedData.inv_dist_weighted_interp(p, windowSize);

	return result;
}

grid<type_t> min_curve(grid<type_t> idw, type_t T1, type_t omega) {
	grid<type_t> result;
	//type_t T1 = 0.35;
	//type_t omega = 0.2;
#if 1
	result = idw.minimum_curvature(T1, omega);
#else
	const int ITERATIONS = 10;
	for (int i = 0; i < ITERATIONS; i++) {
		for (int j = 0; j <= ITERATIONS; j++) {
			type_t val1 = i * T1;
			type_t val2 = j * omega;
			grid<type_t>::grid_t mc = grid<type_t>::minimum_curvature(idr, val1, val2);
			int ival1 = val1 * 100;
			int ival2 = val2 * 10;
			std::string fileName = "results\\mc_" + std::to_string(ival1) + "_" + std::to_string(ival2) + ".csv";
			files<type_t>::write_csv(fileName, mc);
		}
	}
#endif

	return result;
}

static void new_table(std::string_view projectName, std::string_view columnName) {
	// if transforms table doesn't exist create one
	db<type_t>::create_new_table(projectName);
	// write new dataset to transforms table
	std::ostringstream out;
	out << "ALTER TABLE workspace_transforms_" << projectName << " ADD COLUMN " << columnName << " REAL;";
	db<type_t>::general_insert(out.str());
}

static void write_new_dataset(const grid<type_t>& rawDataset, std::string_view projectName, std::string_view columnName) {
	const int EASTING_COLUMN = 1;
	const int NORTHING_COLUMN = 2;
	const int NEW_VALUE_COLUMN = 3;

	int size = rawDataset.get_size().first;
	grid<type_t>::grid_t localGrid = rawDataset.get_grid();

	db<type_t>::use_transaction(true);
	for (size_t rows = 0; rows < size; rows++) {
		std::ostringstream out;
		out << "UPDATE workspace_transforms_" << projectName << " SET "
			<< columnName << "="
			<< std::to_string(localGrid.at(rows).at(NEW_VALUE_COLUMN))
			<< " WHERE easting="
			<< std::to_string(localGrid.at(rows).at(EASTING_COLUMN))
			<< " AND northing="
			<< std::to_string(localGrid.at(rows).at(NORTHING_COLUMN))
			<< ";";
		db<type_t>::general_insert(out.str());
	}
	db<type_t>::use_transaction(false);
}

static void do_math(grid<type_t> rawDataset, std::string_view projectName) {
	type_t mathOperator;
	int selection;
	std::string columnName = "default";

	std::cout << "Choose math option:" << std::endl;
	std::cout <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_ADD << ". Add\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_SUBTRACT << ". Subtract\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_MULTIPLY << ". Multiply\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_DIVIDE << ". Divide\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_POWER << ". Power\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_LOG << ". Log\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_LN << ". Ln\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_EXP << ". Exp\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_COS << ". Cos\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_SIN << ". Sin\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_TAN << ". Tan\n" <<
		"\t" << grid<type_t>::user_math_operations_t::GRID_CLIP << ". Clip\n" <<
		std::endl;

	std::cin >> selection;

	switch (selection)
	{
	case grid<type_t>::user_math_operations_t::GRID_ADD:
		std::cout << "Enter number to add to dataset" << std::endl;
		std::cin >> mathOperator;

		columnName = "addition";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_add(mathOperator);
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_SUBTRACT:
		std::cout << "Enter number to subtract from dataset" << std::endl;
		std::cin >> mathOperator;

		columnName = "subtraction";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_subtract(mathOperator);
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_MULTIPLY:
		std::cout << "Enter number to multiply dataset by" << std::endl;
		std::cin >> mathOperator;

		columnName = "multiplication";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_multiply(mathOperator);
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_DIVIDE:
		std::cout << "Enter number to divide dataset by" << std::endl;
		std::cin >> mathOperator;

		columnName = "division";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_divide(mathOperator);
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_POWER:
		std::cout << "Enter number to divide dataset by" << std::endl;
		std::cin >> mathOperator;

		columnName = "power";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_power(mathOperator);
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_LOG:
		columnName = "log";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_log();
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_LN:
		columnName = "ln";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_ln();
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_EXP:
		columnName = "exp";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_exp();
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_COS:
		columnName = "cos";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_cos();
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_SIN:
		columnName = "sin";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_sin();
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_TAN:
		columnName = "tan";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_tan();
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	case grid<type_t>::user_math_operations_t::GRID_CLIP: {
		type_t minClipping;
		type_t maxClipping;
		std::cout << "Enter number to min clipping dataset by" << std::endl;
		std::cin >> minClipping;
		std::cout << "Enter number to max clipping dataset by" << std::endl;
		std::cin >> maxClipping;

		columnName = "division";
		new_table(projectName, columnName);
		rawDataset = rawDataset.grid_clip(minClipping, maxClipping);
		write_new_dataset(rawDataset, projectName, columnName);

		break;
	}
	default:
		break;
	}
}

void write_gbn_to_db(int count, type_t easting, type_t northing, type_t mag, std::string_view tableName) {
	db<type_t>::insert_into_dataset_table(count, easting, northing, mag, tableName);
}

int main() {
	// Create a local folder for file output beside the application
	std::filesystem::create_directory("results");
	
	int response = 0;

	auto rawDataset = std::make_unique<grid<type_t>>();
	auto binnedDataset = std::make_unique<grid<type_t>>();
	auto idwDataset = std::make_unique<grid<type_t>>();
	auto mcDataset = std::make_unique<grid<type_t>>();

	// Open a local database
	response = db<type_t>::open_database();
	if (response != SQLITE_OK) {
		return -1;
	}

	std::cout << "Welcome to VolcAIno\n" << std::endl;

	int selection = 0;
	std::string resp;

	// User parameters
	const int DEFAULT_CELL_HEIGHT = 25;
	const int DEFAULT_CELL_WIDTH = 25;
	const int DEFAULT_P = 2;
	const int DEFAULT_WINDOW_SIZE = 5;
	const type_t DEFAULT_T1 = 0.35;
	const type_t DEFAULT_OMEGA = 0.2;
	int cellHeight = DEFAULT_CELL_HEIGHT;
	int cellWidth = DEFAULT_CELL_WIDTH;
	int p = DEFAULT_P;
	int windowSize = DEFAULT_WINDOW_SIZE;
	type_t T1 = DEFAULT_T1;
	type_t omega = DEFAULT_OMEGA;

	std::vector<std::string> projectList;
	std::string projectName;
	std::string fileName;
	std::string tableName;

	typedef enum {
		CREATE_PROJECT = 1,
		OPEN_PROJECT,
		OPEN_WORKSPACE,
		CREATE_MESH,
		INTERPOLATE,
		MIN_CURVE,
		IMPORT_CSV,
		EXPORT_CSV,
		GRID_MATH,
		GEOSOFT_IMPORT,
		GET_VERSION_NUMBER,
		USER_EXIT, // Make sure this comes last!
	} user_states_t;

	while (selection != USER_EXIT) {
		std::cout << "Please select an option" << std::endl;
		std::cout << "\t" << CREATE_PROJECT << ". Create new project\n" <<
			"\t" << OPEN_PROJECT << ". Open existing project\n" <<
			"\t" << OPEN_WORKSPACE << ". Open workspace\n" <<
			"\t" << CREATE_MESH << ". Create mesh\n" <<
			"\t" << INTERPOLATE << ". Interpolate\n" <<
			"\t" << MIN_CURVE << ". Minimum Curvature\n" <<
			"\t" << IMPORT_CSV << ". Import CSV\n" <<
			"\t" << EXPORT_CSV << ". Export CSV\n" <<
			"\t" << GRID_MATH << ". Grid math\n" <<
			"\t" << GEOSOFT_IMPORT << ". Import Geosoft .gbn file\n" <<
			"\t" << GET_VERSION_NUMBER << ". Get version number\n" <<
			"\t" << USER_EXIT << ". Exit" << std::endl;

		std::cin >> selection;

		switch (selection)
		{
		case CREATE_PROJECT:
			//create_project();
			std::cout << "Enter name for new project" << std::endl;
			std::cin >> projectName;
			tableName = "project_data_" + projectName;

			create_new_project(projectName);
			std::cout << "Success!" << std::endl;

			break;
		case OPEN_PROJECT:
		case EXPORT_CSV: {
			projectList = list_existing_projects();
			if (projectList.empty() == false) {
				std::cout << "Please select a project" << std::endl;

				std::cin >> selection;
				// decrement by 1 for non-zero vector indexing
				selection -= 1;
				if (selection >= 0 && selection < projectList.size()) {
					tableName = "project_data_" + projectList.at(selection);
					projectName = projectList.at(selection);

					*rawDataset = open_dataset(tableName);

					std::cout << "Success!" << std::endl;

					std::cout << "Write to .csv? (y/n)" << std::endl;
					std::cin >> resp;
					if (resp.compare("y") == 0) {
						write_csv("results\\raw.csv", rawDataset->get_grid());
						std::cout << "Raw file extracted. See 'raw.csv' in the results folder\n" << std::endl;
					}
				}
				else {
					std::cout << "Please enter a valid project" << std::endl;
				}
			}
			else {
				std::cout << "No existing projects" << std::endl;
			}

			break;
		}
		case OPEN_WORKSPACE:
			if (tableName.empty()) {
				std::cout << "Please complete steps " << CREATE_PROJECT << " or " <<
					OPEN_PROJECT << " first" << std::endl;
			}
			else {
				std::cout << "Please select a workspace" << std::endl;
				projectList = list_existing_workspaces(projectName);

				std::cin >> selection;
				if (selection >= 0 && selection < projectList.size()) {
					tableName = "project_data_" + projectList.at(selection);

					*rawDataset = open_dataset(tableName);

					std::cout << "Success!" << std::endl;
				}
				else {
					std::cout << "Please enter a valid workspace" << std::endl;
				}
			}

			break;
		case CREATE_MESH:
			if (rawDataset->get_size().first != 0 && rawDataset->get_size().second != 0) {
				std::cout << "Please enter cell height (m) (default=" << DEFAULT_CELL_HEIGHT << ")" << std::endl;
				std::cin >> cellHeight;
				std::cout << "Please enter cell width (m) (default=" << DEFAULT_CELL_WIDTH << ")" << std::endl;
				std::cin >> cellWidth;

				*binnedDataset = create_mesh(*rawDataset, cellHeight, cellWidth);
				std::cout << "Success" << std::endl;

				std::cout << "Write to .csv? (y/n)" << std::endl;
				std::cin >> resp;
				if (resp.compare("y") == 0) {
					write_csv("results\\mesh.csv", binnedDataset->get_grid());
					std::cout << "Mesh created. See 'mesh.csv' in the results folder\n" << std::endl;
				}
			}
			else {
				std::cout << "Please complete step " << OPEN_PROJECT <<  " first, or, import data into project" << std::endl;
			}

			break;
		case INTERPOLATE:
			if (binnedDataset->get_size().first != 0 && binnedDataset->get_size().second != 0) {
				std::cout << "Please enter p (default=" << DEFAULT_P << ")" << std::endl;
				std::cin >> p;
				std::cout << "Please enter windowSize (default=" << DEFAULT_WINDOW_SIZE << ")" << std::endl;
				std::cin >> windowSize;

				*idwDataset = interpolate(*binnedDataset, p, windowSize);
				std::cout << "Success" << std::endl;

				std::cout << "Write to .csv? (y/n)" << std::endl;
				std::cin >> resp;
				if (resp.compare("y") == 0) {
					write_csv("results\\interpolated.csv", idwDataset->get_grid());
					std::cout << "Interpolation created. See 'interpolated.csv' in the results folder\n" << std::endl;
				}
			}
			else {
				std::cout << "Please complete step " << CREATE_MESH << " first" << std::endl;
			}

			break;
		case GRID_MATH:
			do_math(*rawDataset, projectName);

			break;
		case MIN_CURVE:
			if (idwDataset->get_size().first != 0 && idwDataset->get_size().second != 0) {
				std::cout << "Please enter T1 (default=" << DEFAULT_T1 << ")" << std::endl;
				std::cin >> T1;
				std::cout << "Please enter omega (default=" << DEFAULT_OMEGA << ")" << std::endl;
				std::cin >> omega;

				*mcDataset = min_curve(*idwDataset, T1, omega);
				std::cout << "Success" << std::endl;

				std::cout << "Write to .csv? (y/n)" << std::endl;
				std::cin >> resp;
				if (resp.compare("y") == 0) {
					write_csv("results\\min_curve.csv", mcDataset->get_grid());
					std::cout << "Minimum curvature created. See 'min_curve.csv' in the results folder\n" << std::endl;
				}
			}
			else {
				std::cout << "Please complete step " << INTERPOLATE << " first" << std::endl;
			}

			break;
		case IMPORT_CSV: {
			if (tableName.empty()) {
				std::cout << "Please complete steps " << CREATE_PROJECT << " or " <<
					OPEN_PROJECT << " first" << std::endl;
			}
			else {
				std::cout << "Please enter the name of the file to import" << std::endl;
				std::cin >> fileName;

				import_dataset(fileName, tableName);
			}

			break;
		}
		case USER_EXIT: {
			std::cout << "Exiting" << std::endl;

			break;
		}
		case GEOSOFT_IMPORT: {
			if (tableName.empty()) {
				std::cout << "Please complete steps " << CREATE_PROJECT << " or " <<
					OPEN_PROJECT << " first" << std::endl;
			}
			else {
				// Create the table
				char* zErrMsg = 0;
				//const char query[] = "create table datasets_raw(idx INTEGER, easting REAL, northing REAL, mag REAL);";
				std::string qry1 = "CREATE TABLE IF NOT EXISTS ";
				std::ostringstream out;
				out << qry1 << tableName << "(idx INTEGER, easting REAL, northing REAL, mag REAL);";
				std::string q = out.str();
				const char* qry = q.c_str();

				int rc = sqlite3_exec(db<type_t>::get_db_handle(), qry, NULL, 0, &zErrMsg);
				if (rc != SQLITE_OK) {
					std::cout << "Error executing: " << zErrMsg << std::endl;
					sqlite3_free(zErrMsg);
				}

				std::cout << "Please enter the name of the file to import" << std::endl;
				std::cin >> fileName;

				std::wstring fn(fileName.length(), L' ');
				std::copy(fileName.begin(), fileName.end(), fn.begin());
				db<type_t>::use_transaction(true);
				gx::do_geosoft_stuff(fn, tableName);
				db<type_t>::use_transaction(false);
			}
			
			break;
		}
		case GET_VERSION_NUMBER: {
			std::string version = MAJOR + "." + MINOR + "." + BUILD + "." + REV;
			std::cout << "Version: " << version << std::endl;

			break;
		}
		default: {
			std::cout << "That wasn't an option foo! I'm outta here!" << std::endl;
			selection = USER_EXIT;

			break;
		}
		}
	}

	db<type_t>::close_database();

	return 0;
}

void create_new_project(std::string_view projectName) {
	// 1) add new entry to project table (project_name, creation_date, last_save_date, puid)
	// 2) create default entry in workspace table

	// 1) add new entry to project table (project_name, creation_date, last_save_date, puid)
	std::string qryStart = "insert into projects(project_name, creation_date, last_save_date) values('";
	std::string qryEnd = "', strftime('%s','Now'), strftime('%s','Now'));";
	std::ostringstream out;
	out << qryStart << projectName << qryEnd;
	db<type_t>::general_insert(out.str());

	// 1b) querey table to get puid of the newley added object
	qryStart = "select puid from projects where project_name='";
	std::ostringstream out2;
	out2 << qryStart << projectName << "';";
	std::string qry = out2.str();
	const char* q = qry.c_str();
	int puid = 0;
	sqlite3_stmt* statement;
	int rc = sqlite3_prepare_v2(db<type_t>::get_db_handle(), q, -1, &statement, NULL);
	while (sqlite3_step(statement) != SQLITE_DONE) {
		puid = sqlite3_column_int(statement, 0);
	}
	rc = sqlite3_finalize(statement);

	// 2) create default entry in workspace table
	qryStart = "insert into workspaces(puid, workspace_name, creation_date, last_save_date) values(";
	qryEnd = ", 'default', strftime('%s', 'now'), strftime('%s', 'now'));";
	std::ostringstream out3;
	out3 << qryStart << puid << qryEnd;
	db<type_t>::general_insert(out3.str());
}

enum class states {
	TRY_SELECT,
	READ_PROJECTS,
	CREATE_NEW_TABLE,
	DONE,
};

std::vector<std::string> list_existing_projects() {
	sqlite3_stmt* statement = nullptr;
	states step = states::TRY_SELECT;
	int rc = SQLITE_OK;
	std::vector<std::string> projectList;

	while (step != states::DONE) {
		switch (step)
		{
		case states::TRY_SELECT: {
			std::string qryStart = "select project_name from projects;";
			std::ostringstream out2;
			out2 << qryStart;
			std::string qry = out2.str();
			const char* q = qry.c_str();

			rc = sqlite3_prepare_v2(db<type_t>::get_db_handle(), q, -1, &statement, NULL);

			if (rc == SQLITE_OK) {
				step = states::READ_PROJECTS;
			}
			else {
				step = states::CREATE_NEW_TABLE;
			}

			break;
		}
		case states::READ_PROJECTS: {
			int puid = 1;

			if (statement != nullptr) {
				while (sqlite3_step(statement) != SQLITE_DONE) {
					std::ostringstream out;
					out << sqlite3_column_text(statement, 0);
					std::cout << "\t" << puid << ": " << out.str() << std::endl;
					projectList.push_back(out.str());
					puid++;
				}
				rc = sqlite3_finalize(statement);

				step = states::DONE;
			}

			break;
		}
		case states::CREATE_NEW_TABLE: {
			// Getting here means this is first run and 'projects' table does not yet exist
			db<type_t>::create_projects_table();

			// TODO: Potential infinite loop here, consider exiting after fixed numbero of attempts
			step = states::TRY_SELECT;

			break;
		}
		default:
			break;
		}
	}

	return projectList;
}

std::vector<std::string> list_existing_workspaces(std::string_view projectName) {
	std::vector<std::string> workspaceList;

	std::ostringstream out;
	out << "select puid from projects where project_name='" << projectName << "';";
	std::string qryPUID = out.str();

	const char* q = qryPUID.c_str();
	sqlite3_stmt* statement;
	int rc = sqlite3_prepare_v2(db<type_t>::get_db_handle(), q, -1, &statement, NULL);
	rc = sqlite3_prepare_v2(db<type_t>::get_db_handle(), q, -1, &statement, NULL);
	int projectNumber = 0;
	while (sqlite3_step(statement) != SQLITE_DONE) {
		//std::ostringstream out;
		//out << sqlite3_column_text(statement, 0);
		//workspaceList.push_back(out.str());
		projectNumber = sqlite3_column_int(statement, 0);
	}
	rc = sqlite3_finalize(statement);

	std::string qryStart = "select workspace_name from workspaces where puid='";
	std::string qryEnd = "';";
	std::ostringstream out2;
	out2 << qryStart << projectNumber << qryEnd;
	std::string qry = out2.str();
	q = qry.c_str();

	int wuid = 0;
	rc = sqlite3_prepare_v2(db<type_t>::get_db_handle(), q, -1, &statement, NULL);
	while (sqlite3_step(statement) != SQLITE_DONE) {
		//std::ostringstream out;
		//out << sqlite3_column_text(statement, 0);
		//workspaceList.push_back(out.str());

		//puid = sqlite3_column_int(statement, 0);
		std::ostringstream out;
		out << sqlite3_column_text(statement, 0);
		std::cout << "\t" << wuid << ": " << out.str() << std::endl;
		workspaceList.push_back(out.str());
		wuid++;
	}
	rc = sqlite3_finalize(statement);

	return workspaceList;
}